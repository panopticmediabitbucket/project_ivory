﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedTexture : MonoBehaviour
{
    public Sprite targetTex;
    private Material _mat;
    public float test;
    // Start is called before the first frame update
    void Start()
    {
        var component = gameObject.GetComponent<MeshRenderer>();
        _mat = component.material;
    }

    // Update is called once per frame
    void Update()
    {
        _mat.mainTexture = targetTex.texture;
        
    }
}
