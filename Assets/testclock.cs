﻿using Assets.Code.System.Utilities;
using System;
using Assets.Code.System.TimeUtil;

public class testclock : MiddleBehaviour
{
    public GameStopwatch stopwatch => _shared.value;
    SharedValue<GameStopwatch> _shared;
    // Start is called before the first frame update
    void Start()
    {
        Tuple<SharedValue<GameStopwatch>, Guid> swTuple = SharedValue<GameStopwatch>.CreateSharedValue(new GameStopwatch("test stopwatch"));
        _shared = swTuple.Item1;
        stopwatch.Start();
    }
    protected override void Middle()
    {
        UnityEngine.Debug.Log(_shared.value.elapsedSeconds);
    }
}
