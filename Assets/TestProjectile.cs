﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestProjectile : MonoBehaviour
{
    Vector2 _position;
    public Vector2 velocity;
    // Start is called before the first frame update
    void Start()
    {
        _position = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += (Vector3)(velocity * Time.deltaTime);
        if(transform.position.x <= -100) {
            transform.position = _position;
        } 
    }
}
