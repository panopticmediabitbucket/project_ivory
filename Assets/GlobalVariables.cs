﻿using UnityEngine;
using Assets.Code.Physics.Accelerators;

public class GlobalVariables : MonoBehaviour
{
    public string gmceNote = "Scales all gameworld properties according to a single coefficient.";
    public float GlobalMagCoefficient;
    public float gravity = 9.8f;

    private static GlobalVariables instance;
    public static GlobalVariables getInstance()
    {
        return instance;
    }
    
    void Awake()
    {
        if(this != instance && instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
