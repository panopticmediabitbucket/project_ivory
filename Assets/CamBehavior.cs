﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CamBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = 60;
	}

    [Header("Focal position derived from player")]
    public GameObject focalObj;

    void LateUpdate()
    {
        TrackCenterPoint();
    }

    void TrackCenterPoint()
    {
        if((Vector2)focalObj.transform.position != (Vector2)transform.position)
        {
            transform.position = new Vector3(focalObj.transform.position.x, focalObj.transform.position.y, transform.position.z);
        }
    }

    Shader originalScreenSpaceShadowShader;
    public Shader myScreenSpaceShadowShader;
    void OnPreRender()
    {
        originalScreenSpaceShadowShader = GraphicsSettings.GetCustomShader(UnityEngine.Rendering.BuiltinShaderType.ScreenSpaceShadows);
        GraphicsSettings.SetCustomShader(UnityEngine.Rendering.BuiltinShaderType.ScreenSpaceShadows, myScreenSpaceShadowShader);
    }

    void OnPostRender()
    {
        //GraphicsSettings.SetCustomShader(UnityEngine.Rendering.BuiltinShaderType.ScreenSpaceShadows, originalScreenSpaceShadowShader);
    }
}
