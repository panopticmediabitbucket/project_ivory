﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/DiffuseParticle"
{
	Properties
	{
		_TintColor("Tint Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	}

	Category{
	Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
	Blend SrcAlpha OneMinusSrcAlpha
	ColorMask RGB
	Cull Off ZWrite Off

	SubShader
	{
		Pass
		{
			Tags { "Queue" = "AlphaTest"" CanUseSpriteAtlas" = "True" "LightMode" = "ForwardAdd" }
			LOD 500
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"
			#include "UnityShaderVariables.cginc"
			#include "AutoLight.cginc"
			#include "UnityDeferredLibrary.cginc"

			sampler2D _MainTex;
			fixed4 _TintColor;

			struct appdata_t {
				half3 position : POSITION;
				half3 normal : NORMAL;
				fixed4 color : COLOR;
				float4 texcoords : TEXCOORD0;
				float texcoordBlend : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				fixed3 lightColor : COLOR1;
				float2 texcoord : TEXCOORD0;
				fixed blend : TEXCOORD2;
				UNITY_VERTEX_OUTPUT_STEREO
			};

			float4 _MainTex_ST;

			v2f vert(appdata_t v) {
				v2f o;
				half3 norm = UnityObjectToWorldNormal(v.normal);
				half lightMult = max(0, dot(norm, _WorldSpaceLightPos0.xyz));
				float3 lightColor = _LightColor0 * lightMult;

				//for (int i = 0; i < 1; i++) {
				//	// -1 for x indicates non-spot light
				//	if (unity_LightAtten[i].x == -1) {
				//		half3 lightPos = unity_LightPosition[i];
				//		float3 distanceVect = v.position - lightPos;
				//		float distanceSquared = dot(distanceVect, distanceVect);
				//		float distance = sqrt(distanceSquared);
				//		// z is the quadratic attenuation
				//		float3 light = (1 / (1 + distanceSquared)) * unity_LightColor[i].xyz; 	
				//		lightColor = float3(light.x + lightColor.x, light.y + lightColor.y, light.z + lightColor.z);
				//	}
				//}

				lightColor += unity_AmbientSky;
				o.lightColor = lightColor;
				o.color = v.color * _TintColor;
				o.vertex = UnityObjectToClipPos(v.position);
				o.texcoord = TRANSFORM_TEX(v.texcoords.xy, _MainTex);
				o.blend = v.texcoordBlend;
				return o;
			}

			fixed4 frag(v2f v) : SV_Target
			{
				fixed4 coreColor = tex2D(_MainTex, v.texcoord) * v.color;
				fixed4 c = fixed4(coreColor.xyz * (v.lightColor), coreColor.w);
	
				return saturate(c);
			}
			ENDCG
			}
		}
	}
}