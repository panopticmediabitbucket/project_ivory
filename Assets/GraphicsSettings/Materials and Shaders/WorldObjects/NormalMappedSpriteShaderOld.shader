﻿Shader "Custom/NormalMappedSprite"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Sprite", 2D) = "white" {}
		_BumpMap("Normal", 2D) = "blue" {}
	}
	SubShader
	{
	
		Tags { "Queue"="Transparent" "RenderType"="Transparent" "LightingMode"="ForwardAdd" }
		LOD 1000
		Cull Off
		Blend One OneMinusSrcAlpha
		Pass
		{
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			// #pragma surface surf Standard fullforwardshadows

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.5

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"


			Texture2D _MainTex;
			Texture2D _BumpMap;
			SamplerState sampler_MainTex;
			fixed4 _Color;
			
			struct appdata
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.pos);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 c = _MainTex.Sample(sampler_MainTex, i.uv) * _Color;
				c.rgb *= c.a;
				return c;
			}
			ENDCG
		}
	}
}
