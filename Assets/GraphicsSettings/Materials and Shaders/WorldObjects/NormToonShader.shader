﻿Shader "Custom/NormToonShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_BumpMap("Normal", 2D) = "blue" {}
		_lut("Toon Texture", 2D) = "white" {}
    }
    SubShader
    {

        Pass
        {
			Tags { "LightMode"="ForwardBase" }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase

            #include "UnityCG.cginc"
#include "Lighting.cginc"

            sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _BumpMap;
			sampler2D _lut;
			
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                float4 col = tex2D(_MainTex, i.uv);
				float3 normal = UnpackNormal(tex2D(_BumpMap, i.uv));
				float ndotl = saturate(dot(_WorldSpaceLightPos0, normal));
				float4 lut = tex2D(_lut, float2(ndotl, 0));
				col *= (lut * _LightColor0) + unity_AmbientSky;

                return col;
            }
            ENDCG
        }
    }
}
