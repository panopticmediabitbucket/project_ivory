﻿Shader "Custom/NormalMappedSprite"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Sprite", 2D) = "white" {}
		_BumpMap("Normal", 2D) = "bump" {}
		_Cutoff("Shadow alpha cutoff", Range(0,1)) = 0.5
	}
		SubShader
	{

		Tags {
		"Queue" = "AlphaTest"
		"CanUseSpriteAtlas" = "True"
		}

		LOD 500
		Cull Off
		Lighting on
		CGPROGRAM

		#pragma target 3.0
		#pragma surface surf Lambert alpha //addshadow fullforwardshadows 
		#include "UnityCG.cginc"

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
		};

		sampler2D _MainTex;
		sampler2D _BumpMap;
		fixed4 _Color;
		fixed _Cutoff;

		void surf(Input i, inout SurfaceOutput o)
		{
			float4 diffColor = tex2D(_MainTex, i.uv_MainTex);
			o.Albedo = diffColor.rgb;
			o.Alpha = diffColor.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, i.uv_BumpMap));
		}

		ENDCG
	}
		Fallback "Legacy Shaders/Transparent/Cutout/VertexLit"
}
