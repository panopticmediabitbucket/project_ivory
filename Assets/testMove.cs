﻿using System.Collections.Generic;
using UnityEngine;
using Assets.Code.System.Utilities;
using Assets.Code.Physics.Accelerators;
using Assets.Code.System.TimeUtil;

public enum TestStates
{
    Idle, RightMoving, LeftMoving
}
public class TestMove : PhysicsStateMachineWorldObject<TestStates>
{
    // Start is called before the first frame update
    public Vector3 movementVector;
    GameStopwatch _rightStopwatch = new GameStopwatch(TestStates.RightMoving.ToString());
    GameStopwatch _leftStopwatch = new GameStopwatch(TestStates.LeftMoving.ToString());

    private void Start()
    {
        _rightStopwatch.Start();
        //_internalAccels.Add(new AnimatedAccel<TestStates>(0, TestStates.RightMoving, Vector2.right,
        //    new List<AccelZone>() { new AccelZone(0, 1f, false, 0, 4), new AccelZone(1, -1f, true, 4, 8) }, "right")
        //    .DeliverRuntimeStopWatch<AnimatedAccel<TestStates>>(_rightStopwatch));
        //_internalAccels.Add(new AnimatedAccel<TestStates>(0, TestStates.LeftMoving, Vector2.left, 
        //    new List<AccelZone>() { new AccelZone(0, 1f, false, 0, 4), new AccelZone(1, -1f, true, 4, 8) }, "left")
        //    .DeliverRuntimeStopWatch<AnimatedAccel<TestStates>>(_leftStopwatch));
        state = TestStates.RightMoving;
    }

    protected override void HitGround() {
        throw new System.NotImplementedException();
    }

    protected override void OnNoGround() {
        throw new System.NotImplementedException();
    }

    protected override void HitWall() {
        throw new System.NotImplementedException();
    }

    protected override void OffWall()
    {
        throw new System.NotImplementedException();
    }

    protected override void HitBackWall() {
        throw new System.NotImplementedException();
    }

    // Update is called once per frame
    //protected override void First()
    //{
    //    if (_rightStopwatch.active)
    //    {
    //        if (_rightStopwatch.elapsedSeconds >= 8)
    //        {
    //            _leftStopwatch.ResponsiveStart(_rightStopwatch.elapsedSeconds - 8);
    //            state = TestStates.LeftMoving;
    //            UnityEngine.Debug.Log("Furtheset Right: " + transform.position);
    //        }
    //    }
    //    else
    //    {
    //        if (_leftStopwatch.elapsedSeconds >= 8)
    //        {
    //            _rightStopwatch.ResponsiveStart(_leftStopwatch.elapsedSeconds - 8);
    //            state = TestStates.RightMoving;
    //            UnityEngine.Debug.Log("Furtheset Left: " + transform.position);
    //        }
    //    }
    //    foreach(var val in System.Enum.GetValues(typeof(TestStates)))
    //    {
    //        UnityEngine.Debug.Log(val);
    //    }
    //    base.First();
    //}

    ////protected override void Second() {
    //    // No implementation
    //}
}
