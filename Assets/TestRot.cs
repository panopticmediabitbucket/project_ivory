﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRot : MonoBehaviour {

    Quaternion testQuat = Quaternion.AngleAxis(1f, Vector3.forward);

	
	// Update is called once per frame
	void Update () {
        transform.rotation = testQuat * transform.rotation;
        //Debug.Log(transform.eulerAngles.ToString());

	}
}
