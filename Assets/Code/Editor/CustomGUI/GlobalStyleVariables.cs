﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;

namespace Assets.Code.Editor.CustomGUI {
    public static class GlobalStyleVariables {
        public static IDependencyBuilder DB => DependencyManager.Instance;

        public static readonly IColor DarkGrayEditor = DB.NewColor(.2f, .2f, .2f);
        public static readonly IColor MidGrayEditor = DB.NewColor(.5f, .5f, .5f);
        public static readonly IColor LightGrayEditor = DB.NewColor(.8f, .8f, .8f);
        public static readonly IColor TurquoiseHighlight = DB.NewColor(.2f, .6f, .6f);
        public static readonly IColor TurquoiseLowlight = DB.NewColor(.1f, .3f, .3f);
        public static readonly IColor OffGray = DB.NewColor(.6f, .6f, .65f);
    }
}
