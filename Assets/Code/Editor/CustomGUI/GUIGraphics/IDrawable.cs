﻿using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.CustomGUI.GUIGraphics {
    public interface IDrawable {
        /*
         * IDrawables called with the Draw method are responsible for designating their own drawspace
         * or inheriting drawspace from a parent element through a SharedValue of some sort.
         * Those that do not designate their own draw space will call Draw(ZeroRect);
         */
        void Draw();
        /*
         * A drawable called with a rectangle will confine it's drawing to that space, whenever possible.
         */
        void Draw(IRect rect);
    }
}
