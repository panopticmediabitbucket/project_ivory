﻿using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using JetBrains.Annotations;

namespace Assets.Code.Editor.CustomGUI.GUIGraphics {
    public class GUIFunctions {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;

        public static IVector2 GetGUIContentTextSize(IGUIContent content, IGUIStyle style) {
            IFont font = style.font;
            int totWidth = 0;
            int height = 0;
            foreach (var ch in content.Text()) {
                font.GetCharacterInfo(ch, out ICharacterInfo characterInfo);
                totWidth += characterInfo.advance;
                height = characterInfo.maxY;
            }

            return DB.NewVector2(totWidth, height);
        }

        public static void DrawBorder(IRect rect, IColor color, int thickness) {
            if (thickness > 0) {
                IRect leftEdge = DB.NewRect(rect.x, rect.y, thickness, rect.height);
                EditorGUI.DrawRect(leftEdge, color);
                IRect rightEdge = DB.NewRect(leftEdge);
                rightEdge.x += rect.width - thickness;
                EditorGUI.DrawRect(rightEdge, color);

                IRect topEdge = DB.NewRect(rect.x, rect.y, rect.width, thickness);
                EditorGUI.DrawRect(topEdge, color);
                IRect bottomEdge = DB.NewRect(topEdge);
                bottomEdge.y += rect.height - thickness;
                EditorGUI.DrawRect(bottomEdge, color);
            }
        }

        public static void DrawOutlinedShape([NotNull] IColor outlineColor, [NotNull] IColor rectColor, int thickness, [NotNull] params IRect[] rects) {
            foreach (var rect in rects) {
                DrawBorder(rect, outlineColor, thickness);
            }
            foreach (var rect in rects) {
                EditorGUI.DrawRect(rect, rectColor);
            }
        }

        public static void DrawTabRect([NotNull] IRect rect, [NotNull] IGUIContent label, [NotNull] IGUIStyle style, int thickness, [NotNull] IColor rectColor, IColor outlineColor) {
            IVector2 guiContentSize = DB.NewVector2(style.fontSize / 1.5f * label.Text().Length, style.fontSize);
            guiContentSize.x += 20;
            guiContentSize.y += 10;
            IRect tabRect = DB.NewRect(rect.x, rect.y, guiContentSize.x, guiContentSize.y);
            IRect bodyRect = DB.NewRect(rect.x, rect.y + tabRect.height, rect.width, rect.height - tabRect.height);
            DrawOutlinedShape(outlineColor, rectColor, thickness, tabRect, bodyRect);

            IRect labelRect = DB.NewRect(tabRect.x + 10, tabRect.y + 5, tabRect.width, tabRect.height - 18);
            GUI.Label(labelRect, label, style);
        }

        public static void DrawBorderedRect(IRect rect, IColor borderColor, IColor rectColor, int borderThickness) {
            EditorGUI.DrawRect(rect, rectColor);
            DrawBorder(rect, borderColor, borderThickness);
        }
        public static IColor DarkenColor(IColor original) {
            return DB.NewColor(.7f * original.r, .7f * original.g, .7f * original.b);
        }

        public static readonly IGUIStyle textureStyle = DB.NewGUIStyle(x => x.border = DB.NewRect(0, 0, 0, 0));

        public static IColor LightenColor(IColor original) {
            return DB.NewColor(1.2f * original.r, 1.2f * original.g, 1.2f * original.b);
        }

        public static IRect ShrinkRect(IRect rect, int v) {
            rect = DB.NewRect(rect);
            rect.x += v;
            rect.y += v;
            rect.width -= v * 2;
            rect.height -= v * 2;
            return rect;
        }
    }
}
