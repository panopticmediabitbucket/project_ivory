﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.CustomGUI.Handlers {

    public delegate void SelectionChangeDelegate<in T>(T item);
    public interface ISelectionChangeHandler<T> {
        void RegisterSelectionChangedHandler(SelectionChangeDelegate<T> callback);
        void RemoveSelectionChangedHandler(SelectionChangeDelegate<T> callback);
        void ClearSelectionChangedHandler();
    }
}
