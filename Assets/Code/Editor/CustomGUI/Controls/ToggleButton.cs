﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public class ToggleButton : Button {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IGUI GUI => GUIAdapter.Instance;

        ITexture _pressed;
        ITexture _notPressed;
        public bool active { get; protected set; }

        public ToggleButton(ITexture notPressed, ITexture pressed, IVector2 dimensions) {
            _notPressed = notPressed;
            _pressed = pressed;
            _dimensions = dimensions.normalized;
            float scale = 1 / dimensions.y;
            _dimensions = dimensions.Mul(scale);
        }

        public void Deactivate() {
            active = false;
        }

        /*
         * The button is scaled by the height dimension
         */
        public override ReturnedValue Control(IRect rect) {
            IRect controlRect = DB.NewRect(rect.position, _dimensions.Mul(rect.height));

            if (base.Control(controlRect)) {
                active = !active;
            }

            return active ? new ReturnedValue(active) : ReturnedValue.noReturnValue;
        }

        public override void Draw(IRect rect) {
            IRect drawSpace = DB.NewRect(rect.position, _dimensions.Mul(rect.height));
            ITexture drawTexture = active ? _pressed : _notPressed;
            GUI.DrawTexture(drawSpace, drawTexture);
        }
    }
}
