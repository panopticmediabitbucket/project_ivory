﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public abstract class AbstractGUIElement<T> : IControllable<T>, IDrawable {
        public static IDependencyBuilder DB => DependencyManager.Instance;

        public int height { get; protected set; }

        public AbstractGUIElement(int height = 20) {
            this.height = height;
        }

        public abstract ReturnedValue<T> Control(IRect rect);
        public abstract ReturnedValue<T> Control();

        public abstract void Draw();
        public abstract void Draw(IRect rect);
        public virtual ReturnedValue<T> OnGUI() {
            Draw();
            return (ReturnedValue<T>)Control();
        }

        public virtual ReturnedValue<T> OnGUI(IRect rect) {
            IRect controlRect = DB.NewRect(rect);
            Draw(rect);
            return (ReturnedValue<T>)Control(rect);
        }

        ReturnedValue IControllable.Control(IRect rect) {
            throw new global::System.NotImplementedException();
        }

        ReturnedValue IControllable.Control() {
            throw new global::System.NotImplementedException();
        }
    }
    public abstract class AbstractGUIElement: IControllable, IDrawable {

        public virtual ReturnedValue OnGUI() {
            Draw();
            return Control();
        }

        public virtual ReturnedValue OnGUI(IRect rect) {
            Draw(rect);
            return Control(rect);
        }
        public abstract ReturnedValue Control(IRect rect);

        public abstract ReturnedValue Control();

        public abstract void Draw();

        public abstract void Draw(IRect rect);
    }
}
