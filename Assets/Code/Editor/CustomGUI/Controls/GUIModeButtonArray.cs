﻿using System.Linq;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.CustomGUI.Handlers;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using UnityEditor;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public class GUIModeButtonArray<T> : AbstractGUIWrapper<T>, ISelectionChangeHandler<T> {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        private GUIModeButton<T>[] _buttons;
        private SelectionChangeDelegate<T> OnSelectionChange = (T item) => { };

        public GUIModeButtonArray(GUIButtonContent<T>[] buttonContents, IVector2 buttonDimensions) {

            _buttons = buttonContents.Select(x => new GUIModeButton<T>(x)).ToArray();
        }

        private void DeactivateOthers(GUIModeButton<T> except) {
            foreach (var button in _buttons) {
                if (button != except) {
                    button.Deactivate();
                }
            }
        }

        public override ReturnedValue<T> OnGUI(IRect rect) {
            ReturnedValue<T> retVal = ReturnedValue<T>.noReturnValue;
            float padding = (rect.width - (_buttons.Length * rect.height)) / (_buttons.Length + 1);
            IRect eachRect = DB.NewRect(rect.x, rect.y, rect.width / (_buttons.Length + 1), rect.height);
            eachRect.x += padding;
            foreach (var button in _buttons) {
                var checkVal = button.OnGUI(eachRect);
                if (checkVal) {
                    OnSelectionChange(checkVal.value);
                    var except = button;
                    retVal = checkVal;
                    DeactivateOthers(except);
                }
                eachRect.x += eachRect.height + padding;
            }
            return retVal;
        }

        public void SetSelection(T item) {
            foreach (var button in _buttons) {
                if (button.buttonContent.item.Equals(item)) {
                    button.SetActive();
                    var except = button;
                    DeactivateOthers(button);
                    return;
                }
            }
        }

        public override ReturnedValue<T> OnGUI() {
            return ReturnedValue<T>.noReturnValue;
        }

        public void RegisterSelectionChangedHandler(SelectionChangeDelegate<T> callback) {
            OnSelectionChange += callback;
        }

        public void RemoveSelectionChangedHandler(SelectionChangeDelegate<T> callback) {
            OnSelectionChange -= callback;
        }

        public void ClearSelectionChangedHandler() {
            OnSelectionChange = item => { };
        }
    }

    public class GUIModeButton<T> : ToggleButton {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEvent Event => EventAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        private static IEditorGUIUtility EditorGUIUtility => EditorGUIUtilityAdapter.Instance;
        private static IAssetDatabase AssetDatabase => AssetDatabaseAdapter.Instance;

        public GUIButtonContent<T> buttonContent { get; private set; }

        private static ITexture _hover = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("489fc4bb14ed6f94f8ceee3f05ddb4d2"));
        private static ITexture _selected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("9c665688e4a7eb141b3345c253154d96"));
        private static ITexture _unselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("6849dab5fd2db4c479262670dce67f41"));

        public void SetActive() {
            active = true;
        }

        public GUIModeButton(GUIButtonContent<T> buttonContent) : base(_selected, _unselected, DB.NewVector2(1, 1)) {
            ITexture unselected = AssetDatabase.LoadAssetAtPath<ITexture>("6849dab5fd2db4c479262670dce67f41");
            this.buttonContent = buttonContent;
        }

        public new ReturnedValue<T> OnGUI(IRect rect) {
            return base.OnGUI(rect) ? new ReturnedValue<T>(buttonContent.item) : ReturnedValue<T>.noReturnValue;
        }

        public override void Draw(IRect rect) {
            bool hover = DB.NewRect(rect.position, _dimensions.Mul(rect.height)).Contains(Event.current.mousePosition);
            IRect drawSpace = DB.NewRect(rect.position, _dimensions.Mul(rect.height));
            if (!active && hover) {
                GUI.DrawTexture(drawSpace, _hover);
            } else {
                base.Draw(rect);
            }

            if (active) {
                GUI.DrawTexture(drawSpace, buttonContent.selected);
            } else {
                GUI.DrawTexture(drawSpace, buttonContent.unselected);
            }
        }

        public override ReturnedValue Control(IRect rect) {
            IRect contRect = GUIFunctions.ShrinkRect(rect, 5);
            EditorGUIUtility.AddCursorRect(contRect, MouseCursor.Link);
            return !active ? base.Control(contRect) : ReturnedValue.noReturnValue;
        }

        public new ReturnedValue<T> OnGUI() {
            return ReturnedValue<T>.noReturnValue;
        }
    }

    public struct GUIButtonContent<T> {
        public T item;
        public ITexture unselected;
        public ITexture selected;
        public GUIButtonContent(T item, ITexture unselected, ITexture selected) {
            this.item = item;
            this.unselected = unselected;
            this.selected = selected;
        }
    }
}
