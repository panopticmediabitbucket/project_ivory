﻿using UnityEditor;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public abstract class Drag : IControllable {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEditorGUIUtility EditorGUIUtility => EditorGUIUtilityAdapter.Instance;
        private static IEvent Event => EventAdapter.Instance;

        protected IRect _space;
        protected MouseCursor _cursor;
        protected int _pixelPadding;
        protected readonly bool _responsive; //is this an ineractable element?
        //space is the interactable space, cursor is the designated mouse icon change, and 
        //pixelPadding is the amount of extra space surrounding the icon where it can be interacted with
        public Drag(IRect space, MouseCursor cursor, int pixelPadding) {
            _space = space;
            _cursor = cursor;
            _pixelPadding = pixelPadding;
            _responsive = true;
        }

        public Drag(IRect space, int pixelPadding, MouseCursor cursor, bool responsive) {
            _responsive = responsive;
            _cursor = cursor;
            _pixelPadding = pixelPadding;
            _space = space;
        }

        protected Drag() { }

        protected IVector2 _prevPos;
        protected bool _grabbed;

        //Override/decorate to change draw behavior
        public virtual ReturnedValue Control() {
            IRect controlRect = DB.NewRect(_space);
            return Control(controlRect);
        }

        public virtual ReturnedValue Control(IRect controlRect) {
            if (_responsive) {
                controlRect.x -= _pixelPadding; controlRect.width += 2 * _pixelPadding;
                controlRect.y -= _pixelPadding; controlRect.height += 2 * _pixelPadding;
                EditorGUIUtility.AddCursorRect(controlRect, _cursor);
                if (_grabbed == true) {
                    if (Event.current.rawType != EventType.MouseUp) {
                        IVector2 delta = Event.current.mousePosition.Sub(_prevPos);
                        if (delta.x != 0) {
                            OnHorizontalChange(delta.x);
                        }
                        if (delta.y != 0) {
                            OnVerticalChange(delta.y);
                        }
                        _prevPos = Event.current.mousePosition;
                    } else {
                        OnReleaseHandle();
                    }
                } else if (controlRect.Contains(Event.current.mousePosition) && Event.current.rawType == AdapterLayer.Impl.Static_Classes.EventType.MouseDown) {
                    _grabbed = true;
                    _prevPos = Event.current.mousePosition;
                }

                return _grabbed ? new ReturnedValue(_grabbed) : ReturnedValue.noReturnValue;
            }

            return ReturnedValue.noReturnValue;
        }

        //To be overridden by inheriting classes
        protected abstract void OnHorizontalChange(float x);

        //To be overridden by inheriting classes
        protected abstract void OnVerticalChange(float y);

        //Decorate to reinitialize relevent data for inheriting classes; could be the case that there's nothing to change.
        protected virtual void OnReleaseHandle() {
            _grabbed = false;
        }
    }
}
