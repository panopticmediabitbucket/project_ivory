﻿using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Controls {
    // This abstract template is intended to wrap a host of child IControllables, AbstractGUIElements, and IDrawables
    // These elements can be consolidated into pipelines that the wrapper is responsible for drawing in the appropriate order
    // All draw and control functions within the defined rect are self-contained
    public abstract class AbstractGUIWrapper<T> {
        protected AbstractGUIWrapper() { }
        protected AbstractGUIWrapper(int h, int w) {
            height = h;
            width = w;
        }
        public virtual int height { get; protected set; }
        public virtual int width { get; protected set; }
        public abstract ReturnedValue<T> OnGUI(IRect rect);
        public abstract ReturnedValue<T> OnGUI();
    }
}
