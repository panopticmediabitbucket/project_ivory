﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public class PopoutDrawer<T> : AbstractGUIElement<T> where T : struct {
        private static IEvent Event => EventAdapter.Instance;
        private static IPopupWindow PopupWindow => PopupWindowAdapter.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;

        // content and style information
        private readonly IPopoutContent<T> _content;
        private IPopoutDrawerStyle style => _content.style;

        private bool _open = false;

        public T contentSummary => _content.contentSummary.value;

        public PopoutDrawer(IPopoutContent<T> content, int height = 30) : base(height) {
            _content = content;
            _content.RegisterOnCloseCallback(() => this._open = false);
        }

        public override ReturnedValue<T> Control() {
            return Control(DB.ZeroRect());
        }

        public override ReturnedValue<T> Control(IRect rect) {
            return Control(rect, true);
        }

        private ReturnedValue<T> Control(IRect rect, bool openable) {
            bool clicked = Event.current.rawType.Equals(EventType.mouseDown) && rect.Contains(Event.current.mousePosition);
            if (clicked && openable) {
                IRect popupRect = DB.NewRect(rect.x, rect.y, rect.width, rect.height);
                _content.Activate((int)popupRect.width, (int)popupRect.height);
                PopupWindow.Show(popupRect, _content);
                _open = true;
            }
            return new ReturnedValue<T>(contentSummary);
        }

        public override void Draw() {
            Draw(DB.ZeroRect());
        }

        public override void Draw(IRect rect) {
            Draw(rect, true);
        }

        private void Draw(IRect rect, bool openable) {
            IRect contentRect = DB.NewRect(rect);
            if (style.isBordered) {
                GUIFunctions.DrawBorderedRect(rect, GUIFunctions.DarkenColor(style.drawerColor), style.drawerColor, 2);
            } else {
                EditorGUI.DrawRect(rect, style.drawerColor);
            }

            if (openable) {
                _content.contentSummary.Draw(rect);
            } else {
                _content.contentSummary.DrawNotOpenable(rect);
            }
        }

        public ReturnedValue<T> OnGUINotOpenable(IRect rect) {
            Draw(rect, false);
            return Control(rect, false);
        }
    }

    public interface IPopoutDrawerStyle {
        IColor drawerColor { get; }
        IColor openDrawerColor { get; }
        bool isBordered { get; }
        IGUIContent guiContent { get; }
    }

    public interface IPopoutDrawerContentSummary<T> : IDrawable where T : struct {
        T value { get; }
        void DrawNotOpenable(IRect rect);
    }
}
