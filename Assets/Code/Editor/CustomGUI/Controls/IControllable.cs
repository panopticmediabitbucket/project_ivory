﻿using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public interface IControllable<T>: IControllable {
        new ReturnedValue<T> Control(IRect rect);
        new ReturnedValue<T> Control();
    }

    public interface IControllable {
        /*
         * Control called with a rect will attempt to build the control in that rectangle
         */
        ReturnedValue Control(IRect rect);
        /*
         * Control called without a rect assumes that the controllable has some way of managing where it
         * should be drawn. In some cases, this may mean defaulting to the ZeroRect
         */
        ReturnedValue Control();
    }
}
