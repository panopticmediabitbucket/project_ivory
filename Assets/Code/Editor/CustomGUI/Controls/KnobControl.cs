﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using System;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public sealed class KnobControl : AbstractGUIElement<float> {
        private static IDependencyBuilder DB => DependencyManager.Instance;
       // private static IAssetDatabase AssetDatabase => AssetDatabaseAdapter.Instance;
        private static IEvent Event => EventAdapter.Instance;
        private static IHandles Handles => HandlesAdapter.Instance;

        private static ITexture _knobControl = MetaUtilities.AssetFromGuid<ITexture>("9a23d97c5aa8dea41b8560f770646c89");

        private static readonly IGUIStyle degreeStyle = DB.NewGUIStyle(x => x.fontSize = 12, x => x.alignment = AdapterLayer.Objects.TextAnchor.MiddleCenter);
        private static readonly IGUIStyle knobGUIStyle = DB.NewGUIStyle(x => x.border = DB.NewRect(0, 0, 0, 0));

        private int _pixelPadding;
        private bool _grabbed;

        private static IGUI GUI => GUIAdapter.Instance;

        public IVector2 vector { get; private set; }
        private double _degree;

        public KnobControl(IVector2 vector) {
            this.vector = vector;
            _degree = CalculateDegree(vector);
            degreeStyle.normal.textColor = DB.NewColor(1f, 1f, 1f);
        }

        /*
         * height value of the drawspace designates sidelength of the knob control
         */
        public override ReturnedValue<float> Control(IRect drawSpace) {
            IRect controlRect = DB.NewRect(drawSpace.x + _pixelPadding, drawSpace.y + _pixelPadding, drawSpace.height - (2 *_pixelPadding), drawSpace.height - (2 *_pixelPadding));
            IVector2 midPoint = controlRect.position.Add(controlRect.size.DividedBy(2));
            IEvent @event = Event.current;
            IVector2 mousePosition = @event.mousePosition;
            var type = @event.rawType;

            if (_grabbed) {
                bool released = type.Equals(EventType.MouseUp);
                if (!released) {
                    vector = OnGrabbedKnob(mousePosition, midPoint);
                } else {
                    OnReleaseKnob();
                }
            } else {
                bool containsMouse = controlRect.Contains(mousePosition);
                bool mouseClicked = type.Equals(EventType.MouseDown);
                if (containsMouse && mouseClicked) {
                    vector = OnGrabbedKnob(mousePosition, midPoint);
                }
            }

            return _grabbed ? new ReturnedValue<float>((float)_degree) : ReturnedValue<float>.noReturnValue;
        }

        public override void Draw(IRect drawSpace) {
            drawSpace = DB.NewRect(drawSpace.position, DB.NewVector2(drawSpace.height, drawSpace.height));

            Handles.color = DB.NewColor(.9f, .9f, .9f);
            Handles.DrawSolidDisc(DB.NewVector3(drawSpace.x + drawSpace.width / 2, drawSpace.y + drawSpace.height / 2, 0), DB.NewVector3(0, 0, 1), drawSpace.width / 2);
            Handles.color = DB.NewColor(.3f, .4f, .1f);
            Handles.DrawWireDisc(DB.NewVector3(drawSpace.x + drawSpace.width / 2, drawSpace.y + drawSpace.height / 2, 0), DB.NewVector3(0, 0, 1), drawSpace.width / 2);

            IVector2 midPoint = drawSpace.position.Add(drawSpace.size.DividedBy(2));

            Handles.color = GetHandleColor(vector);
            IVector2 drawVector = DB.NewVector2(vector);
            drawVector.y *= -1;
            Handles.DrawAAPolyLine(midPoint, drawVector.Mul(drawSpace.height / 2).Add(midPoint));

            IRect degreeRect = DB.NewRect(drawSpace.x + drawSpace.width + 20, drawSpace.y + drawSpace.height / 2 - 10f, 50, 20);
            GUI.Label(degreeRect, ((int)_degree).ToString() + "°", degreeStyle);
        }

        IColor GetHandleColor(IVector2 direction) {
            IVector2 dir = DB.NewVector2(direction);
            dir.x += 1;
            dir.x /= 3;
            dir.y += 1;
            dir.y /= 3;
            return DB.NewColor(dir.x, dir.y, 0);
        }

        public override void Draw() {
            Draw(DB.NewRect(0, 0, 10, 10));
        }

        public override ReturnedValue<float> Control() {
            return Control(DB.NewRect(0, 0, 10, 10));
        }

        /*
         * returns vector between the midpoint and the mousePosition normalized
         * and adjusted 
         */
        private IVector2 OnGrabbedKnob(IVector2 mousePosition, IVector2 midPoint) {
            _grabbed = true;

            IVector2 mouseDirection = mousePosition.Sub(midPoint);
            mouseDirection.y = -mouseDirection.y;
            _degree = CalculateDegree(mouseDirection);
            _degree = SnapRoutine(_degree);
            double radians = ToRadians(_degree);

            // return a normalized vector
            return DB.NewVector2((float)Math.Cos(radians), (float)Math.Sin(radians));
        }

        private void OnReleaseKnob() {
            _grabbed = false;
        }

        /*
         * Calculates the degree between the i-hat vector and the mouseDirection
         */
        private double CalculateDegree(IVector2 mouseDirection) {
            double degree = 0;

            // Case that vector is on an axis
            if(mouseDirection.x * mouseDirection.y == 0) {
                degree = mouseDirection.x == 0
                    ? mouseDirection.y > 0 ? 90 : 270
                    : mouseDirection.x > 0 ? 0f : 180;
            } 
            // Case that vector is in quadrant 2 or 4
            else if (mouseDirection.x * mouseDirection.y < 0) {
                // quadrant 2
                if(mouseDirection.x < 0) {
                    degree += 90;
                } 
                // quadrant 4
                else {
                    degree += 270;
                }
                degree += ToDegrees(Math.Atan(Math.Abs(mouseDirection.x / mouseDirection.y)));
            }
            // Case that vector is in quadrant 1 or 3
            else {
                if(mouseDirection.x < 0) {
                    degree += 180;
                } else {
                    degree += 0;
                }
                degree += ToDegrees(Math.Atan(mouseDirection.y / mouseDirection.x));
            }

            return degree;
        }

        /*
         * Snaps degrees to the nearest 15-degree increment when LeftControl is held
         */
        private double SnapRoutine(double degree) {
            if (InputUtilities.IsKeyHeld(KeyCode.LeftControl)) {
                degree /= 15;
                degree = Math.Round(degree);
                degree = degree < 24 ? degree : 0;
                degree *= 15;
            }

            return degree;
        }

        private double ToRadians(double degrees) {
            return (degrees * 2 * (Math.PI) / 360);
        }

        private double ToDegrees(double radians) {
            return radians * 360 / Math.PI / 2;
        }
    }
}
