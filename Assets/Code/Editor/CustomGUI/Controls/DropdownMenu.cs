﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using EventType = Assets.Code.Editor.AdapterLayer.Impl.Static_Classes.EventType;
using System;
using System.Data;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public sealed class DropdownMenu : AbstractGUIElement {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEvent Event => EventAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;

        private static ITexture _dropdownClosed = MetaUtilities.AssetFromGuid<ITexture>("6d2f4679b749c214995f67657012006b");
        private static ITexture _dropdownOpened = MetaUtilities.AssetFromGuid<ITexture>("2af28e1764be4144f99118792c196da3");

        private static readonly IGUIStyle _titleStyle = DB.NewGUIStyle(x => x.fontSize = 12, x => x.fontStyle = FontStyle.Bold, x => x.alignment = AdapterLayer.Objects.TextAnchor.MiddleLeft);
        private static readonly IGUIStyle _dropdownStyle = DB.NewGUIStyle(x => x.border = DB.NewRect(0, 0, 0, 0));
        public bool active { get; private set; }
        private int _height;
        private readonly string _title;

        //constructor
        public DropdownMenu(int height, string title) {
            _height = height;
            _title = title;
            _dropdownStyle.normal.textColor = DB.NewColor(1, 1, 1);
        }

        public override ReturnedValue Control(IRect rect) {
            IRect ctrlRect = DB.NewRect(rect.x, rect.y, rect.width, rect.height);
            IEvent @event = Event.current;
            IVector2 mousePosition = @event.mousePosition;
            var type = @event.rawType;

            bool containsMouse = ctrlRect.Contains(mousePosition);
            bool mouseClicked = type.Equals(EventType.MouseDown);
            if (mouseClicked && containsMouse) {
                active = !active;
                return new ReturnedValue(active);
            } else {
                return ReturnedValue.noReturnValue;
            }
        }

        public override ReturnedValue Control() {
            return Control(DB.NewRect(0, 0, 10, 10));
        }

        public override void Draw() {
            Draw(DB.NewRect(0, 0, 10, 10));
        }

        public override void Draw(IRect drawSpace) {
            drawSpace = DB.NewRect(drawSpace);
            EditorGUI.DrawRect(drawSpace, DB.NewColor(.9f, .9f, .9f));
            HorizontalLine(drawSpace);

            IRect buttonRect = DB.NewRect(drawSpace.x + 5, drawSpace.y + 2, drawSpace.height - 4, drawSpace.height - 4);
            //Click on the closed dropdown icon, it opens, and vice versa.
            if (active) {
                GUI.Box(buttonRect, _dropdownOpened, _dropdownStyle); //Open
            } else {
                GUI.Box(buttonRect, _dropdownClosed, _dropdownStyle); //Close
            }

            IRect titleRect = DB.NewRect(buttonRect.x + (buttonRect.width) + 10, drawSpace.y + drawSpace.height / 2 - 12f, drawSpace.width, 20);
            GUI.Label(titleRect, _title, _titleStyle);

            drawSpace.y += drawSpace.height;
            HorizontalLine(drawSpace);
        }
        public static void HorizontalLine(IRect rect) {
            IRect horizontalLine = DB.NewRect(rect.x, rect.y - 1, rect.width, 2f);
            IColor color = DB.NewColor(.5f, .5f, .5f);
            EditorGUI.DrawRect(horizontalLine, color);
        }
    }
}
