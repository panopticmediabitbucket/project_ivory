﻿using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using UnityEditor;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public class Button : AbstractGUIElement {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        private static IEvent Event => EventAdapter.Instance;
        private static IEditorGUIUtility EditorGUIUtility => EditorGUIUtilityAdapter.Instance;

        ITexture _image;
        protected IVector2 _dimensions;

        private Action OnPressed = () => { };

        protected Button() { }
        public Button(ITexture image, IVector2 dimensions) {
            _image = image;
            _dimensions = dimensions.normalized;
            float scale = 1 / dimensions.y;
            _dimensions = dimensions.Mul(scale);
        }

        public void RegisterOnPressedCallback(Action action) {
            OnPressed += action;
        }

        public void RemoveOnPressedCallback(Action action) {
            OnPressed -= action;
        }

        public override ReturnedValue Control(IRect controlRect) {
            controlRect = DB.NewRect(controlRect.position, _dimensions.Mul(controlRect.height));

            EditorGUIUtility.AddCursorRect(controlRect, MouseCursor.Link);
            bool pressed = controlRect.Contains(Event.current.mousePosition) && Event.current.rawType.Equals(EventType.mouseDown);
            if (pressed) {
                OnPressed();
            }

            return pressed ? new ReturnedValue(true) : ReturnedValue.noReturnValue;
        }

        public override void Draw(IRect rect) {
            IRect drawSpace = DB.NewRect(rect.position, _dimensions.Mul(rect.height));
            GUI.DrawTexture(drawSpace, _image);
        }


        public override ReturnedValue Control() {
            return Control(DB.NewRect(0, 0, 0, 10f));
        }

        public override void Draw() {
            Draw(DB.NewRect(0, 0, 0, 10f));
        }
    }
}
