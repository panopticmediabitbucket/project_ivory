﻿using System;
using System.Collections.Generic;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Controls {
    public class ControlPipeline<T>: IControllable<Tuple<T, ReturnedValue>> where T : Enum {
        private readonly IEnumerable<ControlStep<T>> _controlSteps;

        public ControlPipeline(IEnumerable<ControlStep<T>> controlSteps) {
            _controlSteps = controlSteps;
        }

        public virtual ReturnedValue<Tuple<T, ReturnedValue>> Control() {
            foreach (var control in _controlSteps) {
                var interacted = control.Control();
                if (interacted) {
                    return new ReturnedValue<Tuple<T, ReturnedValue>>(new Tuple<T, ReturnedValue>(control.type, interacted));
                }
            }
            return ReturnedValue<Tuple<T, ReturnedValue>>.noReturnValue;
        }

        public virtual ReturnedValue<Tuple<T, ReturnedValue>> Control(IRect rect) {
            foreach (var control in _controlSteps) {
                var interacted = control.Control(rect);
                if (interacted) {
                    return new ReturnedValue<Tuple<T, ReturnedValue>>(new Tuple<T, ReturnedValue>(control.type, interacted));
                }
            }
            return ReturnedValue<Tuple<T, ReturnedValue>>.noReturnValue;
        }

        ReturnedValue IControllable.Control(IRect rect) { return Control(rect); }

        ReturnedValue IControllable.Control() { return Control(); }
    }

    public struct ControlStep<T> where T : Enum {
        private IControllable _controllable;
        public T type;

        public ReturnedValue Control() => _controllable.Control();
        public ReturnedValue Control(IRect rect) => _controllable.Control();

        public ControlStep(IControllable controllable, T type) {
            _controllable = controllable;
            this.type = type;
        }
    }
}
