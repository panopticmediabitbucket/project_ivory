﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEditor.IMGUI.Controls;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.CustomGUI.Controls;

namespace Assets.Code.Editor.CustomGUI.Columns {
    public class Column {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        public Column(int index) {
            blockHeight = 20;
            blockPadding = 5;
            padding = 3;
            this.index = index;
        }

        public readonly int index;

        float _width = 100f;
        public float width {
            get { return _width; }
            set {
                if (value < minWidth)
                    _width = minWidth;
                else if (value > maxWidth)
                    _width = maxWidth;
                else
                    _width = value;
            }
        }

        public float minWidth = 350f; //default value
        public float maxWidth = 500f; //default value
        public int padding { get; set; }
        public float columnx { get; set; } //starting location of the column
        public float columny { get; set; }
        public float height { get; set; }

        public void Rewind() //used by the ColumnGroup at the beginning of each redraw
        {
            _drawPointer = DB.NewRect(DB.NewVector2(columnx + padding, columny + padding), DB.NewVector2(width - 2 * padding, blockHeight));
        }

        private IRect _drawPointer;
        public IRect drawPointer { get { return DB.NewRect(_drawPointer); } }
        internal int blockHeight { get; set; }
        internal int blockPadding { get; set; }

        public void Space(float space) {
            _drawPointer.y += space;
        }

        public T PostContent<T>(Func<IRect, T> func) {
            T t = func(drawPointer);
            _drawPointer.y += _drawPointer.height;
            return t;
        }

        // a version that allows us to change the height of the drawn function
        public T PostContent<T>(Func<IRect, T> func, float height) {
            IRect drawSpace = DB.NewRect(drawPointer);
            drawSpace.height = height;
            T t = func(drawSpace);
            _drawPointer.y += drawSpace.height;
            return t;
        }

        public ReturnedValue<T> PostContent<T>(AbstractGUIWrapper<T> wrapper, bool usePadding = true) {
            return PostContent(wrapper.OnGUI, wrapper.height, usePadding);
        }

        public ReturnedValue<T> PostContent<T>(AbstractGUIElement<T> abstractGUIElement, bool usePadding = true) {
            return PostContent(abstractGUIElement.OnGUI, abstractGUIElement.height, usePadding);
        }

        public T PostContent<T>(Func<IRect, T> func, float height, bool usePadding) {
            int pad = !usePadding ? padding : 0;
            IRect temp = DB.NewRect(drawPointer);
            temp.height = height;
            temp.x -= pad;
            temp.width += pad * 2;
            T t = func(temp);
            _drawPointer.y += height;
            return t;
        }

        private List<object> _retList = new List<object>();
        public List<object> SplitHorizontalPost(List<Func<IRect, object>> funcList) {
            return SplitHorizontalPost(drawPointer, funcList);
        }

        private List<object> SplitHorizontalPost(IRect drawSpace, List<Func<IRect, object>> funcList) {
            _retList.Clear();
            float split = funcList.Count();
            IRect rect = DB.NewRect(drawSpace.position, DB.NewVector2(drawSpace.width / funcList.Count(), drawSpace.height));
            foreach (var func in funcList) {
                _retList.Add(func(rect));
                rect.x += rect.width;
            }
            _drawPointer.y += blockHeight + blockPadding;
            return _retList;
        }

        public List<object> SplitHorizontalPost(List<Func<IRect, object>> funcList, params SizeTuple[] sizes) {
            if (sizes.Count() != funcList.Count()) {
                return SplitHorizontalPost(drawPointer, funcList); //if they don't match, just do without proportions
            } else {
                return SplitHorizontalPost(drawPointer, funcList, sizes);
            }
        }

        private List<object> SplitHorizontalPost(IRect drawSpace, List<Func<IRect, object>> funcList, params SizeTuple[] sizes) {
            float tot = 0; float remainingSpace = drawSpace.width;
            foreach (var x in sizes) if (!x.isFixed) tot += x; else remainingSpace -= x.width; //add up total

            _retList.Clear();
            IRect rect = DB.NewRect(drawSpace.position, DB.NewVector2(0, drawSpace.height));
            for (int i = 0; i < funcList.Count(); i++) {
                rect.width = sizes[i].isFixed ? sizes[i].width : remainingSpace * (sizes[i] / tot);
                rect.y += (1 - sizes[i].heightScale) / 2 * rect.height;
                rect.height *= sizes[i].heightScale;
                _retList.Add(funcList[i](rect));
                rect.x += rect.width;
            }
            _drawPointer.y += blockHeight + blockPadding;
            return _retList;
        }

        IRect _storedDrawPointer;
        public IVector2 BeginScrollView(IVector2 currentPosition, IVector2 contentSize, bool alwaysShowHorizontal, bool alwaysShowVertical) {
            IRect position = DB.NewRect(drawPointer);
            position.width += (2 * padding);
            position.height = (height - position.y);
            position.x -= padding;
            contentSize.x -= 15f;
            _storedDrawPointer = drawPointer;
            _storedDrawPointer.y += contentSize.y;
            Rewind();
            _drawPointer.y -= padding;
            return GUI.BeginScrollView(position, currentPosition, DB.NewRect(DB.ZeroVector(), contentSize), alwaysShowHorizontal, alwaysShowVertical);
        }

        public void EndScrollView() {
            _drawPointer = _storedDrawPointer;
            GUI.EndScrollView();
        }
    }

    public struct SizeTuple {
        public float width;
        public bool isFixed;
        public float heightScale;
        public SizeTuple(float width, bool isFixed, float heightScale = 1f) {
            this.width = width;
            this.isFixed = isFixed;
            this.heightScale = heightScale;
        }

        public static SizeTuple operator +(SizeTuple lhs, SizeTuple rhs) {
            return new SizeTuple(lhs.width + rhs.width, lhs.isFixed && rhs.isFixed);
        }

        public static SizeTuple operator +(SizeTuple lhs, float rhs) {
            return new SizeTuple(lhs.width + rhs, lhs.isFixed);
        }

        public static float operator /(SizeTuple lhs, float rhs) {
            return lhs.width / rhs;
        }

        public static float operator +(float lhs, SizeTuple rhs) {
            return lhs + rhs.width;
        }
    }
}
