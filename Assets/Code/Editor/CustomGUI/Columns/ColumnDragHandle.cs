﻿using UnityEditor;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.Columns {
    public class ColumnDragHandle : Drag {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;

        private ColumnDragHandle() : base() { } //no argument-less construction

        private readonly Column _column;
        public float width, height;
        public ColumnDragHandle(Column column, float width, float height) : base(DB.ZeroRect(), MouseCursor.ResizeHorizontal, 4) {
            _column = column;
            this.width = width;
            this.height = height;
        }

        public ColumnDragHandle(Column column, float width, float height, bool mutable) : base(DB.ZeroRect(), 4, MouseCursor.ResizeHorizontal, mutable) {
            _column = column;
            this.width = width;
            this.height = height;
        }

        public override ReturnedValue Control() {
            //space inheritied from Column
            _space = DB.NewRect(DB.NewVector2(_column.columnx + _column.width - width / 2, _column.columny), DB.NewVector2(width, height));
            EditorGUI.DrawRect(_space, DB.NewColor(.5f, .5f, .5f));
            return base.Control();
        }

        float _loffset; //the amount we're offset from the control handle when we've reached the minimum width
        float _roffset; //the amount we're offset from the control handle when we've reached the maximum width
        protected override void OnHorizontalChange(float x) {
            if (x > 0) {
                float w = _column.width + x;
                if (w > _column.maxWidth) {
                    _column.width = _column.maxWidth;
                    _roffset += (w - _column.maxWidth);
                } else if (_loffset > 0) {
                    _loffset -= x;
                    if (_loffset < 0) {
                        _column.width -= _loffset;
                        _loffset = 0;
                    }
                } else {
                    _column.width = w;
                }
            } else if (x < 0) {
                float w = _column.width + x;
                if (w < _column.minWidth) {
                    _column.width = _column.minWidth;
                    _loffset += (_column.minWidth - w);
                } else if (_roffset > 0) {
                    _roffset += x;
                    if (_roffset < 0) {
                        _column.width += _roffset;
                        _roffset = 0;
                    }
                } else {
                    _column.width = w;
                }
            }
        }

        protected override void OnVerticalChange(float y) {
            return;
        }

        protected override void OnReleaseHandle() {
            base.OnReleaseHandle();
            _loffset = 0;
            _roffset = 0;
        }
    }
}
