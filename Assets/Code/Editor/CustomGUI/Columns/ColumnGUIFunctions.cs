﻿using System;
using System.Collections.Generic;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;

namespace Assets.Code.Editor.CustomGUI.Columns {
    public class ColumnGUIFunctions {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;
        private static IEditorStyles EditorStyles => EditorStylesAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;

        public static readonly List<float> oneToTwo = new List<float>() { 1, 2 };
        public static IGUIStyle MidButton() {
            var style = DB.NewGUIStyle(EditorStyles.MiniButtonMid());
            style.SetFontSize(8);
            return style;
        }
        public static IGUIStyle LeftButton() {
            IGUIStyle style = DB.NewGUIStyle(EditorStyles.MiniButtonLeft());
            style.SetFontSize(8);
            return style;
        }
        public static IGUIStyle RightButton() {
            var style = DB.NewGUIStyle(EditorStyles.MiniButtonRight());
            style.SetFontSize(8);
            return style;
        }
        public static readonly IGUIStyle headerStyle = DependencyManager.Instance.NewGUIStyle( x => x.fontStyle = FontStyle.Bold, x => x.fontSize = 16);
        public static readonly IGUIStyle subHeaderStyle = DependencyManager.Instance.NewGUIStyle( x => x.fontSize = 12, x => x.fontStyle = FontStyle.Bold );
        public static readonly IGUIStyle dndBoxStyle = DependencyManager.Instance.NewGUIStyle( x => x.border = DB.NewRect(0, 0, 0, 0) );
        //public static readonly GUIStyle 

        public static Func<IRect, object>
            TextField(string a, IGUIStyle style) => (IRect rect) => EditorGUI.TextField(rect, a, style);
        public static Func<IRect, object>
            PopUp(int a, IGUIContent[] b, IGUIStyle style) => (IRect rect) => EditorGUI.Popup(rect, a, b, style);
        // we have void functions simply return a boxed 0
        public static Func<IRect, object>
            Label(string a, IGUIStyle style) => (IRect rect) => { GUI.Label(rect, a, style); return 0; };
        public static Func<IRect, object>
            LabelGC(IGUIContent a, IGUIStyle style) => (IRect rect) => { EditorGUI.LabelField(rect, a, style); return 0; };
        public static Func<IRect, object>
            Button(string a, IGUIStyle style) => (IRect rect) => GUI.Button(rect, a, style);
        public static Func<IRect, object>
            Button(ITexture texture) => (IRect rect) => GUI.Button(rect, texture);

        public static Func<IRect, object>
            ButtonOffset(string a, IGUIStyle style, IVector2 offset) => (IRect rect) => { DB.NewRect(rect.position.Add(offset), rect.size); return GUI.Button(rect, a, style); };
        public static Func<IRect, object>
            ButtonGC(IGUIContent a, IGUIStyle style) => (IRect rect) => GUI.Button(rect, a, style);
        public static Func<IRect, object>
            Toggle(string a, bool b) => (IRect rect) => EditorGUI.ToggleLeft(rect, a, b);
        public static Func<IRect, object>
            ToggleGC(IGUIContent a, bool b) => (IRect rect) => EditorGUI.ToggleLeft(rect, a, b);
        public static Func<IRect, object>
            Warning(string a) => (IRect rect) => { EditorGUI.Warning(rect, a); return 0; };
        public static Func<IRect, object>
            Error(string a) => (IRect rect) => { EditorGUI.Error(rect, a); return 0; };
        public static Func<IRect, object>
            DrawRect(IRect size, IColor b) => (IRect rect) => { EditorGUI.DrawRect(rect, size, b); return 0; };
        public static Func<IRect, object>
            ObjField(object obj, Type type) => (IRect rect) => EditorGUI.ObjectField(rect, obj, type, false);
        public static Func<IRect, object>
            Icon(IRect rect, ITexture tex) => (IRect space) => { GUI.Box(DB.NewRect(space.position.Add(rect.position), rect.size), tex, dndBoxStyle); return 0; };
        public static Func<IRect, object>
            ControlName(string s) => (IRect space) => { GUI.SetNextControlName(s); return 0; };
        public static Func<IRect, object>
            Emptiness() => (IRect space) => { return 0; };

        public static void HorizontalLine(Column column) {
            IRect horizontalLine = DB.NewRect(DB.ZeroVector(), DB.NewVector2(column.width, 2f));
            IRect hline = DB.NewRect(horizontalLine.x, horizontalLine.y, horizontalLine.width, horizontalLine.height);
            IColor color = DB.NewColor(.5f, .5f, .5f);
            column.PostContent(DrawRect(hline, color), 2, false);
        }
    }
}
