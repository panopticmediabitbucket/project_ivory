﻿using UnityEngine;
using UnityEditor;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Adapter;
using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.CustomGUI.Columns {
    public class ColumnGroup {
        public delegate void DrawFunction(Column col, params object[] args);

        public static readonly DrawFunction doNothing = delegate (Column column, object[] args) { return; };

        private readonly int _columnCount;
        private readonly bool _finalColumnFillsSpace = false;
        private DrawFunction[] _drawFunctions;
        private object[][] _drawArguments;
        public Column[] _columns;
        public ColumnDragHandle[] _dragHandles;
        private IRect _columnSpace;
        public IRect columnSpace {
            get { return _columnSpace; }
            set {
                float remainingWidth = value.width;
                foreach (var column in _columns) {
                    if (column.index == _columnCount - 1 && _finalColumnFillsSpace) {
                        column.width = remainingWidth;
                        column.height = value.height;
                    } else {
                        float proportion = column.width / _columnSpace.width;
                        float width = proportion * value.width;
                        column.width = width > column.minWidth ? (width > column.maxWidth ? column.maxWidth : width) : column.minWidth;
                        remainingWidth -= column.width;
                        column.height = value.height;
                    }
                }
                foreach (var handle in _dragHandles) {
                    handle.height = value.height;
                }
                _columnSpace = value;
            }
        }

        IEditorWindow _window;
        public ColumnGroup(int columnCount, IRect columnSpace, IEditorWindow window, bool mutable, bool fillSpace) {
            _window = window;

            _columnCount = columnCount;
            _finalColumnFillsSpace = fillSpace;
            _drawFunctions = new DrawFunction[columnCount];
            _drawArguments = new object[columnCount][];
            _columns = new Column[columnCount];
            _dragHandles = new ColumnDragHandle[columnCount];
            for (int i = 0; i < columnCount; i++) {
                _drawFunctions[i] = doNothing;
                _columns[i] = new Column(i) { height = columnSpace.height, width = columnSpace.width / _columns.Length, columnx = i * columnSpace.width / _columns.Length };
                _dragHandles[i] = new ColumnDragHandle(_columns[i], 2f, columnSpace.height, mutable);
            }
            if (fillSpace) {
                SetMinMaxWidthForColumn(columnCount - 1, 0, 1500f);
            }

            _columnSpace = columnSpace;
            this.columnSpace = columnSpace;
        }

        public Column GetColumn(int index) {
            return _columns[index];
        }

        int _count = 0;
        bool _grabbed = false;
        Event last;
        Vector2 _prevPos;
        public void OnGUI() {
            float colWidth = _columnSpace.x; //used to set starting draw position
            for (int i = 0; i < _columns.Length; i++) {
                _columns[i].columnx = colWidth;
                _columns[i].columny = _columnSpace.y;
                _columns[i].Rewind();
                colWidth += _columns[i].width;
                _drawFunctions[i](_columns[i], _drawArguments[i]);
            }

            for (int i = 0; i < _dragHandles.Length; i++) {
                _dragHandles[i].height = _columnSpace.height;
                if (_dragHandles[i].Control()) _window.Repaint();
            }
        }

        public void SetMinMaxWidthForColumn(int index, float minWidth, float maxWidth) {
            _columns[index].minWidth = minWidth; _columns[index].maxWidth = maxWidth;
        }

        public void SetWidthForColumn(int index, float width) {
            _columns[index].width = width;
        }

        public void SetColumnPadding(int index, int padding) {
            _columns[index].padding = padding;
        }

        public void SetDrawFunction(DrawFunction draw, int columnIndex, params object[] args) {
            _drawFunctions[columnIndex] = draw;
            _drawArguments[columnIndex] = args;
        }
    }
}
