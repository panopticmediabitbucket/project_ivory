﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using Assets.Code.System.Utilities;
using System;
using UnityEditor;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel {
    public enum KeyHandleInteraction { None, Drag, Delete }
    public sealed class KeyHandle : Drag, IControllable<KeyHandleInteraction>, IDrawable, IComparable<KeyHandle> {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEvent Event => EventAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        private static ITexture keyIcon = MetaUtilities.AssetFromGuid<ITexture>("a18beadb21b030d4c93c90abe72b7639");

        private readonly Canvas _canvas;

        public readonly _Key keyId;
        public Key key => keyId.key;
        public float time { get => key.time; private set => key.time = value; }
        public float v { get => key.v; private set => key.v = value; }
        public Guid id => keyId.id;


        public IRect GetSpace() {
            CanvasScale scale = _canvas.scale;
            IVector2 position = DB.NewVector2(scale.pixelsPerMS * time, -(scale.pixelsPerMPS * v));
            _space = DB.NewRect(_canvas.origin.Add(position), DB.ZeroVector());
            return _space;
        }

        public KeyHandle(_Key keyId, Canvas canvas, bool startGrabbed = false) : base(DB.ZeroRect(), 6, MouseCursor.Arrow, true) {
            this.keyId = keyId;
            _canvas = canvas;
            _grabbed = startGrabbed;
            if (_grabbed) {
                _prevPos = Event.current.mousePosition;
            }
        }

        public new ReturnedValue<KeyHandleInteraction> Control() {
            // Extract scale to calculate key location
            IRect space = GetSpace();
            return Control(space);
        }

        public new ReturnedValue<KeyHandleInteraction> Control(IRect space) {
            bool leftControl = InputUtilities.IsKeyHeld(KeyCode.LeftControl);

            if (leftControl && !_grabbed) {
                if (base.Control(space)) {
                    return new ReturnedValue<KeyHandleInteraction>(KeyHandleInteraction.Delete);
                } else {
                    return ReturnedValue<KeyHandleInteraction>.noReturnValue;
                }
            }

            return base.Control(space) ? new ReturnedValue<KeyHandleInteraction>(KeyHandleInteraction.Drag) :
                ReturnedValue<KeyHandleInteraction>.noReturnValue;
        }

        public void Draw() {
            CanvasScale scale = _canvas.scale;
            IVector2 drawPos = DB.NewVector2(scale.pixelsPerMS * time - 8f, -(scale.pixelsPerMPS * v + 8f));
            GUI.DrawTexture(DB.NewRect(_canvas.origin.Add(drawPos), DB.NewVector2(16, 16)), keyIcon);
        }

        public void Draw(IRect rect) {
            Draw();
        }


        protected override void OnHorizontalChange(float mousex) {
            IVector2 origin = _canvas.origin;
            if (Event.current.mousePosition.x <= origin.x) {
                time = 0;
            } else if (Event.current.mousePosition.x - origin.x < mousex) {
                time += ((Event.current.mousePosition.x - origin.x) / _canvas.scale.pixelsPerMS);
            } else {
                time += mousex / _canvas.scale.pixelsPerMS;
            }
        }
        protected override void OnVerticalChange(float mousey) {
            IVector2 origin = _canvas.origin;
            if (Event.current.mousePosition.y >= origin.y) {
                v = 0;
            } else if (origin.y - Event.current.mousePosition.y < mousey) {
                v -= ((Event.current.mousePosition.x - origin.y) / _canvas.scale.pixelsPerMPS);
            } else {
                v -= mousey / _canvas.scale.pixelsPerMPS;
            }
        }

        public int CompareTo(KeyHandle other) {
            return keyId.CompareTo(other.keyId);
        }
    }
}
