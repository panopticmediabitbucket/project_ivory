﻿using Assets.Code.Physics.Accelerators;
using System;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel {
    // Runtime class for identifying and manipulating keys in the editor
    public struct _Key : IComparable<_Key> {
        public readonly Key key;
        public readonly Guid id;
        public float v => key.v;
        public float time => key.time;
        public float percentage => key.percentage;
        public _Key(Key key, Guid? id = null) {
            this.key = key;
            this.id = id == null ? Guid.NewGuid() : id.Value;
        }

        public _Key Dup() {
            Key key = new Key() { v = this.v, position = this.key.position, time = this.time };
            return new _Key(key, id);
        }

        public int CompareTo(_Key x) {
            return this.key.time < x.key.time ? -1 : this.key.time > x.key.time ? 1 : 0;
        }
    }
}
