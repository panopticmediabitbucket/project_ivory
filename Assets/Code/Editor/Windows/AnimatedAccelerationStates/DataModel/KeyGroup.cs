﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel {
    public class KeyGroup {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IHandles Handles => HandlesAdapter.Instance;

        private readonly List<_Key> _keys = new List<_Key>();
        public Guid targetKeyId { get; private set; }
        public bool dirty { get; private set; }


        public object GetTargetKeyId() {
            return (object)targetKeyId;
        }

        public KeyGroup(Key[] keys) {
            if (keys.Length == 0) {
                Key key = new Key(0, 0);
                _keys.Add(new _Key(key));
            } else {
                keys.ToList().ForEach(x => {
                    _Key key = new _Key(x);
                    _keys.Add(key);
                });
            }
            dirty = true;
        }

        public void DeleteKey(Guid keyId) {
            var key = _keys.Find(x => x.id.Equals(keyId));
            _keys.Remove(key);
            if (_keys.Count == 0) {
                _Key newkey = new _Key(new Key(0, 0));
                _keys.Add(newkey);
            }
            dirty = true;
        }

        public void RemoveDuplicates() {
            List<_Key> removals = new List<_Key>();
            for (int i = 0; i < _keys.Count; i++) {
                var main = _keys.ElementAt(i);
                for (int j = i + 1; j < _keys.Count; j++) {
                    var duplicate = _keys.ElementAt(j);
                    if (main.time.Equals(duplicate.time)) {
                        dirty = true;
                        removals.Add(duplicate);
                    }
                }
            }
            removals.ForEach(x => DeleteKey(x.id));
        }


        public Guid AddKey(float time, float v) {
            dirty = true;
            _Key key = new _Key(new Key(time, v));
            _keys.Add(key);
            _keys.Sort();
            return key.id;
        }

        public Guid AddActiveKeyFromCoords(float time, float value) {
            dirty = true;
            _Key key = new _Key(new Key(time, value));
            _keys.Add(key);
            _keys.Sort();
            targetKeyId = key.id;
            return key.id;
        }

        public void ManualUpdate(Guid keyId, float time, float v) {
            _Key key = _keys.Find(x => x.id.Equals(keyId));
            key.key.v = v;
            key.key.time = time;
        }

        public List<KeyHandle> GetKeyHandles(Canvas canvas) {
            dirty = false;
            _keys.Sort();
            int i = 0;
            _keys.ForEach(x => x.key.position = i++);
            return _keys.Select(x => new KeyHandle(x, canvas, x.id.Equals(targetKeyId))).ToList();
        }

        public List<_Key> GetKeys() {
            _keys.Sort();
            return _keys;
        }

        public Key[] GetKeyArray() {
            return GetKeys().Select(x => x.key).ToArray();
        }

    }
}
