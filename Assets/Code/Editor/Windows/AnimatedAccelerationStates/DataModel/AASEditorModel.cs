﻿using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.GameCode.System.Schemata;
using Assets.Code.Physics.Accelerators;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel
{
    public class AASEditorModel
    {
        public string name { get; set; }
        public KeyGroup keyGroup { get; set; }
        public KnobControl knobControl { get; set; }
        public string pairedAnimation { get; set; }
        public PopoutDrawer<PopoutSelectorEntry<string>> animationTriggerSelector { get; set; }
        public ScaleType scaleType { get; set; }
        public FrictionResponseType frictionResponse { get; set; }
        public DirectionType directionType { get; set; }
        public float interruptTime { get; set; }
        public ConditionSchema interruptSchema { get; set; }
        public float attenuationRatio { get; set; }
    }
}
