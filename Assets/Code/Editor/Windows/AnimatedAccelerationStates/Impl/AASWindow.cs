﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Adapter;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using UnityEditor;
using UnityEngine;
using PrefabAssetType = Assets.Code.Editor.AdapterLayer.Static_Classes.PrefabAssetType;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl {
    public class AASWindow : IEditorWindow {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static ISelection Selection => SelectionAdapter.Instance;
        private static IPrefabUtility PrefabUtility => PrefabUtilityAdapter.Instance;
        private static IReflectionHandler ReflectionHandler => ReflectionHandlerAdapter.Instance;

        private static AASWindow _instance;
        private readonly IEditorWindow _window;

        // accessors to underlying window adapter improve legibility
        public IRect position => _window.position;
        public void Repaint() => _window.Repaint();

        private AASWindow(IEditorWindow editorWindow) {
            _window = editorWindow;
        }

        public static AASWindow GetInstance(IEditorWindow window) {
            if (_instance == null || _instance._window != window) {
                _instance = new AASWindow(window);
            }
            return _instance;
        }

        IRect _windowRect;
        ColumnGroup _columnGroup;
        IAASFunctionModel _functionModel;
        public void OnEnable() {
            _windowRect = DB.NewRect(DB.ZeroVector(), position.size);
            _columnGroup = new ColumnGroup(2, _windowRect, this, false, true);
            _columnGroup.SetColumnPadding(0, 12);
            _columnGroup.SetWidthForColumn(0, 400);
            _columnGroup.SetMinMaxWidthForColumn(0, 400, 400);
            _columnGroup.SetDrawFunction(ColumnGroup.doNothing, 0);
            _columnGroup.SetDrawFunction(ColumnGroup.doNothing, 1);
            EditorApplication.playModeStateChanged += OnPlayEvent;
        }

        public void OnPlayEvent(PlayModeStateChange state) {
            if (_functionModel != null && state == PlayModeStateChange.ExitingEditMode) {
                _functionModel.ForceSave();
            }
            EditorApplication.playModeStateChanged -= OnPlayEvent;
        }

        public void OnSelectionChange() {
            if (_functionModel != null) {
                _functionModel.ForceSave();
            }
            AnalyzeTarget();
        }

        private void AnalyzeTarget() {
            List<IComponent> list = Selection.activeGameObject.GetComponents().ToList();
            list = list.FindAll(x => x.GetComponent().GetType().BaseType.IsConstructedGenericType);
            var componentWrapper = ReflectionHandler.GetGenericComponent(list, typeof(PhysicsStateMachineWorldObject<>));
            var component = componentWrapper == null ? null : componentWrapper.GetComponent();
            IAnimator animator = Selection.activeGameObject.GetComponent<IAnimator>();
            if (component != null && animator != null) {
                PrefabAssetType prefabAssetType = PrefabUtility.GetPrefabAssetType(Selection.activeGameObject);
                switch (prefabAssetType) {
                    case PrefabAssetType.Regular:
                        // Use enumeration type to construct 
                        Type[] tEnum = component.GetType().BaseType.GenericTypeArguments;
                        Type functionModelType = typeof(AASFunctionModel<>).MakeGenericType(tEnum[0]);
                        IGameObject target = Selection.activeGameObject;
                        _functionModel = BuildFunctionModel(functionModelType, target);

                        // Assign columngroup functions
                        _columnGroup.SetDrawFunction(_functionModel.SelectionWindow, 0);
                        _columnGroup.SetDrawFunction(_functionModel.EditWindow, 1);
                        _columnGroup.SetColumnPadding(1, 0);
                        break;
                    default:
                        //AssetPathUtil.CreatePrefabDialog(Selection.activeGameObject);
                        break;
                }
            } else {
                _functionModel = null;
            }

            Repaint();
        }

        private IAASFunctionModel BuildFunctionModel(Type type, IGameObject gameObject) => ((IAASFunctionModel)Activator.CreateInstance(type)).DeliverGameObject(gameObject).DeliverRepaintCallback(Repaint);

        public void OnGUI() {
            if (_functionModel == null) {
                AnalyzeTarget();
            }
            _columnGroup.columnSpace = DB.NewRect(DB.ZeroVector(), position.size);
            _columnGroup.OnGUI();
        }

        public void Update() {
            if (_functionModel != null)
                _functionModel.Update();
        }
    }
}
