﻿using System.Linq;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Interfaces;
using System;
using System.Collections.Generic;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities.Time;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.System.TimeUtil;
using UnityEditorInternal;
using UnityEngine;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl {
    public partial class AASPreviewStack<TEnum> : AbstractGUIWrapper<bool> where TEnum : Enum {
        private interface IStepSampler {
            float animationLength { get; }
            IAnimatorState targetState { get; }
            int loopCount { get; }
            bool IsMuted();
        }

        private abstract class PreviewStackStep: AbstractGUIWrapper<StatePair>, IStepSampler {

            // Style data
            static IGUIStyle _errorMessageStyle = DB.NewGUIStyle((x => x.normal.textColor = DB.NewColor(.9f, .2f, .2f)), (x => x.fontSize = 10));
            static ITexture _turqLoop = MetaUtilities.AssetFromGuid<ITexture>("4d15f87888a0e3e42bc3b485a9588b62");
            static ITexture _notMuted = MetaUtilities.AssetFromGuid<ITexture>("8a016c028df14fe4db88548d29533027");
            static ITexture _muted = MetaUtilities.AssetFromGuid<ITexture>("997f26ca0a2c49b4dacd71c3a68fdcf2");
            static ITexture _immutable = MetaUtilities.AssetFromGuid<ITexture>("62f86935c66e5284ca4f51ab97aad0bc");

            // immutable data
            static readonly IEnumerable<PopoutSelectorEntry<int>> _numbers =
                new List<PopoutSelectorEntry<int>>() { new PopoutSelectorEntry<int>(1, "1"), new PopoutSelectorEntry<int>(2, "2"), new PopoutSelectorEntry<int>(3, "3") };

            // instance data
            public float animationLength { get; protected set; }
            public abstract IAnimatorState targetState { get; }
            protected IColor _color { get; set; }

            private readonly bool _isMutable;
            private readonly bool _isLoopable;

            // subordinate elements
            private IEnumerable<PopoutSelectorEntry<StatePair>> _selectionList;
            protected PopoutDrawer<PopoutSelectorEntry<StatePair>> _selector { get; private set; }
            protected PopoutDrawer<PopoutSelectorEntry<int>> _loopCount { get; private set; }
            protected readonly ToggleButton _muteButton;

            public PreviewStackStep(IColor color, int height, IEnumerable<PopoutSelectorEntry<StatePair>> selectionList, bool isMutable = true, bool isLoopable = true) : base(height, 0) {
                _selectionList = selectionList;
                PopoutSelector<StatePair> selector = new PopoutSelector<StatePair>(selectionList, selectionList.Count() == 0 ? new PopoutSelectorEntry<StatePair>(default, null) : selectionList.First(), height);
                _selector = selectionList == null ? null : new PopoutDrawer<PopoutSelectorEntry<StatePair>>(selector);
                _color = color;
                _isMutable = isMutable;
                _isLoopable = isLoopable;
                PopoutSelector<int> loopSelector = new PopoutSelector<int>(_numbers, _numbers.First());
                _loopCount = new PopoutDrawer<PopoutSelectorEntry<int>>(loopSelector);
                _muteButton = new ToggleButton(_notMuted, _muted, DB.NewVector2(height, height));
            }

            public override ReturnedValue<StatePair> OnGUI() {
                return OnGUI(DB.NewRect(0, 0, width, height));
            }

            IColor bgColor = DB.NewColor(.3f, .3f, .3f);

            public override ReturnedValue<StatePair> OnGUI(IRect rect) {
                IRect colorRect = DB.NewRect(rect);
                colorRect.width = colorRect.height;
                GUIFunctions.DrawBorderedRect(colorRect, GUIFunctions.DarkenColor(_color), _color, 1);

                IRect bgRect = DB.NewRect(rect);
                bgRect.x += colorRect.width;
                bgRect.width -= colorRect.width;
                GUIFunctions.DrawBorderedRect(bgRect, GUIFunctions.DarkenColor(bgColor), bgColor, 1);

                IRect selectorRect = DB.NewRect(bgRect);
                selectorRect.width *= .7f;
                ReturnedValue<PopoutSelectorEntry<StatePair>> returnValue = DrawSelectorAndSelection(selectorRect);

                IRect optionsRect = DB.NewRect(bgRect.x + bgRect.width * .7f, bgRect.y, bgRect.width * .3f, bgRect.height);
                LoopOptions(optionsRect);

                return PackageReturnValue();
            }

            private void LoopOptions(IRect optionsRect) {
                IRect iconRect = DB.NewRect(optionsRect.x, optionsRect.y, optionsRect.height, optionsRect.height);
                IRect loopCountRect = DB.NewRect(iconRect.x + iconRect.width, iconRect.y, optionsRect.width - iconRect.width - optionsRect.height, iconRect.height);
                IRect muteRect = DB.NewRect(optionsRect.x + optionsRect.width - optionsRect.height, optionsRect.y, optionsRect.height, optionsRect.height);
                iconRect = GUIFunctions.ShrinkRect(iconRect, 2);
                GUI.Box(iconRect, _turqLoop, GUIFunctions.textureStyle);
                if (_isMutable) {
                    _loopCount.OnGUI(loopCountRect);
                    _muteButton.OnGUI(muteRect);
                } else {
                    _loopCount.OnGUINotOpenable(loopCountRect);
                    GUI.Box(muteRect, _immutable, GUIFunctions.textureStyle);
                }
            }

            public ReturnedValue<PopoutSelectorEntry<StatePair>> DrawSelectorAndSelection(IRect selectorRect) {
                if (_isMutable) {
                    if (_selectionList.Count() == 0) {
                        GUI.Label(selectorRect, noValidSelectionMessage, _errorMessageStyle);
                        return default;
                    }
                    return _selector.OnGUI(selectorRect);
                } else {
                    return _selector.OnGUINotOpenable(selectorRect);
                }
            }

            public override bool Equals(object obj) {
                PreviewStackStep other = (PreviewStackStep)obj;
                return _selectionList.SequenceEqual(other._selectionList);
            }

            protected abstract string noValidSelectionMessage { get; }

            public int loopCount => _loopCount.contentSummary.item;

            protected abstract ReturnedValue<StatePair> PackageReturnValue();
            public abstract bool IsMuted();
        }

        private sealed class PreviewStackEntry : PreviewStackStep {
            public readonly TriggerMotionMatches motionMatches;
            public StatePair selectedPair => _selector.contentSummary.item;

            public PreviewStackEntry(TriggerMotionMatches motionMatches, int height)
                : base(DB.NewColor(.8f, .3f, 0), height, GetSelectorEntriesForStatePairs(motionMatches)) {
                this.motionMatches = motionMatches;
            }

            protected override string noValidSelectionMessage => "No edge traversed by trigger";

            public override IAnimatorState targetState => selectedPair.A;

            private static IEnumerable<PopoutSelectorEntry<StatePair>> GetSelectorEntriesForStatePairs(TriggerMotionMatches motionMatches) {
                return motionMatches.statePairs.Select(x => new PopoutSelectorEntry<StatePair>(x, x.A.name + " ----> " + x.B.name));
            }

            public PreviewStackMainState GetSubsequentStep() {
                return new PreviewStackMainState(selectedPair, height);
            }

            public override bool IsMuted() {
                return _muteButton.active;
            }

            protected override ReturnedValue<StatePair> PackageReturnValue() {
                return new ReturnedValue<StatePair>(selectedPair);
            }
        }

        private sealed class PreviewStackMainState : PreviewStackStep {
            public StatePair pair => _selector.contentSummary.item;

            public IAnimatorState state => pair.B;
            public PreviewStackMainState(StatePair pair, int height) :
                base(DB.NewColor(.2f, .9f, 1), height, 
                    pair.B == null ? new List<PopoutSelectorEntry<StatePair>>() 
                    : new List<PopoutSelectorEntry<StatePair>>() { new PopoutSelectorEntry<StatePair>(pair, pair.B.name) },
                    false, false) { }

            protected override string noValidSelectionMessage => "No valid state to target";

            public override IAnimatorState targetState => pair.B;

            public override bool IsMuted() {
                return false;
            }

            protected override ReturnedValue<StatePair> PackageReturnValue() {
                return new ReturnedValue<StatePair>(pair);
            }

            public PreviewStackOutro GetSubsequentStep() {
                List<StatePair> pairs = state == null ? new List<StatePair>() : AnimationGraphUtil.GetTerminalOutroStates(_animator.runtimeAnimatorController, state);
                return new PreviewStackOutro(pairs.Select(x => new PopoutSelectorEntry<StatePair>(x, x.B.name)), height);
            }
        }

        private sealed class PreviewStackOutro : PreviewStackStep {
            public IAnimatorState state => contentSummary.B;
            protected StatePair contentSummary => _selector.contentSummary.item;
            public PreviewStackOutro(IEnumerable<PopoutSelectorEntry<StatePair>> entries, int height)
                : base(DB.NewColor(.8f, .5f, .2f), height, entries) { }

            protected override string noValidSelectionMessage => "ERROR: test state is terminal";

            public override IAnimatorState targetState => state;

            public override bool IsMuted() {
                return _muteButton.active;
            }

            protected override ReturnedValue<StatePair> PackageReturnValue() {
                return new ReturnedValue<StatePair>(contentSummary);
            }
        }

        private class PreviewStackSampler {
            private Queue<StateAnimationSampler> _samplers;
            private PreviewTimer _previewTimer;
            private Accel<TEnum> _motion;

            public PreviewStackSampler(IEnumerable<IStepSampler> steps, Accel<TEnum> motion, PreviewTimer previewTimer, bool useGravity) {
                _samplers = BuildSamplers(steps, motion, useGravity);
                _previewTimer = previewTimer;
                _motion = motion;
            }

            private Queue<StateAnimationSampler> BuildSamplers(IEnumerable<IStepSampler> steps, Accel<TEnum> motion, bool useGravity) {
                Queue<StateAnimationSampler> samplers = new Queue<StateAnimationSampler>();
                foreach (var step in steps) {
                    if (!step.IsMuted()) {
                        for (int i = 0; i < step.loopCount; i++) {
                            if (step.GetType().Equals(typeof(PreviewStackMainState))) {
                                samplers.Enqueue(new StateAnimationSampler(step.targetState.clip, motion, useGravity));
                            } else {
                                samplers.Enqueue(new StateAnimationSampler(step.targetState.clip));
                            }
                        }
                    }
                }
                return samplers;
            }

            // Returns false if the queue is empty and the sampling is complete
            public bool RunSampler(IGameObject gameObject, IStateBasedMotionMachine<TEnum> motionMachine, float speed) {
                if (motionMachine.IsActive(_motion.state)) {
                    ((PreviewTimer)(_motion.timer)).Update(speed);
                }
                _previewTimer.Update(speed);
                return Sample(gameObject, motionMachine);
            }

            private bool Sample(IGameObject gameObject, IStateBasedMotionMachine<TEnum> motionMachine) {
                StateAnimationSampler sampler = _samplers.Count == 0 ? null : _samplers.Peek();
                if (sampler != null && sampler.CheckFinished(_previewTimer)) {
                    _previewTimer = new PreviewTimer(sampler.GetExcessTime(_previewTimer));
                    _samplers.Dequeue();
                    return Sample(gameObject, motionMachine);
                } else if (sampler != null) {
                    sampler.SampleMotionAndAnimation(_previewTimer, gameObject, motionMachine);
                    return true;
                } else {
                    return false;
                }
            }

            private class StateAnimationSampler {

                private readonly IAnimationClip _clip;
                private Accel<TEnum> _motionData;
                private bool _useGravity;

                public StateAnimationSampler(IAnimationClip clip, Accel<TEnum> motionData = null, bool useGravity = false) {
                    _clip = clip;
                    _motionData = motionData;
                    _useGravity = useGravity;
                }

                public bool CheckFinished(PreviewTimer timer) {
                    return _clip.length <= timer.elapsedSeconds;
                }

                public void SampleMotionAndAnimation(PreviewTimer timer, IGameObject gameObject, IStateBasedMotionMachine<TEnum> motionMachine) {
                    AnimationMode.SampleAnimationClip(gameObject, _clip, timer.elapsedSeconds);
                    if (_motionData != null && !motionMachine.IsActive(_motionData.state)) {
                        _motionData.DeliverRuntimeStopWatch(new PreviewTimer(timer.deltaTime));
                        motionMachine.DeliverInternalAccel(_motionData);

                        if (_useGravity) {
                            GameObject go = GameObject.Find("GameManager");
                            GameManager manager = (GameManager)go.GetComponent(typeof(GameManager));
                            GlobalPhysicsVariables variables = new GlobalPhysicsVariables(manager.GRAVITY);
                            var gravity = variables.gravity;
                            gravity.DeliverRuntimeStopWatch(_motionData.timer);
                            motionMachine.DeliverExternalAccel(gravity);
                        }

                        _motionData = null;
                    }
                    ((PreviewTimeZone)(motionMachine.timeZone)).underlyingTimer = timer;
                    motionMachine.CalculateFrameMotionAndUpdateMomentum();
                }

                public float GetExcessTime(PreviewTimer timer) {
                    return timer.elapsedSeconds - _clip.length;
                }
            }
        }
    }
}
