﻿using System;
using System.Linq;
using Assets.Code.Editor.CustomGUI.Columns;
using CGF = Assets.Code.Editor.CustomGUI.Columns.ColumnGUIFunctions;
using Assets.Code.Editor.Utilities;
using System.Collections.Generic;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using System.Diagnostics;
using Assets.Code.Editor.CustomGUI.Handlers;
using Assets.Code.Editor.Schemata.Editors;
using UnityEngine;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.System;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.CustomGUI;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl {
    public sealed partial class AASFunctionModel<TEnum> : IAASFunctionModel where TEnum : Enum {

        // callback systems, signals
        internal delegate void EditorChangeCallback(AASEditorModel model);
        private Callback Repaint;
        private EditorChangeCallback OnEditorChange;

        // primary instance data
        private IGameObject _gameObject;
        private AASPreviewStack<TEnum> _previewStack;
        private AASKeyEditor _keyEditor;
        public List<AASEditorModel> editorModels { get; private set; }
        AASEditorModel _targetState;

        private static DropdownMenu _knobRefDropdown = new DropdownMenu(30, "Direction and Reference Type");
        private static DropdownMenu _keysDropdown = new DropdownMenu(30, "Keys");
        private static DropdownMenu _interruptConditionsDropdown = new DropdownMenu(30, "Interrupt Conditions");
        private static DropdownMenu _previewDropdown = new DropdownMenu(30, "Preview Stack");

        ConditionSchemaEditor _schemaEditor = new ConditionSchemaEditor(new Dictionary<string, Type>() { { "blah", typeof(Vector2) } }, 0);

        public readonly List<string> enumNames;
        private string _assetName;

        private Stopwatch _saveTimer;
        private int _saveInterval = 60000;

        private static PopoutSelectorStyle _stateSelectorStyle = PopoutSelectorStyle.BuildStyle(true, _stateIconContent,
            DB.NewGUIStyle((x => x.normal.textColor = DB.NewColor(1, 1, 1)), (x => x.fontSize = 14), (x => x.fontStyle = AdapterLayer.Objects.FontStyle.Normal)));
        private readonly PopoutDrawer<PopoutSelectorEntry<string>> _stateSelector;

        // The constructor takes no parameters because it needs to be invoked using reflection
        // so the main thing we need to do here is load generic parameter related data
        public AASFunctionModel() {
            enumNames = Enum.GetNames(typeof(TEnum)).ToList();
            IEnumerable<PopoutSelectorEntry<string>> stateEntries = enumNames.Select(x => new PopoutSelectorEntry<string>(x, x));

            var selector = new PopoutSelector<string>(stateEntries, stateEntries.First(), 30, _stateSelectorStyle);
            selector.RegisterSelectionChangedHandler(SetTargetEditor);
            _stateSelector = new PopoutDrawer<PopoutSelectorEntry<string>>(selector, 40);
            _numberField = DB.NewGUIStyle(EditorStyles.NumberField());
            _numberField.SetHeight(18f);
        }

        public IAASFunctionModel DeliverGameObject(IGameObject gameObject) {
            _gameObject = gameObject;
            _previewStack = new AASPreviewStack<TEnum>(gameObject);
            _keyEditor = new AASKeyEditor();
            IAnimator animator = _gameObject.GetComponent<IAnimator>();
            editorModels = ConversionUtilities.UnpackAll(AASFileUtility.LoadAssetModels<TEnum>(gameObject, enumNames, _assetName),
                AnimationGraphUtil.GetAnimationTriggers(animator)).ToList();

            _gameObjectIcon = AssetPreview.GetMiniThumbnail(_gameObject);
            _assetName = "/" + gameObject.name.Replace(' ', '_') + "_AcceleratedStates.asset";

            _saveTimer = new Stopwatch();
            _saveTimer.Start();

            RegisterCallbacks();
            SetTargetEditor(_stateSelector.contentSummary.item);
            return this;
        }

        public IAASFunctionModel DeliverRepaintCallback(Callback callback) {
            Repaint += callback;
            return this;
        }

        public void ForceSave() {
            List<AccelAsset> assetModels = ConversionUtilities.PackageAll(editorModels).ToList();
            _gameObject.GetComponent<PhysicsStateMachineWorldObject<TEnum>>().accelAssets = assetModels.ToArray();
            AASFileUtility.SaveModels(_gameObject, assetModels, _assetName);
        }

        private void RegisterCallbacks() {
            OnEditorChange += _keyEditor.OnEditorModelChange;
            OnEditorChange += (model) => _previewStack.targetState = model;
            OnEditorChange += (model) => _schemaEditor.UpdateTypeDictionary(BuildDictionary(model));
            OnEditorChange += (model) => _schemaEditor.UpdateTargetSchema(model.interruptSchema);
            KnobAndReferenceWrapper.DeliverCallback(_keyEditor.OnScaleTypeChange);
        }

        private Dictionary<string, Type> BuildDictionary(AASEditorModel model) {
            Dictionary<string, Type> typeDictionary = new Dictionary<string, Type>();
            typeDictionary.Add(model.name, typeof(AnimatedAccel<TEnum>));
            typeDictionary.Add("Control Manager", typeof(ControlManager));
            return typeDictionary;
        }

        private void SetTargetEditor(string stateName) {
            _targetState = editorModels.Find(x => x.name.Equals(stateName));
            FindAnimationEndTime();
            OnEditorChange(_targetState);
        }

        private void FindAnimationEndTime() {
            var clips = _gameObject.GetComponent<IAnimator>().runtimeAnimatorController.animationClips;
            var pairedAnimationValue = _targetState.pairedAnimation;

            foreach (var clip in clips) {
                if (pairedAnimationValue.Equals(clip.name)) {
                    float timeLength = clip.length * 1000f;
                    _keyEditor.animationLength = timeLength;
                    break;
                }
            }
        }

        IVector2 _scrollViewLocation = DB.ZeroVector();
        public void SelectionWindow(Column column, params object[] args) {
            SelectionHeader(column);

            StateAndAnimationSelectors(column);

            SetScrollView(column);

            var redraw = DrawDirectionControl(column);

            redraw |= column.PostContent(_keysDropdown.OnGUI, 25, false);
            if (_keysDropdown.active) {
                DrawKeys(column);
            }

            redraw |= column.PostContent(_interruptConditionsDropdown.OnGUI, 25, false);
            if (_interruptConditionsDropdown.active) {
                redraw |= column.PostContent(_schemaEditor, false);
            }

            redraw |= column.PostContent(_previewDropdown.OnGUI, 25, false);
            if (_previewDropdown.active) {
                redraw |= DrawPreviewStack(column);
            }

            column.Space(10);
            column.EndScrollView();

            if (redraw)
                Repaint();
        }

        private void SetScrollView(Column column) {
            float dynamicHeight = 100f;
            dynamicHeight += _keysDropdown.active ? _keyEditor.GetKeys().Count() * 20 : 0;
            dynamicHeight += _knobRefDropdown.active ? KnobAndReferenceWrapper.GetInstance(_targetState).height : 0;
            dynamicHeight += _interruptConditionsDropdown.active ? _schemaEditor.height : 0;
            dynamicHeight += _previewDropdown.active ? _schemaEditor.height : 0;
            _scrollViewLocation = column.BeginScrollView(_scrollViewLocation, DB.NewVector2(column.width, dynamicHeight), false, false);
        }

        // Draw the selection header and the save and revert buttons
        private void SelectionHeader(Column column) {
            column.Space(-column.padding);
            column.PostContent(CGF.DrawRect(DB.NewRect(DB.ZeroVector(), DB.NewVector2(column.width, 50f)), _headerColor), 0f, false);
            column.Space(18f);
            IVector2 buttonOffset = DB.NewVector2(0, -3);
            string test = _gameObject.name;
            var returnList = column.SplitHorizontalPost(new List<Func<IRect, object>>() {
            CGF.Icon(DB.NewRect(0, -8f, 30f, 30f), _gameObjectIcon),
            CGF.Emptiness(),
            CGF.Label("Obj: " + _gameObject.name, CGF.headerStyle),
            CGF.Emptiness(),
            CGF.ButtonOffset("Save As", CGF.LeftButton(), buttonOffset),
            CGF.ButtonOffset("Load", CGF.MidButton(), buttonOffset),
            CGF.ButtonOffset("Revert", CGF.RightButton(), buttonOffset),
            CGF.Emptiness() },
            _headerSizes);

            column.Space(6f);
            CGF.HorizontalLine(column);

            if ((bool)returnList[6] &&
                EditorUtility.DisplayDialog("Revert Changes?", "Are you sure you want to revert to the last saved version? ", "Yes", "No")) //revert pressed
            {
                editorModels = ConversionUtilities.UnpackAll(AASFileUtility.LoadAssetModels<TEnum>(_gameObject, enumNames, _assetName, true),
                    AnimationGraphUtil.GetAnimationTriggers(_gameObject.GetComponent<IAnimator>())).ToList();
                SetTargetEditor(_stateSelector.contentSummary.item);
            }

            if ((bool)returnList[4]) {
                List<AccelAsset> assetModels = ConversionUtilities.PackageAll(editorModels).ToList();
                AASFileUtility.SaveAsModels(_gameObject, assetModels);
                _gameObject.GetComponent<PhysicsStateMachineWorldObject<TEnum>>().accelAssets = assetModels.ToArray();
                UnityEditor.PrefabUtility.ApplyPrefabInstance((GameObject)_gameObject.GetGameObject(), UnityEditor.InteractionMode.UserAction);
                SetTargetEditor(_stateSelector.contentSummary.item);
            }

            if ((bool)returnList[5]) {
                var path = EditorUtility.OpenFilePanel("Load assets", "", "asset");
                editorModels =
                    ConversionUtilities.UnpackAll(AASFileUtility.LoadUsingButton(_gameObject, enumNames, _assetName, path),
                        AnimationGraphUtil.GetAnimationTriggers(_gameObject.GetComponent<IAnimator>())).ToList();
                SetTargetEditor(_stateSelector.contentSummary.item);
            }
        }

        private bool DrawDirectionControl(Column column) {
            var wrapper = KnobAndReferenceWrapper.GetInstance(_targetState);
            bool redraw = column.PostContent(_knobRefDropdown.OnGUI, 25, false); ;
            if (_knobRefDropdown.active) {
                redraw |= column.PostContent(wrapper, false);
            }
            return redraw;
        }

        private class KnobAndReferenceWrapper : AbstractGUIWrapper<bool> {
            private static PopoutSelectorStyle _frictionSelectorStyle = PopoutSelectorStyle.BuildStyle(true, _muIconContent,
                DB.NewGUIStyle((x => x.normal.textColor = DB.NewColor(1, 1, 1)), (x => x.fontSize = 12), (x => x.fontStyle = AdapterLayer.Objects.FontStyle.Normal)));

            private static KnobAndReferenceWrapper _instance = new KnobAndReferenceWrapper();

            private AASEditorModel _editorModel;
            private PopoutSelector<ScaleType> _scaleSelector;
            private PopoutSelector<FrictionResponseType> _frictionSelector;
            private PopoutSelector<DirectionType> _directionSelector;
            private PopoutDrawer<PopoutSelectorEntry<ScaleType>> _scaleDrawer;
            private PopoutDrawer<PopoutSelectorEntry<FrictionResponseType>> _frictionDrawer;
            private PopoutDrawer<PopoutSelectorEntry<DirectionType>> _directionDrawer;

            internal static void DeliverCallback(SelectionChangeDelegate<ScaleType> callback) {
                _instance._scaleSelector.RegisterSelectionChangedHandler(callback);
            }

            private KnobAndReferenceWrapper() {
                IEnumerable<PopoutSelectorEntry<ScaleType>> syncAccelStates = ((ScaleType[])Enum.GetValues(typeof(ScaleType))).Select(x => new PopoutSelectorEntry<ScaleType>(x, x.ToString()));
                IEnumerable<PopoutSelectorEntry<FrictionResponseType>> frictionResponseTypes =
                    ((FrictionResponseType[])Enum.GetValues(typeof(FrictionResponseType))).Select(x => new PopoutSelectorEntry<FrictionResponseType>(x, x.ToString()));
                IEnumerable<PopoutSelectorEntry<DirectionType>> directionTypes = ((DirectionType[])Enum.GetValues(typeof(DirectionType))).Select(x => new PopoutSelectorEntry<DirectionType>(x, x.ToString()));

                _scaleSelector = new PopoutSelector<ScaleType>(syncAccelStates, syncAccelStates.First(), 25);
                _frictionSelector = new PopoutSelector<FrictionResponseType>(frictionResponseTypes, frictionResponseTypes.First(), 25, _frictionSelectorStyle);
                _directionSelector = new PopoutSelector<DirectionType>(directionTypes, directionTypes.First(), 25);

                _scaleDrawer = new PopoutDrawer<PopoutSelectorEntry<ScaleType>>(_scaleSelector, 25);
                _frictionDrawer = new PopoutDrawer<PopoutSelectorEntry<FrictionResponseType>>(_frictionSelector, 25);
                _directionDrawer = new PopoutDrawer<PopoutSelectorEntry<DirectionType>>(_directionSelector, 25);
            }

            internal static KnobAndReferenceWrapper GetInstance(AASEditorModel editor) {
                if (_instance._editorModel != editor) {
                    _instance._scaleSelector.SetSelection(editor.scaleType);
                    _instance._frictionSelector.SetSelection(editor.frictionResponse);
                    _instance._directionSelector.SetSelection(editor.directionType);
                }
                _instance._editorModel = editor;
                _instance.height = 70;
                return _instance;
            }

            public override ReturnedValue<bool> OnGUI(IRect rect) {
                IRect drawSpace = DB.NewRect(rect);
                drawSpace.width *= (2f / 5f);
                EditorGUI.DrawRect(drawSpace, GlobalStyleVariables.OffGray);

                IRect leftRect = DB.NewRect(drawSpace);

                // Drawing the knob control
                int knobPadding = 10;
                IRect knobSpace = DB.NewRect(leftRect.x + knobPadding, leftRect.y + knobPadding, leftRect.width - knobPadding * 2, leftRect.height - knobPadding * 2);
                var knob = _editorModel.knobControl.OnGUI(knobSpace);

                drawSpace.x += drawSpace.width;
                drawSpace.width *= 1.5f;

                IRect darkRect = DB.NewRect(drawSpace);
                IColor darkRectColor = DB.NewColor(.2f, .2f, .2f);

                // drawing the scale and friction drawers
                EditorGUI.DrawRect(darkRect, darkRectColor);
                darkRect.height /= 3;
                var initDirectionType = _editorModel.directionType;
                _editorModel.directionType = _directionDrawer.OnGUI(darkRect).value.item;

                if (_editorModel.directionType == DirectionType.AttenuatedRelative) {
                    IRect attenuationRect = DB.NewRect(darkRect.x - 25, darkRect.y, 25, darkRect.height);
                    _editorModel.attenuationRatio = float.Parse(EditorGUI.TextField(attenuationRect, _editorModel.attenuationRatio.ToString(), EditorStyles.NumberField()).ToString());

                    if (_editorModel.attenuationRatio > 1)
                        _editorModel.attenuationRatio = 1;
                    else if (_editorModel.attenuationRatio < 0)
                        _editorModel.attenuationRatio = 0;
                }

                darkRect.y += darkRect.height;
                var initScaleType = _editorModel.scaleType;
                _editorModel.scaleType = _scaleDrawer.OnGUI(darkRect).value.item;

                darkRect.y += darkRect.height;
                var initFrictionResponse = _editorModel.frictionResponse;
                _editorModel.frictionResponse = _frictionDrawer.OnGUI(darkRect).value.item;

                return knob || initFrictionResponse != _editorModel.frictionResponse || initScaleType != _editorModel.scaleType || _editorModel.directionType != initDirectionType
                    ? new ReturnedValue<bool>(knob) : ReturnedValue<bool>.noReturnValue;
            }

            public override ReturnedValue<bool> OnGUI() {
                throw new NotImplementedException();
            }
        }

        private static ITexture _deleteIcon = MetaUtilities.AssetFromGuid<ITexture>("f14901fdd641118428ec36818eb8701d");
        private Button _deleteButton = new Button(_deleteIcon, DB.NewVector2(1, 1));
        private void DrawKeys(Column column) {

            foreach (var key in _targetState.keyGroup.GetKeys()) {
                string initialT = ((int)key.time).ToString();
                string initialV = ((int)key.v).ToString();
                List<object> returnList = column.SplitHorizontalPost(new List<Func<IRect, object>>() {
                    CGF.Emptiness(),
                    CGF.Label("Key #" + key.key.position, EditorStyles.MiniLabel()),
                    CGF.Label("T:", EditorStyles.MiniLabel()),
                    CGF.TextField(initialT, _numberField),
                    CGF.Emptiness(),
                    CGF.Label("V:", EditorStyles.MiniLabel()),
                    CGF.TextField(initialV, _numberField),
                    CGF.Emptiness(),
                    _deleteButton.OnGUI},
                    new SizeTuple[] {
                        new SizeTuple(.1f, false),
                        new SizeTuple(.4f, false),
                        new SizeTuple(.05f, false),
                        new SizeTuple(.15f, false),
                        new SizeTuple(.1f, false),
                        new SizeTuple(.05f, false),
                        new SizeTuple(.15f, false),
                        new SizeTuple(.1f, false),
                        new SizeTuple(.07f, false, .8f)}
                    );
                column.Space(-4f);

                if ((ReturnedValue)returnList[8]) {
                    _targetState.keyGroup.DeleteKey(key.id);
                    Repaint();
                }
                if (!returnList[3].ToString().Equals(initialT)) {
                    key.key.time = float.Parse(returnList[3].ToString());
                    float time = key.key.time >= 0 ? key.key.time : 0;
                    _targetState.keyGroup.ManualUpdate(key.id, time, key.v);
                }
                if (!returnList[6].ToString().Equals(initialV)) {
                    key.key.v = float.Parse(returnList[6].ToString());
                    float v = key.key.v >= 0 ? key.key.v : 0;
                    _targetState.keyGroup.ManualUpdate(key.id, key.time, v);
                }
            }
        }

        private bool DrawPreviewStack(Column column) {
            return column.PostContent(_previewStack, false);
        }

        private SizeTuple[] _headerSizes = new SizeTuple[] {
            new SizeTuple(30f, true),
            new SizeTuple(10f, true),
            new SizeTuple(1, false),
            new SizeTuple(30f, true),
            new SizeTuple(.4f, false),
            new SizeTuple(.4f, false),
            new SizeTuple(10f, true)
        };

        private void StateAndAnimationSelectors(Column column) {
            Tuple<string, string> stateAndTrigger = column.PostContent(StateAndTriggerPopoutWrapper.GetWrapper(_targetState, _stateSelector), false).value;
        }

        private class StateAndTriggerPopoutWrapper : AbstractGUIWrapper<Tuple<string, string>> {
            private static readonly StateAndTriggerPopoutWrapper _instance = new StateAndTriggerPopoutWrapper();
            private PopoutDrawer<PopoutSelectorEntry<string>> _triggerDrawer;
            private PopoutDrawer<PopoutSelectorEntry<string>> _stateSelector;

            public static StateAndTriggerPopoutWrapper GetWrapper(AASEditorModel targetState, PopoutDrawer<PopoutSelectorEntry<string>> stateSelector) {
                _instance._triggerDrawer = targetState.animationTriggerSelector;
                _instance._stateSelector = stateSelector;
                return _instance;
            }

            private StateAndTriggerPopoutWrapper() : base(40, 0) { }

            public override ReturnedValue<Tuple<string, string>> OnGUI(IRect rect) {
                IRect left = DB.NewRect(rect.x, rect.y, rect.width / 2, rect.height);
                IRect right = DB.NewRect(rect.x + left.width, rect.y, left.width, rect.height);
                string state = _stateSelector.OnGUI(left).value.item;
                string trigger = _triggerDrawer.OnGUI(right).value.item;
                return new ReturnedValue<Tuple<string, string>>(new Tuple<string, string>(state, trigger));
            }

            public override ReturnedValue<Tuple<string, string>> OnGUI() {
                throw new NotImplementedException();
            }
        }

        public void EditWindow(Column column, params object[] args) {
            // Laying down the background
            IRect dp = column.drawPointer;
            IRect space = DB.NewRect(dp);
            space.height = column.height;
            EditorGUI.DrawRect(space, _keyWindow);

            // Drawing the Key handle editor
            if (_keyEditor.OnGUI(column)) {
                Repaint();
            }
            return;
        }

        public void Update() {
            if (_previewStack != null) {
                _previewStack.Update();
            }

            if (_saveTimer.ElapsedMilliseconds > _saveInterval) {
                ForceSave();
                _saveTimer.Restart();
            }

        }
    }

    public interface IAASFunctionModel {
        IAASFunctionModel DeliverGameObject(IGameObject gameObject);
        IAASFunctionModel DeliverRepaintCallback(Callback repaint);
        void SelectionWindow(Column column, params object[] args);
        void EditWindow(Column column, params object[] args);
        void Update();
        void ForceSave();
    }

    public delegate void Callback();
}

