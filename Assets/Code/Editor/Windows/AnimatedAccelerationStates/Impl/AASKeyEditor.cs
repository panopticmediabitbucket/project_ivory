﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using Assets.Code.System.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Canvas = Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities.Canvas;
using EventType = Assets.Code.Editor.AdapterLayer.Impl.Static_Classes.EventType;
using FontStyle = Assets.Code.Editor.AdapterLayer.Objects.FontStyle;
using KeyCode = Assets.Code.Editor.AdapterLayer.Impl.Static_Classes.KeyCode;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl {
    public enum KeyWindowControl {
        CanvasTab, KeyHandle, CanvasSpace, CanvasZoom, CanvasDrag, None
    }

    public class AASKeyEditor {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEvent Event => EventAdapter.Instance;
        private static IHandles Handles => HandlesAdapter.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;

        private AASEditorModel _target;
        private KeyGroup _keyGroup;
        private ControlPipeline<KeyWindowControl> _controlPipeline;

        public float currentSpacing = 5;

        public float animationLength { get; set; }

        // Shared values and keys follow
        public static Canvas _canvas;
        private CanvasTab _canvasTab;
        public CanvasScale canvasScale { get => _sharedCanvas.value; private set => _sharedCanvas.SetValue(value, _sharedCanvasKey); }
        private SharedValue<CanvasScale> _sharedCanvas;
        private Guid _sharedCanvasKey;

        public IRect canvasDim { get => _sharedCanvasDim.value; private set => _sharedCanvasDim.SetValue(value, _sharedCanvasDimKey); }
        private SharedValue<IRect> _sharedCanvasDim;
        private Guid _sharedCanvasDimKey;

        IGUIStyle xValueStyle = DB.NewGUIStyle(x => x.fontSize = 8, x => x.alignment = AdapterLayer.Objects.TextAnchor.MiddleRight);
        IGUIStyle yValueStyle = DB.NewGUIStyle(x => x.fontSize = 8, x => x.alignment = AdapterLayer.Objects.TextAnchor.MiddleCenter);

        // building the canvas
        public AASKeyEditor() {
            xValueStyle.normal.textColor = DB.NewColor(1, 1, 1); // white
            yValueStyle.normal.textColor = DB.NewColor(1, 1, 1);

            var sharedCanvasTuple = SharedValue<CanvasScale>.CreateSharedValue(new CanvasScale(.8f, 15f));
            _sharedCanvas = sharedCanvasTuple.Item1;
            _sharedCanvasKey = sharedCanvasTuple.Item2;

            var sharedCanvasDimTuple = SharedValue<IRect>.CreateSharedValue(DB.ZeroRect());
            _sharedCanvasDim = sharedCanvasDimTuple.Item1;
            _sharedCanvasDimKey = sharedCanvasDimTuple.Item2;

            _canvas = new Canvas(_sharedCanvas, _sharedCanvasDim);
        }

        public void OnEditorModelChange(AASEditorModel newEditor) {
            _keyGroup = newEditor.keyGroup;
            _target = newEditor;
            _canvasTab = new CanvasTab(_canvas, newEditor);
            _controlPipeline = BuildControlPipeline();
        }

        public void OnScaleTypeChange(ScaleType type) {
            _canvas.scaleType = type;
            switch (type) {
                case ScaleType.Regular:
                    _canvas.fillColor = DB.NewColor(.6392f, .9098f, .9686f);
                    _canvas.lineColor = DB.NewColor(0, .5725f, .8196f);
                    break;
                case ScaleType.Terminal:
                    _canvas.fillColor = DB.NewColor(.9f, .95f, .65f);
                    _canvas.lineColor = DB.NewColor(.6f, .82f, 0f);
                    break;
                case ScaleType.Stopper:
                    _canvas.fillColor = DB.NewColor(.90f, .64f, .96f);
                    _canvas.lineColor = DB.NewColor(.60f, 0f, .82f);
                    break;
                case ScaleType.FrictionBooster:
                    _canvas.fillColor = DB.NewColor(.80f, .84f, .96f);
                    _canvas.lineColor = DB.NewColor(.70f, .7f, .82f);
                    break;
            }
        }

        private ControlPipeline<KeyWindowControl> BuildControlPipeline() {
            ControlStep<KeyWindowControl> tabControl = new ControlStep<KeyWindowControl>(_canvasTab, KeyWindowControl.CanvasTab);
            KeyHandlesControl keyHandlesControl = new KeyHandlesControl(_canvas, _keyGroup);
            ControlStep<KeyWindowControl> keyHandleStep = new ControlStep<KeyWindowControl>(keyHandlesControl, KeyWindowControl.KeyHandle);
            CanvasControl canvasControl = new CanvasControl(_canvas, _keyGroup);
            ControlStep<KeyWindowControl> canvasControlStep = new ControlStep<KeyWindowControl>(canvasControl, KeyWindowControl.CanvasSpace);
            CanvasZoomControl canvasZoomControl = new CanvasZoomControl(_canvas, _sharedCanvasKey);
            ControlStep<KeyWindowControl> canvasZoomControlStep = new ControlStep<KeyWindowControl>(canvasZoomControl, KeyWindowControl.CanvasZoom);
            CanvasDrag canvasDrag = new CanvasDrag(_canvas, MouseCursor.Pan, 0);
            ControlStep<KeyWindowControl> canvasDragControlStep = new ControlStep<KeyWindowControl>(canvasDrag, KeyWindowControl.CanvasDrag);
            return new ControlPipeline<KeyWindowControl>(new List<ControlStep<KeyWindowControl>>() { tabControl, canvasDragControlStep, keyHandleStep, canvasControlStep, canvasZoomControlStep });
        }

        /*
         * Draws the Key Editor window. Returns true if a Repaint is necessary.
         */
        public bool OnGUI(Column column) {
            IRect drawRect = DB.NewRect(column.columnx, column.columny, column.width, column.height);
            canvasDim = drawRect;

            DrawMomentumArea();

            ReturnedValue<Tuple<KeyWindowControl, ReturnedValue>> controlReturn = _controlPipeline.Control();

            DrawGrid();

            KeyHandlesControl.instance.Draw();

            // Draws the red bar for the end of the animation timeline
            var animationEnd = (animationLength * canvasScale.pixelsPerMS) + _canvas.origin.x;
            Handles.color = DB.NewColor(1, 0, 0); // Red
            Handles.DrawLine(DB.NewVector2(animationEnd, 0), DB.NewVector2(animationEnd, column.height));

            _canvasTab.Draw();

            if (!controlReturn) {
                _keyGroup.RemoveDuplicates();
            }

            return controlReturn;
        }

        private static IColor _bgColor = DB.NewColor(.2f, .2f, .2f);
        private void DrawMomentumArea() {
            IVector2 origin = _canvas.origin;
            // Collect a list of keys that includes the origin
            List<IVector2> points = new List<IVector2>();
            points.Add(origin);
            foreach (var key in _keyGroup.GetKeys()) {
                CanvasScale scale = _canvas.scale;
                IVector2 position = DB.NewVector2(scale.pixelsPerMS * key.time, -(scale.pixelsPerMPS * key.v));
                IRect space = DB.NewRect(_canvas.origin.Add(position), DB.ZeroVector());
                points.Add(space.position);
            }

            // Remaining space goes arbitrarily far to the right, extending off screen
            points.Add(DB.NewVector2(10000f, points.Last().y));
            points.Add(DB.NewVector2(points.Last().x, origin.y));

            // Draw triangle for first key
            Handles.color = _bgColor.Add(_canvas.fillColor.Scale(.6f)); // transparency rating
            IVector2 startPoint = DB.NewVector2(origin);
            Handles.DrawAAConvexPolygon(startPoint, points[1], DB.NewVector2(points[1].x, startPoint.y));

            // Draw underlying area beneath subsequent keys
            for (int i = 2; i < points.Count(); i++) {
                startPoint.x = points[i - 1].x;
                Handles.DrawAAConvexPolygon(startPoint, points[i - 1], points[i], DB.NewVector2(points[i].x, startPoint.y));
            }

            // Draw bounding line
            points.Add(points.First());
            Handles.color = _canvas.lineColor;
            for (int i = 0; i < 3; i++) {
                Handles.DrawAAPolyLine(points.ToArray());
                points.ForEach(x => x = DB.NewVector2(x.x, x.y - 1));
            }
        }

        private readonly float _spacingThreshold = 65;

        public void DrawGrid() {
            IRect drawRect = _canvas.controlRect;
            IRect xAxis = DB.NewRect(drawRect.x, drawRect.height + drawRect.y, drawRect.width, drawRect.height);
            IRect yAxis = DB.NewRect(drawRect.x, drawRect.height + drawRect.y, drawRect.width, drawRect.height);
            Handles.color = DB.NewColor(1, 1, 1); // white

            // shifting line indicators in Timeline
            float currentSpacingInPixels = currentSpacing * _canvas.scale.pixelsPerMPS;
            if (currentSpacingInPixels < _spacingThreshold) {
                currentSpacing *= 2;
            } else if (currentSpacingInPixels > _spacingThreshold * 2) {
                currentSpacing /= 2;
            }

            float value = 0;
            float valueSpacing = _canvas.ConvertByMode(currentSpacing);

            for (int i = 0; i <= 50; i++) {
                // main white line
                Handles.color = DB.WhiteColor();
                Handles.DrawLine(DB.NewVector2(xAxis.x, xAxis.y), DB.NewVector2(xAxis.x + xAxis.width, xAxis.y));

                // meters per second label
                IRect labelRect = DB.NewRect(xAxis.x - 18, xAxis.y - 5, 15f, 8f);
                GUI.Label(labelRect, value.ToString(), xValueStyle);

                // sub line intervals
                xAxis.y -= (currentSpacing * canvasScale.pixelsPerMPS) / 2;
                Handles.color = DB.GrayColor();
                Handles.DrawLine(DB.NewVector2(xAxis.x, xAxis.y), DB.NewVector2(xAxis.x + xAxis.width, xAxis.y));
                value += valueSpacing;
                xAxis.y -= (currentSpacing * canvasScale.pixelsPerMPS) / 2;
            }

            value = 0f;

            for (int i = 0; i < 8; i++) {
                // main white line
                Handles.color = DB.WhiteColor();
                Handles.DrawLine(DB.NewVector2(yAxis.x, yAxis.y), DB.NewVector2(yAxis.x, yAxis.y - yAxis.height));

                // meters per second label
                IRect labelRect = DB.NewRect(yAxis.x - 4, yAxis.y + 2, 15f, 8f);
                GUI.Label(labelRect, value.ToString() + "s", yValueStyle);

                // sub line intervals
                yAxis.x += 500f * canvasScale.pixelsPerMS;
                Handles.color = DB.GrayColor();
                Handles.DrawLine(DB.NewVector2(yAxis.x, yAxis.y), DB.NewVector2(yAxis.x, yAxis.y - yAxis.height));
                value += 1f;
                yAxis.x += 500f * canvasScale.pixelsPerMS;
            }
        }

        public List<_Key> GetKeys() => _keyGroup.GetKeys();
        public Key[] GetKeyArray() => _keyGroup.GetKeyArray();
        public void DeleteKey(Guid id) => _keyGroup.DeleteKey(id);
        public void ManualUpdate(Guid id, float time, float v) => _keyGroup.ManualUpdate(id, time, v);

        private class KeyHandlesControl : IControllable, IDrawable {
            KeyGroup _keyGroup;
            List<KeyHandle> _handles;
            Canvas _canvas;

            public static KeyHandlesControl instance { get; private set; }

            public KeyHandlesControl(Canvas canvas, KeyGroup keyGroup) {
                _canvas = canvas;
                _keyGroup = keyGroup;
                _handles = keyGroup.GetKeyHandles(canvas);
                instance = this;
            }

            public ReturnedValue Control(IRect rect) {
                if (_keyGroup.dirty) {
                    _handles = _keyGroup.GetKeyHandles(_canvas);
                }

                ReturnedValue<Guid> targetKey = ReturnedValue<Guid>.noReturnValue;
                ReturnedValue<KeyHandleInteraction> interaction = ReturnedValue<KeyHandleInteraction>.noReturnValue;

                foreach (var key in _handles) {
                    interaction = key.Control();
                    if (interaction) {
                        targetKey = new ReturnedValue<Guid>(key.id);
                        break;
                    }
                }

                if (interaction.value.Equals(KeyHandleInteraction.Delete)) {
                    _keyGroup.DeleteKey(targetKey.value);
                }

                return targetKey;
            }

            public ReturnedValue Control() {
                return Control(_canvas.controlRect);
            }

            public void Draw() {
                Draw(_canvas.controlRect);
            }

            public void Draw(IRect rect) {
                if (_keyGroup.dirty) {
                    _handles = _keyGroup.GetKeyHandles(_canvas);
                }

                foreach (var key in _handles) {
                    key.Draw();
                }
            }
        }

        private class CanvasControl : IControllable {
            Canvas _canvas;
            private KeyGroup _keyGroup;

            public CanvasControl(Canvas canvas, KeyGroup keyGroup) {
                _canvas = canvas;
                _keyGroup = keyGroup;
            }

            public ReturnedValue Control() {
                return Control(_canvas.controlRect);
            }

            public ReturnedValue Control(IRect rect) {
                if (rect.Contains(Event.current.mousePosition) && Event.current.rawType.Equals(EventType.MouseDown)) {
                    var coords = Event.current.mousePosition;
                    IVector2 adjustedCoords = coords.Sub(_canvas.origin);
                    float time = adjustedCoords.x / _canvas.scale.pixelsPerMS;
                    float value = adjustedCoords.y / -_canvas.scale.pixelsPerMPS;
                    return new ReturnedValue(_keyGroup.AddActiveKeyFromCoords(time, value));
                }
                return ReturnedValue.noReturnValue;
            }
        }

        private class CanvasZoomControl : IControllable {
            private Canvas _canvas;
            private Guid _sharedCanvasKey;

            public CanvasZoomControl(Canvas canvas, Guid sharedCanvasKey) {
                _canvas = canvas;
                _sharedCanvasKey = sharedCanvasKey;
            }

            public ReturnedValue Control() {
                return Control(_canvas.controlRect);
            }

            private static float _lowerBound = 5.78f;
            private static float _upperBound = 38.88f;
            public ReturnedValue Control(IRect rect) {
                if (rect.Contains(Event.current.mousePosition) && Event.current.rawType.Equals(EventType.scrollWheel)) {
                    var coords = Event.current.mousePosition;
                    var scrollDir = Event.current.delta.y;
                    double zoomDeltaMult = 1.1;
                    double adjustedY = coords.y - (_canvas.controlRect.y + _canvas.controlRect.height);
                    double vOfMouse = adjustedY / -_canvas.scale.pixelsPerMPS;
                    double deltaV = _canvas.position - vOfMouse;
                    double deltaY = deltaV * _canvas.scale.pixelsPerMPS;

                    //if scrollDir<0, zoom in, else zoom out
                    if (scrollDir < 0) {
                        var orig = _canvas.scale;
                        CanvasScale newCanvasScale = new CanvasScale(orig.pixelsPerMS, (float)(orig.pixelsPerMPS * zoomDeltaMult));
                        _canvas.ChangeCanvasScale(newCanvasScale, _sharedCanvasKey);

                        if (_canvas.scale.pixelsPerMPS > _upperBound) {
                            newCanvasScale = new CanvasScale(orig.pixelsPerMS, _upperBound);
                            _canvas.ChangeCanvasScale(newCanvasScale, _sharedCanvasKey);
                        } else
                            _canvas.position = (float)(vOfMouse + deltaY / newCanvasScale.pixelsPerMPS);
                    } else {
                        var orig = _canvas.scale;
                        CanvasScale newCanvasScale = new CanvasScale(orig.pixelsPerMS, (float)(orig.pixelsPerMPS / zoomDeltaMult));
                        _canvas.ChangeCanvasScale(newCanvasScale, _sharedCanvasKey);

                        if (_canvas.scale.pixelsPerMPS < _lowerBound) {
                            newCanvasScale = new CanvasScale(orig.pixelsPerMS, _lowerBound);
                            _canvas.ChangeCanvasScale(newCanvasScale, _sharedCanvasKey);
                        } else {
                            _canvas.position = (float)(vOfMouse + deltaY / newCanvasScale.pixelsPerMPS);
                        }
                    }
                }
                return ReturnedValue.noReturnValue;
            }
        }

        private class CanvasDrag : Drag {
            private Canvas _canvas;
            private static IEditorGUIUtility EditorGUIUtility => EditorGUIUtilityAdapter.Instance;

            public CanvasDrag(Canvas canvas, MouseCursor cursor, int pixelPadding) : base(canvas.controlRect, cursor, pixelPadding) {
                _canvas = canvas;
                _cursor = cursor;
                _pixelPadding = pixelPadding;
            }
            public override ReturnedValue Control() {
                return Control(_canvas.controlRect);
            }

            public bool spaceDown = false;
            public override ReturnedValue Control(IRect controlRect) {
                // control needs to be freed for the keyboard to receive keyboard events
                if (controlRect.Contains(Event.current.mousePosition)) {
                    UnityEngine.GUI.FocusControl(null);
                }

                if (Event.current.rawType.Equals(EventType.KeyDown) && Event.current.keyCode == KeyCode.Space) {
                    spaceDown = true;
                } else if (Event.current.rawType.Equals(EventType.keyUp) && Event.current.keyCode == KeyCode.Space) {
                    spaceDown = false;
                } else if (spaceDown) {
                    return base.Control(controlRect);
                }
                return ReturnedValue.noReturnValue;
            }

            protected override void OnHorizontalChange(float x) {
                //throw new NotImplementedException();
            }

            protected override void OnVerticalChange(float y) {
                float maxPosition, minPosition;
                _canvas.position += y / _canvas.scale.pixelsPerMPS;

                var height = (_canvas.window.height - Canvas.yMargin) / _canvas.scale.pixelsPerMPS;
                minPosition = Canvas.minV + height / 2;
                maxPosition = Canvas.maxV - (height / 2);
                _canvas.position = Math.Max(_canvas.position, minPosition);
                _canvas.position = Math.Min(_canvas.position, maxPosition);
            }
        }

        private class CanvasTab : AbstractGUIElement {

            private Canvas _canvas;
            private InterruptScroller _interrupt;
            private AASEditorModel _editorModel;

            public CanvasTab(Canvas canvas, AASEditorModel editorModel) {
                _canvas = canvas;
                _interrupt = new InterruptScroller(canvas, editorModel);
                _editorModel = editorModel;
            }

            private IRect GetTabSpace(IRect canvasWindowRect) {
                IRect drawRect = DB.NewRect(canvasWindowRect);
                drawRect.height = 30;
                return drawRect;
            }


            private IRect GetInterruptSpace(IRect totalSpace, IRect tabSpace) {
                var rect = DB.NewRect(totalSpace.x, totalSpace.y + tabSpace.height, tabSpace.width, totalSpace.height - tabSpace.height);
                rect.x += 30;
                return rect;
            }

            public override ReturnedValue Control(IRect rect) {
                IRect interruptRect = GetInterruptSpace(rect, GetTabSpace(rect));
                if (_interrupt.Control(interruptRect)) {
                    return new ReturnedValue(true);
                }
                return ReturnedValue.noReturnValue;
            }

            public override ReturnedValue Control() {
                return Control(_canvas.window);
            }

            public override void Draw() {
                Draw(_canvas.window);
                _interrupt.Draw(GetInterruptSpace(_canvas.window, GetTabSpace(_canvas.window)));
            }

            IGUIStyle _valueStyle = DB.NewGUIStyle(x => x.fontSize = 8, x => x.fontStyle = FontStyle.Bold);
            IGUIStyle _scaleStyle = DB.NewGUIStyle(x => x.fontSize = 10);

            public override void Draw(IRect rect) {
                IRect tabRect = GetTabSpace(rect);
                EditorGUI.DrawRect(tabRect, DB.NewColor(.6f, .6f, .6f));
                IRect valueLabRect = DB.NewRect(tabRect.x, tabRect.y, tabRect.height, tabRect.height);
                DrawValueTypeLabel(valueLabRect);
                IRect scaleTypeRect = DB.NewRect(valueLabRect.x + valueLabRect.width, valueLabRect.y, valueLabRect.width * 2, valueLabRect.height);
                DrawScaleTypeLabel(scaleTypeRect);
                IRect interruptTimeRect = DB.NewRect(scaleTypeRect.x + 70, scaleTypeRect.y, scaleTypeRect.width, scaleTypeRect.height);
                DrawInterruptTime(interruptTimeRect);
            }

            private void DrawScaleTypeLabel(IRect scaleTypeRect) {
                scaleTypeRect = DB.NewRect(scaleTypeRect);
                scaleTypeRect.x += 5; scaleTypeRect.y += 8;
                GUI.Label(scaleTypeRect, _canvas.scaleType.ToString(), _scaleStyle);
            }

            private void DrawValueTypeLabel(IRect labelRect) {
                labelRect = DB.NewRect(labelRect);
                GUIFunctions.DrawBorderedRect(labelRect, _canvas.lineColor, _canvas.fillColor, 2);
                labelRect.x += 5; labelRect.y += 10;
                GUI.Label(labelRect, GetLabelForScaleType(_canvas.scaleType), _valueStyle);
            }

            private void DrawInterruptTime(IRect rect) {
                IRect squareRect = DB.NewRect(rect.x, rect.y + 10, 10, 10);
                EditorGUI.DrawRect(squareRect, DB.NewColor(.8f, .9f, .8f));
                IRect labelrect = DB.NewRect(rect.x + 20, rect.y + 8, rect.width, rect.height - 10);
                GUI.Label(labelrect, GetLabelForInterruptTime(), _scaleStyle);
            }

            private string GetLabelForInterruptTime() {
                return "Interruptable time = " + _editorModel.interruptTime.ToString() + " ms";
            }

            private string GetLabelForScaleType(ScaleType type) {
                switch (type) {
                    case ScaleType.Regular:
                    case ScaleType.Terminal:
                        return "m/s";
                    default:
                        return "  %";
                }
            }
        }

        private class InterruptScroller : Drag, IDrawable {
            private Canvas _canvas;
            private AASEditorModel _model;

            public InterruptScroller(Canvas canvas, AASEditorModel model) : base(DB.ZeroRect(), 1, MouseCursor.MoveArrow, true) {
                _canvas = canvas;
                _model = model;
            }

            protected override void OnHorizontalChange(float x) {
                int pastOrigin = (int)(x * _canvas.scale.pixelsPerMS) + (int)Event.current.mousePosition.x;
                if (pastOrigin < _canvas.origin.x) {
                    x -= _canvas.origin.x - pastOrigin;
                }

                float msChanged = x / _canvas.scale.pixelsPerMS;
                if (_model.interruptTime + x < 0) {
                    _model.interruptTime = 0;
                } else if (_model.interruptTime > 1000) {
                    _model.interruptTime = 1000;
                } else {
                    _model.interruptTime += msChanged;
                }
            }

            protected override void OnVerticalChange(float y) { }

            public void Draw() { }

            public override ReturnedValue Control() {
                return base.Control();
            }

            public override ReturnedValue Control(IRect rect) {
                rect = GetHandleLocation(rect);
                rect.height = rect.width;
                return base.Control(rect);
            }

            public IRect GetHandleLocation(IRect rect) {
                rect = DB.NewRect(rect.x + _model.interruptTime * _canvas.scale.pixelsPerMS, rect.y, 10, rect.height);
                rect.x -= rect.width / 2;
                return rect;
            }

            public void Draw(IRect rect) {
                rect = GetHandleLocation(rect);

                IRect handleRect = DB.NewRect(rect.x, rect.y, rect.width, rect.width);
                EditorGUI.DrawRect(handleRect, DB.NewColor(.8f, .9f, .8f));
                IVector2 top = DB.NewVector2(rect.x + rect.width / 2, rect.y);
                IVector2 bottom = DB.NewVector2(top.x, top.y + rect.height);
                Handles.color = DB.NewColor(.8f, .9f, .8f);
                Handles.DrawLine(top, bottom);
            }
        }
    }
}
