﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities.Time;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.CustomGUI;
using Assets.Code.System.TimeUtil;
using UnityEditor;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl {
    public partial class AASPreviewStack<TEnum> : AbstractGUIWrapper<bool> where TEnum : Enum {
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;
        private static IEditorStyles EditorStyles => EditorStylesAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IAnimationMode AnimationMode => AnimationModeAdapter.Instance;
        private static IHandles Handles => HandlesAdapter.Instance;

        // private static for child classes
        private static IAnimator _animator;

        // 'cache' data
        private List<TriggerMotionMatches> _motionMatches;
        private List<string> _animationTriggers;

        // instance data
        private PreviewTimer _previewTimer;
        private IGameObject _gameObject;
        private Tuple<IVector3, IQuaternion> _captureTransform;
        private IStateBasedMotionMachine<TEnum> _previewMotionMachine;
        private PreviewStackSampler _previewStackSampler;
        float _speed;
        string _animTrigger;

        KnobControl _initialDirectionKnob;
        float _initialMomentum;
        bool _gravity;
        float _gravityVal;

        // sub GUI elements/wrappers 
        PreviewStackEntry _stackEntry;
        PreviewStackMainState _stackMain;
        PreviewStackOutro _stackOutro;
        ToggleButton _playButton;
        ToggleButton _loopButton;

        private AASEditorModel _targetState;
        public AASEditorModel targetState {
            get => _targetState;
            set {
                if (_previewTimer.active) {
                    StopAnimation();
                }
                _targetState = value;
                _animTrigger = _targetState.animationTriggerSelector.contentSummary.item;

                _stackEntry = new PreviewStackEntry(_motionMatches.Find(x => x.trigger.Equals(_targetState.animationTriggerSelector.contentSummary.item)), 22);
                _stackMain = _stackEntry.GetSubsequentStep();
                _stackOutro = _stackMain.GetSubsequentStep();

                _initialDirectionKnob = new KnobControl(DB.NewVector2(1, 0));
                _initialMomentum = 0;
            }
        }

        internal bool _looping { get; set; }

        public AASPreviewStack(IGameObject gameObject) : base(200, 400) {
            _gameObject = gameObject;
            _animator = _gameObject.GetComponent<IAnimator>();

            _previewTimer = new PreviewTimer();
            _previewMotionMachine = DB.NewStateBasedMotionMachine<TEnum>(_gameObject);
            _previewMotionMachine.timeZone = new PreviewTimeZone();

            // Retrieve animation data and store in list 
            _motionMatches = AnimationGraphUtil.FindAnimationPairsByTrigger(_animator);
            _animationTriggers = AnimationGraphUtil.GetAnimationTriggers(_animator);

            _playButton = new ToggleButton(MetaUtilities.AssetFromGuid<ITexture>("3107459bdc4f31b479479cace0eb22cf"), // pressed
                MetaUtilities.AssetFromGuid<ITexture>("62d49951aecb91f43b3b38354891851c"), // not pressed
                DB.NewVector2(200, 100));
            _loopButton = new ToggleButton(MetaUtilities.AssetFromGuid<ITexture>("2bde81c2fa42eab44a395cb4c1fc95e4"), // pressed
               MetaUtilities.AssetFromGuid<ITexture>("b069d2e7b06396a4697f08bb1c3aecc4"), // not pressed
               DB.NewVector2(200, 100));
            _speed = 1;

            _initialDirectionKnob = new KnobControl(DB.NewVector2(1, 0));
            _initialMomentum = 0;
        }

        IColor _previewStackBG = GlobalStyleVariables.MidGrayEditor;
        public override ReturnedValue<bool> OnGUI(IRect rect) {
            // check if animation trigger has changed
            if (!_targetState.animationTriggerSelector.contentSummary.name.Equals(_animTrigger)) {
                // retargets animation triggers
                targetState = targetState;
            }

            GUIFunctions.DrawBorderedRect(rect, GUIFunctions.DarkenColor(_previewStackBG), _previewStackBG, 2);
            IRect buttonRect = DB.NewRect(rect.x + 10, rect.y + 10, rect.width - 20, 30);
            bool redraw = PlayLoopSpeed(buttonRect, out bool start, out bool stop);

            IRect previewStackStepRect = DB.NewRect(buttonRect.x + 20, buttonRect.y + buttonRect.height + 10, buttonRect.width - 40, 22);
            redraw |= ControlPreviewStackSteps(previewStackStepRect);

            IRect initialMomentumControlRect = DB.NewRect(buttonRect.x, previewStackStepRect.y + previewStackStepRect.height + 20, buttonRect.width, buttonRect.height);
            redraw |= InitialMomentumAndGravityControl(initialMomentumControlRect);

            if (stop) {
                StopAnimation();
            }
            if (start) {
                StartAnimation();
            }

            return redraw ? new ReturnedValue<bool>(_previewTimer.active) : ReturnedValue<bool>.noReturnValue;
        }

        IGUIStyle _headerStyle = DB.NewGUIStyle(x => x.fontStyle = FontStyle.Bold, x => x.fontSize = 12);
        private bool InitialMomentumAndGravityControl(IRect rect) {
            rect = DB.NewRect(rect);
            GUI.Label(rect, "Initial Motion", _headerStyle);
            rect.y += 18;
            Handles.color = GUIFunctions.LightenColor(GlobalStyleVariables.MidGrayEditor);
            Handles.DrawDottedLine(DB.NewVector2(rect.x, rect.y), DB.NewVector2(rect.x + rect.width, rect.y), .1f);
            rect.y += 5;

            IRect knobRect = DB.NewRect(rect.x, rect.y, rect.width / 3, rect.height);
            var knobReturn = _initialDirectionKnob.OnGUI(knobRect);

            IRect velRect = DB.NewRect(rect.x + knobRect.width, rect.y + 3, knobRect.width / 3, knobRect.height - 7);
            var initMoment = _initialMomentum;
            _initialMomentum = float.Parse((string)EditorGUI.TextField(velRect, _initialMomentum.ToString(), EditorStyles.NumberField()));
            IRect velLabelRect = DB.NewRect(velRect.x + velRect.width + 5, velRect.y + 3, velRect.width, velRect.height);
            GUI.Label(velLabelRect, "m/s", ColumnGUIFunctions.subHeaderStyle);

            IRect toggleRect = DB.NewRect(velRect.x + knobRect.width, knobRect.y + 3, knobRect.width, 20);
            var initGrav = _gravity;
            _gravity = (bool)EditorGUI.ToggleLeft(toggleRect, "Gravity", _gravity);
            return knobReturn || initMoment != _initialMomentum || _gravity != initGrav;
        }

        private bool ControlPreviewStackSteps(IRect previewStackStepRect) {
            // changes in the entry need to be reflected down the chain
            StatePair changedState = _stackEntry.OnGUI(previewStackStepRect).value;
            bool changed = changedState.B != null && !changedState.B.Equals(_stackMain.state);
            if (changed) {
                _stackMain = _stackEntry.GetSubsequentStep();
                _stackOutro = _stackMain.GetSubsequentStep();
            }

            previewStackStepRect.y += previewStackStepRect.height;
            _stackMain.OnGUI(previewStackStepRect);
            previewStackStepRect.y += previewStackStepRect.height;
            var entryState = _stackOutro.targetState;
            _stackOutro.OnGUI(previewStackStepRect);
            return changed || _stackOutro.targetState != entryState;
        }

        private bool PlayLoopSpeed(IRect rect, out bool start, out bool stop) {
            var play = _playButton.OnGUI(rect);
            IRect loop = DB.NewRect(rect.x + 70, rect.y, rect.width, rect.height);
            bool initLoop = _looping;
            _looping = _loopButton.OnGUI(loop);
            start = play && !_previewTimer.active;
            stop = !play && _previewTimer.active;

            IRect slider = DB.NewRect(loop);
            slider.x += 70;
            slider.height = 20;
            slider.y += 5;
            slider.width = rect.width - 140;
            var startSpeed = _speed;
            _speed = EditorGUI.Slider(slider, _speed, .1f, 1);
            return start || stop || startSpeed != _speed || initLoop != _looping;
        }

        public override ReturnedValue<bool> OnGUI() {
            return OnGUI(DB.NewRect(0, 0, 0, 5));
        }

        // Preview animation & playback slider
        public void Update() {
            if (_previewTimer.active) {
                bool notDone = _previewStackSampler.RunSampler(_gameObject, _previewMotionMachine, _speed);
                if (!notDone) {
                    if (_looping) {
                        RestoreTransform(_gameObject);
                        StartAnimation();
                        Update();
                    } else {
                        StopAnimation();
                    }
                }
            }
        }

        private void StartAnimation() {
            _captureTransform = new Tuple<IVector3, IQuaternion>(_gameObject.transform.position, _gameObject.transform.rotation);

            Accel<TEnum> targetAccel = new AccelBuilder<TEnum>(ConversionUtilities.EditorModelToAssetModel(targetState), _previewMotionMachine).Build();
            List<IStepSampler> steps = new List<IStepSampler>() { _stackEntry, _stackMain, _stackOutro };
            IVector2 initialMomentum = _initialDirectionKnob.vector.Mul(_initialMomentum);
            _previewMotionMachine.ResetMomentum();
            _previewMotionMachine.SetInternalMomentum(initialMomentum.x, initialMomentum.y);
            _previewMotionMachine.InterruptExternal(External.Gravity);
            _previewStackSampler = new PreviewStackSampler(steps, targetAccel, _previewTimer, _gravity);

            AnimationMode.StartAnimationMode();
            _previewTimer.Reset();
            _previewTimer.Start();
            AnimationMode.BeginSampling();
        }

        private void StopAnimation() {
            RestoreTransform(_gameObject);

            _previewMotionMachine.FlagInternalForInterrupt(ConversionUtilities.GetStateFromString<TEnum>(targetState.name));
            _previewMotionMachine.InterruptExternal(External.Gravity);

            _previewTimer.Reset();
            AnimationMode.EndSampling();
            AnimationMode.StopAnimationMode();
            _playButton.Deactivate();
        }

        private void RestoreTransform(IGameObject gameObject) {
            gameObject.transform.position = _captureTransform.Item1;
            gameObject.transform.rotation = _captureTransform.Item2;
            _previewMotionMachine.ResetMomentum();
        }
    }
}
