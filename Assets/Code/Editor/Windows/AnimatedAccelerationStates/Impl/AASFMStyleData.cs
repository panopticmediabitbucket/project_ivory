﻿using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl {
    public sealed partial class AASFunctionModel<TEnum> : IAASFunctionModel where TEnum : Enum {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;
        private static IAssetPreview AssetPreview => AssetPreviewAdapter.Instance;
        private static IEditorStyles EditorStyles => EditorStylesAdapter.Instance;
        private static IEditorUtility EditorUtility => EditorUtilityAdapter.Instance;

        private readonly IColor _keyWindow = DB.NewColor(.2f, .2f, .2f);
        private readonly IColor _headerColor = GlobalStyleVariables.OffGray;
        private static IGUIContent _stateIconContent = DB.NewGUIContent(MetaUtilities.AssetFromGuid<ITexture>("99461741176fbfe499a3b4ffa5032d39"), "Selected state: the programmed motion will occur when state is activated");
        private static IGUIContent _muIconContent = DB.NewGUIContent(MetaUtilities.AssetFromGuid<ITexture>("817fad6e777d4f94ca87567b7d3d2089"), "Friction response type: does the motion scale in response to friction");
        private ITexture _gameObjectIcon;

        private IGUIStyle _numberField;
    }
}
