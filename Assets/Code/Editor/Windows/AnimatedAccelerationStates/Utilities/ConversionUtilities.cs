﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using System;
using System.Linq;
using System.Collections.Generic;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.GameCode.System.CodeGeneration;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities {
    /*
     * This class handles conversion between the Asset models used to save a representation
     * of the Animated Acceleration States in the file system and the EditorModels that contain
     * and manage all relevant data and the classes needed to interact with those data
     * (i.e. the knob controls, keyeditors, keygroups, etc.)
     */
    public class ConversionUtilities {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        // Not an instantiable object, these are all stateless utilities
        private ConversionUtilities() { }

        private static IGUIContent _animationIconContent = DB.NewGUIContent(MetaUtilities.AssetFromGuid<ITexture>("4cd0c319485a62c4b9f9f8e3db905ec5"),
            "Sets the animation trigger to be flipped when the state is activated");
        private static PopoutSelectorStyle _triggerSelectorStyle = PopoutSelectorStyle.BuildStyle(true, _animationIconContent,
            DB.NewGUIStyle((x => x.normal.textColor = DB.NewColor(1, 1, 1)), (x => x.fontSize = 14), (x => x.fontStyle = AdapterLayer.Objects.FontStyle.Normal)));

        public static IEnumerable<AASEditorModel> UnpackAll(List<AccelAsset> models, List<string> triggers) {
            return models.Select(x => AssetModelToEditorModel(x, triggers));
        }

        public static AASEditorModel AssetModelToEditorModel(AccelAsset model, List<string> triggers) {
            AASEditorModel editor = new AASEditorModel();
            editor.name = model.name;
            editor.pairedAnimation = model.pairedAnimation;
            editor.interruptTime = model.interruptTime;

            bool parsed = Enum.TryParse(model.scaleType, out ScaleType parsedType);
            editor.scaleType = parsed ? parsedType : ScaleType.Regular;

            List<PopoutSelectorEntry<string>> triggerEntries = new List<PopoutSelectorEntry<string>>() { new PopoutSelectorEntry<string>("", "None") };
            triggerEntries = triggerEntries.Concat(triggers.Select(x => new PopoutSelectorEntry<string>(x, x))).ToList();

            parsed = Enum.TryParse(model.frictionResponse, out FrictionResponseType responseType);
            editor.frictionResponse = parsed ? responseType : FrictionResponseType.None;

            parsed = Enum.TryParse(model.directionType, out DirectionType directionType);
            editor.directionType = parsed ? directionType : DirectionType.Relative;

            editor.attenuationRatio = model.directionAttenuation;

            PopoutSelectorEntry<string> match = triggerEntries.Find(x => x.item.Equals(model.animationTrigger));
            match = match.name == null ? triggerEntries.Find(x => x.Equals(model.name)) : match;
            match = match.name == null ? triggerEntries.First() : match;
            editor.animationTriggerSelector = new PopoutDrawer<PopoutSelectorEntry<string>>(new PopoutSelector<string>(triggerEntries, match, 30, _triggerSelectorStyle));

            // zero vectors become i-hat vectors
            IVector2 direction = DB.NewVector2(model.xDirection, model.yDirection).Equals(DB.ZeroVector())
                ? DB.NewVector2(1, 0) : DB.NewVector2(model.xDirection, model.yDirection);

            editor.knobControl = new KnobControl(direction);
            editor.keyGroup = new KeyGroup(model.keys);

            editor.interruptSchema = model.interruptConditions;
            return editor;
        }

        public static IEnumerable<AccelAsset> PackageAll(List<AASEditorModel> lst) {
            return lst.Select(x => EditorModelToAssetModel(x));
        }

        public static AccelAsset EditorModelToAssetModel(AASEditorModel editorModel) {
            AccelAsset assetModel = new AccelAsset();
            assetModel.keys = editorModel.keyGroup.GetKeyArray();
            assetModel.name = editorModel.name;
            assetModel.pairedAnimation = editorModel.pairedAnimation;
            assetModel.animationTrigger = editorModel.animationTriggerSelector.contentSummary.name;
            assetModel.xDirection = editorModel.knobControl.vector.x;
            assetModel.yDirection = editorModel.knobControl.vector.y;
            assetModel.interruptTime = editorModel.interruptTime;
            assetModel.scaleType = editorModel.scaleType.ToString();
            assetModel.frictionResponse = editorModel.frictionResponse.ToString();
            assetModel.directionType = editorModel.directionType.ToString();
            assetModel.interruptConditions = editorModel.interruptSchema;
            assetModel.directionAttenuation = editorModel.attenuationRatio;
            return assetModel;
        }

        public static TEnum GetStateFromString<TEnum>(string state) where TEnum : Enum {
            TEnum[] values = (TEnum[])Enum.GetValues(typeof(TEnum));
            return values.ToList().Find(x => x.ToString().Equals(state));
        }

        public static void PopulateSchemataWithMethodSchemata(IEnumerable<AccelAsset> assets) {
            foreach (var model in assets) {
                ConditionFactory.GetMethodSchemaForAll(model.interruptConditions);
            }
        }
    }
}
