﻿
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Physics.Accelerators;
using Assets.Code.System.Utilities;
using System;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities {
    public class Canvas {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        public static readonly int maxV = 60;
        public static readonly int minV = 0;
        public static readonly int maxT = 2000;
        public static readonly int minT = 0;

        public static readonly int yMargin = 25;
        public static readonly int xMargin = 30;

        public IColor fillColor { get; set; }
        public IColor lineColor { get; set; }

        // position is the m/s value that the window is centered on
        public float position { get; set; }

        public readonly SharedValue<CanvasScale> sharedScale;
        public CanvasScale scale => sharedScale.value;

        private readonly SharedValue<IRect> _windowDim;
        public float ConvertByMode(float currentSpacing) {
            switch (scaleType) {
                default:
                case ScaleType.Regular:
                case ScaleType.Terminal:
                    return currentSpacing;
                case ScaleType.Stopper:
                case ScaleType.FrictionBooster:
                    return currentSpacing * (5);
            }
        }

        public IRect window => _windowDim.value;
        public ScaleType scaleType { get; set; }

        public Canvas(SharedValue<CanvasScale> scale, SharedValue<IRect> windowDim) {
            position = 0;
            this.sharedScale = scale;
            sharedScale.AddCallback(RecalculateControlRectAndOrigin);
            _windowDim = windowDim;
            _windowDim.AddCallback(RecalculateControlRectAndOrigin);
            RecalculateControlRectAndOrigin(window);
            scaleType = ScaleType.Regular;
        }

        private void RecalculateControlRectAndOrigin(IRect newWindowDimensions) {
            controlRect = CalculateControlRect(newWindowDimensions);
            origin = DB.NewVector2(controlRect.x, controlRect.y + maxV * scale.pixelsPerMPS);
        }

        private void RecalculateControlRectAndOrigin(CanvasScale newScale) {
            RecalculateControlRectAndOrigin(window);
        }

        private IRect CalculateControlRect(IRect windowDimensions) {
            float screenSpaceMax = maxV * scale.pixelsPerMPS, max = maxV, min = 0;

            IRect rect = DB.NewRect(windowDimensions.x + xMargin, 0, maxT * scale.pixelsPerMS, screenSpaceMax);
            float bottom = screenSpaceMax - ((max - position) * scale.pixelsPerMPS - window.height / 2);
            bottom = Math.Max(bottom, window.height - yMargin);
            float top = Math.Min(-(screenSpaceMax - bottom), yMargin);
            position = ClarifyPosition(bottom, top);

            // convert back to flipped screen-space coordinates
            rect.y = top;

            return rect;
        }

        // Set the value of the position to actually equal the m/s in the centre of the window
        private float ClarifyPosition(float bottom, float top) {
            if (top == yMargin) {
                return maxV - (window.height - 25) / 2 / scale.pixelsPerMPS;
            } else if (bottom == window.height - yMargin) {
                return (window.height - 25) / 2 / scale.pixelsPerMPS;
            }
            return position;
        }

        public IRect controlRect { get; private set; }
        public IVector2 origin { get; private set; }

        public void ChangeCanvasScale(CanvasScale newScale, Guid key) {
            sharedScale.SetValue(newScale, key);
        }
    }
}
