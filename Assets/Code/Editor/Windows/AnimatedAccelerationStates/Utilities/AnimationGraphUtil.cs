﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEditorInternal;
using AnimatorControllerParameterType = Assets.Code.Editor.AdapterLayer.Objects.AnimatorControllerParameterType;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities {
    public class AnimationGraphUtil {

        public static List<string> GetAnimationTriggers(IAnimator animator) {
            var controller = animator.runtimeAnimatorController;
            return controller.parameters.ToList().FindAll(x => x.type.Equals(AnimatorControllerParameterType.Trigger)).Select(x => x.name).ToList();
        }

        public static List<TriggerMotionMatches> FindAnimationPairsByTrigger(IAnimator animator) {
            IRuntimeAnimatorController controller = animator.runtimeAnimatorController;
            List<string> triggers = controller.parameters.ToList()
                .FindAll(param => param.type.Equals(AnimatorControllerParameterType.Trigger))
                .Select(param => param.name).ToList();

            List<TriggerMotionMatches> retList = new List<TriggerMotionMatches>();
            GraphCrawler graphCrawler = new GraphCrawler(controller);
            foreach (var trigger in triggers) {
                retList.Add(new TriggerMotionMatches(graphCrawler.GetAnimationPairsByTrigger(trigger), trigger));
            }
            retList.Add(new TriggerMotionMatches(new List<StatePair>(), ""));
            return retList;
        }

        public static List<StatePair> GetTerminalOutroStates(IRuntimeAnimatorController controller, IAnimatorState state) {
            GraphCrawler gc = new GraphCrawler(controller);
            IAnimatorStateMachine stateMachine = gc.GetStateOwnerStateMachine(state);
            return gc.TraverseState(state, gc.GetParentStatemachine(stateMachine), stateMachine, null);
        }

        private class GraphCrawler {
            private readonly Dictionary<int, object> _visitations = new Dictionary<int, object>();
            private readonly IRuntimeAnimatorController _animatorController;

            internal GraphCrawler() { }

            internal GraphCrawler(IRuntimeAnimatorController animatorController) {
                _animatorController = animatorController;
            }

            internal IAnimatorStateMachine GetParentStatemachine(IAnimatorStateMachine child) {
                return child == null ? null : GetParentStatemachine(_animatorController.layers[0].stateMachine, child);
            }

            private IAnimatorStateMachine GetParentStatemachine(IAnimatorStateMachine parent, IAnimatorStateMachine child) {
                foreach (var machine in parent.stateMachines) {
                    if (machine.name.Equals(child.name)) {
                        return parent;
                    }
                    var recursion = GetParentStatemachine(machine, child);
                    if (recursion != null) {
                        return recursion;
                    }
                }
                return null;
            }

            internal IAnimatorStateMachine GetStateOwnerStateMachine(IAnimatorState state) {
                return state == null ? null : GetStateOwnerStateMachine(_animatorController.layers[0].stateMachine, state);
            }

            private IAnimatorStateMachine GetStateOwnerStateMachine(IAnimatorStateMachine parent, IAnimatorState state) {
                foreach(var child in parent.states) {
                    if (child.name.Equals(state.name)) {
                        return parent;
                    }
                }

                foreach(var machine in parent.stateMachines) {
                    if(GetStateOwnerStateMachine(machine, state) != null) {
                        return machine;
                    }
                }

                return null;
            }

            internal List<StatePair> GetAnimationPairsByTrigger(string trigger) {
                _visitations.Clear();
                var layers = _animatorController.layers.ToList();

                List<StatePair> retList = new List<StatePair>();
                foreach (var layer in layers) {
                    retList = retList.Concat(TraverseLayer(layer, trigger)).ToList();
                }

                return retList;
            }

            private List<StatePair> TraverseLayer(IAnimatorControllerLayer layer, string trigger) {
                return TraverseStateMachine(null, layer.stateMachine, trigger);
            }

            private List<StatePair> TraverseStateMachine(IAnimatorStateMachine parent, IAnimatorStateMachine targetMachine, string trigger) {
                List<StatePair> retList = new List<StatePair>();
                if (!_visitations.ContainsKey(targetMachine.GetHashCode())) {
                    _visitations.Add(targetMachine.GetHashCode(), targetMachine);

                    int i = 0;
                    foreach (var machine in targetMachine.stateMachines) {
                        retList = retList.Concat(TraverseStateMachine(targetMachine, machine, trigger)).ToList();
                    }

                    foreach (var state in targetMachine.states) {
                        List<StatePair> stateList = TraverseState(state, parent, targetMachine, trigger);
                        retList = retList.Concat(stateList).ToList();
                    }
                }
                return retList;
            }

            internal List<StatePair> TraverseState(IAnimatorState state, IAnimatorStateMachine parentMachine, IAnimatorStateMachine ownerMachine, string trigger) {
                List<StatePair> pairs = new List<StatePair>();

                if (!_visitations.ContainsKey(state.GetHashCode())) {
                    _visitations.Add(state.GetHashCode(), state);

                    foreach (var transition in state.transitions) {

                        if (trigger == null || transition.conditions.ToList().Find(x => trigger.Equals(x.parameter)) != null) {
                            if (transition.destinationState == null) {
                                StatePair incompletePair = new StatePair(state);
                                if (transition.isExit() && parentMachine != null) {
                                    pairs = pairs.Concat(FindTransitionEndPointBetweenStateMachines(parentMachine, ownerMachine, incompletePair, trigger)).ToList();
                                } else {
                                    pairs = pairs.Concat(FindStateMachineTransitionEndPoint(transition.destinationStateMachine, incompletePair, trigger)).ToList();
                                }
                            } else {
                                pairs.Add(new StatePair(state, transition.destinationState));
                            }
                        }
                    }
                }

                return pairs;
            }

            private List<StatePair> FindTransitionEndPointBetweenStateMachines(IAnimatorStateMachine parentMachine, IAnimatorStateMachine childMachine, StatePair incompletePair, string trigger) {
                List<StatePair> retList = new List<StatePair>();
                List<IAnimatorTransitionBase> stateMachineTransitions = parentMachine.GetStateMachineTransitions(childMachine);
                foreach (var transition in stateMachineTransitions) {
                    bool validTransition = trigger == null || transition.conditions.ToList().Find(y => y.parameter.Equals(trigger)) != null;
                    if (validTransition) {
                        if (transition.destinationState == null) {
                            if (transition.isExit()) {
                                retList = retList.Concat(FindTransitionEndPointBetweenStateMachines(GetParentStatemachine(parentMachine), parentMachine, incompletePair, trigger)).ToList();
                            } else {
                                retList = retList.Concat(FindStateMachineTransitionEndPoint(transition.destinationStateMachine, incompletePair, trigger)).ToList();
                            }
                        } else {
                            retList.Add(new StatePair(incompletePair.A, transition.destinationState));
                        }
                    }
                }

                return retList;
            }

            private List<StatePair> FindStateMachineTransitionEndPoint(IAnimatorStateMachine stateMachine, StatePair incompletePair, string trigger) {
                List<StatePair> retList = new List<StatePair>();

                List<IAnimatorTransitionBase> entryTransitions = stateMachine.entryTransitions().ToList();
                foreach (var transition in entryTransitions) {
                    bool validTransition = trigger == null || transition.conditions.ToList().Find(y => y.parameter.Equals(trigger)) != null;
                    if (validTransition) {
                        if (transition.destinationState == null) {
                            retList = retList.Concat(FindStateMachineTransitionEndPoint(transition.destinationStateMachine, incompletePair, trigger)).ToList();
                        } else {
                            retList.Add(new StatePair(incompletePair.A, transition.destinationState));
                        }
                    }
                }

                if ((retList.Count == 0 || trigger == null) && stateMachine.defaultState != null) {
                    retList.Add(new StatePair(incompletePair.A, stateMachine.defaultState));
                }

                return retList;
            }

        }
    }

    public struct TriggerMotionMatches {
        public readonly IEnumerable<StatePair> statePairs;
        public readonly string trigger;

        public TriggerMotionMatches(IEnumerable<StatePair> statePairs, string trigger) {
            this.statePairs = statePairs;
            this.trigger = trigger;
        }
    }

    public struct StatePair {
        public readonly IAnimatorState A;
        public readonly IAnimatorState B;

        public StatePair(IAnimatorState a, IAnimatorState b) {
            A = a;
            B = b;
        }

        public StatePair(IAnimatorState a) {
            A = a;
            B = null;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(StatePair)) {
                return false;
            }

            StatePair other = (StatePair)obj;
            return A == other.A && B == other.B;
        }

        public bool incomplete => B == null;
    }
}
