﻿namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities {
    public struct CanvasScale {
        public readonly float pixelsPerMPS; // for the y scale - m/s/pixel
        public readonly float pixelsPerMS;  // for the x scale - milliseconds/pixel

        public CanvasScale(float pixelsPerMS, float pixelsPerMPS) {
            this.pixelsPerMS = pixelsPerMS;
            this.pixelsPerMPS = pixelsPerMPS;
        }
    }
}
