﻿using System;
using Assets.Code.System.TimeUtil;
using Assets.Code.System.Utilities;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities.Time {
    public class PreviewTimeZone : ITimeZone {

        private readonly Guid _id = Guid.NewGuid();

        public Guid id => _id;

        public float deltaTime { get => underlyingTimer.deltaTime;  }

        public float timeScale => 1f;

        public PreviewTimer underlyingTimer { get; set; }

        public void Update() { }
    }
}
