﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using Assets.Code.Physics.Accelerators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Assets.Code.GameCode.System.Schemata;
using Assets.Code.System.TimeUtil;
using UnityEditor;
using Assets.Code.GameCode.System.Schemata.Validator;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities {
    public class AASFileUtility {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IPrefabUtility PrefabUtility => PrefabUtilityAdapter.Instance;
        private static IAssetDatabase AssetDatabase => AssetDatabaseAdapter.Instance;
        private static IAssetPathUtil AssetPathUtil => AssetPathUtilAdapter.Instance;
        private static IJsonUtility JsonUtility => JsonUtilityAdapter.Instance;
        private static IEditorUtility EditorUtility => EditorUtilityAdapter.Instance;

        private static readonly string _acceleratedStatesAssetsFolderProperty = "AcceleratedStatesAsset";
        private static readonly string _acceleratedStatesAutoSavesProperty = "AcceleratedStatesAutoSaves";

        private static int saveCount;

        private static void RetrievePrefabPaths(IGameObject gameObject, out string prefabFilePath, out string prefabMetaPath, out string prefabFolderPath) {
            prefabFilePath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(gameObject);
            prefabMetaPath = AssetDatabase.GetTextMetaFilePathFromAssetPath(prefabFilePath);
            prefabFolderPath = AssetPathUtil.GetAssetFolderPath(prefabFilePath);
        }

        public static List<AccelAsset> LoadAssetModels<T>(IGameObject gameObject, IEnumerable<string> enumNames, string assetName, bool revert = false) where T : Enum {
            RetrievePrefabPaths(gameObject, out string prefabFilePath, out string prefabMetaPath,
                out string prefabFolderPath);
            LocateAutosaves(gameObject, out string acceleratedStatesAutoSaveGuid);
            MetaSuccess found = MetaUtilities.GetAssociatedValue(prefabMetaPath,
                _acceleratedStatesAssetsFolderProperty, out string acceleratedStatesAssetGuid);
            switch (found) {
                case MetaSuccess.FileNotFound:
                    throw new Exception(".meta file for the prefab " + prefabFilePath + " could not be found");
                case MetaSuccess.ValueNotFound:
                    var firstModel = new AccelAsset();
                    firstModel.name = enumNames.ElementAt(0);
                    firstModel.keys = new Key[1];
                    firstModel.keys[0] = new Key(0, 0);
                    firstModel.scaleType = ScaleType.Regular.ToString();
                    ITextAsset firstAsset = DB.NewTextAsset(JsonUtility.ToJson(firstModel));
                    firstAsset.SetName(firstModel.name);
                    AssetDatabase.CreateAsset(firstAsset, prefabFolderPath + assetName);
                    acceleratedStatesAssetGuid = AssetDatabase.AssetPathToGUID(prefabFolderPath + assetName);
                    MetaUtilities.WriteAssociatedValue(prefabMetaPath, _acceleratedStatesAssetsFolderProperty,
                        acceleratedStatesAssetGuid);
                    break;

                case MetaSuccess.ValueFound:
                    break;
            }

            string assetPath = AssetDatabase.GUIDToAssetPath(acceleratedStatesAssetGuid);

            if (mostRecentSave(assetPath, acceleratedStatesAutoSaveGuid, out string newAssetPath,
                    out int unusedCount) && !revert) {
                assetPath = newAssetPath;
            }

            List<ITextAsset> textAssets = AssetDatabase.LoadAllAssetsAtPath<ITextAsset>(assetPath).ToList();
            List<AccelAsset> models = textAssets
                .Select(x => (AccelAsset)JsonUtility.FromJson(x.Text(), typeof(AccelAsset))).ToList();

            // create default assets
            foreach (var name in enumNames) {
                if (models.Find(x => x.name.Equals(name)) == null) {
                    AccelAsset model = new AccelAsset();
                    model.name = name;
                    model.keys = new Key[1];
                    model.xDirection = 1;
                    model.keys[0] = new Key(0, 0);
                    model.scaleType = ScaleType.Regular.ToString();
                    ITextAsset textAsset = DB.NewTextAsset(JsonUtility.ToJson(model));
                    textAsset.SetName(name);
                    AssetDatabase.AddObjectToAsset(textAsset, assetPath);
                    models.Add(model);
                    textAssets.Add(textAsset);
                }
            }

            // remove deprecated states
            foreach (var model in models) {
                bool remove = true;
                foreach (var name in enumNames) {
                    if (name.Equals(model.name)) {
                        remove = false;
                    }
                }

                if (remove) {
                    models.Remove(model);
                }
            }

            // Enter default condition schemata
            foreach (var model in models) {
                if (model.interruptConditions == null || (model.interruptConditions.targetSchema.rootObjectKey == null && model.interruptConditions.argumentSchemata.Length == 0)
                    || !ConditionSchemaValidator.ValidateConditionSchema(model.interruptConditions)) {
                    model.interruptConditions = GetDefaultSchema<T>(model.name);
                }
            }

            AssetDatabase.SaveAssets();
            return models;
        }

        private static ConditionSchema GetDefaultSchema<T>(string name) where T : Enum {
            return new ConditionSchema() {
                targetSchema = new PropertySchema() {
                    rootObjectKey = name,
                    propertyTypeName = typeof(AnimatedAccel<T>).AssemblyQualifiedName,
                    primitiveType = PrimitiveType.NonPrimitive,
                    propertySequence = new PropertyNode[]
                    {
                                new PropertyNode("timer", typeof(ITimer)),
                                new PropertyNode("elapsedSeconds", typeof(Single))
                    }
                },
                comparisonType = ComparisonType.GreaterEqual,
                conditionType = ConditionType.Atom,
                argumentSchemata = new PropertySchema[]
                {
                            new PropertySchema()
                            {
                                rootObjectKey = name,
                                propertyTypeName = typeof(AnimatedAccel<T>).AssemblyQualifiedName,
                                primitiveType = PrimitiveType.NonPrimitive,
                                propertySequence =  new PropertyNode[]
                                {
                                    new PropertyNode("interruptableTime", typeof(Single))
                                }
                            }
                }
            };

        }

        public static List<AccelAsset> LoadUsingButton(IGameObject gameObject, List<string> enumNames,
            string assetName, string loadPath) {
            if (loadPath.Contains("Assets")) {
                loadPath = loadPath.Substring(loadPath.IndexOf("Assets"));
                List<ITextAsset> textAssets = AssetDatabase.LoadAllAssetsAtPath<ITextAsset>(loadPath).ToList();
                List<AccelAsset> models = textAssets
                    .Select(x => (AccelAsset)JsonUtility.FromJson(x.Text(), typeof(AccelAsset))).ToList();

                foreach (var name in enumNames) {
                    if (models.Find(x => x.name.Equals(name)) == null) {
                        AccelAsset model = new AccelAsset();
                        model.name = name;
                        model.keys = new Key[1];
                        model.xDirection = 1;
                        model.keys[0] = new Key(0, 0);
                        model.scaleType = ScaleType.Regular.ToString();
                        ITextAsset textAsset = DB.NewTextAsset(JsonUtility.ToJson(model));
                        textAsset.SetName(name);
                        AssetDatabase.AddObjectToAsset(textAsset, loadPath);
                        models.Add(model);
                        textAssets.Add(textAsset);
                    }
                }

                foreach (var model in models) {
                    bool remove = true;
                    foreach (var name in enumNames) {
                        if (name == model.name) {
                            remove = false;
                        }
                    }

                    if (remove) {
                        models.Remove(model);
                    }
                }

                AssetDatabase.SaveAssets();
                return models;
            } else {
                throw new ArgumentException("Invalid Filepath. Make sure your save is within the Assets/ directory");
            }
        }

        public static void SaveModels(IGameObject gameObject, List<AccelAsset> assetModels, string assetName, string autoSaveAssetPath = null) {
            ConversionUtilities.PopulateSchemataWithMethodSchemata(assetModels);

            RetrievePrefabPaths(gameObject, out string prefabFilePath, out string prefabMetaPath, out string prefabFolderPath);
            string acceleratedStatesAssetGuid = AssetDatabase.AssetPathToGUID(prefabFolderPath + assetName);
            string assetPath = AssetDatabase.GUIDToAssetPath(acceleratedStatesAssetGuid);

            List<ITextAsset> textAssets = new List<ITextAsset>();
            ITextAsset firstAsset = DB.NewTextAsset(JsonUtility.ToJson(assetModels[0]));
            firstAsset.SetName(assetModels[0].name);

            if (autoSaveAssetPath != null) { //Checks if an Autosave path was provided to set the path to the autosave location
                assetPath = autoSaveAssetPath + assetName;
                firstAsset.SetName(assetName);
            }

            AssetDatabase.CreateAsset(firstAsset, assetPath);

            foreach (var model in assetModels.Skip(1)) {
                ITextAsset textAsset = DB.NewTextAsset(JsonUtility.ToJson(model));
                textAsset.SetName(model.name);
                AssetDatabase.AddObjectToAsset(textAsset, assetPath);
                textAssets.Add(textAsset);
            }

            AssetDatabase.SaveAssets();
        }

        public static void SaveAsModels(IGameObject gameObject, List<AccelAsset> assetModels) {
            RetrievePrefabPaths(gameObject, out string prefabFilePath, out string prefabMetaPath, out string prefabFolderPath);
            var path = EditorUtility.SaveFilePanel("Save asset file", prefabFolderPath,
                gameObject.name.Replace(' ', '_') + "AccelerationStates_backup00.asset", "asset");
            if (path.Length == 0) {
                path = prefabFolderPath + "/" + gameObject.name.Replace(' ', '_') + "AccelerationStates_backup00.asset";
            }
            path = path.Substring(path.IndexOf("Assets"));
            List<ITextAsset> textAssets = new List<ITextAsset>();
            ConversionUtilities.PopulateSchemataWithMethodSchemata(assetModels);
            string json = JsonUtility.ToJson(assetModels[0]);
            ITextAsset firstAsset = DB.NewTextAsset(JsonUtility.ToJson(assetModels[0]));
            firstAsset.SetName(assetModels[0].name);

            AssetDatabase.CreateAsset(firstAsset, path);

            foreach (var model in assetModels.Skip(1)) {
                ITextAsset textAsset = DB.NewTextAsset(JsonUtility.ToJson(model));
                textAsset.SetName(model.name);
                AssetDatabase.AddObjectToAsset(textAsset, path);
                textAssets.Add(textAsset);
            }

            AssetDatabase.SaveAssets();
        }

        public static void AutoSaveModels(IGameObject gameObject, List<AccelAsset> assetModels, string assetName) {
            LocateAutosaves(gameObject, out string acceleratedStatesAutoSaveGuid);
            string autoSaveAssetPath = AssetDatabase.GUIDToAssetPath(acceleratedStatesAutoSaveGuid);
            mostRecentSave(autoSaveAssetPath, acceleratedStatesAutoSaveGuid, out string unusedPath, out int saveCount);
            string autoSaveName = "/" + gameObject.name.Replace(' ', '_') + "_AcceleratedStates_auto_0" + saveCount + ".asset";
            SaveModels(gameObject, assetModels, autoSaveName, autoSaveAssetPath);
        }

        private static bool mostRecentSave(string assetPath, string acceleratedStatesAutoSaveGuid, out string newAssetPath, out int saveCount) {
            string autoPath = AssetDatabase.GUIDToAssetPath(acceleratedStatesAutoSaveGuid);
            FileInfo assetInfo = new FileInfo(assetPath);
            DirectoryInfo autoFolder = new DirectoryInfo(autoPath);
            if (autoFolder.GetFiles().Any() && acceleratedStatesAutoSaveGuid != null) {
                var latestSave = autoFolder.GetFiles().OrderByDescending(f => f.LastWriteTime).First();

                if ((assetInfo.LastWriteTime < latestSave.LastWriteTime) && latestSave.Name.Contains("asset")) {
                    string saveName = (latestSave.Name.Contains(".meta")) ? latestSave.Name.Remove(latestSave.Name.Length - 5) : latestSave.Name;
                    saveCount = Int32.Parse(Regex.Match(saveName, @"\d+").Value) + 1;
                    if (saveCount > 9) {
                        saveCount = 0;
                    }
                    newAssetPath = autoPath + "/" + saveName;
                    return true;
                } else {
                    newAssetPath = assetPath;
                    saveCount = 0;
                    return false;
                }
            } else {
                newAssetPath = assetPath;
                saveCount = 0;
                return false;
            }
        }

        public static void LocateAutosaves(IGameObject gameObject, out string acceleratedStatesAutoSaveGuid) {
            RetrievePrefabPaths(gameObject, out string prefabFilePath, out string prefabMetaPath, out string prefabFolderPath);
            MetaSuccess found = MetaUtilities.GetAssociatedValue(prefabMetaPath, _acceleratedStatesAutoSavesProperty, out acceleratedStatesAutoSaveGuid);
            switch (found) {
                case MetaSuccess.ValueNotFound:
                    acceleratedStatesAutoSaveGuid = AssetDatabase.CreateFolder(prefabFolderPath, "AcceleratedStatesAutoSaves");
                    MetaUtilities.WriteAssociatedValue(prefabMetaPath, _acceleratedStatesAutoSavesProperty, acceleratedStatesAutoSaveGuid);
                    break;

                case MetaSuccess.ValueFound:
                    break;
            }
        }
    }
}