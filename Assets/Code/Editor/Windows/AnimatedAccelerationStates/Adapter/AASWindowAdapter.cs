﻿using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using UnityEditor;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Adapter {
    public class AASWindowAdapter : EditorWindow, IEditorWindow {
        public static EditorWindow window;

        IRect IEditorWindow.position { get => new RectAdapter(base.position); }

        [MenuItem("Window/Acceleration States Editor")]
        static void Init() {
            window = GetWindow<AASWindowAdapter>();
            window.Show();
        }

        public void OnEnable() {
            AASWindow.GetInstance(this).OnEnable();
        }

        int i = 0;
        public void OnGUI() {
            AASWindow.GetInstance(this).OnGUI();
        }

        public void OnSelectionChange() {
            AASWindow.GetInstance(this).OnSelectionChange();
        }

        public void Update()
        {
            AASWindow.GetInstance(this).Update();
        }

        public void OnLostFocus() {
            Repaint();
        }
    }
}
