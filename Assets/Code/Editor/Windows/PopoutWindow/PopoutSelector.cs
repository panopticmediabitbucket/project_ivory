﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.CustomGUI;
using Assets.Code.Editor.CustomGUI.Handlers;

namespace Assets.Code.Editor.Windows.PopoutWindow {
    public class PopoutSelector<V> : IPopoutContent<PopoutSelectorEntry<V>>, ISelectionChangeHandler<V> {
        static void Nothing(V v) { }
        private SelectionChangeDelegate<V> OnSelectionChange = Nothing;

        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IEvent Event => EventAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;
        private static IHandles Handles => HandlesAdapter.Instance;

        private static PopoutSelectorStyle _defaultStyle = PopoutSelectorStyle.BuildStyle();

        public IEnumerable<PopoutSelectorEntry<V>> items { get; protected set; }
        private PopoutSelectorEntry<V> _selection;

        public PopoutSelectorEntry<V> selection {
            get => _selection;
            private set {
                _selection = value;
                OnSelectionChange.Invoke(value.item);
            }
        }

        private Action _callback = DoNothing;
        private int _elementHeight;
        public int windowHeight => _elementHeight * items.Count();
        public int windowWidth { get; private set; }

        private static void DoNothing() { }
        public PopoutSelector(IEnumerable<PopoutSelectorEntry<V>> items, PopoutSelectorEntry<V> selection, int elementHeight = 30, PopoutSelectorStyle style = default(PopoutSelectorStyle)) {
            this.style = style.Equals(default(PopoutSelectorStyle)) ? _defaultStyle : style;
            this.items = items;
            _selection = selection;
            windowWidth = 100;
            _elementHeight = elementHeight;
        }

        public IPopoutDrawerContentSummary<PopoutSelectorEntry<V>> contentSummary => new PopoutSelectorContentSummary<V>(_selection, selectorStyle);

        public IPopoutDrawerStyle style { get; private set; }
        private PopoutSelectorStyle selectorStyle => (PopoutSelectorStyle)style;

        public IVector2 GetWindowDimensions() {
            return DB.NewVector2(windowWidth, windowHeight);
        }

        public void RegisterContentChangeCallback(ContentChangeCallback<PopoutSelectorEntry<V>> callback) {
            OnSelectionChange += item => callback(_selection);
        }

        public void RemoveContentChangeCallback(ContentChangeCallback<PopoutSelectorEntry<V>> callback) {
            if (OnSelectionChange != null) OnSelectionChange = OnSelectionChange - (item => callback(_selection));
        }

        public void SetSelection(V item, bool callback = true) {
            var found = items.ToList().Find(x => x.item.Equals(item));
            if (found.Equals(default)) {
                throw new ArgumentNullException(item.ToString() + "not found");
            }
            if (callback) {
                selection = found;
            } else {
                _selection = found;
            }
        }

        public void OnClose() {
            _callback.Invoke();
        }

        public void Activate(int width, int height) {
            windowWidth = width;
        }

        public void OnGUI(IRect rect) {
            EditorGUI.DrawRect(rect, selectorStyle.dropdownColor);
            IVector2 elementSize = DB.NewVector2(rect.width, _elementHeight);

            for (int i = 0; i < items.Count(); i++) {
                IColor diskColor = selectorStyle.highLightColor;
                if (IsHovered(i, rect.position, elementSize, out IVector2 drawPosition)) {
                    diskColor = selectorStyle.dropdownColor;
                    EditorGUI.DrawRect(DB.NewRect(drawPosition, elementSize), selectorStyle.highLightColor);
                    if (Event.current.rawType.Equals(EventType.mouseDown)) {
                        selection = items.ElementAt(i);
                    }
                }
                DrawSelectorBubble(diskColor, DB.NewRect(drawPosition, elementSize), items.ElementAt(i).Equals(_selection));
                IRect labelRect = DB.NewRect(drawPosition.x + elementSize.y, drawPosition.y + (elementSize.y - selectorStyle.textStyle.fontSize) / 2 - 2, elementSize.x - elementSize.y, elementSize.y);
                GUI.Label(labelRect, items.ElementAt(i).name, selectorStyle.textStyle);
            }
        }

        private void DrawSelectorBubble(IColor color, IRect overallRect, bool isSelected) {
            IColor prevColor = Handles.color;
            Handles.color = color;
            IVector3 center = DB.NewVector3(overallRect.x + overallRect.height / 2, overallRect.y + overallRect.height / 2, 0);
            IVector3 normal = DB.NewVector3(0, 0, 1);

            Handles.DrawWireDisc(center, normal, overallRect.height * .2f);
            if (isSelected) {
                Handles.DrawSolidDisc(center, normal, overallRect.height * .15f);
            }

            Handles.color = prevColor;
        }

        private bool IsHovered(int index, IVector2 startingPosition, IVector2 elementSize, out IVector2 drawPosition) {
            drawPosition = DB.NewVector2(startingPosition.x, startingPosition.y + index * elementSize.y);
            IRect hoverRect = DB.NewRect(drawPosition, elementSize);
            return hoverRect.Contains(Event.current.mousePosition);
        }

        public void RegisterSelectionChangedHandler(SelectionChangeDelegate<V> callback) {
            OnSelectionChange += callback;
        }
        public void RemoveSelectionChangedHandler(SelectionChangeDelegate<V> callback) {
            OnSelectionChange -= callback;
        }

        public void ClearSelectionChangedHandler() {
            OnSelectionChange = Nothing;
        }

        public void RegisterOnCloseCallback(Action callback) {
            _callback += callback;
        }

    }

    public struct PopoutSelectorStyle : IPopoutDrawerStyle {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        /*
         * Pass null to use default objects/values
         */
        public static PopoutSelectorStyle BuildStyle(bool isBordered = true, IGUIContent guiContent = null, IGUIStyle textStyle = null, IColor selectorColor = null, IColor dropdownColor = null, IColor highLightColor = null) {
            textStyle = textStyle == null
                ? DB.NewGUIStyle((x => x.normal.textColor = DB.NewColor(1, 1, 1)), (x => x.fontSize = 10), (x => x.fontStyle = FontStyle.Normal)) : textStyle;
            selectorColor = selectorColor == null
                ? GlobalStyleVariables.DarkGrayEditor : selectorColor;
            dropdownColor = dropdownColor == null
                ? GlobalStyleVariables.DarkGrayEditor : dropdownColor;
            highLightColor = highLightColor == null
                ? GlobalStyleVariables.TurquoiseHighlight : highLightColor;

            PopoutSelectorStyle popoutSelectorStyle = new PopoutSelectorStyle() {
                textStyle = textStyle, selectorColor = selectorColor, dropdownColor = dropdownColor, highLightColor = highLightColor,
                drawerColor = selectorColor, isBordered = isBordered, guiContent = guiContent
            };
            return popoutSelectorStyle;
        }

        public IGUIStyle textStyle;
        public IColor selectorColor;
        public IColor dropdownColor;
        public IColor highLightColor;

        public IColor drawerColor { get; set; }
        public bool isBordered { get; set; }
        public IGUIContent guiContent { get; set; }
        public IColor openDrawerColor => GUIFunctions.LightenColor(drawerColor);
    }

    public struct PopoutSelectorContentSummary<V> : IPopoutDrawerContentSummary<PopoutSelectorEntry<V>> {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IGUI GUI => GUIAdapter.Instance;

        private static ITexture _dropDownArrow = MetaUtilities.AssetFromGuid<ITexture>("1b2c4809781bc3d4a9b1d5a54adf5925");

        public PopoutSelectorEntry<V> value { get; private set; }
        private readonly PopoutSelectorStyle _style;

        public PopoutSelectorContentSummary(PopoutSelectorEntry<V> popoutEntry, PopoutSelectorStyle style) {
            value = popoutEntry;
            _style = style;
        }

        // not intended behavior
        public void Draw() {
            Draw(DB.NewRect(0, 0, 100, 20));
        }

        public void Draw(IRect rect) {
            rect = DB.NewRect(rect);

            if (_style.guiContent != null && _style.guiContent.texture != null) {
                float ratio = _style.guiContent.texture.width / _style.guiContent.texture.height;
                IRect iconRect = DB.NewRect(rect.x, rect.y, rect.height * ratio, rect.height);
                rect.x += iconRect.width - 5;
                rect.width -= iconRect.width;
                iconRect = GUIFunctions.ShrinkRect(iconRect, (int)rect.height / 5);
                GUI.Box(iconRect, _style.guiContent, GUIFunctions.textureStyle);
            }

            DrawLabel(rect);

            // Draw popout arrow
            IRect arrowRect = DB.NewRect(rect.x + rect.width - rect.height * .6f, rect.y + (rect.height * .7f) / 2, rect.height * .3f, rect.height * .3f);
            GUI.Box(arrowRect, _dropDownArrow, GUIFunctions.textureStyle);
        }

        public void DrawNotOpenable(IRect rect) {
            DrawLabel(rect);
        }

        private void DrawLabel(IRect rect) {
            IRect labelRect = DB.NewRect(rect);
            labelRect.y += (rect.height - _style.textStyle.fontSize) / 2 - 2;
            labelRect.x += 10;
            labelRect.width -= 20;
            GUI.Label(labelRect, value.name, _style.textStyle);
        }
    }

    public struct PopoutSelectorEntry<V> {
        public readonly V item;
        public readonly string name;

        public PopoutSelectorEntry(V item, string name) {
            this.item = item;
            this.name = name;
        }

        public override bool Equals(object obj) {
            if (obj == null || obj.GetType() != typeof(PopoutSelectorEntry<V>)) {
                return false;
            }
            PopoutSelectorEntry<V> other = (PopoutSelectorEntry<V>)obj;
            return name.Equals(other.name) && item.Equals(other.item);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public static bool operator ==(PopoutSelectorEntry<V> left, PopoutSelectorEntry<V> right) {
            return left.Equals(right);
        }

        public static bool operator !=(PopoutSelectorEntry<V> left, PopoutSelectorEntry<V> right) {
            return !(left == right);
        }
    }

    public struct PopoutSelectorAndDrawer<V> {
        public PopoutDrawer<PopoutSelectorEntry<V>> drawer;
        public PopoutSelector<V> selector;
    }
}
