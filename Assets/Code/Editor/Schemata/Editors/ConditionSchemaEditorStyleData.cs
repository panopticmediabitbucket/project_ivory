﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.GameCode.System.Schemata;

namespace Assets.Code.Editor.Schemata.Editors {
    public partial class ConditionSchemaEditor : AbstractSchemaEditor<ConditionSchema> {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IAssetDatabase AssetDatabase => AssetDatabaseAdapter.Instance;
        private static IGUI GUI => GUIAdapter.Instance;
        private static IEditorGUI EditorGUI => EditorGUIAdapter.Instance;
        private static IHandles Handles => HandlesAdapter.Instance;

        private static readonly IEnumerable<PopoutSelectorEntry<ConditionType>> conditionTypeEntries =
            Enum.GetNames(typeof(ConditionType)).Select(x => new PopoutSelectorEntry<ConditionType>((ConditionType)Enum.Parse(typeof(ConditionType), x, false), x));

        private static readonly ITexture _equalsSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("01edf008185107d4790aa61a72c7d39e"));
        private static readonly ITexture _equalsUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("e7a6a2150169a954683a88de0ffa5e19"));
        private static readonly ITexture _lEqualSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("f879304dc7824d144afae8917ddd8ba7"));
        private static readonly ITexture _lEqualsUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("2f53af03579a906488243e69be42443b"));
        private static readonly ITexture _lessSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("682f49ffe07a6fd4581f4efd00f5b962"));
        private static readonly ITexture _lessUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("2ba4ab9f0d87cf64ba8b50ef7a13d069"));
        private static readonly ITexture _greaterSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("03e44e389a7a05b4ebfbc714e390585b"));
        private static readonly ITexture _greaterUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("93929173ef81a864ebe8e49d7399f1f5"));
        private static readonly ITexture _grEqualSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("77e5881a94b346b4b9c8f6762b0e1c85"));
        private static readonly ITexture _grEqualUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("4ac0dff6f937f6d40a80f8839faecc03"));
        private static readonly ITexture _fxSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("c0a8f7800f2e39944a3d3c140dcbb9a7"));
        private static readonly ITexture _fxUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("e37715368341e4f42af959e7547cd76f"));
        private static readonly ITexture _trueSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("3ee319524494b784899cf3135b43a1f6"));
        private static readonly ITexture _trueUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("97bd1e0149d226b45817db156b144311"));
        private static readonly ITexture _falseSelected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("1a16329148e28b843b35424a732c50c5"));
        private static readonly ITexture _falseUnselected = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("d32f332027e85eb488a4d26e81ca7ba4"));

        private static readonly ITexture _undoArrowGray = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("9b67702874d98a94e9e2a9127c8cf60c"));
        private static readonly ITexture _undoArrow = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("7cd14899972a7614f9f7202cfa6ca2ce"));
        private static readonly ITexture _redoArrowGray = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("ab3254b88ade46e4d958cb706a6a0e89"));
        private static readonly ITexture _redoArrow = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("88050eedaeb8c2b4bb7c6ed9812486d6"));

        private static readonly ITexture _vennDiagram = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("175d29c023cff7443b258873cdf90c6d"));
        private static readonly ITexture _validated = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("c019bc5f7127a1d4788609253caf3380"));
        private static readonly ITexture _invalidated = AssetDatabase.LoadAssetAtPath<ITexture>(AssetDatabase.GUIDToAssetPath("d8e06a06c01c60f4093ef166322a9742"));

        private static readonly IGUIContent _inspect = DB.NewGUIContent("Inspect");
        private static readonly IGUIStyle _inspectStyle = DB.NewGUIStyle(x => x.normal.textColor = DB.NewColor(0, 0, 0), x => x.fontSize = 11, x => x.fontStyle = FontStyle.Bold);
        private static readonly IGUIContent _with = DB.NewGUIContent("With");
        private static readonly IGUIStyle _withStyle = DB.NewGUIStyle(x => x.normal.textColor = DB.NewColor(1, 1, 1), x => x.fontSize = 11, x => x.fontStyle = FontStyle.Bold);
        private static readonly IGUIContent _against = DB.NewGUIContent("Against");
        private static readonly IGUIStyle _againstStyle = DB.NewGUIStyle(x => x.normal.textColor = DB.NewColor(0, 0, 0), x => x.fontSize = 11, x => x.fontStyle = FontStyle.Bold);
        private static readonly IGUIContent _settings = DB.NewGUIContent("Settings");
        private static readonly IGUIStyle _settingsStyle = DB.NewGUIStyle(x => x.normal.textColor = DB.NewColor(0, 0, 0), x => x.fontSize = 11, x => x.fontStyle = FontStyle.Bold);
        private static readonly IGUIContent _methodName = DB.NewGUIContent("Method: ");
        private static readonly IGUIStyle _methodStyle = DB.NewGUIStyle(x => x.fontSize = 12, x => x.fontStyle = FontStyle.Bold, x => x.alignment = TextAnchor.MiddleCenter, x => x.normal.textColor = DB.NewColor(1, 1, 1));
        private static readonly IGUIStyle _argumentLabelStyle = DB.NewGUIStyle(x => x.fontSize = 10, x => x.alignment = TextAnchor.MiddleLeft);
        private static readonly IGUIStyle _andOrStyle = DB.NewGUIStyle(x => x.fontSize = 12, x => x.alignment = TextAnchor.MiddleCenter, x => x.fontStyle = FontStyle.Bold, x => x.normal.textColor = DB.NewColor(1, 1, 1));

        // element heights
        private static readonly int _comparisonBlockHeight = 80;
        private static readonly int _headerHeight = 35;
        private static readonly int _tabHeight = 20;
        private static readonly int _methodHeight = 30;
        private static readonly int _argLabelHeight = 20;
        private static readonly int _andOrRectHeight = 40;
        private static readonly int _plusMinusButtonsHeight = 40;
        private static readonly int _settingsHeight = 40;

        private static readonly int _internalPadding = 8;

        private static readonly IColor _evenDepthColor = DB.NewColor(.8f, .7f, .75f);
        private static readonly IColor _oddDepthColor = DB.NewColor(.75f, .7f, .9f);
        private static readonly IColor _oddDepthColorDark = DB.NewColor(.17f, .14f, .25f);
        private static readonly IColor _evenDepthColorDark = DB.NewColor(.2f, .14f, .17f);

        private static readonly IColor _andOrRectColor = DB.NewColor(.1f, .3f, .3f);
    }
}
