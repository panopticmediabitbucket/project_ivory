﻿using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.GameCode.System.Schemata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Assets.Code.Editor.Schemata.Editors {
    public partial class PropertySchemaEditor : AbstractSchemaEditor<PropertySchema> {
        private readonly int _depth;
        private readonly bool _canBePrimitive;

        private readonly Dictionary<string, Type> _typeDictionary;
        PopoutSelectorAndDrawer<PropertyNode> _property;
        List<PopoutSelectorAndDrawer<PropertyNode>> _propertyNodeSelectors = new List<PopoutSelectorAndDrawer<PropertyNode>>();

        public override int height {
            get {
                int accumulatedHeight = 0;
                accumulatedHeight += _subPropertySelectorsHeight;
                accumulatedHeight += _propertyNodeSelectors.Count * _subPropertySelectorsHeight;
                return accumulatedHeight;
            }
            protected set {
            }
        }

        internal PropertySchemaEditor(Dictionary<string, Type> typeDictionary, List<Type> typeExclusionFilter, ISchemaEditor parent, int depth, bool canBePrimitive) : base(parent) {
            height = 25;
            _typeDictionary = typeDictionary;
            _depth = depth;

            IEnumerable<PopoutSelectorEntry<PropertyNode>> propertyEntries = typeDictionary.ToList().FindAll(x => !typeExclusionFilter.Contains(x.Value))
                .Select(x => new PopoutSelectorEntry<PropertyNode>(new PropertyNode(x.Key, x.Value), x.Value.Name + ": " + x.Key));
            PopoutSelector<PropertyNode> propertySelector = new PopoutSelector<PropertyNode>(propertyEntries, propertyEntries.First(), 25, _propertySelectorStyle);
            propertySelector.RegisterSelectionChangedHandler(UpdateObjectKeyAndPushUndoRoutine);
            propertySelector.RegisterSelectionChangedHandler(item => ClearRedoStack());
            PopoutDrawer<PopoutSelectorEntry<PropertyNode>> propertyDrawer = new PopoutDrawer<PopoutSelectorEntry<PropertyNode>>(propertySelector, 25);
            _property = new PopoutSelectorAndDrawer<PropertyNode>() { drawer = propertyDrawer, selector = propertySelector };

        }

        public void UpdateTargetSchema(PropertySchema schema) {
            _targetSchema = schema;
            if (!string.IsNullOrEmpty(schema.rootObjectKey)) {
                _property.selector.SetSelection(new PropertyNode(schema.rootObjectKey, schema.propertyType), false);
                if (schema.propertySequence.Length > 0) {
                    Type type = schema.propertyType;
                    foreach (var t in schema.propertySequence) {
                        var nodeSelector = AddPropertyNodeDrawerToList(type);
                        nodeSelector.selector.SetSelection(t, false);
                        type = t.propertyType;
                    }
                    AddPropertyNodeDrawerToList(schema.destinationType);
                }
            } else {
                _property.selector.SetSelection(_property.selector.items.ElementAt(0).item, false);
                schema.rootObjectKey = _property.selector.contentSummary.value.item.propertyName;
                schema.propertyTypeName = _property.selector.contentSummary.value.item.propertyTypeName;
                AddPropertyNodeDrawerToList(_property.selector.items.ElementAt(0).item.propertyType);
            }
            ClearRedoStack();
            ClearUndoStack();
        }

        public override ReturnedValue<PropertySchema> OnGUI(IRect rect) {
            return _canBePrimitive ? OnGUICanBePrimitive(rect) : OnGUIReference(rect);
        }

        private ReturnedValue<PropertySchema> OnGUICanBePrimitive(IRect rect) {
            return OnGUIReference(rect);
        }

        private ReturnedValue<PropertySchema> OnGUIReference(IRect rect) {
            rect = DB.NewRect(rect.position, DB.NewVector2(rect.width, _targetPropertySelectorHeight));
            _property.drawer.OnGUI(rect);
            IVector2 dottedLineRoot = DB.NewVector2(rect.x + _subPropertySelectorsHeight / 2, rect.y + rect.height);
            rect.y += _targetPropertySelectorHeight;
            rect.height = _subPropertySelectorsHeight;
            rect.x += _subPropertySelectorsHeight;
            rect.width -= _subPropertySelectorsHeight;
            IVector2 dottedLineStem = DB.NewVector2(dottedLineRoot);
            dottedLineStem.y += _subPropertySelectorsHeight / 2;
            IVector2 dottedLineLeaf = DB.NewVector2(dottedLineStem);
            dottedLineLeaf.x += _targetPropertySelectorHeight;

            Handles.color = DB.NewColor(.2f, .2f, 1);
            foreach (var selector in _propertyNodeSelectors) {
                Handles.DrawLine(dottedLineRoot, dottedLineStem);
                Handles.DrawLine(dottedLineStem, dottedLineLeaf);
                selector.drawer.OnGUI(rect);
                rect.y += rect.height;
                dottedLineRoot = DB.NewVector2(dottedLineStem);
                dottedLineStem.y += _subPropertySelectorsHeight;
                dottedLineLeaf.y += _subPropertySelectorsHeight;
            }
            return ReturnedValue<PropertySchema>.noReturnValue;
        }

        public override ReturnedValue<PropertySchema> OnGUI() {
            return ReturnedValue<PropertySchema>.noReturnValue;
        }

        private void UpdateObjectKeyAndPushUndoRoutine(PropertyNode propertyNode) {
            var cachedKey = _targetSchema.rootObjectKey;
            var cachedType = _targetSchema.propertyType;
            var cachedSequence = _targetSchema.propertySequence;
            var cachedNodeSelectors = _propertyNodeSelectors;
            _targetSchema.rootObjectKey = propertyNode.propertyName;
            _targetSchema.propertyType = propertyNode.propertyType;
            _targetSchema.propertySequence = Array.Empty<PropertyNode>();
            _propertyNodeSelectors = new List<PopoutSelectorAndDrawer<PropertyNode>>();
            AddPropertyNodeDrawerToList(_targetSchema.destinationType);

            RegisterUndoAction(() => {
                _property.selector.SetSelection(new PropertyNode(cachedKey, cachedType), false);
                _targetSchema.rootObjectKey = cachedKey;
                _targetSchema.propertyType = cachedType;
                _targetSchema.propertySequence = cachedSequence;
                _propertyNodeSelectors = cachedNodeSelectors;
                RegisterRedoAction(() => {
                    _property.selector.SetSelection(propertyNode, false);
                    UpdateObjectKeyAndPushUndoRoutine(propertyNode);
                });
            });

            _parent?.NotifyOfChangeInChild(_targetSchema);
        }

        private void PropertyNodeSelectionChangeRoutine(PropertyNode newProperty, int index) {
            var cachedSelectorList = _propertyNodeSelectors;
            var cachedPropertySequence = _targetSchema.propertySequence;
            var cachedPropertyNode = index < cachedPropertySequence.Length ? cachedPropertySequence[index] : default(PropertyNode);

            _propertyNodeSelectors = _propertyNodeSelectors.Take(index + 1).ToList();

            if (_propertyNodeSelectors.Last().selector.contentSummary.value.item.propertyName.Equals("*")) {
                _targetSchema.propertySequence = cachedPropertySequence.Take(index).ToArray();
            } else {
                _targetSchema.propertySequence = cachedPropertySequence.Take(index).Append(_propertyNodeSelectors.Last().selector.selection.item).ToArray();
                AddPropertyNodeDrawerToList(newProperty.propertyType);
            }

            RegisterUndoAction(() => {
                _targetSchema.propertySequence = cachedPropertySequence;
                if (index < cachedPropertySequence.Length) {
                    _targetSchema.propertySequence[index] = cachedPropertyNode;
                }
                _propertyNodeSelectors = cachedSelectorList;
                _propertyNodeSelectors[index].selector.SetSelection(cachedPropertyNode, false);

                RegisterRedoAction(() => {
                    _propertyNodeSelectors[index].selector.SetSelection(newProperty, false);
                    PropertyNodeSelectionChangeRoutine(newProperty, index);
                });
            });

            _parent?.NotifyOfChangeInChild(_targetSchema);
        }

        private PopoutSelectorAndDrawer<PropertyNode> AddPropertyNodeDrawerToList(Type type) {
            var selectorAndDrawer = GetTypePropertyDrawer(type);
            int index = _propertyNodeSelectors.Count;
            selectorAndDrawer.selector.RegisterSelectionChangedHandler(item => PropertyNodeSelectionChangeRoutine(item, index));
            selectorAndDrawer.selector.RegisterSelectionChangedHandler(item => ClearRedoStack());
            _propertyNodeSelectors.Add(selectorAndDrawer);
            return selectorAndDrawer;
        }

        private PopoutSelectorAndDrawer<PropertyNode> GetTypePropertyDrawer(Type type) {
            var mainPropList = type.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Select(x => new PopoutSelectorEntry<PropertyNode>(new PropertyNode(x.Name, x.PropertyType.AssemblyQualifiedName), x.PropertyType.Name + ":" + x.Name)).ToList();
            mainPropList.AddRange(type.GetFields(BindingFlags.Instance | BindingFlags.Public)
                .Select(x => new PopoutSelectorEntry<PropertyNode>(new PropertyNode(x.Name, x.FieldType.AssemblyQualifiedName), x.FieldType.Name + ": " + x.Name)));
            mainPropList = mainPropList.Prepend(new PopoutSelectorEntry<PropertyNode>(new PropertyNode("*", ""), "Self")).ToList();

            PopoutSelector<PropertyNode> selector = new PopoutSelector<PropertyNode>(mainPropList, mainPropList.First(), 25, _subPropertySelectorStyle);
            PopoutDrawer<PopoutSelectorEntry<PropertyNode>> drawer = new PopoutDrawer<PopoutSelectorEntry<PropertyNode>>(selector, 25);

            return new PopoutSelectorAndDrawer<PropertyNode>() { drawer = drawer, selector = selector };
        }

        public override void NotifyOfChangeInChild(BaseSchema child) { }
    }
}

