﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.CustomGUI;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.GameCode.System.Schemata;

namespace Assets.Code.Editor.Schemata.Editors {
    public partial class PropertySchemaEditor : AbstractSchemaEditor<PropertySchema> {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static IPrefabUtility PrefabUtility => PrefabUtilityAdapter.Instance;
        private static IHandles Handles => HandlesAdapter.Instance;

        private static readonly PopoutSelectorStyle _propertySelectorStyle = 
            PopoutSelectorStyle.BuildStyle(true, DB.NewGUIContent(PrefabUtility.GetIconForGameObject(null), "Reference object"), null, GlobalStyleVariables.MidGrayEditor, GlobalStyleVariables.MidGrayEditor, GlobalStyleVariables.TurquoiseLowlight);
        private static readonly PopoutSelectorStyle _subPropertySelectorStyle = 
            PopoutSelectorStyle.BuildStyle(true, DB.NewGUIContent(AssetDatabaseAdapter.Instance.LoadAssetAtPath<ITexture>(AssetDatabaseAdapter.Instance.GUIDToAssetPath("c7e185b8d5c2c3d418489e24c8015695")), "Property"),
                DB.NewGUIStyle(x => x.normal.textColor = DB.NewColor(0, 0, 0), x => x.fontSize = 10), GlobalStyleVariables.LightGrayEditor, GlobalStyleVariables.LightGrayEditor, GlobalStyleVariables.TurquoiseHighlight);

        private static readonly int _targetPropertySelectorHeight = 25;
        private static readonly int _subPropertySelectorsHeight = 25;
    }
}
