﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.CustomGUI.GUIGraphics;
using Assets.Code.Editor.Utilities;
using Assets.Code.Editor.Windows.PopoutWindow;
using Assets.Code.GameCode.System.Schemata;
using FontStyle = Assets.Code.Editor.AdapterLayer.Objects.FontStyle;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.Editor.Schemata.Editors {
    public partial class ConditionSchemaEditor : AbstractSchemaEditor<ConditionSchema> {
        public ConditionSchemaEditor(Dictionary<string, Type> typeDictionary, int depth) : base(null) {
            _typeDictionary = typeDictionary;
            _depth = depth;
            BuildGUIElements();
        }

        protected internal ConditionSchemaEditor(ISchemaEditor parent, Dictionary<string, Type> typeDictionary, int depth) : base(parent) {
            _typeDictionary = typeDictionary;
            _depth = depth;
            BuildGUIElements();
        }

        public override int height {
            get {
                int accumulator = 0;
                switch (_targetSchema.conditionType) {
                    case ConditionType.And:
                    case ConditionType.Or:
                        accumulator += _headerHeight;
                        foreach (var editor in _childEditors) {
                            accumulator += editor.height;
                        }
                        accumulator += (_childEditors.Count - 1) * _andOrRectHeight;
                        accumulator += _internalPadding * 2;
                        break;
                    case ConditionType.Atom:
                        foreach (var editor in _argumentEditors) {
                            accumulator += _argLabelHeight;
                            accumulator += editor.height;
                        }
                        accumulator += _comparisonBlockHeight;
                        accumulator += _headerHeight;
                        accumulator += _targetPropertyEditor.height;
                        accumulator += _tabHeight;
                        if (_targetSchema.comparisonType == ComparisonType.Custom) {
                            accumulator += _methodHeight;
                        }
                        if (_targetSchema.comparisonType != ComparisonType.False && _targetSchema.comparisonType != ComparisonType.True) {
                            accumulator += _tabHeight;
                        }
                        break;
                }
                accumulator += _settingsHeight;
                accumulator += _tabHeight;
                return accumulator;
            }
            protected set { }
        }

        private Dictionary<string, Type> _typeDictionary;

        PropertySchemaEditor _targetPropertyEditor;
        PopoutSelectorAndDrawer<ConditionType> _conditionTypeDrawer;
        PopoutSelectorAndDrawer<MethodInfo> _methodInfoDrawer;
        private GUIModeButtonArray<ComparisonType> _comparisonButtonArray;
        private GUIModeButtonArray<ComparisonType> _truthButtonArray;
        private Button _undoButton;
        private Button _redoButton;

        private bool _parametersValidated;
        public bool parametersValidated {
            get {
                bool acc = _parametersValidated;
                if (_targetSchema.conditionType != ConditionType.Atom) {
                    foreach (var child in _childEditors) {
                        acc &= child.parametersValidated;
                    }
                }
                return acc;
            }
        }

        private readonly int _depth;
        private List<ConditionSchemaEditor> _childEditors = new List<ConditionSchemaEditor>();
        private List<PropertySchemaEditor> _argumentEditors = new List<PropertySchemaEditor>();

        // This method needs to clear and re-register callback functions of wrapped GUI elements that notify 
        // the editor of changes in the selected content
        public void UpdateTargetSchema(ConditionSchema schema) {
            _comparisonButtonArray.ClearSelectionChangedHandler();
            ClearRedoStack();
            ClearUndoStack();

            _argumentEditors.Clear();
            _childEditors.Clear();

            _targetSchema = schema;
            _conditionTypeDrawer.selector.SetSelection(_targetSchema.conditionType, false);
            _childEditors = new List<ConditionSchemaEditor>();
            if (schema.conditionType != ConditionType.Atom) {
                foreach (var child in _targetSchema.children) {
                    var childEditor = new ConditionSchemaEditor(this, _typeDictionary, _depth + 1);
                    childEditor.UpdateTargetSchema(child);
                    _childEditors.Add(childEditor);
                }
            } else {
                _targetPropertyEditor = new PropertySchemaEditor(_typeDictionary, new List<Type>(), this, _depth, false);
                _targetPropertyEditor.UpdateTargetSchema(_targetSchema.targetSchema == null ? new PropertySchema() : _targetSchema.targetSchema);
                BuildMethodDrawer(_targetSchema.targetSchema.destinationType);
                if (!string.IsNullOrEmpty(_targetSchema.truthMethodName)) {
                    _methodInfoDrawer.selector.SetSelection(_targetSchema.targetSchema.destinationType.GetMethod(_targetSchema.truthMethodName), false);
                }

                if (schema.comparisonType == ComparisonType.Custom) {
                    var method = schema.targetSchema.destinationType.GetMethod(schema.truthMethodName);
                    var parameters = method.GetParameters();
                    for (int i = 0; i < parameters.Length; i++) {
                        Type paramType = parameters[i].ParameterType;
                        bool canBePrimitive = paramType == typeof(Single) || paramType == typeof(String) || paramType == typeof(Int32);
                        var editor = new PropertySchemaEditor(_typeDictionary, new List<Type>(), this, _depth, canBePrimitive);
                        editor.UpdateTargetSchema(schema.argumentSchemata[i]);
                        _argumentEditors.Add(editor);
                    }
                } else {
                    if (schema.argumentSchemata.Length == 0) {
                        schema.argumentSchemata = new PropertySchema[] { new PropertySchema() };
                    }
                    Type paramType = schema.targetSchema.destinationType;
                    bool canBePrimitive = paramType == typeof(Single) || paramType == typeof(String) || paramType == typeof(Int32);
                    var editor = new PropertySchemaEditor(_typeDictionary, new List<Type>(), this, _depth, canBePrimitive);
                    editor.UpdateTargetSchema(schema.argumentSchemata[0]);
                    _argumentEditors.Add(editor);
                }
            }

            ValidateParameters();

            if (_targetSchema.comparisonType == ComparisonType.False || _targetSchema.comparisonType == ComparisonType.True) {
                _truthButtonArray.SetSelection(_targetSchema.comparisonType);
            } else {
                _comparisonButtonArray.SetSelection(_targetSchema.comparisonType);
            }
            _comparisonButtonArray.RegisterSelectionChangedHandler(UpdateComparisonTypeAndPushUndoFunction);
            _comparisonButtonArray.RegisterSelectionChangedHandler(item => ClearRedoStack());
            _truthButtonArray.RegisterSelectionChangedHandler(UpdateTruthTypeAndPushUndoFunction);
            _truthButtonArray.RegisterSelectionChangedHandler(item => ClearRedoStack());
        }

        public void UpdateTypeDictionary(Dictionary<string, Type> typeDictionary) {
            _typeDictionary = typeDictionary;
        }

        private void BuildGUIElements() {
            var style = PopoutSelectorStyle.BuildStyle(true, DB.NewGUIContent(_vennDiagram, "Comparison Mode"), DB.NewGUIStyle(x => x.fontSize = 13, x => x.normal.textColor = DB.NewColor(1, 1, 1), x => x.fontStyle = FontStyle.Bold), null, null, null);
            PopoutSelector<ConditionType> conditionTypeSelector = new PopoutSelector<ConditionType>(conditionTypeEntries, conditionTypeEntries.First(), 25, style);
            conditionTypeSelector.RegisterSelectionChangedHandler(UpdateConditionTypeAndPushUndoFunction);
            PopoutDrawer<PopoutSelectorEntry<ConditionType>> conditionDrawer = new PopoutDrawer<PopoutSelectorEntry<ConditionType>>(conditionTypeSelector, 25);
            _conditionTypeDrawer = new PopoutSelectorAndDrawer<ConditionType>() { drawer = conditionDrawer, selector = conditionTypeSelector };

            GUIButtonContent<ComparisonType>[] conditionContent = new GUIButtonContent<ComparisonType>[6];
            conditionContent[0] = new GUIButtonContent<ComparisonType>(ComparisonType.Equal, _equalsUnselected, _equalsSelected);
            conditionContent[1] = new GUIButtonContent<ComparisonType>(ComparisonType.Less, _lessUnselected, _lessSelected);
            conditionContent[2] = new GUIButtonContent<ComparisonType>(ComparisonType.LessEqual, _lEqualsUnselected, _lEqualSelected);
            conditionContent[3] = new GUIButtonContent<ComparisonType>(ComparisonType.Greater, _greaterUnselected, _greaterSelected);
            conditionContent[4] = new GUIButtonContent<ComparisonType>(ComparisonType.GreaterEqual, _grEqualUnselected, _grEqualSelected);
            conditionContent[5] = new GUIButtonContent<ComparisonType>(ComparisonType.Custom, _fxUnselected, _fxSelected);
            _comparisonButtonArray = new GUIModeButtonArray<ComparisonType>(conditionContent, DB.NewVector2(40, 40));

            GUIButtonContent<ComparisonType>[] truthConditionContent = new GUIButtonContent<ComparisonType>[2];
            truthConditionContent[0] = new GUIButtonContent<ComparisonType>(ComparisonType.True, _trueUnselected, _trueSelected);
            truthConditionContent[1] = new GUIButtonContent<ComparisonType>(ComparisonType.False, _falseUnselected, _falseSelected);
            _truthButtonArray = new GUIModeButtonArray<ComparisonType>(truthConditionContent, DB.NewVector2(40, 40));

            _targetPropertyEditor = new PropertySchemaEditor(_typeDictionary, new List<Type>(), this, _depth, false);

            _undoButton = new Button(_undoArrow, DB.NewVector2(1, 1));
            _undoButton.RegisterOnPressedCallback(Undo);
            _redoButton = new Button(_redoArrow, DB.NewVector2(1, 1));
            _redoButton.RegisterOnPressedCallback(Redo);
        }

        private string BuildMethodStringPreview(MethodInfo method) {
            StringBuilder sb = new StringBuilder();
            sb.Append(method.Name);
            sb.Append("( ");
            var parameters = method.GetParameters();
            for (int i = 0; i < parameters.Length; i++) {
                sb.Append(parameters[i].ParameterType.Name);
                sb.Append(" a" + i);
                if (i + 1 < parameters.Length) {
                    sb.Append(",");
                }
            }
            sb.Append(" )");
            return sb.ToString();
        }

        private void BuildMethodDrawer(Type type) {
            var methodList = type.GetMethods(BindingFlags.Instance | BindingFlags.Public).ToList().FindAll(x => x.ReturnType == (typeof(bool)));
            var popoutEntries = methodList.Select(x => new PopoutSelectorEntry<MethodInfo>(x, BuildMethodStringPreview(x)));
            popoutEntries.Prepend(new PopoutSelectorEntry<MethodInfo>(null, "None"));
            PopoutSelector<MethodInfo> selector = new PopoutSelector<MethodInfo>(popoutEntries, popoutEntries.First(), 25);
            PopoutDrawer<PopoutSelectorEntry<MethodInfo>> drawer = new PopoutDrawer<PopoutSelectorEntry<MethodInfo>>(selector, 25);
            _methodInfoDrawer = new PopoutSelectorAndDrawer<MethodInfo>() { drawer = drawer, selector = selector };
            selector.RegisterSelectionChangedHandler(UpdateSelectedMethodAndPushUndo);
        }

        public override ReturnedValue<ConditionSchema> OnGUI(IRect rect) {
            IVector2 position = rect.position;

            DrawHeaderControl(ref position, rect.width);
            IRect bodyRect = DB.NewRect(position.x, position.y, rect.width, height - _headerHeight);
            EditorGUI.DrawRect(bodyRect, _depth % 2 == 0 ? _evenDepthColorDark : _oddDepthColorDark);
            switch (_targetSchema.conditionType) {
                case ConditionType.And:
                    DrawChildEditors(ref position, rect.width);
                    break;
                case ConditionType.Or:
                    DrawChildEditors(ref position, rect.width);
                    break;
                case ConditionType.Atom:
                    DrawTargetPropertyControl(ref position, rect.width);
                    ComparisonTypeControl(ref position, rect.width);
                    if (_targetSchema.comparisonType != ComparisonType.False && _targetSchema.comparisonType != ComparisonType.True) {
                        DrawMethodSelector(ref position, rect.width);
                        DrawArgumentsControl(ref position, rect.width);
                    }
                    break;
            }
            DrawSettings(ref position, rect.width);

            return interaction ? new ReturnedValue<ConditionSchema>(_targetSchema) : ReturnedValue<ConditionSchema>.noReturnValue;
        }

        private int HeightOfChildren() {
            int height = 0;
            foreach (var x in _childEditors) {
                height += x.height;
            }
            return height + _andOrRectHeight * (_childEditors.Count - 1);
        }

        private void DrawChildEditors(ref IVector2 position, float rectWidth) {
            IRect rect = DB.NewRect(position.x + _internalPadding - 2, position.y + _internalPadding - 2, rectWidth - _internalPadding * 2 + 4, HeightOfChildren() + 4);
            EditorGUI.DrawRect(rect, _depth % 2 == 0 ? _evenDepthColor : _oddDepthColor);
            rect.x += 2; rect.width -= 4; rect.y += 2;
            position.y += _internalPadding;
            for (int i = 0; i < _childEditors.Count; i++) {
                var editor = _childEditors[i];
                rect.height = editor.height;
                position.y += editor.height;
                editor.OnGUI(rect);
                rect.y += rect.height;

                if (i + 1 < _childEditors.Count) {
                    IRect andRect = DB.NewRect(rect.x, rect.y, rectWidth, _andOrRectHeight);
                    GUIFunctions.DrawBorderedRect(andRect, GUIFunctions.DarkenColor(_andOrRectColor), _andOrRectColor, 2);
                    GUI.Label(andRect, _targetSchema.conditionType == ConditionType.And ? "&&" : "||", _andOrStyle);
                    rect.y += _andOrRectHeight;
                    position.y += _andOrRectHeight;
                }
            }
            position.y += _internalPadding;
        }

        private void DrawMethodSelector(ref IVector2 position, float rectWidth) {
            if (_targetSchema.comparisonType == ComparisonType.Custom) {
                IRect drawRect = DB.NewRect(position, DB.NewVector2(rectWidth, _methodHeight));
                EditorGUI.DrawRect(drawRect, _depth % 2 == 0 ? _evenDepthColorDark : _oddDepthColorDark);

                IRect labelRect = DB.NewRect(drawRect.x, drawRect.y, 70, drawRect.height);
                GUI.Label(labelRect, _methodName, _methodStyle);

                drawRect.x += 70;
                drawRect.width -= 70;
                _methodInfoDrawer.drawer.OnGUI(drawRect);
                position.y += _methodHeight;
            }
        }

        private void DrawArgumentsControl(ref IVector2 position, float rectWidth) {
            int heightAccumulator = 0;
            heightAccumulator += _tabHeight;
            foreach (var x in _argumentEditors) {
                heightAccumulator += _argLabelHeight;
                heightAccumulator += x.height;
            }
            IRect rect = DB.NewRect(position.x, position.y, rectWidth, heightAccumulator);
            position.y += heightAccumulator;

            GUIFunctions.DrawTabRect(rect, _against, _againstStyle, 0, _depth % 2 == 0 ? _evenDepthColor : _oddDepthColor, null);
            rect.y += _tabHeight;

            IRect argLabelRect = DB.NewRect(rect.x + 10, rect.y, rect.width - 10, _argLabelHeight);

            int i = 0;
            Handles.color = DB.NewColor(.5f, .5f, .5f);
            foreach (var editor in _argumentEditors) {
                GUI.Label(argLabelRect, "Argument " + i++, _argumentLabelStyle);
                rect.y += argLabelRect.height;

                IVector2 leftPoint = DB.NewVector2(rect.x, rect.y - 2);
                IVector2 rightPoint = DB.NewVector2(rect.x + rect.width, rect.y - 2);
                Handles.DrawDottedLine(leftPoint, rightPoint, 1.5f);

                rect.height = editor.height;
                editor.OnGUI(rect);
                rect.y += rect.height;
            }
        }

        private void DrawTargetPropertyControl(ref IVector2 position, float rectWidth) {
            IRect propertyRect = DB.NewRect(position, DB.NewVector2(rectWidth, _targetPropertyEditor.height + _tabHeight));
            position.y += propertyRect.height;

            GUIFunctions.DrawTabRect(propertyRect, _inspect, _inspectStyle, 0, _depth % 2 == 0 ? _evenDepthColor : _oddDepthColor, null);
            propertyRect.y += 20;
            propertyRect.height -= 20;
            _targetPropertyEditor.OnGUI(propertyRect);
        }

        private void DrawSettings(ref IVector2 position, float rectWidth) {
            IRect settingsRect = DB.NewRect(position, DB.NewVector2(rectWidth, _tabHeight + _settingsHeight));
            GUIFunctions.DrawTabRect(settingsRect, _settings, _settingsStyle, 0, GlobalStyleVariables.LightGrayEditor, null);
            settingsRect.y += _tabHeight; settingsRect.x += 10; settingsRect.width -= 20;
            settingsRect.height -= _tabHeight;
            var initFrameCheck = _targetSchema.checkEveryFrame;
            var initOnceMet = _targetSchema.onceMetAlwaysMet;
            settingsRect.width /= 2;
            _targetSchema.checkEveryFrame = (bool)EditorGUI.ToggleLeft(settingsRect, "Check Every Frame", _targetSchema.checkEveryFrame);
            settingsRect.x += settingsRect.width;
            _targetSchema.onceMetAlwaysMet = (bool)EditorGUI.ToggleLeft(settingsRect, "Met Until Reset", _targetSchema.onceMetAlwaysMet);
            if (initFrameCheck != _targetSchema.checkEveryFrame || initOnceMet != _targetSchema.onceMetAlwaysMet) {
                interaction = true;
            }
        }

        public override ReturnedValue<ConditionSchema> OnGUI() {
            throw new NotImplementedException();
        }

        private void ComparisonTypeControl(ref IVector2 position, float rectWidth) {
            IRect comparisonBlockRect = DB.NewRect(position.x, position.y, rectWidth, _comparisonBlockHeight);
            position.y += _comparisonBlockHeight;

            GUIFunctions.DrawTabRect(comparisonBlockRect, _with, _withStyle, 2, GlobalStyleVariables.DarkGrayEditor, GUIFunctions.LightenColor(GlobalStyleVariables.DarkGrayEditor));
            comparisonBlockRect.y += 30;
            comparisonBlockRect.height = 40;
            if (_targetSchema.comparisonType == ComparisonType.False || _targetSchema.comparisonType == ComparisonType.True) {
                _truthButtonArray.OnGUI(comparisonBlockRect);
            } else {
                _comparisonButtonArray.OnGUI(comparisonBlockRect);
            }
        }

        private void DrawHeaderControl(ref IVector2 position, float width) {
            IRect headerRect = DB.NewRect(position.x, position.y, width, _headerHeight);
            GUIFunctions.DrawBorderedRect(headerRect, GUIFunctions.DarkenColor(GlobalStyleVariables.MidGrayEditor), GlobalStyleVariables.MidGrayEditor, 1);
            IRect conditionModeRect = DB.NewRect(headerRect.x, headerRect.y, 120, _headerHeight);
            _conditionTypeDrawer.drawer.OnGUI(conditionModeRect);
            IRect validationRect = DB.NewRect(conditionModeRect);
            validationRect.x += conditionModeRect.width + 3;
            validationRect.width = validationRect.height;
            validationRect = GUIFunctions.ShrinkRect(validationRect, 8);

            GUI.Box(validationRect, parametersValidated ? _validated : _invalidated, GUIFunctions.textureStyle);

            if (_parent == null) {
                DrawUndoBlock(headerRect);
            }

            position.y += _headerHeight;
        }

        private void DrawUndoBlock(IRect headerRect) {
            IRect redoRect = DB.NewRect(headerRect.x + headerRect.width - headerRect.height, headerRect.y + 5, headerRect.height - 10, headerRect.height - 10);
            if (!RedoStackEmpty()) {
                _redoButton.OnGUI(redoRect);
            } else {
                GUI.DrawTexture(redoRect, _redoArrowGray);
            }
            IRect undoRect = DB.NewRect(redoRect);
            undoRect.x -= undoRect.width;
            if (!UndoStackEmpty()) {
                _undoButton.OnGUI(undoRect);
            } else {
                GUI.DrawTexture(undoRect, _undoArrowGray);
            }
        }

        private void UpdateComparisonTypeAndPushUndoFunction(ComparisonType compType) {
            interaction = true;
            var cachedType = _targetSchema.comparisonType;
            var cachedMethodName = _targetSchema.truthMethodName;
            var cachedArguments = _targetSchema.argumentSchemata;
            var cachedArgumentEditors = _argumentEditors;
            if (_targetSchema.comparisonType == ComparisonType.Custom) {
                GenerateArgumentSchema();
            }
            _targetSchema.comparisonType = compType;
            ValidateParameters();

            RegisterUndoAction(() => {
                interaction = true;
                _comparisonButtonArray.SetSelection(cachedType);
                _targetSchema.comparisonType = cachedType;
                _targetSchema.truthMethodName = cachedMethodName;
                _targetSchema.argumentSchemata = cachedArguments;
                _argumentEditors = cachedArgumentEditors;
                ValidateParameters();
                RegisterRedoAction(() => {
                    _comparisonButtonArray.SetSelection(compType);
                    UpdateComparisonTypeAndPushUndoFunction(compType);
                });
            });
        }

        private void UpdateTruthTypeAndPushUndoFunction(ComparisonType compType) {
            interaction = true;
            var cachedType = _targetSchema.comparisonType;
            ValidateParameters();

            RegisterUndoAction(() => {
                interaction = true;
                _truthButtonArray.SetSelection(cachedType);
                _targetSchema.comparisonType = cachedType;
                ValidateParameters();
                RegisterRedoAction(() => {
                    _truthButtonArray.SetSelection(compType);
                    UpdateTruthTypeAndPushUndoFunction(compType);
                });
            });
        }

        private void UpdateSelectedMethodAndPushUndo(MethodInfo method) {
            var cachedMethodName = _targetSchema.truthMethodName;
            var cachedMethodInfo = string.IsNullOrEmpty(cachedMethodName) ? null : _targetSchema.targetSchema.destinationType.GetMethod(cachedMethodName);
            var cachedArguments = _targetSchema.argumentSchemata;
            var cachedArgumentEditors = _argumentEditors;

            _targetSchema.truthMethodName = method.Name;
            GenerateCustomMethodArgumentSchemata(method);
            ValidateParameters();

            RegisterUndoAction(() => {
                _methodInfoDrawer.selector.SetSelection(cachedMethodInfo, false);

                _targetSchema.truthMethodName = cachedMethodName;
                _targetSchema.argumentSchemata = cachedArguments;
                _argumentEditors = cachedArgumentEditors;
                ValidateParameters();
                RegisterRedoAction(() => {
                    _methodInfoDrawer.selector.SetSelection(method, false);
                    UpdateSelectedMethodAndPushUndo(method);
                });
            });
        }

        private void ValidateParameters() {
            if (_targetSchema.conditionType == ConditionType.Atom) {
                if (_targetSchema.comparisonType == ComparisonType.Custom) {
                    var destinationType = _targetSchema.targetSchema.destinationType;
                    var method = destinationType.GetMethod(_targetSchema.truthMethodName);
                    var parameters = method.GetParameters();
                    bool match = _targetSchema.argumentSchemata.Length == parameters.Length;
                    if (match) {
                        for (int i = 0; i < parameters.Length; i++) {
                            match &= parameters[i].ParameterType == _targetSchema.argumentSchemata[i].destinationType;
                        }
                        _parametersValidated = match;
                    } else {
                        _parametersValidated = match;
                    }
                } else if (_targetSchema.comparisonType != ComparisonType.False && _targetSchema.comparisonType != ComparisonType.True) {
                    var destinationType = _targetSchema.targetSchema.destinationType;
                    bool match = _targetSchema.argumentSchemata[0].destinationType == destinationType;
                    _parametersValidated = match && destinationType.IsPrimitive;
                } else {
                    _parametersValidated = true;
                }
            } else {
                _parametersValidated = true;
            }
        }

        private void GenerateArgumentSchema() {
            _targetSchema.argumentSchemata = new PropertySchema[1];
            _targetSchema.argumentSchemata[0] = new PropertySchema();
            _argumentEditors = new List<PropertySchemaEditor>();
            _argumentEditors.Add(new PropertySchemaEditor(_typeDictionary, new List<Type>(), this, _depth, true));
        }

        private void GenerateCustomMethodArgumentSchemata(MethodInfo method) {
            _argumentEditors = new List<PropertySchemaEditor>();
            ParameterInfo[] parameters = method.GetParameters();
            _targetSchema.argumentSchemata = new PropertySchema[parameters.Length];

            for (int i = 0; i < parameters.Length; i++) {
                _targetSchema.argumentSchemata[i] = new PropertySchema();

                Type paramType = parameters[i].ParameterType;
                bool canBePrimitive = paramType == typeof(Single) || paramType == typeof(String) || paramType == typeof(Int32);

                var propEditor = new PropertySchemaEditor(_typeDictionary, new List<Type>(), this, _depth, canBePrimitive);
                propEditor.UpdateTargetSchema(_targetSchema.argumentSchemata[i]);
                _argumentEditors.Add(propEditor);
            }
        }

        private void UpdateConditionTypeAndPushUndoFunction(ConditionType newType) {
            interaction = true;
            var cachedType = _targetSchema.conditionType;
            var cachedChildSchemata = _targetSchema.children;
            var cachedProperty = _targetSchema.targetSchema;
            var cachedArguments = _targetSchema.argumentSchemata;

            var cachedChildEditors = _childEditors;

            Action andOrFunction = () => {
                if (_targetSchema.conditionType == ConditionType.Atom) {
                    _childEditors = new List<ConditionSchemaEditor>();
                    _targetSchema.children = new ConditionSchema[2];
                    _targetSchema.children[0] = new ConditionSchema();
                    _targetSchema.children[1] = new ConditionSchema();
                    var childEditor = new ConditionSchemaEditor(this, _typeDictionary, _depth + 1);
                    childEditor.UpdateTargetSchema(_targetSchema.children[0]);
                    _childEditors.Add(childEditor);
                    childEditor = new ConditionSchemaEditor(this, _typeDictionary, _depth + 1);
                    childEditor.UpdateTargetSchema(_targetSchema.children[1]);
                    _childEditors.Add(childEditor);
                }

                _targetSchema.conditionType = newType;
                _targetSchema.targetSchema = null;
                _targetSchema.argumentSchemata = Array.Empty<PropertySchema>();
                RegisterUndoAction(() => {
                    interaction = true;
                    _conditionTypeDrawer.selector.SetSelection(cachedType, false);
                    _childEditors = cachedChildEditors;

                    _targetSchema.children = cachedChildSchemata;
                    _targetSchema.conditionType = cachedType;
                    _targetSchema.argumentSchemata = cachedArguments;
                    _targetSchema.targetSchema = cachedProperty;
                    RegisterRedoAction(() => {
                        _conditionTypeDrawer.selector.SetSelection(newType, false);
                        UpdateConditionTypeAndPushUndoFunction(newType);
                    });
                });
            };

            switch (newType) {
                case ConditionType.Atom:
                    if (_targetSchema.conditionType != ConditionType.Atom) {
                        _targetSchema.targetSchema = new PropertySchema();
                        _targetSchema.conditionType = newType;
                        RegisterUndoAction(() => {
                            interaction = true;
                            _conditionTypeDrawer.selector.SetSelection(cachedType, false);
                            _targetSchema.conditionType = cachedType;
                            _targetSchema.targetSchema = cachedProperty;
                            RegisterRedoAction(() => {
                                _conditionTypeDrawer.selector.SetSelection(newType, false);
                                UpdateConditionTypeAndPushUndoFunction(newType);
                            });
                        });
                    }
                    break;
                case ConditionType.And:
                    if (_targetSchema.conditionType != ConditionType.And) {
                        andOrFunction();
                    }
                    break;
                case ConditionType.Or:
                    if (_targetSchema.conditionType != ConditionType.Or) {
                        andOrFunction();
                    }
                    break;
            }
        }

        public override void NotifyOfChangeInChild(BaseSchema child) {
            interaction = true;
            switch (child) {
                case PropertySchema propertySchema when child.Equals(_targetSchema.targetSchema):
                    Type destinationType = propertySchema.destinationType;
                    var cachedComparisonType = _targetSchema.comparisonType;
                    if (destinationType != typeof(bool)) {
                        if (cachedComparisonType == ComparisonType.False || cachedComparisonType == ComparisonType.True) {
                            _targetSchema.comparisonType = ComparisonType.Equal;
                            _comparisonButtonArray.SetSelection(ComparisonType.Equal);
                        }

                        var cachedMethodDrawer = _methodInfoDrawer;
                        BuildMethodDrawer(destinationType);
                        ValidateParameters();
                        RegisterUndoAction(() => {
                            _methodInfoDrawer = cachedMethodDrawer;
                            _targetSchema.comparisonType = cachedComparisonType;
                            if (cachedComparisonType == ComparisonType.True || cachedComparisonType == ComparisonType.False) {
                                _truthButtonArray.SetSelection(cachedComparisonType);
                            } else {
                                _comparisonButtonArray.SetSelection(cachedComparisonType);
                            }
                            Undo();
                            ValidateParameters();
                            RegisterRedoAction(() => {
                                Redo();
                                ValidateParameters();
                            });
                        });
                    } else {
                        if (cachedComparisonType != ComparisonType.False && cachedComparisonType != ComparisonType.True) {
                            _targetSchema.comparisonType = ComparisonType.True;
                            _truthButtonArray.SetSelection(ComparisonType.True);
                        }

                        var cachedArguments = _targetSchema.argumentSchemata;
                        var cachedMethodDrawer = _methodInfoDrawer;
                        var cachedArgumentEditors = _argumentEditors;
                        _argumentEditors = new List<PropertySchemaEditor>();
                        _targetSchema.argumentSchemata = Array.Empty<PropertySchema>();
                        ValidateParameters();
                        RegisterUndoAction(() => {
                            _methodInfoDrawer = cachedMethodDrawer;
                            _argumentEditors = cachedArgumentEditors;
                            _targetSchema.argumentSchemata = cachedArguments;

                            if (cachedComparisonType != ComparisonType.True && cachedComparisonType != ComparisonType.False) {
                                _targetSchema.comparisonType = cachedComparisonType;
                                _truthButtonArray.SetSelection(cachedComparisonType);
                            }
                            Undo();
                            ValidateParameters();
                            RegisterRedoAction(() => {
                                Redo();
                                ValidateParameters();
                            });
                        });

                    }
                    break;
                case PropertySchema propertySchema when !child.Equals(_targetSchema.targetSchema):
                    ValidateParameters();
                    RegisterUndoAction(() => {
                        Undo();
                        ValidateParameters();
                        RegisterRedoAction(() => {
                            Redo();
                            ValidateParameters();
                        });
                    });
                    break;
                case ConditionSchema conditionSchema:
                    break;
            }
        }
    }
}
