﻿using System;
using System.Collections.Generic;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.GameCode.System.Schemata;

namespace Assets.Code.Editor.Schemata.Editors {
    public abstract class AbstractSchemaEditor<V> : AbstractGUIWrapper<V>, ISchemaEditor where V : BaseSchema {
        protected ISchemaEditor _parent;

        private Stack<Action> _undoActions;
        private Stack<Action> _redoActions;

        protected V _targetSchema;
        protected AbstractSchemaEditor(ISchemaEditor parent) {
            _parent = parent;
            if (_parent == null) {
                _undoActions = new Stack<Action>();
                _redoActions = new Stack<Action>();
            }
        }

        protected bool _interaction;

        public bool interaction {
            get {
                if (_interaction) {
                    _interaction = false;
                    return true;
                }
                return false;
            }
            set {
                if (_parent != null) _parent.interaction = value;
                else _interaction = value;
            }
        }

        private AbstractSchemaEditor() { }

        void ISchemaEditor.RegisterUndoAction(Action undo) {
            if (_parent == null) {
                _undoActions.Push(undo);
            } else {
                _parent.RegisterUndoAction(undo);
            }
        }

        // These are not intended to be used externally, hence the hiding with the interface
        // They are intended to be used by implementing classes to undo
        void ISchemaEditor.RegisterRedoAction(Action redo) {
            if (_parent == null) {
                _redoActions.Push(redo);
            } else {
                _parent.RegisterRedoAction(redo);
            }
        }

        protected void RegisterRedoAction(Action redo) {
            ((ISchemaEditor)this).RegisterRedoAction(redo);
        }

        protected void RegisterUndoAction(Action undo) {
            ((ISchemaEditor)this).RegisterUndoAction(undo);
        }

        protected void ClearUndoStack() {
            ((ISchemaEditor)this).ClearUndoStack();
        }

        protected void ClearRedoStack() {
            ((ISchemaEditor)this).ClearRedoStack();
        }

        public abstract void NotifyOfChangeInChild(BaseSchema child);

        void ISchemaEditor.ClearUndoStack() {
            if (_parent == null) {
                _undoActions.Clear();
            } else {
                _parent.ClearUndoStack();
            }
        }

        void ISchemaEditor.ClearRedoStack() {
            if (_parent == null) {
                _redoActions.Clear();
            } else {
                _parent.ClearRedoStack();
            }
        }

        protected void Undo() {
            if (_parent == null) {
                _undoActions.Pop()();
                interaction = true;
            }
        }

        protected bool UndoStackEmpty() {
            return _undoActions.Count == 0;
        }

        protected bool RedoStackEmpty() {
            return _redoActions.Count == 0;
        }

        protected void Redo() {
            if (_parent == null) {
                _redoActions.Pop()();
                interaction = true;
            }
        }
    }

    public interface ISchemaEditor {
        void RegisterUndoAction(Action undo);
        void RegisterRedoAction(Action redo);
        void ClearUndoStack();
        void ClearRedoStack();
        void NotifyOfChangeInChild(BaseSchema child);
        bool interaction { get; set; }
    }
}
