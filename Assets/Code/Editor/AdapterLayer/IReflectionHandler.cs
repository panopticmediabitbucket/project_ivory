﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System;
using System.Collections.Generic;

namespace Assets.Code.Editor.AdapterLayer {
    public interface IReflectionHandler {
        IComponent GetGenericComponent(List<IComponent> list, Type type);
    }
}
