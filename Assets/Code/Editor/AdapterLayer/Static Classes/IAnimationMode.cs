﻿using Assets.Code.Editor.AdapterLayer.Objects;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IAnimationMode {
        void StartAnimationMode();
        void BeginSampling();
        void EndSampling();
        void StopAnimationMode();
        void SampleAnimationClip(IGameObject gameObject, IAnimationClip animationClip, float elapsedSeconds);
    }
}
