﻿using Assets.Code.Editor.AdapterLayer.Objects;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IAssetPreview {
        ITexture GetMiniThumbnail(IGameObject o);
    }
}
