﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IPopupWindow {
        void Show<T>(IRect displayRect, IPopoutContent<T> content) where T: struct;
    }
}
