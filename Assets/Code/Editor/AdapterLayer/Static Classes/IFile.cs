﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IFileUtil {
        string GetProjectRelativePath(string fullPath);
    }
}
