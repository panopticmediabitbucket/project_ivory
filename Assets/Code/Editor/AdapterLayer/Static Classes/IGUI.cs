﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IGUI {
        IVector2 BeginScrollView(IRect position, IVector2 currentPosition, IRect rect, bool alwaysShowHorizontal, bool alwaysShowVertical);
        void EndScrollView();
        void Box(IRect rect, ITexture tex, IGUIStyle dndBoxStyle);
        void Box(IRect rect, IGUIContent guiContent, IGUIStyle guiStyle);
        void SetNextControlName(string s);
        object Button(IRect rect, ITexture texture);
        object Button(IRect rect, string a, IGUIStyle style);
        object Button(IRect rect, IGUIContent a, IGUIStyle style);
        void Label(IRect rect, string v, IGUIStyle style);
        void Label(IRect rect, IGUIContent content, IGUIStyle style);
        void DrawTexture(IRect rect, ITexture texture);
    }
}
