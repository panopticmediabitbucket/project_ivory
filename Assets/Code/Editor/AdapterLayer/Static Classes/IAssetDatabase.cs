﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IAssetDatabase {
        string GUIDToAssetPath(string x);
        T LoadAssetAtPath<T>(string x);
        string GetAssetPath<T>(T x);
        string AssetPathToGUID(string v);
        string CreateFolder(string parent, string child);
        string[] FindAssets(string filter, string[] folders);
        string GetTextMetaFilePathFromAssetPath(string prefabFilePath);
        void CreateAsset(IAsset firstAsset, string v);
        IEnumerable<T> LoadAllAssetsAtPath<T>(string assetPath);
        void AddObjectToAsset(IAsset asset, string assetPath);
        void SaveAssets();
    }
}
