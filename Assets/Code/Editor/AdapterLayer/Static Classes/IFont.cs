﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IFont {
        object GetFont();
        IFont CreateDynamicFontFromOSFont(string fontName, int size);
        bool GetCharacterInfo(char ch, out ICharacterInfo characterInfo, int size = 0, FontStyle style = FontStyle.Normal);
    }
}
