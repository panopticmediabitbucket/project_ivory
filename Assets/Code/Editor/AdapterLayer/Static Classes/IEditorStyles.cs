﻿using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IEditorStyles {
        IGUIStyle NumberField();
        IGUIStyle MiniLabel();
        IGUIStyle BoldLabel();
        IGUIStyle Popup();
        IGUIStyle ToolbarPopup();
        IGUIStyle MiniButtonLeft();
        IGUIStyle MiniButtonRight();
        IGUIStyle MiniButtonMid();
    }
}
