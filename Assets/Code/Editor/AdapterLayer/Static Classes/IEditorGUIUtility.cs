﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEditor;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IEditorGUIUtility {
        void AddCursorRect(IRect rect, MouseCursor cursor);
    }
}
