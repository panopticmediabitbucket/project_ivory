﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface ISelection {
        IGameObject activeGameObject { get; }
    }
}
