﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IEditorUtility {
        bool DisplayDialog(string title, string message, string ok, string cancel);
        string SaveFilePanel(string message, string defaultPath, string defaultName, string extension);
        string OpenFilePanel(string title, string directory, string extension);
    }
}
