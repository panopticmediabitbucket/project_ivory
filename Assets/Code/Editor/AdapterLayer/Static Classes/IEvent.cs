﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IEvent {
        IEvent current { get; }
        IVector2 mousePosition { get; }
        EventType rawType { get; }
        KeyCode keyCode { get; }
        IVector2 delta { get; }

    }
}
