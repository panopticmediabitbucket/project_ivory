﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IJsonUtility {
        string ToJson(object obj);
        object FromJson(string text, Type type);
    }
}
