﻿using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IHandles {
        IColor color { get; set; }

        void DrawLine(IVector2 vector1, IVector2 vector2);
        void DrawDottedLine(IVector2 vector1, IVector2 vector2, float v);
        void DrawAAConvexPolygon(params IVector2[] vectors);
        void DrawAAPolyLine(params IVector2[] vectors);
        void DrawWireDisc(IVector3 center, IVector3 normal, float radius);
        void DrawSolidDisc(IVector3 center, IVector3 normal, float radius);
    }
}
