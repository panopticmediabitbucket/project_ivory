﻿using Assets.Code.Editor.AdapterLayer.Objects;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IPrefabUtility {
        string GetPrefabAssetPathOfNearestInstanceRoot(IGameObject gameObject);
        PrefabAssetType GetPrefabAssetType(IGameObject activeGameObject);
        void SaveAsPrefabAssetAndConnect(IGameObject gameObject, string relativePath, InteractionMode userAction);
        ITexture GetIconForGameObject(IGameObject gameObject);
    }

    public enum PrefabAssetType {
        NotAPrefab = 0,
        Regular = 1,
        Model = 2,
        Variant = 3,
        MissingAsset = 4
    }

    public enum InteractionMode {
        AutomatedAction = 0,
        UserAction = 1
    }
}
