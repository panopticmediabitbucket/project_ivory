﻿using System;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Static_Classes {
    public interface IEditorGUI {
        void DrawRect(IRect space, IColor _keyWindow);
        object TextField(IRect rect, string a, IGUIStyle guiStyle);
        object Popup(IRect rect, int a, IGUIContent[] guiContent, IGUIStyle guiStyle);
        void LabelField(IRect rect, string a, IGUIStyle style);
        void LabelField(IRect rect, IGUIContent a, IGUIStyle style);
        object ToggleLeft(IRect rect, string a, bool b);
        object ToggleLeft(IRect rect, IGUIContent a, bool b);
        void Warning(IRect rect, string a);
        void Error(IRect rect, string a);
        void DrawRect(IRect rect, IRect size, IColor b);
        object ObjectField(IRect rect, object obj, Type type, bool v);
        float Slider(IRect rect, float value, float leftValue, float rightValue);

    }
}
