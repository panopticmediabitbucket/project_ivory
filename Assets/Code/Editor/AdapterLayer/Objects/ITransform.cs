﻿using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface ITransform {
        object GetTransform();
        IVector3 position { get; set; }
        IQuaternion rotation { get; set; }
    }
}
