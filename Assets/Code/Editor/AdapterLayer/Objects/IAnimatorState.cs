﻿using System.Collections.Generic;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimatorState {
        string name { get; }
        IEnumerable<IAnimatorTransitionBase> transitions { get; }
        IAnimationClip clip { get; }
    }
}
