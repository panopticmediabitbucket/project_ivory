﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface ITexture {
        float width { get; }
        float height { get; }
        object GetTexture();
    }
}
