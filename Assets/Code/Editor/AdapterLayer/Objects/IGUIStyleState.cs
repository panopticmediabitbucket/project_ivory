﻿using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IGUIStyleState {
        object GetState();
        IColor textColor { get; set; }
    }
}