﻿namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IRuntimeAnimatorController {
        object GetRuntimeAnimatorController();
        IAnimationClip[] animationClips { get; }
        IAnimatorControllerParameter[] parameters { get; }
        IAnimatorControllerLayer[] layers { get; }
    }
}