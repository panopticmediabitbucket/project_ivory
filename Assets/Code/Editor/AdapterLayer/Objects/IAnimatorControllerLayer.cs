﻿using UnityEditor.Animations;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimatorControllerLayer {
        IAnimatorStateMachine stateMachine { get; }

        object GetLayer();
    }
}