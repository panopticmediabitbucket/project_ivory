﻿using System.Collections.Generic;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimatorTransitionBase {
        IEnumerable<IAnimatorCondition> conditions { get; }
        IAnimatorState destinationState { get; }
        IAnimatorStateMachine destinationStateMachine { get; }
        bool isExit();
    }
}