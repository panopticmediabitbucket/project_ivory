﻿namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimationClip {
        string name { get; }
        object GetAnimationClip();
        float length { get; }
    }
}