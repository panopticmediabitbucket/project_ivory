﻿using System.Collections.Generic;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IGameObject {
        object GetGameObject();
        T GetComponent<T>();
        string name { get; }

        ITransform transform { get; }
        List<IComponent> GetComponents();
    }
}
