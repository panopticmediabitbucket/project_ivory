﻿namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimatorCondition {
        string parameter { get; }
    }
}