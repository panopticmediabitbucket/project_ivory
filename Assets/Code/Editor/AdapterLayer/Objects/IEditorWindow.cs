﻿using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.Windows.AnimatedAccelerationStates.Adapter {
    public interface IEditorWindow {
        IRect position { get; }
        void Repaint();
    }
}