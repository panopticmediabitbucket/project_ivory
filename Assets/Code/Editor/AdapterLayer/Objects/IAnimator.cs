﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimator {
        object animator { get; }
        IRuntimeAnimatorController runtimeAnimatorController { get; }
    }
}
