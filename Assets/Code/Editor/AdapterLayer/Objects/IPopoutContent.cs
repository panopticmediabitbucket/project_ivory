﻿using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using System;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public delegate void ContentChangeCallback<V>(V content) where V : struct;
    public interface IPopoutContent<T> where T: struct {
        IPopoutDrawerStyle style { get; }
        IPopoutDrawerContentSummary<T> contentSummary { get; }
        int windowHeight { get; }
        void Activate(int width, int height);
        void OnGUI(IRect rect);
        IVector2 GetWindowDimensions();
        void RegisterContentChangeCallback(ContentChangeCallback<T> callback);
        void RemoveContentChangeCallback(ContentChangeCallback<T> callback);

        void OnClose();

        void RegisterOnCloseCallback(Action callback);
    }
}
