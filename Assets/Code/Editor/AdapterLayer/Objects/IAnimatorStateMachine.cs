﻿using System.Collections.Generic;
using UnityEditor.Animations;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimatorStateMachine {
        IEnumerable<IAnimatorStateMachine> stateMachines { get; }
        IEnumerable<IAnimatorState> states { get; }
        IAnimatorState defaultState { get; }
        string name { get; }
        IEnumerable<IAnimatorTransitionBase> entryTransitions();
        List<IAnimatorTransitionBase> GetStateMachineTransitions(IAnimatorStateMachine stateMachine);
        IAnimatorTransitionBase AddStatemachineTransition(IAnimatorStateMachine source, IAnimatorStateMachine destination);
        object GetStateMachine();
    }
}