﻿using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;

namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IGUIStyle {
        object GetGUIStyle();
        void SetHeight(float v);
        void SetFontSize(int v);
        IGUIStyleState normal { get; set; }
        int fontSize { get; set; }
        TextAnchor alignment { get; set; }
        FontStyle fontStyle { get; set; }
        IFont font { get; set; }
        IRect border { get; set; }
    }
    public enum TextAnchor {
        UpperLeft = 0,
        UpperCenter = 1,
        UpperRight = 2,
        MiddleLeft = 3,
        MiddleCenter = 4,
        MiddleRight = 5,
        LowerLeft = 6,
        LowerCenter = 7,
        LowerRight = 8
    }

    public enum FontStyle {
        Normal = 0,
        Bold = 1,
        Italic = 2,
        BoldAndItalic = 3
    }
}
