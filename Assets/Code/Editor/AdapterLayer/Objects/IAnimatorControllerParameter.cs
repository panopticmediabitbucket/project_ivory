﻿namespace Assets.Code.Editor.AdapterLayer.Objects {
    public interface IAnimatorControllerParameter {
        AnimatorControllerParameterType type { get; }
        string name { get; }

        object GetParameter();

    }

    public enum AnimatorControllerParameterType {
        Float = 1,
        Int = 3,
        Bool = 4,
        Trigger = 9
    }
}
