﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Physics.Interfaces;
using System;

namespace Assets.Code.Editor.AdapterLayer {
    public interface IDependencyBuilder {
        IGUIContent NewGUIContent(string text);
        IGUIContent NewGUIContent(ITexture texture, string tooltip);
        IGUIContent NewGUIContent(string v1, string v2);
        IColor NewColor(float r, float g, float b);
        IColor WhiteColor();
        IColor GrayColor();
        IRect NewRect(float x, float y, float width, float height);
        IRect NewRect(IVector2 position, IVector2 height);
        IRect NewRect(IRect rect);
        IGUIStyle NewGUIStyle(IGUIStyle x);
        IGUIStyle NewGUIStyle(object x);
        IGUIStyle NewGUIStyle(params Action<IGUIStyle>[] actions);
        IVector2 NewVector2(float v1, float v2);
        IVector2 NewVector2(IVector2 vector);
        IVector2 ZeroVector();
        IVector3 NewVector3(float x, float y, float z);
        ITextAsset NewTextAsset(string text);
        IRect ZeroRect();
        IStateBasedMotionMachine<TEnum> NewStateBasedMotionMachine<TEnum>(IGameObject gameObject) where TEnum : Enum;
    }
}
