﻿using Assets.Code.Editor.AdapterLayer.Objects;

namespace Assets.Code.Editor.AdapterLayer {
    public interface IGUIContent {
        string Text();
        ITexture texture { get; }
        string tooltip { get; }
        object GetGUIContent();
    }
}
