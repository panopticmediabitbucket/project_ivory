﻿namespace Assets.Code.Editor.AdapterLayer.Structs {
    public interface IVector3 {
        object GetVector();
    }
}
