﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Structs {
    public interface IVector2 {
        object GetVector();
        float x { get; set; }
        float y { get; set; }
        IVector2 normalized { get; }
        IVector2 Add(IVector2 vector2);
        IVector2 Sub(IVector2 vector);
        IVector2 DividedBy(float x);
        IVector2 Mul(float x);
    }
}
