﻿namespace Assets.Code.Editor.AdapterLayer.Structs {
    public interface ICharacterInfo {
        int advance { get; }
        int bearing { get; }
        int glyphHeight { get; }
        int glyphWidth { get; }
        int index { get; }
        int maxX { get; }
        int maxY { get; }
        int minX { get; }
        int minY { get; }

    }
}
