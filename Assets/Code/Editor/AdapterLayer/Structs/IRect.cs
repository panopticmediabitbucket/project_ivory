﻿using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Structs {
    public interface IRect {
        object GetRect();
        bool Contains(IVector2 vector);
        float x { get; set; }
        float y { get; set; }
        float width { get; set; }
        float height { get; set; }
        IVector2 position { get; set; }
        IVector2 size { get; set; }
    }
}
