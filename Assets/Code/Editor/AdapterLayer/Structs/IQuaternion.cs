﻿namespace Assets.Code.Editor.AdapterLayer.Structs {
    public interface IQuaternion {
        object GetQuaternion();
    }
}
