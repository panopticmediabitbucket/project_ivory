﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Structs {
    public interface IColor {
        float r { get; }
        float g { get; }
        float b { get; }

        object GetColor();
        IColor Add(IColor col);
        IColor Scale(float x);
    }
}
