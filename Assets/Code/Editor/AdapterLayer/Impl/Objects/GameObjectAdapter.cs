﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class GameObjectAdapter : IGameObject {
        private GameObject _gameObject;

        public string name => _gameObject.name;

        public ITransform transform { get => new TransformAdapter(_gameObject.transform); }

        public GameObjectAdapter(GameObject gameObject) {
            _gameObject = gameObject;
        }

        public T GetComponent<T>() {
            if (_gameObject == null)
                return default(T);
            if (typeof(T).Equals(typeof(IAnimator))) {
                var component = _gameObject.GetComponent<Animator>();
                if(component != null) {
                    return (T)(object)new AnimatorAdapter(component);
                }
                return default(T);
            }
            return _gameObject.GetComponent<T>();
        }

        public List<IComponent> GetComponents() {
            return _gameObject == null ? new List<IComponent>() : _gameObject.GetComponents(typeof(MonoBehaviour)).Select(x => (IComponent)new ComponentAdapter(x)).ToList();
        }

        public object GetGameObject() {
            return _gameObject;
        }
    }
}
