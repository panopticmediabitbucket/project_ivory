﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class GUIStyleAdapter: IGUIStyle {
        GUIStyle _guiStyle;
        RectOffset _border => _guiStyle.border;

        public GUIStyleAdapter() {
            _guiStyle = new GUIStyle();
        }

        public GUIStyleAdapter(GUIStyle x) {
            _guiStyle = new GUIStyle(x);
        }

        public IGUIStyleState normal { get => new GUIStyleStateAdapter(_guiStyle.normal); set => _guiStyle.normal = (GUIStyleState)value.GetState(); }
        public int fontSize { get => _guiStyle.fontSize; set => _guiStyle.fontSize = value; }
        public AdapterLayer.Objects.TextAnchor alignment { get => (AdapterLayer.Objects.TextAnchor)(int)_guiStyle.alignment; set => _guiStyle.alignment = (UnityEngine.TextAnchor)(int)value; }
        public AdapterLayer.Objects.FontStyle fontStyle { get => (AdapterLayer.Objects.FontStyle)(int)_guiStyle.fontStyle; set => _guiStyle.fontStyle = (UnityEngine.FontStyle)(int)value; }
        public IFont font { get => new FontAdapter(_guiStyle.font); set { _guiStyle.font = (Font)value.GetFont(); } }
        public IRect border { get => new RectAdapter(_border.left, _border.right, _border.top, _border.bottom); set => _guiStyle.border = new RectOffset((int)value.x, (int)value.y, (int)value.width, (int)value.height); }

        public object GetGUIStyle() {
            return _guiStyle;
        }

        public void SetFontSize(int v) {
            _guiStyle.fontSize = v;
        }

        public void SetHeight(float v) {
            _guiStyle.fixedHeight = v;
        }
    }
}
