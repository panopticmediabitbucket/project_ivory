﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System;
using System.Collections.Generic;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class ReflectionHandlerAdapter : AbstractSingletonAdapter<IReflectionHandler, ReflectionHandlerAdapter>, IReflectionHandler {
        public IComponent GetGenericComponent(List<IComponent> list, Type type) {
            return list.Find(x => x.GetComponent().GetType().BaseType.GetGenericTypeDefinition().Equals(type));
        }
    }
}
