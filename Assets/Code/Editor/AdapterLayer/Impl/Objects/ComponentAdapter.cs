﻿using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class ComponentAdapter: IComponent {
        private Component _component;

        public object GetComponent() {
            return _component;
        }

        public ComponentAdapter(Component component) {
            _component = component;
        }
    }
}
