﻿using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEditor.Animations;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public struct AnimatorStateAdapter : IAnimatorState {
        private AnimatorState _state;

        public AnimatorStateAdapter(AnimatorState state) {
            this._state = state;
        }

        public IEnumerable<IAnimatorTransitionBase> transitions => _state.transitions.Select(x => (IAnimatorTransitionBase)new AnimatorTransitionBaseAdapter(x));

        public string name => _state.name;

        public IAnimationClip clip => new AnimationClipAdapter((AnimationClip)_state.motion);
    }
}
