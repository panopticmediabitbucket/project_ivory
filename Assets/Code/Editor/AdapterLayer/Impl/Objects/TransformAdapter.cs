﻿using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class TransformAdapter : ITransform {
        private Transform _transform;

        public TransformAdapter(Transform transform) {
            _transform = transform;
        }

        public IVector3 position { get => new Vector3Adapter(_transform.position); set => _transform.position = (Vector3)value.GetVector(); }
        public IQuaternion rotation { get => new QuaternionAdapter(_transform.rotation); set => _transform.rotation = (Quaternion)value.GetQuaternion(); }

        public object GetTransform() {
            return _transform;
        }
    }
}
