﻿using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class AnimatorControllerParameterAdapter : IAnimatorControllerParameter {
        private AnimatorControllerParameter _parameter;

        public AnimatorControllerParameterAdapter(AnimatorControllerParameter parameter) {
            this._parameter = parameter;
        }

        public AdapterLayer.Objects.AnimatorControllerParameterType type => (AdapterLayer.Objects.AnimatorControllerParameterType)_parameter.type;

        public string name => _parameter.name;

        public object GetParameter() {
            return _parameter;
        }
    }
}
