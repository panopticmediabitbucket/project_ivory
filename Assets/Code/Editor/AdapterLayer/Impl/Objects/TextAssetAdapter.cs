﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class TextAssetAdapter : ITextAsset {
        private TextAsset _textAsset;

        public TextAssetAdapter(string text) {
            _textAsset = new TextAsset(text);
        }
        public object GetAsset() {
            return _textAsset;
        }

        public void SetName(string name) {
            _textAsset.name = name;
        }

        public string Text() {
            return _textAsset.text;
        }
    }
}
