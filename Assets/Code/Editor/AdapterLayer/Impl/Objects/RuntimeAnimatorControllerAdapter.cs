﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System.Linq;
using UnityEditor.Animations;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class RuntimeAnimatorControllerAdapter : IRuntimeAnimatorController {
        private AnimatorController _runtime;

        public RuntimeAnimatorControllerAdapter(RuntimeAnimatorController runtime) {
            _runtime = (AnimatorController)runtime;
        }

        public IAnimationClip[] animationClips => _runtime.animationClips.Select(x => new AnimationClipAdapter(x)).ToArray();

        public IAnimatorControllerParameter[] parameters => _runtime.parameters.Select(x => new AnimatorControllerParameterAdapter(x)).ToArray();

        public IAnimatorControllerLayer[] layers => _runtime.layers.Select(x => new AnimatorControllerLayerAdapter(x)).ToArray();

        public object GetRuntimeAnimatorController() {
            return _runtime;
        }
    }
}
