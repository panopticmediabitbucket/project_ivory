﻿using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class GUIStyleStateAdapter : IGUIStyleState {
        private GUIStyleState _state;

        public GUIStyleStateAdapter(GUIStyleState state) {
            _state = state;
        }
        public IColor textColor 
            { get { Color col = _state.textColor; return new ColorAdapter(col.r, col.g, col.b); } set => _state.textColor = (Color)value.GetColor(); }

        public object GetState() {
            return _state;
        }
    }
}
