﻿using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEditor.Animations;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public struct AnimatorConditionAdapter : IAnimatorCondition {
        private AnimatorCondition _condition;

        public AnimatorConditionAdapter(AnimatorCondition condition) {
            this._condition = condition;
        }

        public string parameter => _condition.parameter;
    }
}