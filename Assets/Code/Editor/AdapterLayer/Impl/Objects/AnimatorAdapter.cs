﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class AnimatorAdapter : IAnimator {
        private Animator _animator;
        private IRuntimeAnimatorController _controller;

        public AnimatorAdapter(Animator animator) {
            _animator = animator;
            _controller = new RuntimeAnimatorControllerAdapter(animator.runtimeAnimatorController);
        }

        public IRuntimeAnimatorController runtimeAnimatorController => _controller;

        public object animator => _animator;
    }
}
