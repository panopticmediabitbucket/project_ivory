﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using UnityEditor;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class PopoutContentAdapter<T> : PopupWindowContent where T : struct {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        readonly IPopoutContent<T> _content;

        private ContentChangeCallback<T> _closeCallback;
        public PopoutContentAdapter(IPopoutContent<T> content) {
            _content = content;
        }

        public override void OnOpen() {
            editorWindow.wantsMouseMove = true;
            _closeCallback = item => editorWindow.Close();
            _content.RegisterContentChangeCallback(_closeCallback);
            base.OnOpen();
        }

        public override Vector2 GetWindowSize() {
            return (Vector2)_content.GetWindowDimensions().GetVector();
        }

        public override void OnGUI(Rect rect) {
            _content.OnGUI(DB.NewRect(rect.x, rect.y, rect.width, rect.height));
            if (Event.current.rawType.Equals(EventType.MouseMove) || Event.current.rawType.Equals(EventType.MouseDown)) {
                editorWindow.Repaint();
            }
        }

        public override void OnClose() {
            _content.OnClose();
            _content.RegisterContentChangeCallback(_closeCallback);
            base.OnClose();
        }
    }
}
