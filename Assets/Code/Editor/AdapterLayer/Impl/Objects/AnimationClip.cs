﻿using Assets.Code.Editor.AdapterLayer.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class AnimationClipAdapter : IAnimationClip {
        private AnimationClip _animationClip;
        public string name => _animationClip.name;
        public float length
        {
            get
            {
                return _animationClip.length;
            }
        }   

public AnimationClipAdapter(AnimationClip animationClip) {
            _animationClip = animationClip;
        }
        public object GetAnimationClip() {
            return _animationClip;
        }
    }
}
