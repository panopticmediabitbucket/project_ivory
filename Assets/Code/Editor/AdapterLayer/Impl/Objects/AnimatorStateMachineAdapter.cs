﻿using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEditor.Animations;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    internal class AnimatorStateMachineAdapter : IAnimatorStateMachine {
        private AnimatorStateMachine _stateMachine;

        public AnimatorStateMachineAdapter(AnimatorStateMachine stateMachine) {
            this._stateMachine = stateMachine;
        }

        public IEnumerable<IAnimatorStateMachine> stateMachines => _stateMachine.stateMachines.Select(x => new AnimatorStateMachineAdapter(x.stateMachine));

        public IEnumerable<IAnimatorState> states => _stateMachine.states.Select(x => (IAnimatorState)new AnimatorStateAdapter(x.state));
        public IAnimatorState defaultState => _stateMachine.defaultState == null ? (IAnimatorState)null : new AnimatorStateAdapter(_stateMachine.defaultState);
        public string name => _stateMachine.name; 

        public IEnumerable<IAnimatorTransitionBase> entryTransitions() {
            return _stateMachine.entryTransitions.Select(x => (IAnimatorTransitionBase)new AnimatorTransitionBaseAdapter(x)).ToList();
        }


        public List<IAnimatorTransitionBase> GetStateMachineTransitions(IAnimatorStateMachine stateMachine) {
            return _stateMachine.GetStateMachineTransitions((AnimatorStateMachine)stateMachine.GetStateMachine()).Select(x => (IAnimatorTransitionBase)new AnimatorTransitionBaseAdapter(x)).ToList();
        }

        public IAnimatorTransitionBase AddStatemachineTransition(IAnimatorStateMachine source, IAnimatorStateMachine destination) {
            return new AnimatorTransitionBaseAdapter(_stateMachine.AddStateMachineTransition((AnimatorStateMachine)source.GetStateMachine(), (AnimatorStateMachine)destination.GetStateMachine()));
        }

        public object GetStateMachine() {
            return _stateMachine; 
        }
    }
}