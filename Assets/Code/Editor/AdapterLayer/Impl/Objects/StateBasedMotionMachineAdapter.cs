﻿using Assets.Code.Physics;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Interfaces;
using Assets.Code.System.Utilities;
using System;
using Assets.Code.System.TimeUtil;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class StateBasedMotionMachineAdapter<TEnum> : IStateBasedMotionMachine<TEnum> where TEnum : Enum {
        private readonly StateBasedMotionMachine<TEnum> _stateBasedMotionMachine;

        public StateBasedMotionMachineAdapter(StateBasedMotionMachine<TEnum> stateBasedMotionMachine) {
            _stateBasedMotionMachine = stateBasedMotionMachine;
        }
        public ITimeZone timeZone { get => _stateBasedMotionMachine.timeZone; set => _stateBasedMotionMachine.timeZone = value; }

        public void DeliverInternalAccel(Accel<TEnum> accel) {
            _stateBasedMotionMachine.DeliverInternalAccel(accel);
        }

        public void DeliverExternalAccel(Accel<External> accel) {
            _stateBasedMotionMachine.DeliverExternalAccel(accel);
        }


        public void FlagInternalForInterrupt(TEnum state) {
            _stateBasedMotionMachine.FlagInternalForInterrupt(state);
        }

        public void InterruptExternal(External external) {
            _stateBasedMotionMachine.InterruptExternal(external);
        }

        public bool IsActive(TEnum state) {
            return _stateBasedMotionMachine.IsActive(state);
        }

        public void ResetMomentum() {
            _stateBasedMotionMachine.ResetMomentum();
        }

        public void SetInternalMomentum(float x, float y) {
            _stateBasedMotionMachine.SetInternalMomentum(x, y);
        }

        public Vector2 CalculateFrameMotionAndUpdateMomentum() {
            return _stateBasedMotionMachine.CalculateFrameMotionAndUpdateMomentum();
        }

        public GameObject gameObject { get => _stateBasedMotionMachine.gameObject; }
        public Vector2 momentum { get => _stateBasedMotionMachine.momentum; }
        public Vector2 voluntaryMomentum { get => _stateBasedMotionMachine.voluntaryMomentum; }
        public Vector2 involuntaryMomentum { get => _stateBasedMotionMachine.involuntaryMomentum; }
    }
}
