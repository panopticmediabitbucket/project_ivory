﻿using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEditor.Animations;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public struct AnimatorTransitionBaseAdapter : IAnimatorTransitionBase {
        private AnimatorTransitionBase _transition;

        public AnimatorTransitionBaseAdapter(AnimatorTransitionBase transition) {
            this._transition = transition;
        }

        public IEnumerable<IAnimatorCondition> conditions => _transition.conditions.Select(x => (IAnimatorCondition)(new AnimatorConditionAdapter(x)));

        public IAnimatorState destinationState => _transition.destinationState == null ? (IAnimatorState)null : new AnimatorStateAdapter(_transition.destinationState);
        public IAnimatorStateMachine destinationStateMachine => _transition.destinationStateMachine == null ? (IAnimatorStateMachine)null : new AnimatorStateMachineAdapter(_transition.destinationStateMachine); 

        public bool isExit() {
            return _transition.isExit; 
        }
    }
}