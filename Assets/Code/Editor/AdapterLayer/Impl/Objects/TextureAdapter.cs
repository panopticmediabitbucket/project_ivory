﻿using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class TextureAdapter: ITexture{
        private Texture _texture;

        public TextureAdapter(Texture texture) {
            _texture = texture;
        }

        public float width => _texture.width;
        public float height => _texture.height; 

        public object GetTexture() {
            return _texture;
        }
    }
}
