﻿using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEditor.Animations;

namespace Assets.Code.Editor.AdapterLayer.Impl.Objects {
    public class AnimatorControllerLayerAdapter: IAnimatorControllerLayer {
        private AnimatorControllerLayer _layer;

        public AnimatorControllerLayerAdapter(AnimatorControllerLayer layer) {
            this._layer = layer;
        }

        public IAnimatorStateMachine stateMachine => new AnimatorStateMachineAdapter(_layer.stateMachine);

        public object GetLayer() {
            return _layer;
        }
    }
}