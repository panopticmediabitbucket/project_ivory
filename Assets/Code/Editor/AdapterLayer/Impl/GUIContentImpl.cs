﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Objects;
using UnityEngine;

namespace Assets.Code.Editor.InterfaceAdapterLayer.Impl {
    public class GUIContentAdapter : IGUIContent {

        GUIContent _guiContent;

        public GUIContentAdapter(string text) {
            _guiContent = new GUIContent(text);
        }

        public GUIContentAdapter(string v1, string v2) {
            _guiContent = new GUIContent(v1, v2);
        }

        public GUIContentAdapter(ITexture texture, string tooltip)
        {
            _guiContent = new GUIContent((Texture)texture.GetTexture(), tooltip);
        }

        public string tooltip { get => _guiContent.tooltip; }

        public object GetGUIContent() {
            return _guiContent;
        }

        public string Text() {
            return _guiContent.text;
        }

        public ITexture texture { get => new TextureAdapter(_guiContent.image); }
    }
}
