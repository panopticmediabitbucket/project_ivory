﻿using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Physics.Interfaces;
using UnityEditor;
using UnityEngine;

namespace Assets.Code.Editor.InterfaceAdapterLayer.Impl {
    public class DependencyBuilder : IDependencyBuilder {
        public IGUIContent NewGUIContent(string text) {
            return new GUIContentAdapter(text);
        }

        public IGUIContent NewGUIContent(ITexture texture, string tooltip)
        {
            return new GUIContentAdapter(texture, tooltip);
        }

        public IGUIContent NewGUIContent(string v1, string v2) {
            return new GUIContentAdapter(v1, v2);
        }

        public IColor NewColor(float r, float g, float b) {
            return new ColorAdapter(r, g, b);
        }

        public IRect NewRect(float x, float y, float width, float height) {
            return new RectAdapter(x, y, width, height);
        }

        public IGUIStyle NewGUIStyle(IGUIStyle x) {
            return new GUIStyleAdapter((GUIStyle)x.GetGUIStyle());
        }

        public IGUIStyle NewGUIStyle(object x) {
            return new GUIStyleAdapter((GUIStyle)x);
        }

        public IRect NewRect(IVector2 position, IVector2 height) {
            return new RectAdapter(position, height);
        }

        public IVector2 NewVector2(float v1, float v2) {
            return new Vector2Adapter(v1, v2);
        }

        public IRect NewRect(IRect rect) {
            return new RectAdapter((Rect)rect.GetRect());
        }

        public IVector2 ZeroVector() {
            return new Vector2Adapter(0, 0);
        }

        public ITextAsset NewTextAsset(string text) {
            return new TextAssetAdapter(text);
        }

        public IGUIStyle NewGUIStyle(params Action<IGUIStyle>[] actions) {
            var guiStyle = new GUIStyleAdapter();
            foreach(var action in actions) {
                action(guiStyle);
            }
            return guiStyle;
        }

        ColorAdapter _white = new ColorAdapter(1, 1, 1);
        public IColor WhiteColor() {
            return _white;
        }

        public IColor GrayColor() {
            return new ColorAdapter(.4f, .4f, .4f);
        }

        public IRect ZeroRect() {
            return new RectAdapter(0, 0, 0, 0);
        }

        public IVector2 NewVector2(IVector2 vector) {
            return new Vector2Adapter(vector);
        }

        public IStateBasedMotionMachine<TEnum> NewStateBasedMotionMachine<TEnum>(IGameObject gameObject) where TEnum: Enum {
            return new StateBasedMotionMachineAdapter<TEnum>(new Physics.StateBasedMotionMachine<TEnum>((GameObject)gameObject.GetGameObject()));
        }

        public IVector3 NewVector3(float x, float y, float z) {
            return new Vector3Adapter(x, y, z);
        }
    }
}
