﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Impl {
    public abstract class AbstractSingletonAdapter<T, V> where V: class, T, new() {
        private static T _instance;
        public static T Instance { get {
                if((object)default(T) == (object)_instance) {
                    _instance = new V();
                }
                return _instance;
            } set {
                // To enforce that this is only for testing
                _instance = value;
            }
        }
    }
}
