﻿using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Structs {
    public struct Vector2Adapter : IVector2 {
        Vector2 _vector;

        public Vector2Adapter(float x, float y) {
            _vector = new Vector2(x, y);
        }

        public Vector2Adapter(Vector2 vector2) {
            _vector = vector2;
        }

        public Vector2Adapter(IVector2 vector2) {
            _vector = new Vector2(vector2.x, vector2.y);
        }

        public float x { get => _vector.x; set => _vector.x = value; }
        public float y { get => _vector.y; set => _vector.y = value; }

        public IVector2 normalized => new Vector2Adapter(_vector.normalized);

        public IVector2 Add(IVector2 vector2) {
            var vec = _vector + (Vector2)vector2.GetVector();
            return new Vector2Adapter(vec.x, vec.y);
        }

        public object GetVector() {
            return _vector;
        }

        public float GetX() {
            return _vector.x;
        }

        public float GetY() {
            return _vector.y;
        }

        public void SetX(float x) {
            _vector.x = x;
        }

        public void SetY(float y) {
            _vector.y = y;
        }

        public IVector2 Sub(IVector2 vector) {
            var vec = _vector - (Vector2)vector.GetVector();
            return new Vector2Adapter(vec);
        }

        public IVector2 DividedBy(float x) {
            return new Vector2Adapter(_vector / x);
        }

        public override bool Equals(object obj) {
            Vector2Adapter other = (Vector2Adapter)obj;
            return this.x == other.x && this.y == other.y;
        }

        public IVector2 Mul(float x) {
            return new Vector2Adapter(_vector * x);
        }
    }
}
