﻿using Assets.Code.Editor.AdapterLayer.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Structs {
    public struct ColorAdapter: IColor {
        Color _color;

        public float r => _color.r;

        public float g => _color.g;

        public float b => _color.b;

        public ColorAdapter(float r, float g, float b) {
            _color = new Color(r, g, b);
        }

        public ColorAdapter(Color color) {
            _color = color;
        }

        public object GetColor() {
            return _color;
        }

        public IColor Add(IColor col) {
            Color x = (Color)GetColor() + (Color)col.GetColor();
            return new ColorAdapter(x);
        }

        public IColor Scale(float x) {
            return new ColorAdapter(x * r, x * g, x * b);
        }
    }
}
