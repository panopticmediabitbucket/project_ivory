﻿using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Structs {
    public struct Vector3Adapter : IVector3 {
        private Vector3 _vector;

        public object GetVector() {
            return _vector;
        }
        public Vector3Adapter(Vector3 vector3) {
            _vector = vector3;
        }
        public Vector3Adapter(float x, float y, float z) {
            _vector = new Vector3(x, y, z);
        }
    }
}
