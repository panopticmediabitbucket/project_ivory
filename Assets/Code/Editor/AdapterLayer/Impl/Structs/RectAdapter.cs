﻿using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Structs {
    public struct RectAdapter : IRect {
        Rect _rect;

        public float x { get => _rect.x; set => _rect.x = value; }
        public float y { get => _rect.y; set => _rect.y = value; }
        public float width { get => _rect.width; set => _rect.width = value; }
        public float height { get => _rect.height; set => _rect.height = value; }
        public IVector2 position { get => new Vector2Adapter(_rect.position); set => _rect.position = (Vector2)value.GetVector(); }
        public IVector2 size { get => new Vector2Adapter(_rect.size); set => _rect.size = (Vector2)value.GetVector(); }

        public RectAdapter(float x, float y, float width, float height) {
            _rect = new Rect(x, y, width, height);
        }

        public RectAdapter(IVector2 position, IVector2 size) {
            _rect = new Rect((Vector2)position.GetVector(), (Vector2)size.GetVector());
        }

        public RectAdapter(Rect rect) : this() {
            _rect = rect;
        }

        public object GetRect() {
            return _rect;
        }

        public void SetHeight(float height) {
            _rect.height = height;
        }

        public float GetHeight() {
            return _rect.height;
        }

        public void SetWidth(float width) {
            _rect.width = width;
        }

        public float GetWidth() {
            return _rect.width;
        }

        public void SetX(float x) {
            _rect.x = x;
        }

        public float GetX() {
            return _rect.x;
        }

        public void SetY(float y) {
            _rect.y = y;
        }

        public float GetY() {
            return _rect.y;
        }

        public IVector2 Position() {
            return new Vector2Adapter(_rect.x, _rect.y);
        }

        public IVector2 Size() {
            return new Vector2Adapter(_rect.width, _rect.height);
        }

        public void SetPosition(IVector2 vector2) {
            _rect.position = (Vector2)vector2.GetVector();
        }

        public bool Contains(IVector2 vector) {
            return _rect.Contains((Vector2)vector.GetVector());
        }
    }
}
