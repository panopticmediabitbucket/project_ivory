﻿using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEditorInternal;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Structs {
    public struct CharacterInfoAdapter : ICharacterInfo {
        CharacterInfo _info;
        public CharacterInfoAdapter(CharacterInfo info) {
            _info = info;
        }

        public int advance => _info.advance;
        public int bearing => _info.bearing;
        public int glyphHeight => _info.glyphHeight;
        public int glyphWidth => _info.glyphWidth;
        public int index => _info.index;
        public int maxX => _info.maxX;
        public int maxY => _info.maxY;
        public int minX => _info.minX;
        public int minY => _info.minY;
    }
}
