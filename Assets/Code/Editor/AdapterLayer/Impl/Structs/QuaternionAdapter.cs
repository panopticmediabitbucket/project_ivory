﻿using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Structs {
    public struct QuaternionAdapter : IQuaternion {
        private Quaternion _quaternion;

        public QuaternionAdapter(Quaternion quaternion) {
            _quaternion = quaternion;
        }

        public object GetQuaternion() {
            return _quaternion;
        }
    }
}
