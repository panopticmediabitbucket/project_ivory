﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using System;
using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.Utilities;
using UnityEditor;
using UnityEditor.VersionControl;
using UnityEngine;
using InteractionMode = UnityEditor.InteractionMode;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class PrefabUtilityAdapter : AbstractSingletonAdapter<IPrefabUtility, PrefabUtilityAdapter>, IPrefabUtility {
        public string GetPrefabAssetPathOfNearestInstanceRoot(IGameObject gameObject) {
            return PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot((GameObject)gameObject.GetGameObject());
        }

        public AdapterLayer.Static_Classes.PrefabAssetType GetPrefabAssetType(IGameObject activeGameObject) {
            return (AdapterLayer.Static_Classes.PrefabAssetType)(int)PrefabUtility.GetPrefabAssetType((GameObject)activeGameObject.GetGameObject());
        }

        public void SaveAsPrefabAssetAndConnect(IGameObject gameObject, string relativePath, AdapterLayer.Static_Classes.InteractionMode userAction) {
            PrefabUtility.SaveAsPrefabAssetAndConnect((GameObject)gameObject.GetGameObject(), relativePath, (UnityEditor.InteractionMode)(int)userAction);
        }

        public ITexture GetIconForGameObject(IGameObject gameObject) {
            return new TextureAdapter(PrefabUtility.GetIconForGameObject(gameObject == null ? AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath("96f99c695e0cda74e82f1e808ea73bf2")) 
                : (GameObject)gameObject.GetGameObject()));
        }

        public void ApplyPrefabInstance(IGameObject gameObject, AdapterLayer.Static_Classes.InteractionMode userAction)
        {
            PrefabUtility.ApplyPrefabInstance((GameObject)gameObject.GetGameObject(), (UnityEditor.InteractionMode)(int)userAction);
        }
    }
}
