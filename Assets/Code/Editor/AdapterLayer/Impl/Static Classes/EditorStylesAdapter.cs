﻿using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using UnityEditor;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class EditorStylesAdapter : AbstractSingletonAdapter<IEditorStyles, EditorStylesAdapter>, IEditorStyles {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        public IGUIStyle NumberField() {
            return new GUIStyleAdapter(EditorStyles.numberField);
        }

        public IGUIStyle MiniLabel() {
            return new GUIStyleAdapter(EditorStyles.miniLabel);
        }

        public IGUIStyle BoldLabel() {
            return new GUIStyleAdapter(EditorStyles.boldLabel);
        }

        public IGUIStyle Popup() {
            return new GUIStyleAdapter(EditorStyles.popup);
        }

        public IGUIStyle MiniButtonLeft() {
            return new GUIStyleAdapter(EditorStyles.miniButtonLeft);
        }

        public IGUIStyle MiniButtonRight() {
            return new GUIStyleAdapter(EditorStyles.miniButtonRight);
        }

        public IGUIStyle MiniButtonMid() {
            return new GUIStyleAdapter(EditorStyles.miniButtonMid);
        }

        public IGUIStyle ToolbarPopup() {
            return new GUIStyleAdapter(EditorStyles.toolbarPopup);
        }
    }
}
