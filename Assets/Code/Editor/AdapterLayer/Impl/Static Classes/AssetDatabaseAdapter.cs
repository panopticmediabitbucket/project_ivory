﻿using System.Collections.Generic;
using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class AssetDatabaseAdapter: AbstractSingletonAdapter<IAssetDatabase, AssetDatabaseAdapter>, IAssetDatabase {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        public void AddObjectToAsset(IAsset asset, string assetPath) {
            AssetDatabase.AddObjectToAsset((Object)asset.GetAsset(), assetPath);
        }

        public string AssetPathToGUID(string v) {
            return AssetDatabase.AssetPathToGUID(v);
        }

        public void CreateAsset(IAsset firstAsset, string v) {
            AssetDatabase.CreateAsset((Object)firstAsset.GetAsset(), v);
        }

        public string CreateFolder(string parentFolder, string newFolder) {
            return AssetDatabase.CreateFolder(parentFolder, newFolder);
        }

        public string[] FindAssets(string filter, string[] folders)
        {
            return AssetDatabase.FindAssets(filter, folders);
        }

        public string GetAssetPath<T>(T x) {

            return AssetDatabase.GetAssetPath((UnityEngine.Object)(object)x);
        }

        public string GetTextMetaFilePathFromAssetPath(string prefabFilePath) {
            return AssetDatabase.GetTextMetaFilePathFromAssetPath(prefabFilePath);
        }

        public string GUIDToAssetPath(string x) {
            return AssetDatabase.GUIDToAssetPath(x);
        }

        public IEnumerable<T> LoadAllAssetsAtPath<T>(string assetPath) {
            var list = AssetDatabase.LoadAllAssetsAtPath(assetPath).ToList().Cast<object>();
            if (typeof(T).Equals(typeof(ITextAsset))) {
                foreach(var x in list) {
                    yield return (T)DB.NewTextAsset(((TextAsset)x).text);
                }
            }
        }

        public T LoadAssetAtPath<T>(string x) {
            if (typeof(T).Equals(typeof(ITexture))){
                return (T)(object)new TextureAdapter((Texture)AssetDatabase.LoadAssetAtPath(x, typeof(Texture)));
            }
            return (T)(object)AssetDatabase.LoadAssetAtPath(x, typeof(UnityEngine.Object));
        }

        public void SaveAssets() {
            AssetDatabase.SaveAssets();
        }
    }
}
