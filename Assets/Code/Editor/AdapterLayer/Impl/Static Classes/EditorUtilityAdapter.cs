﻿using Assets.Code.Editor.AdapterLayer.Static_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class EditorUtilityAdapter : AbstractSingletonAdapter<IEditorUtility, EditorUtilityAdapter>, IEditorUtility {
        public bool DisplayDialog(string title, string message, string ok, string cancel) {
            return EditorUtility.DisplayDialog(title, message, ok, cancel);
        }

        public string SaveFilePanel(string message, string defaultPath, string defaultName, string extension) {
            return EditorUtility.SaveFilePanel(message, defaultPath, defaultName, extension);
        }

        public string OpenFilePanel(string title, string directory, string extension)
        {
            return EditorUtility.OpenFilePanel(title, directory, extension);
        }
    }
}
