﻿using Assets.Code.Editor.AdapterLayer.Static_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class JsonUtilityAdapter : AbstractSingletonAdapter<IJsonUtility, JsonUtilityAdapter>, IJsonUtility {
        public object FromJson(string text, Type type) {
            return JsonUtility.FromJson(text, type);
        }

        public string ToJson(object obj) {
            return JsonUtility.ToJson(obj);
        }
    }
}
