﻿using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class AssetPreviewAdapter : AbstractSingletonAdapter<IAssetPreview, AssetPreviewAdapter>, IAssetPreview {

        public ITexture GetMiniThumbnail(IGameObject o) {
            return new TextureAdapter(AssetPreview.GetMiniThumbnail((UnityEngine.Object) o.GetGameObject()));
        }
    }
}
