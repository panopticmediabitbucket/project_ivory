﻿using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class AssetPathUtilAdapter : AbstractSingletonAdapter<IAssetPathUtil, AssetPathUtilAdapter>, IAssetPathUtil {
        public string GetAssetFolderPath(string filePath) {
            return AssetPathUtil.GetAssetFolderPath(filePath);
        }
    }
}
