﻿using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class HandlesAdapter : AbstractSingletonAdapter<IHandles, HandlesAdapter>, IHandles {
        public IColor color { get => new ColorAdapter(Handles.color); set => Handles.color = new Color(value.r, value.g, value.b); }

        public void DrawAAConvexPolygon(params IVector2[] vectors) {
            Handles.DrawAAConvexPolygon(vectors.Select(x => (Vector3)(Vector2)x.GetVector()).ToArray());
        }

        public void DrawAAPolyLine(params IVector2[] vectors) {
            Handles.DrawAAPolyLine(vectors.Select(x => (Vector3)(Vector2)x.GetVector()).ToArray());
        }

        public void DrawDottedLine(IVector2 vector1, IVector2 vector2, float v) {
            Handles.DrawDottedLine((Vector2)vector1.GetVector(), (Vector2)vector2.GetVector(), v);
        }

        public void DrawLine(IVector2 vector1, IVector2 vector2) {
            Handles.DrawLine((Vector2)vector1.GetVector(), (Vector2)vector2.GetVector());
        }

        public void DrawSolidDisc(IVector3 center, IVector3 normal, float radius) {
            Handles.DrawSolidDisc((Vector3)center.GetVector(), (Vector3)normal.GetVector(), radius);
        }

        public void DrawWireDisc(IVector3 center, IVector3 normal, float radius) {
            Handles.DrawWireDisc((Vector3)center.GetVector(), (Vector3)normal.GetVector(), radius);
        }
    }
}
