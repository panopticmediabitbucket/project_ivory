﻿using System.IO;
using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;
using FontStyle = Assets.Code.Editor.AdapterLayer.Objects.FontStyle;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class FontAdapter : AbstractSingletonAdapter<IFont, FontAdapter>, IFont {
        private UnityEngine.Font _font;

        public int fontSize => _font.fontSize;
        public FontAdapter() {
            _font = UnityEngine.Resources.GetBuiltinResource<Font>("Arial.ttf");
        }
        public FontAdapter(UnityEngine.Font font) {
            _font = font != null ? font : UnityEngine.Resources.GetBuiltinResource<Font>("Arial.ttf");
        }

        public IFont CreateDynamicFontFromOSFont(string fontName, int size) {
            return new FontAdapter(UnityEngine.Font.CreateDynamicFontFromOSFont(fontName, size));
        }

        public bool GetCharacterInfo(char ch, out ICharacterInfo characterInfo, int size = 0, FontStyle style = FontStyle.Normal) {
            bool retVal = _font.GetCharacterInfo(ch, out UnityEngine.CharacterInfo info, size, (UnityEngine.FontStyle)(object)style);
            characterInfo = new CharacterInfoAdapter(info);
            return retVal;
        }

        public object GetFont() {
            return _font;
        }
    }
}
