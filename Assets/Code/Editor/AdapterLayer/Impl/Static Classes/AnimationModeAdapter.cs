﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using UnityEditor;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class AnimationModeAdapter : AbstractSingletonAdapter<IAnimationMode, AnimationModeAdapter>, IAnimationMode {
        public void BeginSampling() {
            AnimationMode.BeginSampling();
        }

        public void EndSampling() {
            AnimationMode.EndSampling();
        }

        public void SampleAnimationClip(IGameObject gameObject, IAnimationClip animationClip, float elapsedSeconds) {
            AnimationMode.SampleAnimationClip((GameObject)gameObject.GetGameObject(), (AnimationClip)animationClip.GetAnimationClip(), elapsedSeconds);
        }

        public void StartAnimationMode() {
            AnimationMode.StartAnimationMode();
        }

        public void StopAnimationMode() {
            AnimationMode.StopAnimationMode();
        }
    }
}
