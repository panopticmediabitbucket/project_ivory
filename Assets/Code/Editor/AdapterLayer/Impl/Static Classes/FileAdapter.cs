﻿using Assets.Code.Editor.AdapterLayer.Static_Classes;
using UnityEditor;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class FileUtilAdapter : AbstractSingletonAdapter<IFileUtil, FileUtilAdapter>, IFileUtil {
        public string GetProjectRelativePath(string fullPath) {
            return FileUtil.GetProjectRelativePath(fullPath);
        }
    }
}
