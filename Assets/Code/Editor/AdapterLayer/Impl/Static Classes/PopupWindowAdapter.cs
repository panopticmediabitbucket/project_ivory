﻿using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEditor;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class PopupWindowAdapter : AbstractSingletonAdapter<IPopupWindow, PopupWindowAdapter>, IPopupWindow {
        public void Show<T>(IRect displayRect, IPopoutContent<T> content) where T: struct {
            PopoutContentAdapter<T> adapter = new PopoutContentAdapter<T>(content);
            PopupWindow.Show((Rect)displayRect.GetRect(), adapter);
        }
    }
}
