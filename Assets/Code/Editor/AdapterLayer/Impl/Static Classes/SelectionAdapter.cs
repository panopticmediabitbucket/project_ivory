﻿using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using UnityEditor;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class SelectionAdapter : AbstractSingletonAdapter<ISelection, SelectionAdapter>, ISelection {
        public IGameObject activeGameObject => new GameObjectAdapter(Selection.activeGameObject);
    }
}
