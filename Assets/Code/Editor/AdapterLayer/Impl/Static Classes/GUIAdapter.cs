﻿using Assets.Code.Editor.AdapterLayer.Impl.Structs;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEngine;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class GUIAdapter : AbstractSingletonAdapter<IGUI, GUIAdapter>, IGUI {

        public IVector2 BeginScrollView(IRect position, IVector2 currentPosition, IRect rect, bool alwaysShowHorizontal, bool alwaysShowVertical) {
            Vector2 vector2 = GUI.BeginScrollView((Rect)position.GetRect(), (Vector2)currentPosition.GetVector(), (Rect)rect.GetRect(), alwaysShowHorizontal, alwaysShowVertical);
            return new Vector2Adapter(vector2.x, vector2.y);
        }

        public void Box(IRect rect, ITexture tex, IGUIStyle dndBoxStyle) {
            GUI.Box((Rect)rect.GetRect(), (Texture)tex.GetTexture(), (GUIStyle)dndBoxStyle.GetGUIStyle());
        }

        public void Box(IRect rect, IGUIContent guiContent, IGUIStyle guiStyle) {
            GUI.Box((Rect)rect.GetRect(), (GUIContent)guiContent.GetGUIContent(), (GUIStyle)guiStyle.GetGUIStyle());
        }

        public object Button(IRect rect, string a, IGUIStyle style) {
            return GUI.Button((Rect)rect.GetRect(), a, (GUIStyle)style.GetGUIStyle());
        }

        public object Button(IRect rect, IGUIContent a, IGUIStyle style) {
            return GUI.Button((Rect)rect.GetRect(), (GUIContent)a.GetGUIContent(), (GUIStyle)style.GetGUIStyle());
        }

        public object Button(IRect rect, ITexture texture) {
            return GUI.Button((Rect)rect.GetRect(), (Texture)texture.GetTexture());
        }

        public void DrawTexture(IRect rect, ITexture texture) {
            GUI.DrawTexture((Rect)rect.GetRect(), (Texture)texture.GetTexture());
        }

        public void EndScrollView() {
            GUI.EndScrollView();
        }

        public void Label(IRect rect, string v, IGUIStyle style) {
            GUI.Label((Rect)rect.GetRect(), v, (GUIStyle)style.GetGUIStyle());
        }

        public void Label(IRect rect, IGUIContent content, IGUIStyle style) {
            GUI.Label((Rect)rect.GetRect(), (GUIContent)content.GetGUIContent(), (GUIStyle)style.GetGUIStyle());
        }

        public void SetNextControlName(string s) {
            GUI.SetNextControlName(s);
        }
    }
}
