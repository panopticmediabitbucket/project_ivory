﻿using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using UnityEditor;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class EditorGUIUtilityAdapter : AbstractSingletonAdapter<IEditorGUIUtility, EditorGUIUtilityAdapter>, IEditorGUIUtility {
        public void AddCursorRect(IRect rect, MouseCursor cursor) {
            EditorGUIUtility.AddCursorRect((Rect)rect.GetRect(), cursor);
        }
    }
}
