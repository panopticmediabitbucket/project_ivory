﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Code.Editor.AdapterLayer.Impl.Static_Classes {
    public class EditorGUIAdapter : AbstractSingletonAdapter<IEditorGUI, EditorGUIAdapter>, IEditorGUI {
        public void DrawRect(IRect space, IColor color) {
            EditorGUI.DrawRect((Rect)space.GetRect(), (Color)color.GetColor());
        }

        public void DrawRect(IRect rect, IRect size, IColor b) {
            Rect p = (Rect)rect.GetRect();
            Rect s = (Rect)size.GetRect();
            EditorGUI.DrawRect(new Rect(p.position, s.size), (Color)b.GetColor());
        }

        public void Error(IRect rect, string a) {
            EditorGUI.HelpBox((Rect)rect.GetRect(), a, MessageType.Error);
        }

        public void LabelField(IRect rect, string a, IGUIStyle style) {
            EditorGUI.LabelField((Rect)rect.GetRect(), a, (GUIStyle)style.GetGUIStyle());
        }

        public void LabelField(IRect rect, IGUIContent a, IGUIStyle style) {
            EditorGUI.LabelField((Rect)rect.GetRect(), (GUIContent)a.GetGUIContent(), (GUIStyle)style.GetGUIStyle());
        }

        public object ObjectField(IRect rect, object obj, Type type, bool v) {
            return EditorGUI.ObjectField((Rect)rect.GetRect(), (UnityEngine.Object)obj, type, v);
        }

        public object Popup(IRect rect, int a, IGUIContent[] guiContent, IGUIStyle guiStyle) {
            return EditorGUI.Popup((Rect)rect.GetRect(), a, guiContent.Select(x => (GUIContent)x.GetGUIContent()).ToArray(), (GUIStyle)guiStyle.GetGUIStyle());
        }

        public object TextField(IRect rect, string a, IGUIStyle guiStyle) {
            return EditorGUI.TextField((Rect)rect.GetRect(), a, (GUIStyle)guiStyle.GetGUIStyle());
        }

        public object ToggleLeft(IRect rect, string a, bool b) {
            return EditorGUI.ToggleLeft((Rect)rect.GetRect(), a, b);
        }
        public object ToggleLeft(IRect rect, IGUIContent a, bool b) {
            return EditorGUI.ToggleLeft((Rect)rect.GetRect(), (GUIContent)a.GetGUIContent(), b);
        }
        public void Warning(IRect rect, string a) {
            EditorGUI.HelpBox((Rect)rect.GetRect(), a, MessageType.Warning);
        }

        public float Slider(IRect rect, float value, float leftValue, float rightValue)
        {
            return EditorGUI.Slider((Rect)rect.GetRect(), value, leftValue, rightValue);
        }
    }
}
