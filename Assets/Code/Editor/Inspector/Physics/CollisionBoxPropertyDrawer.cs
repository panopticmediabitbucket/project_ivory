﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.GameCode.Physics.Interfaces;
using Assets.Code.Physics.Collision;
using Assets.Code.Physics.Interfaces;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

[CustomPropertyDrawer(typeof(Assets.Code.Physics.Collision.CollisionBox), useForChildren: true)]
public class CollisionBoxPropertyDrawer : PropertyDrawer {
    bool _foldout = false;
    private readonly global::System.Guid _drawablesGuid = global::System.Guid.NewGuid();
    private static readonly Dictionary<string, GUIContent> _labels = new Dictionary<string, GUIContent>()
        { {"width", new GUIContent("Width: ") }, {"height", new GUIContent("Height: ") }, {"offset", new GUIContent("Offset: ") }, {"count", new GUIContent("Casters Per Side: ") } };

    // active editor is determined based on a selection of CasterArray-associated editors
    // These have to be implemented individually in order to support Scene drawing. 
    AbstractInspectorEditor _editor = GetAbstractInspectorEditor();
    private readonly global::System.Guid _staticDrawId = Guid.NewGuid();

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        _editor = GetAbstractInspectorEditor();

        SerializedProperty width = property.FindPropertyRelative("width");
        SerializedProperty height = property.FindPropertyRelative("height");
        SerializedProperty offset = property.FindPropertyRelative("offset");
        SerializedProperty count = property.FindPropertyRelative("castersPerArray");

        EditorGUI.BeginProperty(position, label, property);
        Rect foldoutRect = position;
        foldoutRect.height = 20;
        _foldout = EditorGUI.BeginFoldoutHeaderGroup(foldoutRect, _foldout, "Collision Box");
        if (_foldout) {
            offset.vector2Value = EditorGUI.Vector2Field(new Rect(position.x + 5, position.y + 15, position.width - 5, 30), _labels["offset"], offset.vector2Value);
            width.floatValue = EditorGUI.FloatField(new Rect(position.x + 5, position.y + 60, position.width - 5, 20), _labels["width"], width.floatValue);
            height.floatValue = EditorGUI.FloatField(new Rect(position.x + 5, position.y + 85, position.width - 5, 20), _labels["height"], height.floatValue);
            count.intValue = EditorGUI.IntField(new Rect(position.x + 5, position.y + 110, position.width - 5, 20), _labels["count"], count.intValue);
        }

        EditorGUI.EndFoldoutHeaderGroup();
        EditorGUI.EndProperty();

        if (_foldout) {
            DrawDelegate del = (Object o) => OnSceneGUI(o);
            _editor.DeliverDrawDelegate(new Tuple<DrawDelegate, Guid>(del, _drawablesGuid));
        } else {
            _editor.RemoveDrawDelegate(_drawablesGuid);
        }
    }

    readonly Func<float, Vector2> NormalizedLocalUp = ((x) => Quaternion.Euler(0, 0, x) * Vector2.up);

    private void OnSceneGUI(Object obj) {
        Color temp = Handles.color;
        Handles.color = Color.cyan;
        ICollidable collidable = (ICollidable)obj;
        CollisionBox box = collidable.GetCollisionSystem();
        IPhysicsStateMachine stateMachine = (IPhysicsStateMachine)obj;
        Vector2 upperLeft = (Vector2)(stateMachine.gameObject.transform.rotation * box.offset) + (Vector2)stateMachine.gameObject.transform.position;
        Vector2 lowerLeft = upperLeft + (Vector2)(stateMachine.gameObject.transform.rotation * new Vector2(0, box.height));
        Vector2 lowerRight = upperLeft + (Vector2)(stateMachine.gameObject.transform.rotation * new Vector2(box.width, box.height));
        Vector2 upperRight = upperLeft + (Vector2)(stateMachine.gameObject.transform.rotation * new Vector2(box.width, 0));
        Vector2[] positions = new Vector2[] { lowerLeft, upperLeft, upperRight, lowerRight };

        for (int i = 0; i < 4; i++) {
            bool topOrBottom = i % 2 == 0;
            float length = topOrBottom ? box.width : box.height;
            var castingLocations = GetCastingLocations(box.castersPerArray, length, positions[i], stateMachine.gameObject.transform.rotation * NormalizedLocalUp(90 * (i - 1)));
            Handles.DrawLine(positions[i], positions[i == 3 ? 0 : i + 1]);
            foreach (var location in castingLocations) {
                Handles.DrawSolidDisc(location, Vector3.forward, .05f);
            }
        }
        Handles.color = temp;
    }

    public Vector3[] GetCastingLocations(int count, float width, Vector2 position, Vector2 localUp) {
        Vector3[] castingLocations = new Vector3[count];
        float tempRes = 0;

        if (count > 1)
            tempRes = width / (count - 1);
        for (int i = 0; i < count; i++) {
            castingLocations[i] = position + i * tempRes * localUp;
        }

        return castingLocations;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        if (_foldout) {
            return 25 * 4 + 25;
        }
        return 25;
    }

    public static AbstractInspectorEditor GetAbstractInspectorEditor() {
        List<Component> components = Selection.activeGameObject.GetComponents(typeof(MonoBehaviour)).ToList();
        components = components.FindAll(x => x.GetType().BaseType.IsConstructedGenericType).ToList();
        AbstractInspectorEditor retVal = components
                                             .Find(x => x.GetType().BaseType.GetGenericTypeDefinition().Equals(typeof(PhysicsStateMachineWorldObject<>))) != null
            ? PhysicsStateMachineWorldObjectInspectorEditor.GetInstance() : null;
        return retVal;
    }
}
