﻿using UnityEditor;
using UnityEngine;
using Assets.Code.Graphics;

[CustomEditor(typeof(AnimationControllerPartner))]
public class AnimationControllerPartnerEditor : Editor
{
    SerializedProperty _matType;
    MaterialType _type;

    string[] _materialTypes;

    private void OnEnable()
    {
        _matType = serializedObject.FindProperty("type");
        _type = ((AnimationControllerPartner)serializedObject.targetObject).type;
        _materialTypes = _matType.enumDisplayNames;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        _type = ((AnimationControllerPartner)serializedObject.targetObject).type;
        _matType.enumValueIndex = (int)(MaterialType)EditorGUILayout.EnumPopup("Type: ", _type);
        serializedObject.ApplyModifiedProperties();
    }

}
