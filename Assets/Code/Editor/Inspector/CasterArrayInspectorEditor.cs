﻿    using UnityEditor;
    using UnityEngine;

[CustomEditor(typeof(CasterArray_old))]
[CanEditMultipleObjects]
public class CasterArrayInspectorEditor : Editor {

    SerializedProperty width;
    SerializedProperty rotation;
    SerializedProperty count;
    SerializedProperty objName;
    CasterArray_old _casterArray;

    void OnEnable()
    {
        count = serializedObject.FindProperty("count");
        width = serializedObject.FindProperty("width");
        rotation = serializedObject.FindProperty("rotation");

        _casterArray = (CasterArray_old)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        count.intValue = EditorGUILayout.IntField("Count: ", count.intValue, options: null);
        width.floatValue = EditorGUILayout.FloatField("Width: ", width.floatValue, options: null);
        EditorGUILayout.Slider(rotation, 0, 360, options: null);
        _casterArray.transform.rotation = Quaternion.AngleAxis(rotation.floatValue, Vector3.forward);
        serializedObject.ApplyModifiedProperties();
    }

    void OnSceneGUI()
    {
        Vector2 center = _casterArray.transform.position;
        Handles.color = Color.cyan;
        Handles.DrawLine(center, center + (_casterArray.width * _casterArray.NormalizedLocalUp));
        Vector3[] ca = _casterArray.GetCastingLocations();

        foreach(var vector in ca)
        {
            Handles.DrawSolidDisc(vector, Vector3.back, 0.15f);
        }
    }
}
