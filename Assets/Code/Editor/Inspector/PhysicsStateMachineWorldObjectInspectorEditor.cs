﻿using UnityEditor;

    [CustomEditor(typeof(PhysicsStateMachineWorldObject<>), editorForChildClasses: true)]
    [CanEditMultipleObjects]
    public class PhysicsStateMachineWorldObjectInspectorEditor : AbstractInspectorEditor {
        static PhysicsStateMachineWorldObjectInspectorEditor _instance;

        public static PhysicsStateMachineWorldObjectInspectorEditor GetInstance() {
            return _instance;
        }

        private void OnEnable() {
            _instance = this;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
        }

        protected override void OnSceneGUI() {
            base.OnSceneGUI();
        }
    }
