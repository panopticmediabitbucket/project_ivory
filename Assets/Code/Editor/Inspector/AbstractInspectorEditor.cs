﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

// Draw delegates are used to pass functions statically to the inspector editor to draw data to the scene
public delegate void DrawDelegate(Object go);
public abstract class AbstractInspectorEditor : UnityEditor.Editor {
    // The Guid is used to identify the given List of Drawables
    public void DeliverDrawDelegate(Tuple<DrawDelegate, global::System.Guid> delegaTuple) {
        Tuple<DrawDelegate, global::System.Guid> item = _drawDelegates.Find(x => x.Item2.Equals(delegaTuple.Item2));
        if (item == null) {
            _drawDelegates.Add(delegaTuple);
        } else {
            _drawDelegates.Remove(item);
            _drawDelegates.Add(delegaTuple);
        }
    }

    public void RemoveDrawDelegate(global::System.Guid id) {
        Tuple<DrawDelegate, global::System.Guid> item = _drawDelegates.Find(x => x.Item2.Equals(id));
        if (item != null) {
            _drawDelegates.Remove(item);
        }
    }

    private readonly List<Tuple<DrawDelegate, global::System.Guid>> _drawDelegates = new List<Tuple<DrawDelegate, global::System.Guid>>();

    // call super class in implementation to draw statically-delivered draw functions for the scene 
    protected virtual void OnSceneGUI() {
        foreach (var del in _drawDelegates) {
            del.Item1.Invoke(target);
        }
    }
}