﻿namespace Assets.Code.Editor.Utilities {
    public class ReturnedValue<T>: ReturnedValue {
        public static new ReturnedValue<T> noReturnValue => new ReturnedValue<T>();

        public new T value => (T)base.value;

        private ReturnedValue(bool hasValue = false) : base(hasValue) {
            base.value = default(T);
        }
        public ReturnedValue(T value, bool hasValue = true): base(value) { }
    }

    public class ReturnedValue {
        public static readonly ReturnedValue noReturnValue = new ReturnedValue();

        public object value { get; protected set; }
        protected bool _hasValue = true;

        protected ReturnedValue(bool hasValue = false) {
            _hasValue = hasValue;
            value = null;
        }
        public ReturnedValue(object obj) {
            value = obj;
        }

        public static implicit operator bool(ReturnedValue retValue) {
            return retValue._hasValue;
        }
    }
}
