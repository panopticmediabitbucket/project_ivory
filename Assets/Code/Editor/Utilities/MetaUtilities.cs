﻿using System.IO;
using Assets.Code.Editor.InterfaceAdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using System;

namespace Assets.Code.Editor.Utilities
{
    public class MetaUtilities
    {
        public static IAssetDatabase AssetDatabase => AssetDatabaseAdapter.Instance;
        //returns the empty string if property is unassigned. Adds property to .meta file
        public static MetaSuccess GetAssociatedValue(string metaPath, string property, out string value)
        {
            property += ": ";
            try
            {
                using(FileStream fs = File.Open(metaPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    StreamWriter sw = new StreamWriter(fs); StreamReader sr = new StreamReader(fs);
                    sw.AutoFlush = true;
                    value = ExtractValue(sr, IndexOfProperty(property, sr, sw));
                    fs.Close();
                    if(value.Equals("NONE")) return MetaSuccess.ValueNotFound; else return MetaSuccess.ValueFound;
                }
            }
            catch(FileNotFoundException e)
            {
                UnityEngine.Debug.LogWarning(e);
                value = string.Empty; return MetaSuccess.FileNotFound;
            }
        }

        //bool indicates sucess
        public static bool WriteAssociatedValue(string metaPath, string property, string newVal)
        {
            property += ": ";
            try
            {
                using(FileStream fs = File.Open(metaPath, FileMode.Open, FileAccess.ReadWrite))
                {
                    StreamWriter sw = new StreamWriter(fs); StreamReader sr = new StreamReader(fs);
                    sw.Write(ReplacementValue(sr, IndexOfProperty(property, sr, sw), newVal));
                    sw.Flush();
                    fs.Close();
                    return true;
                }
            }
            catch(FileNotFoundException e)
            {
                return false;
            }
        }
        
        private static string ExtractValue(StreamReader sr, int index)
        {
            string full = sr.ReadToEnd();
            sr.DiscardBufferedData();
            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            int length = full.IndexOf(' ', index);
            char ind= full[0];
            string tst = " ";
            length -= index;
            return full.Substring(index, length);
        }

        //returns a string of the entire filestream with the proper modifications made to the
        //value listed at 'index.' The newValue replaces the value at 'index.'
        private static string ReplacementValue(StreamReader sr, int index, string newValue)
        {
            string full = sr.ReadToEnd();
            sr.DiscardBufferedData();
            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            int endOldVal = full.IndexOf(' ', index);
            string ret = full.Substring(0, index) + newValue + full.Substring(endOldVal);
            return ret;
        }

        private static int IndexOfProperty(string property, StreamReader sr, StreamWriter sw)
        {
            string full = sr.ReadToEnd();
            sr.DiscardBufferedData();
            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            int index = full.IndexOf(property);
            if(index < 0)
            {
                sw.Write(full + "\r\n" + property + "NONE ");
                sw.BaseStream.Seek(0, SeekOrigin.Begin);
                return IndexOfProperty(property, sr, sw);
            }
            else
            {
                return index + property.Length;
            }
        }

        public static T AssetFromGuid<T>(string guid)
        {
            return (T)AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guid));
        }

        public static string GuidFromAsset<T>(T obj)
        {
            return AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(obj));
        }
    }

    public enum MetaSuccess
    {
        FileNotFound, ValueNotFound, ValueFound
    }
}
