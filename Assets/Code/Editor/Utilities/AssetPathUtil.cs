﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using UnityEngine;

namespace Assets.Code.Editor.Utilities
{
    public class AssetPathUtil
    {
        public static IEditorUtility EditorUtility => EditorUtilityAdapter.Instance;
        private static IFileUtil FileUtil => FileUtilAdapter.Instance;
        private static IPrefabUtility PrefabUtility => PrefabUtilityAdapter.Instance;

        public static readonly string defaultPrefabPath = "Assets\\Prefab";
        public static void CreatePrefabDialog(IGameObject gameObject)
        {
            string fullPath = EditorUtility.SaveFilePanel("Designate Prefab Name and Location", defaultPrefabPath, gameObject.name.Replace(' ', '_') + ".prefab", "prefab");
            string relativePath = FileUtil.GetProjectRelativePath(fullPath);
            if(relativePath is null)
            {
                return;
            }
            else
            {
                PrefabUtility.SaveAsPrefabAssetAndConnect(gameObject, relativePath, InteractionMode.UserAction);
            }
        }

        public static string ProjectRoot()
        {
            return Application.dataPath.Substring(0, Application.dataPath.IndexOf("Assets"));
        }

        public static string GetAssetFolderPath(string assetPath)
        {
            return assetPath.Substring(0, assetPath.LastIndexOf('/'));
        }

        public static string GetParentFolder(string folderPath)
        {
            return folderPath.Substring(0, folderPath.Substring(0, folderPath.LastIndexOf('/')).LastIndexOf('/') + 1);
        }
    }
}
