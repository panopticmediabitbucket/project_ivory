﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Static_Classes;

namespace Assets.Code.Editor.Utilities {
    public class InputUtilities {
        public static IEvent Event => EventAdapter.Instance;

        private static bool[] _keyVector = new bool[510];
        public static bool IsKeyHeld(KeyCode keyCode) {
            if(Event.current.rawType.Equals(EventType.keyDown) && Event.current.keyCode.Equals(keyCode)){
                _keyVector[(int)keyCode] = true;
            } else if(Event.current.rawType.Equals(EventType.KeyUp) && Event.current.keyCode.Equals(keyCode)){
                _keyVector[(int)keyCode] = false;
            }

            return _keyVector[(int)keyCode];
        }
    }
}
