﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Adapter;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using static Assets.Code.Editor.CustomGUI.Columns.ColumnGroup;
using static Assets.Code.Editor.Tests.Mocks.ObjectMocks;
using static Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl.AASFunctionModelTest;
using PrefabAssetType = Assets.Code.Editor.AdapterLayer.Static_Classes.PrefabAssetType;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl {
    public class AASWindowTest {

        private static Mock<IEditorWindow> _editorWindowMock;
        [SetUp]
        public static void Setup() {
            var dependencyBuilder = StaticMocks.DependencyBuilderMock();
            StaticMocks.MockSelection();
            StaticMocks.MockReflectionHandler();
            StaticMocks.MockPrefabUtility();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockEditorUtility();

            _editorWindowMock = new Mock<IEditorWindow>();

            _editorWindowMock.SetupGet(x => x.position).Returns(dependencyBuilder.Object.NewRect(0, 0, 0, 0));
        }

        [Test]
        public void TestOnEnable() {
            AASWindow window = AASWindow.GetInstance(_editorWindowMock.Object);
            window.OnEnable();
            var list = window.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            ColumnGroup columnGroup = (ColumnGroup)list.Find(x => x.Name.Equals("_columnGroup")).GetValue(window);

            // Column group is constructed OnEnable
            Assert.NotNull(columnGroup);
            Assert.AreEqual(columnGroup.GetColumn(0).width, 400);
            Assert.AreEqual(columnGroup.GetColumn(0).minWidth, 400);
            Assert.AreEqual(columnGroup.GetColumn(0).maxWidth, 400);
        }

        [Test]
        public void TestOnSelectionChangeRegularPrefab() {
            AASWindow window = AASWindow.GetInstance(_editorWindowMock.Object);

            var mockPrefabUtility = StaticMocks.MockPrefabUtility();
            mockPrefabUtility.Setup(mock => mock.GetPrefabAssetType(It.IsAny<IGameObject>())).Returns((IGameObject gameobject) => {
                return PrefabAssetType.Regular;
            });

            var mockReflectionHandler = StaticMocks.MockReflectionHandler();
            mockReflectionHandler.Setup(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.IsAny<Type>()))
                .Returns(GetMockComponentWrapper(new TEnumWrapper()).Object);

            // prep mocks for the function model construction
            AASFunctionModelTest.SetUp();
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Impl\\AASFMTestFile0.txt");
            StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);

            window.OnEnable();
            window.OnSelectionChange();

            // verify call to reflection handler to retrieve PhysicsStateMachineWorldObject component from gameobject
            mockReflectionHandler.Verify(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.Is<Type>(x => x.Equals(typeof(PhysicsStateMachineWorldObject<>)))));

            var list = window.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            AASFunctionModel<TestEnum> functionModel = (AASFunctionModel<TestEnum>)list.Find(x => x.Name.Equals("_functionModel")).GetValue(window);
            Assert.AreEqual(functionModel.GetType(), typeof(AASFunctionModel<TestEnum>)); // generic type is constructed and stored
            
            // draw functions for columns are set with the functionModel's selection and edit windows
            ColumnGroup columnGroup = (ColumnGroup)list.Find(x => x.Name.Equals("_columnGroup")).GetValue(window);
            DrawFunction[] drawFunctions = (DrawFunction[])columnGroup.GetType().
                GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList().Find(x => x.Name.Equals("_drawFunctions")).GetValue(columnGroup);
            Assert.AreEqual(drawFunctions[0], (DrawFunction)functionModel.SelectionWindow);
            Assert.AreEqual(drawFunctions[1], (DrawFunction)functionModel.EditWindow);

            window.Update();
        }

        [Test]
        public void TestOnSelectionChangeNotAPrefab() {
            AASWindow window = AASWindow.GetInstance(_editorWindowMock.Object);

            var mockPrefabUtility = StaticMocks.MockPrefabUtility();
            mockPrefabUtility.Setup(mock => mock.GetPrefabAssetType(It.IsAny<IGameObject>())).Returns((IGameObject gameobject) => {
                return PrefabAssetType.NotAPrefab;
            });

            var mockReflectionHandler = StaticMocks.MockReflectionHandler();
            mockReflectionHandler.Setup(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.IsAny<Type>()))
                .Returns(GetMockComponentWrapper(new TEnumWrapper()).Object);

            var mockEditorUtility = StaticMocks.MockEditorUtility();
            var mockFileUtil = StaticMocks.MockFileUtil();

            window.OnEnable();
            window.OnSelectionChange();

            // nothing happens
            // an attempt is made to save the prefab
            //mockEditorUtility.Verify(mock => mock.SaveFilePanel(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce());
            //mockFileUtil.Verify(mock => mock.GetProjectRelativePath(It.IsAny<string>()), Times.AtLeastOnce());
        }

        [Test]
        public void TestOnSelectionChangeNoValidComponent() {
            AASWindow window = AASWindow.GetInstance(_editorWindowMock.Object);

            var mockReflectionHandler = StaticMocks.MockReflectionHandler();
            mockReflectionHandler.Setup(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.IsAny<Type>()))
                .Returns(GetMockComponentWrapper(null).Object);

            window.OnEnable();
            window.OnSelectionChange();

            // column group draw functions are set to doNothing for invalid object
            var list = window.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            ColumnGroup columnGroup = (ColumnGroup)list.Find(x => x.Name.Equals("_columnGroup")).GetValue(window);
            DrawFunction[] drawFunctions = (DrawFunction[])columnGroup.GetType().
                GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList().Find(x => x.Name.Equals("_drawFunctions")).GetValue(columnGroup);
            Assert.AreEqual(drawFunctions[0], ColumnGroup.doNothing);
            Assert.AreEqual(drawFunctions[1], ColumnGroup.doNothing);
        }

        [Test]
        public void TestOnGUI() {
            AASWindow window = AASWindow.GetInstance(_editorWindowMock.Object);

            var mockReflectionHandler = StaticMocks.MockReflectionHandler();
            mockReflectionHandler.Setup(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.IsAny<Type>()))
                .Returns(GetMockComponentWrapper(null).Object);

            window.OnEnable();
            var list = window.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            ColumnGroup columnGroup = (ColumnGroup)list.Find(x => x.Name.Equals("_columnGroup")).GetValue(window);

            Mock<TEnumWrapper> mockFunctionWrapper = new Mock<TEnumWrapper>();

            columnGroup.SetDrawFunction(mockFunctionWrapper.Object.TestColumnMethod, 0);
            columnGroup.SetDrawFunction(mockFunctionWrapper.Object.TestColumnMethod, 1);

            window.OnGUI();
            
            mockFunctionWrapper.Verify(mock => mock.TestColumnMethod(It.Is<Column>(x => x.Equals(columnGroup.GetColumn(0)))));
            mockFunctionWrapper.Verify(mock => mock.TestColumnMethod(It.Is<Column>(x => x.Equals(columnGroup.GetColumn(1)))));

        }

        [Test]
        public void TestOnPlayEvent()
        {
            AASWindow window = AASWindow.GetInstance(_editorWindowMock.Object);

            var mockPrefabUtility = StaticMocks.MockPrefabUtility();
            mockPrefabUtility.Setup(mock => mock.GetPrefabAssetType(It.IsAny<IGameObject>())).Returns((IGameObject gameobject) => {
                return PrefabAssetType.Regular;
            });

            var mockReflectionHandler = StaticMocks.MockReflectionHandler();
            mockReflectionHandler.Setup(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.IsAny<Type>()))
                .Returns(GetMockComponentWrapper(new TEnumWrapper()).Object);

            // prep mocks for the function model construction
            AASFunctionModelTest.SetUp();
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Impl\\AASFMTestFile0.txt");
            StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);

            window.OnEnable();
            window.OnSelectionChange();

            // verify call to reflection handler to retrieve PhysicsStateMachineWorldObject component from gameobject
            mockReflectionHandler.Verify(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.Is<Type>(x => x.Equals(typeof(PhysicsStateMachineWorldObject<>)))));

            var list = window.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            AASFunctionModel<TestEnum> functionModel = (AASFunctionModel<TestEnum>)list.Find(x => x.Name.Equals("_functionModel")).GetValue(window);
            Assert.AreEqual(functionModel.GetType(), typeof(AASFunctionModel<TestEnum>)); // generic type is constructed and stored

            window.OnPlayEvent(PlayModeStateChange.ExitingEditMode);
            window.OnSelectionChange(); //run the method again with a built function model to catch the ForceSave
        }
    }
}
