﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using NUnit.Framework;
using Moq;
using Assets.Code.Editor.CustomGUI.Columns;
using System;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using EventType = Assets.Code.Editor.AdapterLayer.Impl.Static_Classes.EventType;
using Assets.Code.Editor.AdapterLayer.Objects;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl {
    public class AASKeyEditorTest {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void SetUp() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockHandles();
            StaticMocks.MockGUI();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockEditorGUIUtility();
            StaticMocks.MockAssetDatabase(null);

            var animator = ObjectMocks.MockAnimatorWithGraph();

            _accelAsset = new AccelAsset() { keys = new Key[] { new Key(0, 0), new Key(1, 1) }, name = "Run", pairedAnimation = "Run", xDirection = 1, yDirection = 0 };
            _editorModel = ConversionUtilities.AssetModelToEditorModel(_accelAsset, AnimationGraphUtil.GetAnimationTriggers(animator));
        }

        private static AccelAsset _accelAsset;
        private static AASEditorModel _editorModel;
        [Test]
        public void TestDrawGrid() {
            var keyEditor = new AASKeyEditor();
            var mockHandles = StaticMocks.MockHandles();
            keyEditor.DrawGrid();

            mockHandles.VerifySet(mock => mock.color, Times.AtLeastOnce());
            // Handles is called to draw the lines
            mockHandles.Verify(mock => mock.DrawLine(It.IsAny<IVector2>(), It.IsAny<IVector2>()), Times.AtLeast(8));
        }

        [Test]
        public void TestInitializeNullCreatesOneKey() {
            var keyEditor = new AASKeyEditor();
            Assert.AreEqual(keyEditor.GetKeyArray().Length, 1);
        }

        [Test]
        public void TestCanvasControlClickedCreatesNewKey() {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.MouseDown);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 10));
            Assert.AreEqual(keyEditor.GetKeyArray().Length, 2);
            Column col = new Column(0) { width = 800, height = 500 };
            Assert.True(keyEditor.OnGUI(col));
            Assert.AreEqual(keyEditor.GetKeyArray().Length, 3);
        }

        [Test]
        public void TestGrabAndDragKeyHandle() {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            var canvasScale = keyEditor.canvasScale;

            Column col = new Column(0) { width = 800, height = 500 };
            Assert.False(keyEditor.OnGUI(col)); // no interaction 

            var key = keyEditor.GetKeyArray()[1];
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.MouseDown);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(30, 475));
            Assert.True(keyEditor.OnGUI(col));

            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.ignore);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(30 + 2 / canvasScale.pixelsPerMS, 475 + -2 / canvasScale.pixelsPerMPS));
            Assert.True(keyEditor.OnGUI(col));

            key = keyEditor.GetKeyArray()[1];
            Assert.AreEqual(Math.Round(key.time), 1);
            Assert.AreEqual(key.v, 1);

        }

        [Test]
        public void TestOddAccessors() {
            var keyEditor = new AASKeyEditor();
            keyEditor.animationLength = 1;
            Assert.AreEqual(keyEditor.animationLength, 1);
            var keys = keyEditor.GetKeys();
            keyEditor.ManualUpdate(keys[1].id, 2, 2);
            Assert.AreEqual(keyEditor.GetKeyArray()[1].v, 2);
            Assert.AreEqual(keyEditor.GetKeyArray()[1].time, 2);
            keyEditor.DeleteKey(keys[1].id);
            Assert.AreEqual(keyEditor.GetKeys().Count, 1);
        }

        [Test]
        public void TestCanvasControlZoomIn()  
        {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            var oldScale = keyEditor.canvasScale.pixelsPerMPS;
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.scrollWheel);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 10)); // in window 
            eventMock.SetupGet(mock => mock.delta.y).Returns(-3); //if scrollDir<0, zoom in, else zoom out
            Assert.AreEqual(keyEditor.GetKeyArray().Length, 2);
            Column col = new Column(0) { width = 800, height = 500 };
            keyEditor.OnGUI(col);

            var newScale = keyEditor.canvasScale.pixelsPerMPS; 
            Assert.AreEqual(oldScale * 1.1, newScale);
        }

        [Test]
        public void TestCanvasControlZoomOut()
        {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            var oldScale = keyEditor.canvasScale.pixelsPerMPS;
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.scrollWheel);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 10)); // in window 
            eventMock.SetupGet(mock => mock.delta.y).Returns(3); //if scrollDir<0, zoom in, else zoom out
            Assert.AreEqual(keyEditor.GetKeyArray().Length, 2);
            Column col = new Column(0) { width = 800, height = 500 };
            keyEditor.OnGUI(col);

            var newScale = keyEditor.canvasScale.pixelsPerMPS;
            Assert.AreEqual((float)(oldScale / 1.1), newScale);
        }

        Canvas GetKeyEditorCanvas(AASKeyEditor keyEditor) {
            throw new NotImplementedException();
        }

        [Test]
        // Doesn't zoom out 
        public void TestCanvasZoomControlStagnant()
        {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            Column col = new Column(0) { width = 800, height = 500 };
            keyEditor.OnGUI(col);

            // Wrong event type, testing using mouse click
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.mouseDown);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 10)); // in window 
            eventMock.SetupGet(mock => mock.delta.y).Returns(3);
            
            var oldScale = keyEditor.canvasScale.pixelsPerMPS;
            var oldPosition = GetKeyEditorCanvas(keyEditor).position;
            keyEditor.OnGUI(col);
            
            var newScale = keyEditor.canvasScale.pixelsPerMPS;
            var newPosition = GetKeyEditorCanvas(keyEditor).position;
            Assert.AreEqual(oldScale, newScale);
            Assert.AreEqual(oldPosition, newPosition);

            // Mouse outside of rectangle
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.scrollWheel);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(2000, 10)); // outside window 
            eventMock.SetupGet(mock => mock.delta.y).Returns(3);

            keyEditor.OnGUI(col);

            newScale = keyEditor.canvasScale.pixelsPerMPS;
            newPosition = GetKeyEditorCanvas(keyEditor).position;
            Assert.AreEqual(oldScale, newScale);
            Assert.AreEqual(oldPosition, newPosition);
        }
        [Test]
        // Zoom centers around mouse
        public void TestCanvasZoomControlCenter()
        {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.scrollWheel);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 5)); // Y = 5 for test
            eventMock.SetupGet(mock => mock.delta.y).Returns(-3); //if scrollDir<0, zoom in, else zoom out
            Assert.AreEqual(keyEditor.GetKeyArray().Length, 2);
            Column col = new Column(0) { width = 800, height = 500 };
            keyEditor.OnGUI(col);

            var newPosition =(float) GetKeyEditorCanvas(keyEditor).position;
            // Test that new position is the same as expected value, within a certain tolerance. This leaves room for floating point errors
            var tolerance = .0000001f;
            var expected = 23.1388893;
            Assert.Less(newPosition-expected,tolerance);
        }

        [Test]
        public void TestShiftLineIndicators()
        {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            Column col = new Column(0) { width = 800, height = 500 };
            var oldSpacingInPixels = keyEditor.currentSpacing * keyEditor.canvasScale.pixelsPerMPS;
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.scrollWheel);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 5));
            eventMock.SetupGet(mock => mock.delta.y).Returns(-3); //if scrollDir<0, zoom in, else zoom out
            keyEditor.OnGUI(col);
            var newSpacingInPixels = keyEditor.currentSpacing * keyEditor.canvasScale.pixelsPerMPS;
            var oldSpacing = keyEditor.currentSpacing;
            Assert.AreNotEqual(oldSpacingInPixels, newSpacingInPixels);
            keyEditor.OnGUI(col);
            newSpacingInPixels = keyEditor.currentSpacing * keyEditor.canvasScale.pixelsPerMPS;
            var newSpacing = keyEditor.currentSpacing;
            Assert.AreEqual(5.0f, newSpacing);
            Assert.Greater(newSpacingInPixels, 60f);
            Assert.Less(newSpacingInPixels, 100f);
        }
        [Test]
        public void TestCanvasDragControl()
        {
            var keyEditor = new AASKeyEditor();
            var eventMock = StaticMocks.MockEvent();
            var oldPosition = GetKeyEditorCanvas(keyEditor).position;
            Column col = new Column(0) { width = 800, height = 500 };
            
            // Mouse down and space bar works  
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.KeyDown);
            eventMock.SetupGet(mock => mock.keyCode).Returns(KeyCode.Space);
            keyEditor.OnGUI(col);
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.mouseDown);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 10));
            keyEditor.OnGUI(col);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 30));
            Assert.AreEqual(15.833333f, GetKeyEditorCanvas(keyEditor).position);

            // Mouse outside of window
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(2000, 10));
            keyEditor.OnGUI(col);
            Assert.AreEqual(15.833333f, GetKeyEditorCanvas(keyEditor).position); // no changes should be made to y-pos

            // Mouse down doesn't work
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.keyUp);
            eventMock.SetupGet(mock => mock.keyCode).Returns(KeyCode.Space);
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.mouseUp);
            keyEditor.OnGUI(col);
            oldPosition = GetKeyEditorCanvas(keyEditor).position;
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.mouseDown);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 10));
            keyEditor.OnGUI(col);
            Assert.AreEqual(oldPosition, GetKeyEditorCanvas(keyEditor).position);

            // !(mouse down and space bar) doesn't work
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.mouseUp);
            keyEditor.OnGUI(col);
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.KeyDown);
            eventMock.SetupGet(mock => mock.keyCode).Returns(KeyCode.Backspace);
            keyEditor.OnGUI(col);
            eventMock.SetupGet(mock => mock.rawType).Returns(EventType.mouseDown);
            eventMock.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 25));
            keyEditor.OnGUI(col);
            Assert.AreEqual(oldPosition, GetKeyEditorCanvas(keyEditor).position);
        }
    }
}
