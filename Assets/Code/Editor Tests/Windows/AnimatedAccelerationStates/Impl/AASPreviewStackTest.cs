﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using Moq;
using NUnit.Framework;
using System.Threading;
using static Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl.AASFunctionModelTest;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl {

    public class AASPreviewStackTest {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        private static Mock<ITexture> _mockTextureB;
        private static Mock<ITexture> _mockTextureA;
        private static AASEditorModel _testModel;
        private static Mock<IGameObject> _mockGO;


        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockGUI();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockEvent();
            StaticMocks.MockFont();
            StaticMocks.MockHandles();
            StaticMocks.MockAssetDatabase("");
            StaticMocks.MockEditorStyles();

            _mockTextureA = new Mock<ITexture>();
            _mockTextureB = new Mock<ITexture>();
            _mockGO = ObjectMocks.MockGameObject();

            AccelAsset accelAsset = new AccelAsset() { keys = new Key[] { new Key(0, 0), new Key(1, 1) }, pairedAnimation = "Run", name = "Run", xDirection = 1, yDirection = 0 };
            _testModel = ConversionUtilities.AssetModelToEditorModel(accelAsset, AnimationGraphUtil.GetAnimationTriggers(ObjectMocks.MockAnimatorWithGraph()));
        }

        private AASPreviewStack<TestEnum> GetTestPreviewStack(IGameObject gameObject) {
            var previewStack = new AASPreviewStack<TestEnum>(gameObject);
            previewStack.targetState = _testModel;
            return previewStack;
        }

        [Test]
        public void TestControlAndUpdateSetAndSampleAnimation() {
            AccelAsset accelAsset = new AccelAsset() { keys = new Key[] { new Key(0, 0), new Key(1, 1) }, pairedAnimation = "Run", name = "Run", animationTrigger = "Trot", xDirection = 1, yDirection = 0 };
            _testModel = ConversionUtilities.AssetModelToEditorModel(accelAsset, AnimationGraphUtil.GetAnimationTriggers(ObjectMocks.MockAnimatorWithGraph(true)));
            _mockGO = ObjectMocks.MockGameObject(true);

            var previewStack = GetTestPreviewStack(_mockGO.Object);
            var mockEditorGUI = StaticMocks.MockEditorGUI();
            var mockAnimationMode = StaticMocks.MockAnimationMode();
            var mockEvent = StaticMocks.MockEvent();
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(25, 25)); // in the play rect

            Assert.True(previewStack.OnGUI());
            Thread.Sleep(500);
            previewStack.Update();
            // Timer is sampled at full speed
            mockAnimationMode.Verify(mock => mock.SampleAnimationClip(_mockGO.Object, It.IsAny<IAnimationClip>(), It.Is<float>(x => x >= .49f && x <= .51f)));
            mockAnimationMode.Verify(mock => mock.StartAnimationMode(), Times.Once());

            mockEditorGUI.Setup(mock => mock.Slider(It.IsAny<IRect>(), It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>())).Returns((IRect rect, float a, float b, float c) => {
                return b; // the least time value is returned
            });
            Thread.Sleep(600); // 600 + 500 > 1 second, but the speed value is slowed, so the animation mode continues
            var mockLongAnim = new Mock<IAnimationClip>();
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.ignore);
            previewStack.OnGUI();
            previewStack.Update();
            Thread.Sleep(500);
            mockAnimationMode.Verify(mock => mock.SampleAnimationClip(_mockGO.Object, It.IsAny<IAnimationClip>(), It.IsAny<float>()));
            mockAnimationMode.Verify(mock => mock.StopAnimationMode(), Times.Never());
            mockEditorGUI.Setup(mock => mock.Slider(It.IsAny<IRect>(), It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>())).Returns((IRect rect, float a, float b, float c) => {
                return c; // the least time value is returned
            });
            previewStack.OnGUI();
            previewStack.Update();

            StaticMocks.stateBasedMotionMachine.Verify(mock => mock.DeliverInternalAccel(It.IsAny<Accel<TestEnum>>()), Times.AtLeastOnce());
            StaticMocks.stateBasedMotionMachine.Verify(mock => mock.CalculateFrameMotionAndUpdateMomentum(), Times.AtLeast(3));
        }

        [Test]
        public void TestLoopingAnimation() {
            var previewStack = GetTestPreviewStack(_mockGO.Object);
            var mockAnimationMode = StaticMocks.MockAnimationMode();
            var mockEvent = StaticMocks.MockEvent();

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(25, 25)); // in the play rect
            Assert.True(previewStack.OnGUI());
            previewStack.Update();

            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(100, 25)); // in the loop rect
            Assert.True(previewStack.OnGUI());
            Thread.Sleep(3100); // animation will loop
            previewStack.Update();

            mockAnimationMode.Verify(mock => mock.SampleAnimationClip(_mockGO.Object, It.IsAny<IAnimationClip>(), It.IsAny<float>()), Times.Exactly(2));
            mockAnimationMode.Verify(mock => mock.StartAnimationMode(), Times.Exactly(2));
            mockAnimationMode.Verify(mock => mock.StopAnimationMode(), Times.Never());
            StaticMocks.stateBasedMotionMachine.Verify(mock => mock.ResetMomentum());
        }

        [Test]
        public void TestAnimationStopsAfterInterval() {
            var previewStack = GetTestPreviewStack(_mockGO.Object);
            var mockAnimationMode = StaticMocks.MockAnimationMode();
            var mockEvent = StaticMocks.MockEvent();

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(25, 25)); // in the play rect
            Assert.True(previewStack.OnGUI());
            previewStack.Update();

            Thread.Sleep(3100); // animation will be over
            previewStack.Update();

            mockAnimationMode.Verify(mock => mock.SampleAnimationClip(_mockGO.Object, It.IsAny<IAnimationClip>(), It.IsAny<float>()), Times.AtLeastOnce());
            mockAnimationMode.Verify(mock => mock.StartAnimationMode(), Times.Exactly(1));
            mockAnimationMode.Verify(mock => mock.StopAnimationMode(), Times.Once());
            StaticMocks.stateBasedMotionMachine.Verify(mock => mock.ResetMomentum());
        }

        [Test]
        public void TestStopAnimation() {
            var previewStack = GetTestPreviewStack(_mockGO.Object);
            var mockAnimationMode = StaticMocks.MockAnimationMode();
            var mockEvent = StaticMocks.MockEvent();

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(25, 25)); // in the play rect
            Assert.True(previewStack.OnGUI());
            Assert.True(previewStack.OnGUI());

            mockAnimationMode.Verify(mock => mock.StartAnimationMode(), Times.Exactly(1));
            mockAnimationMode.Verify(mock => mock.StopAnimationMode(), Times.Exactly(1));
            StaticMocks.stateBasedMotionMachine.Verify(mock => mock.ResetMomentum());
        }

        [Test]
        public void TestChangeStateMidPreview() {
            var previewStack = GetTestPreviewStack(_mockGO.Object);
            var mockAnimationMode = StaticMocks.MockAnimationMode();
            var mockEvent = StaticMocks.MockEvent();

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(25, 25)); // in the play rect
            Assert.True(previewStack.OnGUI());
            previewStack.Update();
            mockAnimationMode.Verify(mock => mock.StartAnimationMode(), Times.Once());

            AccelAsset accelAsset = new AccelAsset() { keys = new Key[] { new Key(0, 0), new Key(1, 1) }, pairedAnimation = "Idle", name = "Idle", xDirection = 1, yDirection = 0 };
            previewStack.targetState = ConversionUtilities.AssetModelToEditorModel(accelAsset, AnimationGraphUtil.GetAnimationTriggers(ObjectMocks.MockAnimatorWithGraph()));

            mockAnimationMode.Verify(mock => mock.StopAnimationMode());
            StaticMocks.stateBasedMotionMachine.Verify(mock => mock.ResetMomentum());
        }

        [Test]
        public void TestPreviewStackStepsNoValidDisplay() {
            var previewStack = new AASPreviewStack<TestEnum>(_mockGO.Object);
            AccelAsset accelAsset = new AccelAsset() { keys = new Key[] { new Key(0, 0), new Key(1, 1) }, pairedAnimation = "Run", name = "Run", animationTrigger = "Unbound", xDirection = 1, yDirection = 0 };
            previewStack.targetState = ConversionUtilities.AssetModelToEditorModel(accelAsset, AnimationGraphUtil.GetAnimationTriggers(ObjectMocks.MockAnimatorWithGraph()));

            var gui = StaticMocks.MockGUI();
            previewStack.OnGUI();

            gui.Verify(mock => mock.Label(It.IsAny<IRect>(), It.Is<string>(x => "No edge traversed by trigger".Equals(x)), It.IsAny<IGUIStyle>()));
        }
    }
}

