﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Utilities;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.CustomGUI.Controls;
using System.Diagnostics;
using System.Threading;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl {
    public class AASFunctionModelTest {
        public enum TestEnum {
            Run, Trot, StartTrot, Idle
        }
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void SetUp() {

            StaticMocks.DependencyBuilderMock();

            StaticMocks.MockEditorStyles();

            StaticMocks.MockAssetDatabase("");

            StaticMocks.MockAssetPreview();

            StaticMocks.MockAssetPathUtil();

            StaticMocks.MockJsonUtility();

            StaticMocks.MockPrefabUtility();

            StaticMocks.MockEditorGUI();

            StaticMocks.MockGUI();

            StaticMocks.MockFont();

            StaticMocks.MockEditorUtility();

            StaticMocks.MockEvent();

            StaticMocks.MockHandles();

            StaticMocks.MockAnimationMode();

            StaticMocks.MockPopupWindow();
        }

        [TearDown]
        public void After() { }

        [Test]
        public void TestConstructionAndRepaint() {
            var functionModel = new AASFunctionModel<TestEnum>();
            Assert.AreEqual(4, functionModel.enumNames.Count);
            // contains the string values of the enumerations above
            Assert.AreEqual("Run", functionModel.enumNames.ElementAt(0));

            functionModel.DeliverRepaintCallback(TestConstructionAndRepaint);
        }

        public void AssetDatabaseMockExtension(Mock<IAssetDatabase> mockAssetDatabase) {
            mockAssetDatabase.Setup(mock => mock.LoadAllAssetsAtPath<ITextAsset>(It.IsAny<string>())).Returns((string x) => {
                // The assets at path only contain the Run state
                var mockAsset = new Mock<ITextAsset>();
                mockAsset.Setup(asset => asset.Text()).Returns("Run");
                return new List<ITextAsset>() { mockAsset.Object };
            });
        }

        [Test]
        public void TestDeliverGameObject() {
            // DeliverGameObject called in this reusable method
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);

            // A text asset will be added for each missing state. 3 states are missing
            mockAssetDatabase.Verify(mock => mock.AddObjectToAsset(It.IsAny<ITextAsset>(), It.IsAny<string>()), Times.Exactly(3));
            // Assets will be saved after updating

            // a keyEditor is constructed for each associated state
            Assert.AreEqual(functionModel.editorModels.Count, 4);
        }

        [Test]
        public void TestNoAssetExistsCreatesAsset() {
            var functionModel = new AASFunctionModel<TestEnum>();
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();

            // this leads to a file that does not currently have a binding for the asset model property
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Impl\\AASFMTestFile1.txt");

            // reinitialize mock asset database
            var mockAssetDatabase = StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);
            AssetDatabaseMockExtension(mockAssetDatabase);
            mockAssetDatabase.Setup(mock => mock.AssetPathToGUID(It.IsAny<string>())).Returns("testguid00");

            functionModel.DeliverGameObject(mockGO.Object);

            MetaSuccess found = MetaUtilities.GetAssociatedValue(metaPath, "AcceleratedStatesAsset", out string value);
            Assert.AreEqual(found, MetaSuccess.ValueFound);
            Assert.AreEqual("testguid00", value); // the guid of the asset created is written into the metafile
        }

        public AASFunctionModel<TestEnum> GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase) {
            var functionModel = new AASFunctionModel<TestEnum>();
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();
            // generic test path
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Impl\\AASFMTestFile0.txt");

            mockAssetDatabase = StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);
            AssetDatabaseMockExtension(mockAssetDatabase);

            functionModel.DeliverGameObject(mockGO.Object);
            return functionModel;
        }

        [Test]
        public void TestSelectionWindowChangeValuesInDrawKeys() {
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);
            functionModel.DeliverRepaintCallback(DoNothing);

            // text fields return value of 20, and this will update the key's values
            StaticMocks.MockEditorGUI().Setup(mock => mock.TextField(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns("20");

            var fieldList = functionModel.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            var targetState = (AASEditorModel)fieldList.Find(field => field.Name.Equals("_targetState")).GetValue(functionModel);
            fieldList = functionModel.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Static).ToList();
            var dropdown = (DropdownMenu)fieldList.Find(field => field.Name.Equals("_keysDropdown")).GetValue(null);
            var props = dropdown.GetType().GetProperties().ToList();
            var prop = props.Find(property => property.Name.Equals("active"));
            prop.SetValue(dropdown, true);

            var column = new Column(0);
            column.Rewind(); // setup draw pointer
            functionModel.SelectionWindow(column);

            var key = targetState.keyGroup.GetKeys()[0];
            Assert.AreEqual(20, key.v);
            Assert.AreEqual(20, key.time);
        }

        [Test]
        public void TestDeleteKeyButton() {
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);
            functionModel.DeliverRepaintCallback(DoNothing);
            var @event = StaticMocks.MockEvent();
            @event.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5)); // within the delete square
            @event.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            // text fields return value of 20, and this will update the key's values
            StaticMocks.MockEditorGUI().Setup(mock => mock.TextField(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns("20");

            var fieldList = functionModel.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            var targetState = (AASEditorModel)fieldList.Find(field => field.Name.Equals("_targetState")).GetValue(functionModel);
            var key = targetState.keyGroup.GetKeys()[0];
            // the delete button will return true when caled
            StaticMocks.MockGUI().Setup(mock => mock.Button(It.IsAny<IRect>(), It.IsAny<ITexture>())).Returns(true);

            var column = new Column(0);
            column.Rewind(); // setup draw pointer
            functionModel.SelectionWindow(column);

            // the key was deleted in the course of the DrawKeys method, the only remaining key is a default
            Assert.AreNotEqual(key.id, targetState.keyGroup.GetKeys()[0].id);
        }

        [Test]
        public void TestForceSave()
        {
            var functionModel = new AASFunctionModel<TestEnum>();
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();

            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Impl\\AASFMTestFile1.txt");
            var mockAssetDatabase = StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);
            AssetDatabaseMockExtension(mockAssetDatabase);
            mockAssetDatabase.Setup(mock => mock.AssetPathToGUID(It.IsAny<string>())).Returns("testguid00");

            functionModel.DeliverGameObject(mockGO.Object);

            mockAssetDatabase.Verify(mock => mock.CreateAsset(It.IsAny<ITextAsset>(), It.IsAny<string>()));

            functionModel.ForceSave();
        }

        [Test]
        public void TestLoadButton()
        {
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);
            functionModel.DeliverRepaintCallback(DoNothing);

            var newList = new List<bool>();
            while (newList.Count < 7)
            {
                newList.Add(true);
            }
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\AASFUTestSave10.asset.txt");
        
            StaticMocks.MockEditorGUI().Setup(mock => mock.TextField(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns("20");
            StaticMocks.MockEditorUtility().Setup(mock => mock.DisplayDialog(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            StaticMocks.MockGUI().Setup(mock => mock.Button(It.IsAny<IRect>(), It.Is<string>((string x) => x.Equals("Load")), It.IsAny<IGUIStyle>())).Returns(true);
            StaticMocks.MockEditorUtility().Setup(mock => mock.OpenFilePanel(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(metaPath);
            StaticMocks.MockFileUtil().Setup(mock => mock.GetProjectRelativePath(It.IsAny<string>())).Returns(metaPath);

            var column = new Column(0);
            column.Rewind();

            var selectionHeader = functionModel.GetType().GetMethod("SelectionHeader", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(functionModel, new object[] { column });
        }

        [Test]
        public void TestSaveAsButton()
        {
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);
            functionModel.DeliverRepaintCallback(DoNothing);

            var newList = new List<bool>();
            while (newList.Count < 7)
            {
                newList.Add(true);
            }
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\AASFUTestSave10.asset.txt");

            StaticMocks.MockEditorGUI().Setup(mock => mock.TextField(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns("20");
            StaticMocks.MockEditorUtility().Setup(mock => mock.DisplayDialog(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            StaticMocks.MockGUI().Setup(mock => mock.Button(It.IsAny<IRect>(), It.Is<string>((string x) => x.Equals("Save As")), It.IsAny<IGUIStyle>())).Returns(true);
            StaticMocks.MockEditorUtility().Setup(mock => mock.SaveFilePanel(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(metaPath);
            StaticMocks.MockFileUtil().Setup(mock => mock.GetProjectRelativePath(It.IsAny<string>())).Returns(metaPath);

            var column = new Column(0);
            column.Rewind();

            var selectionHeader = functionModel.GetType().GetMethod("SelectionHeader", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(functionModel, new object[] { column });
        }

        [Test]
        public void TestRevertButton()
        {
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);
            functionModel.DeliverRepaintCallback(DoNothing);

            var newList = new List<bool>();
            while (newList.Count < 7)
            {
                newList.Add(true);
            }
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\AASFUTestSave10.asset.txt");

            StaticMocks.MockEditorGUI().Setup(mock => mock.TextField(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns("20");
            StaticMocks.MockEditorUtility().Setup(mock => mock.DisplayDialog(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            StaticMocks.MockGUI().Setup(mock => mock.Button(It.IsAny<IRect>(), It.Is<string>((string x) => x.Equals("Revert")), It.IsAny<IGUIStyle>())).Returns(true);

            var column = new Column(0);
            column.Rewind();

            var selectionHeader = functionModel.GetType().GetMethod("SelectionHeader", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(functionModel, new object[] { column });
        }

        [Test]
        public void TestEditWindow()
        {
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);
            functionModel.DeliverRepaintCallback(DoNothing);


            StaticMocks.MockEditorGUI().Setup(mock => mock.TextField(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns("20");
            StaticMocks.MockEditorGUIUtility().Setup(mock => mock.AddCursorRect(It.IsAny<IRect>(), It.IsAny<UnityEditor.MouseCursor>()));

            var column = new Column(0);
            column.Rewind(); 

            functionModel.EditWindow(column, null);

        }

        [Test]
        public void TestUpdate()
        {
            var functionModel = GetBasicFunctionModelForTest(out Mock<IAssetDatabase> mockAssetDatabase);
            functionModel.DeliverRepaintCallback(DoNothing);

            var saveInterval = functionModel.GetType().GetField("_saveInterval", BindingFlags.NonPublic | BindingFlags.Instance);
            saveInterval.SetValue(functionModel, 5);
            Thread.Sleep(10);
            
            functionModel.Update();

        }

        public void DoNothing() {

        }
    }
}
