﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using NUnit.Framework;
using Moq;
using Assets.Code.Editor.CustomGUI.Columns;
using System;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using EventType = Assets.Code.Editor.AdapterLayer.Impl.Static_Classes.EventType;
using Assets.Code.Editor.AdapterLayer.Objects;
using System.Reflection;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Data_Model
{
    class KeyHandleTest
    {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void SetUp()
        {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockHandles();
            StaticMocks.MockGUI();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockEditorGUIUtility();
            StaticMocks.MockAssetDatabase(null);

            var animator = ObjectMocks.MockAnimatorWithGraph();

            _accelAsset = new AccelAsset() { keys = new Key[] { new Key(0, 0), new Key(1, 1) }, name = "Run", pairedAnimation = "Run", xDirection = 1, yDirection = 0 };
            _editorModel = ConversionUtilities.AssetModelToEditorModel(_accelAsset, AnimationGraphUtil.GetAnimationTriggers(animator));
        }

        [TearDown]
        public void After() { }

        private static AccelAsset _accelAsset;
        private static AASEditorModel _editorModel;
        private static KeyHandle _keyHandle;

        [Test]
        public void TestPublic()
        {
            var keyEditor = new AASKeyEditor();
            var keys = keyEditor.GetKeys();
            var @event = StaticMocks.MockEvent();
            //@event.Setup(mock => mock.current.keyCode).Returns((KeyCode.LeftControl));
            @event.SetupGet(mock => mock.rawType).Returns(EventType.scrollWheel);
            @event.SetupGet(mock => mock.mousePosition).Returns(DB.NewVector2(200, 10));  
            @event.SetupGet(mock => mock.delta.y).Returns(3); 
            Column col = new Column(0) { width = 800, height = 500 };
            keyEditor.OnGUI(col);
            Canvas canvas = GetKeyEditorCanvas(keyEditor);
            _keyHandle = new KeyHandle(keys[0], canvas);
            _keyHandle.Draw(DB.NewRect(0, 0, 10, 10));
 
            _keyHandle.Control();
        }

        private Canvas GetKeyEditorCanvas(AASKeyEditor keyEditor) {
            throw new NotImplementedException();
        }

        [Test]
        public void TestProtected()
        {
            var keyEditor = new AASKeyEditor();
            var keys = keyEditor.GetKeys();
            float mouseX = 100;
            float mouseY = 1100;
            Canvas canvas = GetKeyEditorCanvas(keyEditor);
            _keyHandle = new KeyHandle(keys[0], canvas);
            var @event = StaticMocks.MockEvent();
            @event.Setup(mock => mock.current.mousePosition).Returns(DB.ZeroVector);
            var horizChange = _keyHandle.GetType().GetMethod("OnHorizontalChange", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(_keyHandle, new object[] { mouseX });
            var vertChange = _keyHandle.GetType().GetMethod("OnVerticalChange", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(_keyHandle, new object[] { mouseY });
            @event.Setup(mock => mock.current.mousePosition.y).Returns(1000);
            @event.Setup(mock => mock.current.mousePosition.x).Returns(50);
            horizChange = _keyHandle.GetType().GetMethod("OnHorizontalChange", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(_keyHandle, new object[] { mouseX });
            vertChange = _keyHandle.GetType().GetMethod("OnVerticalChange", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(_keyHandle, new object[] { mouseY });
        }
    }
}
