﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics.Accelerators;
using NUnit.Framework;
using Moq;
using Assets.Code.Editor.CustomGUI.Columns;
using System;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using EventType = Assets.Code.Editor.AdapterLayer.Impl.Static_Classes.EventType;
using Assets.Code.Editor.AdapterLayer.Objects;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Data_Model
{
    class KeyGroupTest
    {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void SetUp()
        {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockHandles();
            StaticMocks.MockGUI();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockEditorGUIUtility();
            StaticMocks.MockAssetDatabase(null);

            var animator = ObjectMocks.MockAnimatorWithGraph();

            _accelAsset = new AccelAsset() { keys = new Key[] { new Key(0, 0), new Key(1, 1) }, name = "Run", pairedAnimation = "Run", xDirection = 1, yDirection = 0 };
            _editorModel = ConversionUtilities.AssetModelToEditorModel(_accelAsset, AnimationGraphUtil.GetAnimationTriggers(animator));
        }

        [TearDown]
        public void After() { }

        private static AccelAsset _accelAsset;
        private static AASEditorModel _editorModel;
        private static KeyGroup _keyGroup;

        [Test]
        public void TestAddKey()
        {
            _keyGroup = new KeyGroup(new Key[0]);
            _keyGroup.AddKey(1, 1);
            var @event = StaticMocks.MockEvent();
            _keyGroup.AddActiveKeyFromCoords(1, 1);
            var key = _keyGroup.GetTargetKeyId();
        }
    }
}
