using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Assets.Code.Physics;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using static Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl.AASFunctionModelTest;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Utilities
{
    internal class AASFileUtilityTest
    {

        [SetUp]
        public static void SetUp()
        {
            StaticMocks.DependencyBuilderMock();

            StaticMocks.MockEditorStyles();

            StaticMocks.MockAssetDatabase("");

            StaticMocks.MockAssetPreview();

            StaticMocks.MockAssetPathUtil();

            StaticMocks.MockJsonUtility();

            StaticMocks.MockPrefabUtility();

            StaticMocks.MockEditorGUI();

            StaticMocks.MockGUI();

            StaticMocks.MockFont();

            StaticMocks.MockEditorUtility();

            StaticMocks.MockEvent();

            StaticMocks.MockHandles();

            StaticMocks.MockAnimationMode();
        }

        [TearDown]
        public void After() { }

        private static AccelAsset BuildAccelModel(string pAnim = "Idle")
        {
            AccelAsset assetModel = new AccelAsset();
            assetModel.name = "model";
            assetModel.pairedAnimation = pAnim;
            assetModel.keys = new Key[1];
            assetModel.keys[0] = new Key(0, 0);
            assetModel.xDirection = 1;
            assetModel.yDirection = 1;
            return assetModel;
        }

        public void AssetDatabaseMockExtension(Mock<IAssetDatabase> mockAssetDatabase)
        {
            mockAssetDatabase.Setup(mock => mock.LoadAllAssetsAtPath<ITextAsset>(It.IsAny<string>())).Returns((string x) =>
            {
                // The assets at path only contain the Run state
                var mockAsset = new Mock<ITextAsset>();
                mockAsset.Setup(asset => asset.Text()).Returns("Run");
                return new List<ITextAsset>() { mockAsset.Object };
            });
        }

        [Test]
        public void TestSaveModels()
        {
            List<AccelAsset> accelModel = new List<AccelAsset>();
            accelModel.Add(BuildAccelModel());
            accelModel.Add(BuildAccelModel("remove"));
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();
            string assetName = "AASFUTestSave.asset";
            AASFileUtility.SaveModels(mockGO.Object, accelModel, assetName);
        }

        [Test]
        public void TestSaveAsModels()
        {
            List<AccelAsset> accelModel = new List<AccelAsset>();
            accelModel.Add(BuildAccelModel());
            accelModel.Add(BuildAccelModel("remove"));
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();
            var prefabUtil = StaticMocks.MockPrefabUtility();
            var assetPathUtil = StaticMocks.MockAssetPathUtil();
            var functionModel = new AASFunctionModel<TestEnum>();
            var db = StaticMocks.DependencyBuilderMock();
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\AASFUTestSave10.asset.txt");
            string folderPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\");

            var mockEditorUtility = StaticMocks.MockEditorUtility();
            var mockFileUtil = StaticMocks.MockFileUtil();
            var mockAssetDatabase = StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);
            AssetDatabaseMockExtension(mockAssetDatabase);
            mockAssetDatabase.Setup(mock => mock.AssetPathToGUID(It.IsAny<string>())).Returns("saveTest00");
            mockAssetDatabase.Setup(mock => mock.GUIDToAssetPath(It.IsAny<string>())).Returns(folderPath);
            prefabUtil.Setup(mock => mock.GetPrefabAssetPathOfNearestInstanceRoot(It.IsAny<IGameObject>())).Returns(metaPath);
            mockAssetDatabase.Setup(mock => mock.GetTextMetaFilePathFromAssetPath(It.IsAny<string>())).Returns(metaPath);
            assetPathUtil.Setup(mock => mock.GetAssetFolderPath(It.IsAny<string>())).Returns(folderPath);

            mockEditorUtility.Setup(mock =>
                    mock.SaveFilePanel(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(metaPath);
            mockFileUtil.Setup(mock => mock.GetProjectRelativePath(It.IsAny<string>())).Returns(metaPath);

            functionModel.DeliverGameObject(mockGO.Object);

            AASFileUtility.SaveAsModels(mockGO.Object, accelModel);
        }

        [Test]
        public void TestLoadAssets()
        {
            List<AccelAsset> accelModel = new List<AccelAsset>();
            accelModel.Add(BuildAccelModel());
            accelModel.Add(BuildAccelModel("remove"));
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();
            var prefabUtil = StaticMocks.MockPrefabUtility();
            var assetPathUtil = StaticMocks.MockAssetPathUtil();
            var jsonUtil = StaticMocks.MockJsonUtility();
            var functionModel = new AASFunctionModel<TestEnum>();
            var db = StaticMocks.DependencyBuilderMock();
            string assetName = "AASFUTestSave10";
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\AASFUTestSave10.asset.txt");
            string folderPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\");

            var mockAssetDatabase = StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);
            AssetDatabaseMockExtension(mockAssetDatabase);
            mockAssetDatabase.Setup(mock => mock.AssetPathToGUID(It.IsAny<string>())).Returns("saveTest00");
            mockAssetDatabase.Setup(mock => mock.GUIDToAssetPath(It.IsAny<string>())).Returns(folderPath);
            prefabUtil.Setup(mock => mock.GetPrefabAssetPathOfNearestInstanceRoot(It.IsAny<IGameObject>())).Returns(metaPath);
            mockAssetDatabase.Setup(mock => mock.GetTextMetaFilePathFromAssetPath(It.IsAny<string>())).Returns(metaPath);
            assetPathUtil.Setup(mock => mock.GetAssetFolderPath(It.IsAny<string>())).Returns(folderPath);

            functionModel.DeliverGameObject(mockGO.Object);

            AASFileUtility.LoadAssetModels<IvoryHuskPSM.IvoryHuskStates>(mockGO.Object, functionModel.enumNames, assetName);
        }

        [Test]
        public void TestLoadUsingButton()
        {
            List<AccelAsset> accelModel = new List<AccelAsset>();
            accelModel.Add(BuildAccelModel());
            accelModel.Add(BuildAccelModel("remove"));
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();
            var prefabUtil = StaticMocks.MockPrefabUtility();
            var assetPathUtil = StaticMocks.MockAssetPathUtil();
            var jsonUtil = StaticMocks.MockJsonUtility();
            var functionModel = new AASFunctionModel<TestEnum>();
            var db = StaticMocks.DependencyBuilderMock();
            string assetName = "AASFUTestSave10";
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\AASFUTestSave10.asset.txt");
            string folderPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\");

            var mockEditorUtility = StaticMocks.MockEditorUtility();
            var mockFileUtil = StaticMocks.MockFileUtil();
            var mockAssetDatabase = StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);
            AssetDatabaseMockExtension(mockAssetDatabase);
            mockAssetDatabase.Setup(mock => mock.AssetPathToGUID(It.IsAny<string>())).Returns("saveTest00");
            mockAssetDatabase.Setup(mock => mock.GUIDToAssetPath(It.IsAny<string>())).Returns(folderPath);
            prefabUtil.Setup(mock => mock.GetPrefabAssetPathOfNearestInstanceRoot(It.IsAny<IGameObject>())).Returns(metaPath);
            mockAssetDatabase.Setup(mock => mock.GetTextMetaFilePathFromAssetPath(It.IsAny<string>())).Returns(metaPath);
            assetPathUtil.Setup(mock => mock.GetAssetFolderPath(It.IsAny<string>())).Returns(folderPath);

            mockEditorUtility.Setup(mock =>
                    mock.OpenFilePanel(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(metaPath);
            mockFileUtil.Setup(mock => mock.GetProjectRelativePath(It.IsAny<string>())).Returns(metaPath);

            functionModel.DeliverGameObject(mockGO.Object);

            AASFileUtility.LoadUsingButton(mockGO.Object, functionModel.enumNames, assetName, metaPath);

        }

        [Test]
        public void TestAutoSave()
        {
            List<AccelAsset> accelModel = new List<AccelAsset>();
            accelModel.Add(BuildAccelModel());
            accelModel.Add(BuildAccelModel("remove"));
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();
            var prefabUtil = StaticMocks.MockPrefabUtility();
            var assetPathUtil = StaticMocks.MockAssetPathUtil();
            var jsonUtil = StaticMocks.MockJsonUtility();
            var functionModel = new AASFunctionModel<TestEnum>();
            var db = StaticMocks.DependencyBuilderMock();
            string assetName = "AASFUTestSave10";
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\AASFUTestSave10.asset.txt");
            string folderPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\");

            var mockAssetDatabase = StaticMocks.MockAssetDatabase(metaPath, TestContext.CurrentContext.TestDirectory);
            AssetDatabaseMockExtension(mockAssetDatabase);
            mockAssetDatabase.Setup(mock => mock.AssetPathToGUID(It.IsAny<string>())).Returns("saveTest00");
            mockAssetDatabase.Setup(mock => mock.GUIDToAssetPath(It.IsAny<string>())).Returns(folderPath);
            prefabUtil.Setup(mock => mock.GetPrefabAssetPathOfNearestInstanceRoot(It.IsAny<IGameObject>())).Returns(metaPath);
            mockAssetDatabase.Setup(mock => mock.GetTextMetaFilePathFromAssetPath(It.IsAny<string>())).Returns(metaPath);
            assetPathUtil.Setup(mock => mock.GetAssetFolderPath(It.IsAny<string>())).Returns(folderPath);

            functionModel.DeliverGameObject(mockGO.Object);

            AASFileUtility.AutoSaveModels(mockGO.Object, accelModel, assetName);
        }
    }
}