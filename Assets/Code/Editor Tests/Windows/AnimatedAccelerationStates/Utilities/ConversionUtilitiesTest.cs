﻿using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Physics.Accelerators;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using JetBrains.Annotations;
using Moq;
using UnityEditor;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.Windows.PopoutWindow;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Utilities
{
    class ConversionUtilitiesTest {
        private static IAnimator _animator;

        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void SetUp() {

            StaticMocks.DependencyBuilderMock();

            StaticMocks.MockAssetDatabase("");

            StaticMocks.MockEditorUtility();

            _animator = ObjectMocks.MockAnimatorWithGraph();

            var ConversionUtil = (ConversionUtilities) Activator.CreateInstance(typeof(ConversionUtilities), true);
        }

        [TearDown]
        public void After() { }

        private static AccelAsset BuildAccelModel()
        {
            AccelAsset assetModel = new AccelAsset();
            assetModel.name = "model";
            assetModel.pairedAnimation = "Idle";
            assetModel.animationTrigger = "Idle";
            assetModel.keys = new Key[1];
            assetModel.keys[0] = new Key(0, 0);
            assetModel.xDirection = 1;
            assetModel.yDirection = 1;
            return assetModel;
        }

        private static IEnumerable<AccelAsset> ListAccelModels()
        {
            foreach (var num in Enumerable.Range(1,4))
            {
                yield return BuildAccelModel();
            }
        }


        private static AASEditorModel BuildEditorModel() {
            AccelAsset assetModel = BuildAccelModel();
            AASEditorModel editorModel = new AASEditorModel();
            editorModel.name = assetModel.name;
            editorModel.pairedAnimation = assetModel.pairedAnimation;
            editorModel.keyGroup = new KeyGroup(assetModel.keys);
            IEnumerable<PopoutSelectorEntry<string>> triggers = new List<PopoutSelectorEntry<string>>() { new PopoutSelectorEntry<string>("Idle", "Idle"), new PopoutSelectorEntry<string>("Trot", "Trot") };
            editorModel.animationTriggerSelector = new PopoutDrawer<PopoutSelectorEntry<string>>(new PopoutSelector<string>(triggers, triggers.ElementAt(1)));

            IVector2 direction = DB.NewVector2(assetModel.xDirection, assetModel.yDirection).Equals(DB.ZeroVector())
                ? DB.NewVector2(1, 0) : DB.NewVector2(assetModel.xDirection, assetModel.yDirection);

            editorModel.knobControl = new KnobControl(direction);

            return editorModel;
        }

        private static IEnumerable<AASEditorModel> ListEditorModels()
        {
            foreach (var num in Enumerable.Range(1, 4))
            {
                yield return BuildEditorModel();
            }
        }


        [Test]
        public void TestAccelModelToEditorModel() {
            var accelModel = BuildAccelModel();
            AASEditorModel editorModel = ConversionUtilities.AssetModelToEditorModel(accelModel, AnimationGraphUtil.GetAnimationTriggers(_animator));

            Assert.AreEqual(accelModel.name, editorModel.name);
            Assert.AreEqual(accelModel.pairedAnimation, editorModel.pairedAnimation);
            Assert.AreEqual(accelModel.yDirection, editorModel.knobControl.vector.y);
            Assert.That(editorModel.keyGroup.GetKeys(), Is.Not.Null);
        }

        [Test]
        public void TestEditorModelToAccelModel() {
            var editorModel = BuildEditorModel();
            AccelAsset assetModel = ConversionUtilities.EditorModelToAssetModel(editorModel);
 
            Assert.AreEqual(editorModel.name, assetModel.name);
            Assert.AreEqual(editorModel.pairedAnimation, assetModel.pairedAnimation);
            Assert.AreEqual(editorModel.knobControl.vector.x, assetModel.xDirection);
            Assert.AreEqual(editorModel.knobControl.vector.y, assetModel.yDirection);
            Assert.That(assetModel.keys, Is.Not.Null);
        }

        [Test]
        public void TestPackageAll()
        {
            List<AASEditorModel> madeList = ListEditorModels().ToList();
            List<AccelAsset> newList = ConversionUtilities.PackageAll(madeList).ToList();
            Assert.AreEqual(newList.Count, madeList.Count);
            Assert.IsInstanceOf<AccelAsset>(newList[0]);
        }

        [Test]
        public void TestUnpackAll()
        {
            List<AccelAsset> madeList = ListAccelModels().ToList();
            List<AASEditorModel> newList = ConversionUtilities.UnpackAll(madeList, AnimationGraphUtil.GetAnimationTriggers(_animator)).ToList();
            Assert.AreEqual(newList.Count, madeList.Count);
            Assert.IsInstanceOf<AASEditorModel>(newList[0]);
            
        }
    }


}
