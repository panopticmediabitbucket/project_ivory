﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Utilities {
    public class AnimationGraphUtilTest {
        static IAnimator _animator;
        [SetUp]
        public static void SetUp() {
            _animator = ObjectMocks.MockAnimatorWithGraph();
        }


        [Test]
        public void TestGetAnimationTriggers() {
            var response = AnimationGraphUtil.GetAnimationTriggers(_animator);
            Assert.Contains("Idle", response);
            Assert.Contains("Trot", response);
        }

        [Test]
        public void TestFindAnimationTriggers() {
            var response = AnimationGraphUtil.FindAnimationPairsByTrigger(_animator);
            Assert.IsNotEmpty(response);
            Assert.AreEqual(response[0].trigger, "Idle");
            Assert.AreEqual(response[0].statePairs.Count(), 2);
            // same state found at the end of the transition
            var e1 = response[0].statePairs.ElementAt(0).B;
            var e2 = _animator.runtimeAnimatorController.layers[0].stateMachine.states.ToList().Find(x => x.name.Equals("Idle"));
            Assert.True(e1 == e2);
            Assert.AreEqual(response[1].trigger, "Trot");
            Assert.AreEqual(response[1].statePairs.Count(), 1);
        }
    }
}
