﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.PopoutWindow;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Code.Editor.Tests.Windows.PopoutWindow {
    public class TestPopoutSelector {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockHandles();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockGUI();
            StaticMocks.MockEvent();
            StaticMocks.MockAssetDatabase(null);
        }

        public virtual void CallbackCaller() { }

        public PopoutSelector<string> GetTestSelector() {
            IEnumerable<PopoutSelectorEntry<string>> entries = new List<PopoutSelectorEntry<string>>() {
                new PopoutSelectorEntry<string>("Idle", "Idle"), new PopoutSelectorEntry<string>("Trot", "Trot")
            };
            return new PopoutSelector<string>(entries, entries.First());
        }

        [Test]
        public void TestOnGUI() {
            var selector = GetTestSelector();
            var mockHandles = StaticMocks.MockHandles();
            var mockEvent = StaticMocks.MockEvent();
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.mouseDown);

            selector.Activate(100, 50);
            Assert.AreEqual(100, selector.GetWindowDimensions().x);
            // height = elementheight * # of entries
            Assert.AreEqual(selector.GetWindowDimensions().y, 60);

            selector.OnGUI(DB.NewRect(0, 0, 100, 50));
            // one drawn for each entrie
            mockHandles.Verify(mock => mock.DrawWireDisc(It.IsAny<IVector3>(), It.IsAny<IVector3>(), It.IsAny<float>()), Times.Exactly(2));
            // one drawn for the selected item
            mockHandles.Verify(mock => mock.DrawSolidDisc(It.IsAny<IVector3>(), It.IsAny<IVector3>(), It.IsAny<float>()), Times.Exactly(1));

            var summary = selector.contentSummary;
            Assert.AreEqual(summary.value, new PopoutSelectorEntry<string>("Idle", "Idle"));
        }

        [Test]
        public void TestCallbackInvoked() {
            PopoutSelector<string> selector = new PopoutSelector<string>(new List<PopoutSelectorEntry<string>>(), default);
            var mockThis = new Mock<TestPopoutSelector>();
            selector.RegisterOnCloseCallback(mockThis.Object.CallbackCaller);
            selector.OnClose();
            mockThis.Verify(mock => mock.CallbackCaller(), Times.Once());
        } 

        [Test]
        public void TestPopoutSelectorContentSummaryDraw() {
            var mockGUI = StaticMocks.MockGUI();
            var mockDB = StaticMocks.MockAssetDatabase(null);
            var dropdownMock = ObjectMocks.GetMockTexture().Object;
            mockDB.Setup(mock => mock.GUIDToAssetPath(It.IsAny<string>())).Returns("dropdownArrow");
            mockDB.Setup(mock => mock.LoadAssetAtPath<ITexture>(It.Is<string>(x => "dropdownArrow".Equals(x)))).Returns(dropdownMock);
            var mockTex = DB.NewGUIContent("");
            PopoutSelectorContentSummary<string> popoutSelectorContentSummary = new PopoutSelectorContentSummary<string>(new PopoutSelectorEntry<string>("Idle", "Idle"),
                PopoutSelectorStyle.BuildStyle(true, mockTex));

            popoutSelectorContentSummary.Draw();
            mockGUI.Verify(mock => mock.Box(It.IsAny<IRect>(), It.Is<ITexture>(x => x == mockTex), It.IsAny<IGUIStyle>()));
            mockGUI.Verify(mock => mock.Box(It.IsAny<IRect>(), It.Is<ITexture>(x => x == dropdownMock), It.IsAny<IGUIStyle>()), Times.Once());

            popoutSelectorContentSummary.DrawNotOpenable(DB.NewRect(0, 0, 0, 0));
            // the arrow will not be drawn for non-openables, thus it is still just called once
            mockGUI.Verify(mock => mock.Box(It.IsAny<IRect>(), It.Is<ITexture>(x => x == dropdownMock), It.IsAny<IGUIStyle>()), Times.Once());

        }

        [Test]
        public void TestPopoutSelectorEntry() {
            PopoutSelectorEntry<string> selectorEntry = new PopoutSelectorEntry<string>("Idle", "Idle");
            PopoutSelectorEntry<string> selectorEntry2 = new PopoutSelectorEntry<string>("Idle", "Idle");
            PopoutSelectorEntry<string> selectorEntry3 = new PopoutSelectorEntry<string>("Idle", "Run");

            Assert.False(selectorEntry.Equals("Idle"));
            Assert.False(selectorEntry.Equals(selectorEntry3));
            Assert.True(selectorEntry.Equals(selectorEntry2));
            Assert.True(selectorEntry == selectorEntry2);
            Assert.True(selectorEntry != selectorEntry3);
            Assert.AreEqual(selectorEntry.GetHashCode(), selectorEntry2.GetHashCode());
        }

        [Test]
        public void TestPopoutSelectorStyle() {
            PopoutSelectorStyle style = PopoutSelectorStyle.BuildStyle(true, null, DB.NewGUIStyle());
            PopoutSelectorStyle.BuildStyle();
            Assert.True(style.isBordered);
            Assert.True(style.guiContent == null);
            Assert.True(style.drawerColor.r == .2f && style.drawerColor.g == .2f && style.drawerColor.b == .2f);
            Assert.True(style.openDrawerColor.r == .4f && style.openDrawerColor.g == .4f && style.openDrawerColor.b == .4f);
        }
    }
}
