﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Utilities;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.CustomGUI.Controls;


namespace Assets.Code.Editor.Tests.General_Utilities
{
    class InputUtilitiesTest
    {
        public static IEvent Event => EventAdapter.Instance;

        [SetUp]
        public static void SetUp()
        {

            StaticMocks.DependencyBuilderMock();

            StaticMocks.MockEditorStyles();

            StaticMocks.MockAssetDatabase("");

            StaticMocks.MockAssetPreview();

            StaticMocks.MockAssetPathUtil();

            StaticMocks.MockJsonUtility();

            StaticMocks.MockPrefabUtility();

            StaticMocks.MockEditorGUI();

            StaticMocks.MockGUI();

            StaticMocks.MockFont();

            StaticMocks.MockEditorUtility();

            StaticMocks.MockEvent();

            StaticMocks.MockHandles();

            StaticMocks.MockAnimationMode();

            StaticMocks.MockPopupWindow();
        }

        [TearDown]
        public void After() { }

        [Test]
        public void TestIsKeyHeld()
        {
            var @event = StaticMocks.MockEvent();
            var keyCode = (KeyCode)32;
            @event.Setup(mock => mock.keyCode).Returns(keyCode);
            @event.Setup(mock => mock.current.rawType).Returns(EventType.keyDown);
            @event.Setup(mock => mock.current.keyCode).Returns(keyCode);
            var down = InputUtilities.IsKeyHeld(keyCode);
            @event.Setup(mock => mock.current.rawType).Returns(EventType.KeyUp);
            var up = InputUtilities.IsKeyHeld(keyCode);
        }
    }
}
