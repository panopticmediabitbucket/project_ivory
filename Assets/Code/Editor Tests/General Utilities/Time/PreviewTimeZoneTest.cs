﻿using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities.Time;
using NUnit.Framework;
using System;
using System.Threading;
using Assets.Code.System.TimeUtil;

namespace Assets.Code.Editor.Utilities.Time {
    public class PreviewTimeZoneTest {
        [Test]
        public void TestMatchesUnderlyingTimer() {
            PreviewTimeZone previewTimeZone = new PreviewTimeZone();
            PreviewTimer previewTimer = new PreviewTimer();

            previewTimeZone.underlyingTimer = previewTimer;

            previewTimer.Start();
            Thread.Sleep(100);
            previewTimer.Update();

            Assert.AreEqual(previewTimer.deltaTime, previewTimeZone.deltaTime);

            Assert.AreNotEqual(previewTimeZone.id, new PreviewTimeZone().id);

            previewTimeZone.Update();

            Assert.AreEqual(1, previewTimeZone.timeScale);
        }
    }
}
