﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Utilities;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using System.Collections;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.CustomGUI.Controls;

namespace Assets.Code.Editor.Tests.General_Utilities
{
    class MetaUtilitiesTest
    {
        
        [SetUp]
        public static void SetUp()
        {

            StaticMocks.DependencyBuilderMock();

            StaticMocks.MockEditorStyles();

            StaticMocks.MockAssetDatabase("");

            StaticMocks.MockAssetPreview();

            StaticMocks.MockAssetPathUtil();

            StaticMocks.MockJsonUtility();

            StaticMocks.MockPrefabUtility();

            StaticMocks.MockEditorGUI();

            StaticMocks.MockGUI();

            StaticMocks.MockFont();

            StaticMocks.MockEditorUtility();

            StaticMocks.MockEvent();

            StaticMocks.MockHandles();

            StaticMocks.MockAnimationMode();

            StaticMocks.MockPopupWindow();
        }

        [TearDown]
        public void After() { }

        [Test]
        public void TestGuidFromAsset()
        {
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();
            MetaUtilities.GuidFromAsset(mockGO.Object);
        }

        [Test]
        public void TestWriteValue()
        {
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\Fake.txt");
            MetaUtilities.WriteAssociatedValue(metaPath, "Not here", "Still not here");
        }

        [Test]
        public void TestGetValue()
        {
            UnityEngine.Debug.unityLogger.logEnabled = false;
            string metaPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Utilities\\TestSaves\\Fake.txt");
            MetaUtilities.GetAssociatedValue(metaPath, "Not here", out string val);
        }
    }
}
