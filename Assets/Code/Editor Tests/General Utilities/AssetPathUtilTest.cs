﻿using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Utilities;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.DataModel;
using System;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.CustomGUI.Controls;
using UnityEngine;

namespace Assets.Code.Editor.Tests.General_Utilities
{
    class AssetPathUtilTest
    {

        [SetUp]
        public static void SetUp()
        {

            StaticMocks.DependencyBuilderMock();

            StaticMocks.MockEditorStyles();

            StaticMocks.MockAssetDatabase("");

            StaticMocks.MockAssetPreview();

            StaticMocks.MockAssetPathUtil();

            StaticMocks.MockJsonUtility();

            StaticMocks.MockPrefabUtility();

            StaticMocks.MockEditorGUI();

            StaticMocks.MockGUI();

            StaticMocks.MockFont();

            StaticMocks.MockEditorUtility();

            StaticMocks.MockEvent();

            StaticMocks.MockHandles();

            StaticMocks.MockAnimationMode();

            StaticMocks.MockPopupWindow();
        }

        [TearDown]
        public void After() { }

      
        [Test]
        public void TestCreatePrefabDialog()
        {
            var path = "Assets\\Code\\Editor\\Tests\\Windows\\AnimatedAccelerationStates\\Impl\\AASFMTestFile1.txt";
            var editorUtility = StaticMocks.MockEditorUtility();
            var fileUtil = StaticMocks.MockFileUtil();
            var prefabUtil = StaticMocks.MockPrefabUtility();
            Mock<IGameObject> mockGO = ObjectMocks.MockGameObject();

            editorUtility.Setup(mock => mock.SaveFilePanel(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(path);
            fileUtil.Setup(mock => mock.GetProjectRelativePath(It.IsAny<string>())).Returns(path);
            prefabUtil.Setup(mock => mock.SaveAsPrefabAssetAndConnect(It.IsAny<IGameObject>(), It.IsAny<string>(), It.IsAny<InteractionMode>()));

            AssetPathUtil.CreatePrefabDialog(mockGO.Object);
        }


        //[Test]
        //public void TestProjectRoot()
        //{
        //    var root = AssetPathUtil.ProjectRoot();
        //    Assert.IsInstanceOf<string>(root);
        //}

        [Test]
        public void TestGetAssetFolderPath()
        {
            var assetPath = "Assets/Code/Editor/Tests/Windows/AnimatedAccelerationStates/Impl/AASFMTestFile1.txt";
            AssetPathUtil.GetAssetFolderPath(assetPath);
        }

        [Test]
        public void TestGetParentFolder()
        {
            var folderPath = "Assets/Code/Editor/Tests/Windows/AnimatedAccelerationStates/Impl/";
            AssetPathUtil.GetParentFolder(folderPath);
        }


    }
}
