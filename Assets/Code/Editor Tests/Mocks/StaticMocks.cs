﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Objects;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Utilities.Time;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Interfaces;
using Assets.Code.System.Utilities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.System.TimeUtil;
using static Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl.AASFunctionModelTest;

namespace Assets.Code.Editor.Tests.Mocks {
    public class StaticMocks {

        private static Mock<IDependencyBuilder> _dependencyBuilder;
        public static Mock<IStateBasedMotionMachine<TestEnum>> stateBasedMotionMachine { get; private set; }
        public static Mock<IDependencyBuilder> DependencyBuilderMock() {
            var dependencyBuilder = new Mock<IDependencyBuilder>();
            _dependencyBuilder = dependencyBuilder;
            dependencyBuilder.Setup(db => db.NewColor(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>())).Returns((float x, float y, float z) => {
                var color = new Mock<IColor>();
                color.Setup(mock => mock.GetColor()).Returns(new float[] { x, y, z });
                color.SetupGet(mock => mock.r).Returns(x);
                color.SetupGet(mock => mock.g).Returns(y);
                color.SetupGet(mock => mock.b).Returns(z);
                return color.Object;
            });
            dependencyBuilder.Setup(mock => mock.NewGUIStyle(It.IsAny<IGUIStyle>())).Returns((IGUIStyle x) => {
                var ret = new Mock<IGUIStyle>();
                return ret.Object;
            });
            dependencyBuilder.Setup(mock => mock.NewGUIContent(It.IsAny<string>())).Returns((string x) => {
                var ret = new Mock<IGUIContent>();
                ret.Setup(guiContent => guiContent.Text()).Returns(x);
                return ret.Object;
            });
            dependencyBuilder.Setup(mock => mock.NewGUIStyle(It.IsAny<Action<IGUIStyle>[]>())).Returns((Action<IGUIStyle>[] actions) => {
                Mock<IGUIStyle> guiStyle = MockGUIStyle();
                return guiStyle.Object;
            });
            dependencyBuilder.Setup(mock => mock.NewTextAsset(It.IsAny<string>())).Returns((string x) => {
                var ret = new Mock<ITextAsset>();
                ret.Setup(ta => ta.Text()).Returns(x);
                return ret.Object;
            });
            dependencyBuilder.Setup(mock => mock.NewVector2(It.IsAny<float>(), It.IsAny<float>())).Returns((float x, float y) => {
                var vect = new Mock<IVector2>();
                float intx = x;
                float inty = y;
                vect.SetupGet(mock => mock.x).Returns(intx);
                vect.SetupSet(mock => mock.x).Callback(val => intx = val);
                vect.SetupGet(mock => mock.y).Returns(inty);
                vect.SetupSet(mock => mock.y).Callback(val => inty = val);
                vect.Setup(mock => mock.Add(It.IsAny<IVector2>())).Returns((IVector2 value) => dependencyBuilder.Object.NewVector2(intx + value.x, inty + value.y));
                vect.Setup(mock => mock.Sub(It.IsAny<IVector2>())).Returns((IVector2 value) => dependencyBuilder.Object.NewVector2(intx - value.x, inty - value.y));
                vect.Setup(mock => mock.DividedBy(It.IsAny<float>())).Returns((float divisor) => dependencyBuilder.Object.NewVector2(intx / divisor, inty / divisor));
                vect.Setup(mock => mock.Mul(It.IsAny<float>())).Returns((float multiplicand) => dependencyBuilder.Object.NewVector2(intx * multiplicand, inty * multiplicand));
                return vect.Object;
            });
            dependencyBuilder.Setup(mock => mock.NewVector2(It.IsAny<IVector2>())).Returns((IVector2 vec2) => dependencyBuilder.Object.NewVector2(vec2.x, vec2.y));
            dependencyBuilder.Setup(mock => mock.ZeroVector()).Returns(dependencyBuilder.Object.NewVector2(0, 0));
            dependencyBuilder.Setup(mock => mock.NewRect(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()))
                .Returns((float x, float y, float width, float height) => {
                    var rect = new MockRectWrapper(x, y, width, height);
                    return rect.rect.Object;
                });
            dependencyBuilder.Setup(mock => mock.NewRect(It.IsAny<IVector2>(), It.IsAny<IVector2>())).Returns((IVector2 a, IVector2 b) => {
                return dependencyBuilder.Object.NewRect(a.x, a.y, b.x, b.y);
            });
            dependencyBuilder.Setup(mock => mock.NewRect(It.IsAny<IRect>())).Returns((IRect rect) => {
                return dependencyBuilder.Object.NewRect(rect.x, rect.y, rect.width, rect.height);
            });
            dependencyBuilder.Setup(mock => mock.ZeroRect()).Returns(dependencyBuilder.Object.NewRect(0, 0, 0, 0));

            dependencyBuilder.Setup(mock => mock.NewStateBasedMotionMachine<TestEnum>(It.IsAny<IGameObject>())).Returns((IGameObject gameObject) => {
                stateBasedMotionMachine = new Mock<IStateBasedMotionMachine<TestEnum>>();
                ITimeZone timeZone = new PreviewTimeZone();
                stateBasedMotionMachine.SetupSet(x => x.timeZone).Callback((ITimeZone tz) => timeZone = tz);
                stateBasedMotionMachine.SetupGet(x => x.timeZone).Returns(timeZone);
                return stateBasedMotionMachine.Object;
            });
            DependencyManager.Instance = dependencyBuilder.Object;

            return dependencyBuilder;
        }

        public static Mock<IHandles> MockHandles() {
            var handles = new Mock<IHandles>();
            HandlesAdapter.Instance = handles.Object;
            return handles;
        }

        public static Mock<IPopupWindow> MockPopupWindow() {
            var popupWindow = new Mock<IPopupWindow>();
            PopupWindowAdapter.Instance = popupWindow.Object;
            return popupWindow;
        }

        public static Mock<IEvent> MockEvent() {
            var @event = new Mock<IEvent>();
            @event.Setup(mock => mock.current).Returns(@event.Object);
            @event.Setup(mock => mock.keyCode).Returns(KeyCode.None);
            @event.Setup(mock => mock.mousePosition).Returns(_dependencyBuilder.Object.NewVector2(0, 0));
            @event.Setup(mock => mock.rawType).Returns(EventType.ignore);
            EventAdapter.Instance = @event.Object;
            return @event;
        }

        public static Mock<IGUIStyle> MockGUIStyle() {
            var guistyle = new Mock<IGUIStyle>();
            guistyle.SetupGet(mock => mock.normal).Returns(new Mock<IGUIStyleState>().Object);
            return guistyle;
        }

        public static Mock<ISelection> MockSelection() {
            var selection = new Mock<ISelection>();
            selection.SetupGet(x => x.activeGameObject).Returns(ObjectMocks.MockGameObject().Object);
            SelectionAdapter.Instance = selection.Object;
            return selection;
        }

        public static Mock<IEditorStyles> MockEditorStyles() {
            var editorStyles = new Mock<IEditorStyles>();
            editorStyles.Setup(mock => mock.BoldLabel()).Returns(new Mock<IGUIStyle>().Object);
            editorStyles.Setup(mock => mock.MiniLabel()).Returns(new Mock<IGUIStyle>().Object);
            editorStyles.Setup(mock => mock.NumberField()).Returns(new Mock<IGUIStyle>().Object);
            EditorStylesAdapter.Instance = editorStyles.Object;
            return editorStyles;
        }

        public static Mock<IAssetDatabase> MockAssetDatabase(string metaPath, string folderPath = "") {
            var textureMock = new Mock<ITexture>();

            var mockAssetDatabase = new Mock<IAssetDatabase>();
            mockAssetDatabase.Setup(x => x.GUIDToAssetPath(It.IsAny<string>())).Returns(folderPath);
            mockAssetDatabase.Setup(x => x.LoadAssetAtPath<ITexture>(It.IsAny<string>())).Returns(textureMock.Object);
            mockAssetDatabase.Setup(x => x.GetTextMetaFilePathFromAssetPath(It.IsAny<string>())).Returns(metaPath);
            AssetDatabaseAdapter.Instance = mockAssetDatabase.Object;

            return mockAssetDatabase;
        }

        public static Mock<IAssetPreview> MockAssetPreview() {
            var textureMock = new Mock<ITexture>();
            var assetPreview = new Mock<IAssetPreview>();
            assetPreview.Setup(mock => mock.GetMiniThumbnail(It.IsAny<IGameObject>())).Returns(textureMock.Object);
            AssetPreviewAdapter.Instance = assetPreview.Object;
            return assetPreview;
        }

        public static Mock<IAssetPathUtil> MockAssetPathUtil() {
            var assetPathUtil = new Mock<IAssetPathUtil>();
            assetPathUtil.Setup(mock => mock.GetAssetFolderPath(It.IsAny<string>())).Returns("");
            AssetPathUtilAdapter.Instance = assetPathUtil.Object;
            return assetPathUtil;
        }

        public static Mock<IJsonUtility> MockJsonUtility() {
            var jsonUtil = new Mock<IJsonUtility>();

            jsonUtil.Setup(mock => mock.ToJson(It.IsAny<object>())).Returns((object o) => {
                return "";
            });
            jsonUtil.Setup(mock => mock.FromJson(It.IsAny<string>(), It.IsAny<Type>())).Returns((string x, Type type) => {
                var model = new AccelAsset();
                model.name = x;
                model.keys = new Key[] { new Key(0, 0) };
                return model;
            });
            JsonUtilityAdapter.Instance = jsonUtil.Object;
            return jsonUtil;
        }

        static Mock<IPrefabUtility> _mockPrefabUtility;
        public static Mock<IPrefabUtility> MockPrefabUtility() {
            var prefabUtility = _mockPrefabUtility == null ? new Mock<IPrefabUtility>() : _mockPrefabUtility;
            _mockPrefabUtility = prefabUtility;
            prefabUtility.Setup(mock => mock.GetPrefabAssetPathOfNearestInstanceRoot(It.IsAny<IGameObject>())).Returns("");
            PrefabUtilityAdapter.Instance = prefabUtility.Object;
            return prefabUtility;
        }
        public static Mock<IAnimationMode> MockAnimationMode() {
            var animationMode = new Mock<IAnimationMode>();
            AnimationModeAdapter.Instance = animationMode.Object;
            return animationMode;
        }

        public static Mock<IEditorGUI> MockEditorGUI() {
            var editorGUI = new Mock<IEditorGUI>();
            editorGUI.Setup(mock => mock.DrawRect(It.IsAny<IRect>(), It.IsAny<IRect>(), It.IsAny<IColor>()));
            editorGUI.Setup(mock => mock.Popup(It.IsAny<IRect>(), It.Is<int>(x => x == 0), It.IsAny<IGUIContent[]>(), It.IsAny<IGUIStyle>())).Returns(0);
            editorGUI.Setup(mock => mock.TextField(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns((IRect a, string b, IGUIStyle c) => { return b; });
            editorGUI.Setup(mock => mock.Slider(It.IsAny<IRect>(), It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>())).Returns((IRect rect, float a, float b, float c) => {
                return a;
            });
            EditorGUIAdapter.Instance = editorGUI.Object;
            return editorGUI;
        }

        public static Mock<IEditorGUIUtility> MockEditorGUIUtility() {
            var editorGUIUtility = new Mock<IEditorGUIUtility>();
            EditorGUIUtilityAdapter.Instance = editorGUIUtility.Object;
            return editorGUIUtility;
        }

        static Mock<IGUI> _mockGUI;
        public static Mock<IGUI> MockGUI() {
            var GUI = _mockGUI == null ? new Mock<IGUI>() : _mockGUI;
            _mockGUI = GUI;
            GUI.Setup(mock => mock.Button(It.IsAny<IRect>(), It.IsAny<string>(), It.IsAny<IGUIStyle>())).Returns(false);
            GUI.Setup(mock => mock.Button(It.IsAny<IRect>(), It.IsAny<ITexture>())).Returns(false);
            GUIAdapter.Instance = GUI.Object;
            return GUI;
        }

        public static Mock<IFont> MockFont() {
            var font = new Mock<IFont>();
            FontAdapter.Instance = font.Object;
            return font;
        }

        static Mock<IEditorUtility> _mockEditorUtility;
        public static Mock<IEditorUtility> MockEditorUtility() {
            var editorUtility = _mockEditorUtility == null ? new Mock<IEditorUtility>() : _mockEditorUtility;
            _mockEditorUtility = editorUtility;
            EditorUtilityAdapter.Instance = editorUtility.Object;
            return editorUtility;
        }

        static Mock<IReflectionHandler> _mockReflectionHandler;
        public static Mock<IReflectionHandler> MockReflectionHandler() {
            var reflectionHandler = _mockReflectionHandler == null ? new Mock<IReflectionHandler>() : _mockReflectionHandler;
            _mockReflectionHandler = reflectionHandler;
            reflectionHandler.Setup(mock => mock.GetGenericComponent(It.IsAny<List<IComponent>>(), It.IsAny<Type>())).Returns((List<IComponent> List, Type type) => {
                var enumWrapper = ObjectMocks.GetEnumWrapper();
                return ObjectMocks.GetMockComponentWrapper(enumWrapper).Object;
            });
            ReflectionHandlerAdapter.Instance = reflectionHandler.Object;
            return reflectionHandler;
        }

        public static Mock<IFileUtil> MockFileUtil() {
            var fileUtil = new Mock<IFileUtil>();
            FileUtilAdapter.Instance = fileUtil.Object;
            return fileUtil;
        }
    }

    public class MockRectWrapper {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        float intx;
        float inty;
        float intwidth;
        float intheight;

        public readonly Mock<IRect> rect;
        public MockRectWrapper(float x, float y, float width, float height) {
            intx = x;
            inty = y;
            intwidth = width;
            intheight = height;

            rect = new Mock<IRect>();
            rect.SetupGet(mock => mock.x).Returns(intx);
            rect.SetupSet(mock => mock.x).Callback(val => intx = val);
            rect.SetupGet(mock => mock.y).Returns(inty);
            rect.SetupSet(mock => mock.y).Callback(val => inty = val);
            rect.SetupGet(mock => mock.width).Returns(intwidth);
            rect.SetupSet(mock => mock.width).Callback(val => intwidth = val);
            rect.SetupGet(mock => mock.height).Returns(intheight);
            rect.SetupSet(mock => mock.height).Callback(val => intheight = val);
            rect.SetupGet(mock => mock.position).Returns(DB.NewVector2(intx, inty));
            rect.SetupSet(mock => mock.position).Callback((IVector2 val) => { intx = val.x; inty = val.y; });
            rect.SetupGet(mock => mock.size).Returns(DB.NewVector2(intwidth, intheight));
            rect.SetupSet(mock => mock.size).Callback((IVector2 val) => { intwidth = val.x; intheight = val.y; });
            rect.Setup(mock => mock.Contains(It.IsAny<IVector2>())).Returns((IVector2 val) => {
                bool withinHeight = val.y >= rect.Object.y && val.y <= rect.Object.y + rect.Object.height;
                bool withinWidth = val.x >= rect.Object.x && val.x <= rect.Object.x + rect.Object.width;
                return withinHeight && withinWidth;
            });
        }
    }
}
