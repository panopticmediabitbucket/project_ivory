﻿using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Moq;
using System;
using System.Collections.Generic;
using static Assets.Code.Editor.Tests.Windows.AnimatedAccelerationStates.Impl.AASFunctionModelTest;

namespace Assets.Code.Editor.Tests.Mocks {
    public class ObjectMocks {

        public static Mock<IGameObject> MockGameObject(bool longMid = false) {
            var gameObject = new Mock<IGameObject>();

            gameObject.Setup(mock => mock.GetComponent<IAnimator>()).Returns(() => {
                return MockAnimatorWithGraph(longMid);

            });

            gameObject.SetupGet(mock => mock.name).Returns("TestGameObject");

            gameObject.SetupGet(mock => mock.transform).Returns(GetMockTransform());

            gameObject.Setup(mock => mock.GetComponents()).Returns(() => {
                var list = new List<IComponent>();
                return list;
            });
            return gameObject;
        }

        public static IAnimatorControllerParameter MockParameter(string v) {
            var param = new Mock<IAnimatorControllerParameter>();
            param.SetupGet(mock => mock.name).Returns(v);
            param.SetupGet(mock => mock.type).Returns(AnimatorControllerParameterType.Trigger);
            return param.Object;
        }

        private static ITransform GetMockTransform() {
            var transform = new Mock<ITransform>();
            transform.SetupGet(mock => mock.position).Returns(new Mock<IVector3>().Object);
            transform.SetupGet(mock => mock.rotation).Returns(new Mock<IQuaternion>().Object);
            return transform.Object;
        }

        public static Mock<ITexture> GetMockTexture() {
            var texture = new Mock<ITexture>();
            return texture;
        }

        public static Mock<IComponent> GetMockComponentWrapper(object wrappable) {
            Mock<IComponent> component = new Mock<IComponent>();
            component.Setup(mock => mock.GetComponent()).Returns(wrappable);
            return component;
        }

        public static TEnumWrapper GetEnumWrapper() {
            return new TEnumWrapper();
        }

        public class TEnumWrapper : BaseTEnumClass<TestEnum> {
            public TEnumWrapper() { }

            public virtual void TestColumnMethod(Column column, params object[] args) { }
        }

        public static IAnimator MockAnimatorWithGraph(bool longMid = false) {
            var animator = new Mock<IAnimator>();
            var controller = new Mock<IRuntimeAnimatorController>();
            controller.SetupGet(rt => rt.animationClips).Returns(() => {

                var animationClips = new IAnimationClip[4];
                for (int i = 0; i < 4; i++) {
                    var clip = new Mock<IAnimationClip>();
                    clip.SetupGet(c => c.name).Returns(((TestEnum)i).ToString());
                    animationClips[i] = clip.Object;
                }
                return animationClips;
            });
            controller.SetupGet(mock => mock.layers).Returns(new IAnimatorControllerLayer[] { ObjectMocks.MockAnimationGraph(longMid).Object });
            controller.SetupGet(mock => mock.parameters).Returns(new IAnimatorControllerParameter[] { ObjectMocks.MockParameter("Idle"), ObjectMocks.MockParameter("Trot"), ObjectMocks.MockParameter("Unbound") });
            animator.SetupGet(mock => mock.runtimeAnimatorController).Returns(controller.Object);
            return animator.Object;
        }

        public abstract class BaseTEnumClass<TEnum> where TEnum : Enum { }

        public static Mock<IAnimatorControllerLayer> MockAnimationGraph(bool longMid) {
            Mock<IAnimatorControllerLayer> layer = new Mock<IAnimatorControllerLayer>();
            layer.SetupGet(mock => mock.stateMachine).Returns(MockStateMachine(longMid).Object);
            return layer;
        }

        private static Mock<IAnimatorStateMachine> MockStateMachine(bool longMid) {
            var machine = new Mock<IAnimatorStateMachine>();
            machine.SetupGet(mock => mock.stateMachines).Returns(new List<IAnimatorStateMachine>() { machine.Object });
            var states = MockAnimatorStates(longMid);
            machine.SetupGet(mock => mock.states).Returns(states);
            return machine;
        }

        private static List<IAnimatorState> MockAnimatorStates(bool longMid) {
            var idleState = new Mock<IAnimatorState>();
            var startTrotState = new Mock<IAnimatorState>();
            var trotState = new Mock<IAnimatorState>();

            idleState.SetupGet(mock => mock.name).Returns("Idle");
            startTrotState.SetupGet(mock => mock.name).Returns("StartTrot");
            trotState.SetupGet(mock => mock.name).Returns("Trot");

            idleState.SetupGet(mock => mock.clip).Returns(MockAnimationClip("Idle", false));
            startTrotState.SetupGet(mock => mock.clip).Returns(MockAnimationClip("StartTrot", longMid));
            trotState.SetupGet(mock => mock.clip).Returns(MockAnimationClip("Trot", longMid));

            idleState.SetupGet(mock => mock.transitions).Returns(new List<IAnimatorTransitionBase>() { MockAnimatorTransition("Trot", startTrotState.Object) });
            trotState.SetupGet(mock => mock.transitions).Returns(new List<IAnimatorTransitionBase>() { MockAnimatorTransition("Idle", idleState.Object) });
            startTrotState.SetupGet(mock => mock.transitions).Returns(new List<IAnimatorTransitionBase>() { MockAnimatorTransition("Idle", idleState.Object) });

            return new List<IAnimatorState>() { idleState.Object, startTrotState.Object, trotState.Object };
        }

        private static IAnimationClip MockAnimationClip(string name, bool isLong) {
            var clip = new Mock<IAnimationClip>();
            clip.SetupGet(c => c.name).Returns(name);
            clip.SetupGet(c => c.length).Returns(isLong ? 100 : 1);
            return clip.Object;
        }

        private static IAnimatorTransitionBase MockAnimatorTransition(string trigger, IAnimatorState stateTarget) {
            var transition = new Mock<IAnimatorTransitionBase>();
            var condition = new Mock<IAnimatorCondition>();
            condition.SetupGet(mock => mock.parameter).Returns(trigger);
            transition.SetupGet(mock => mock.conditions).Returns(new List<IAnimatorCondition>() { condition.Object });
            transition.SetupGet(mock => mock.destinationState).Returns(stateTarget);
            return transition.Object;
        }
    }
}
