﻿using Moq;
using NUnit.Framework;
using Assets.Code.Editor.Tests.Mocks;using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.CustomGUI.Controls;
using EventType = Assets.Code.Editor.AdapterLayer.Impl.Static_Classes.EventType;
using UnityEditor;
using UnityEngine;


namespace Assets.Code.Editor.Tests.TestCustomGUI.TestControls
{
    public class TestDropdown
    {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockEvent();
            StaticMocks.MockGUI();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockGUIStyle();
            StaticMocks.MockFont();
        }

        [Test]
        public void TestDraw() {
            var mockGUI = StaticMocks.MockGUI();
            var drop = new DropdownMenu(20, "");

            drop.Draw();

            mockGUI.Verify(mock => mock.Box(It.Is<IRect>(x => x.x == 0 && x.y == 0 && x.width.Equals(10) && x.height.Equals(10)), It.IsAny<ITexture>(), It.IsAny<IGUIStyle>()));
        }

        [Test]
        public void TestDrawWithRect() {
            var mockGUI = StaticMocks.MockGUI();
            var drop = new DropdownMenu(20, "");

            drop.Draw(DB.NewRect(0, 480, 300, 150));

            mockGUI.Verify(mock => mock.Box(It.IsAny<IRect>(), It.IsAny<ITexture>(), It.IsAny<IGUIStyle>()));
        }

        [Test]
        public void TestClick() {
            var mockEvent = StaticMocks.MockEvent();
            var drop = new DropdownMenu(20, "Testing Title");

            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);

            Assert.True(drop.Control());
        }

        [Test]
        public void TestHorizontalLine() {
            var mockEGUI = StaticMocks.MockEditorGUI();
            var mockEvent = StaticMocks.MockEvent();
            var drop = new DropdownMenu(20, "Testing Title");

            drop.Draw();
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);

            var mockHLine = DB.NewRect(0, 479, 300, 2f);
            var mockColor = DB.NewColor(.5f, .5f, .5f);

            Assert.True(drop.Control());
            drop.OnGUI();
            mockEGUI.Verify(mock => mock.DrawRect(It.IsAny<IRect>(), It.IsAny<IColor>()), Times.AtLeastOnce());
        }
    }
}