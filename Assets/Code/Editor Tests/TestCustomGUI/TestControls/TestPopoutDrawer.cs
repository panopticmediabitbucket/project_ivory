﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Moq;
using NUnit.Framework;

namespace Assets.Code.Editor.Tests.TestCustomGUI.TestControls {
    public class TestPopoutDrawer {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        private static Mock<IPopoutContent<float>> _content;

        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockEvent();
        }

        public static void MockInterfaces(bool bordered, out Mock<IPopoutContent<float>> content, out Mock<IPopoutDrawerStyle> style, out Mock<IPopoutDrawerContentSummary<float>> summary) {
            content = new Mock<IPopoutContent<float>>();
            style = new Mock<IPopoutDrawerStyle>();
            style.SetupGet(mock => mock.guiContent).Returns((IGUIContent)null);
            style.SetupGet(mock => mock.isBordered).Returns(bordered);
            style.SetupGet(mock => mock.drawerColor).Returns(DB.NewColor(.2f, .2f, .2f));

            content.SetupGet(mock => mock.style).Returns(style.Object);
            summary = new Mock<IPopoutDrawerContentSummary<float>>();
            summary.SetupGet(mock => mock.value).Returns(10f);
            content.SetupGet(mock => mock.contentSummary).Returns(summary.Object);
        }

        [Test]
        public void TestDrawBorderedOpenable() {
            MockInterfaces(true, out Mock<IPopoutContent<float>> content, out Mock<IPopoutDrawerStyle> style, out Mock<IPopoutDrawerContentSummary<float>> summary);
            var mockEditorGUI = StaticMocks.MockEditorGUI();

            var drawer = new PopoutDrawer<float>(content.Object);
            drawer.OnGUI();

            mockEditorGUI.Verify(mock => mock.DrawRect(It.IsAny<IRect>(), It.IsAny<IColor>()), Times.AtLeast(5));
            summary.Verify(mock => mock.Draw(It.IsAny<IRect>()), Times.Once());

            var mockEvent = StaticMocks.MockEvent();
            var mockPopup = StaticMocks.MockPopupWindow();
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.mouseDown);
            drawer.OnGUI();
            mockPopup.Verify(mock => mock.Show<float>(It.IsAny<IRect>(), It.Is<IPopoutContent<float>>(x => x == content.Object)));
        }

        [Test]
        public void TestDrawUnborderedNotOpenable() {
            MockInterfaces(false, out Mock<IPopoutContent<float>> content, out Mock<IPopoutDrawerStyle> style, out Mock<IPopoutDrawerContentSummary<float>> summary);
            var mockEditorGUI = StaticMocks.MockEditorGUI();

            var drawer = new PopoutDrawer<float>(content.Object);
            drawer.OnGUINotOpenable(DB.ZeroRect());

            // only the main rect is drawn
            mockEditorGUI.Verify(mock => mock.DrawRect(It.IsAny<IRect>(), It.IsAny<IColor>()), Times.Once());
            summary.Verify(mock => mock.DrawNotOpenable(It.IsAny<IRect>()), Times.Once());

            var mockEvent = StaticMocks.MockEvent();
            var mockPopup = StaticMocks.MockPopupWindow();
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.mouseDown);
            drawer.OnGUINotOpenable(DB.ZeroRect());
            mockPopup.Verify(mock => mock.Show<float>(It.IsAny<IRect>(), It.Is<IPopoutContent<float>>(x => x == content.Object)), Times.Never());

            Assert.AreEqual(10f, drawer.contentSummary);
        }
    }
}
