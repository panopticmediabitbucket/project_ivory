﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.TestCustomGUI.TestControls
{
    public class TestCtrlPipeline {
        private static IDependencyBuilder DB => DependencyManager.Instance;
        enum TestControlPipelineEnum { Step_1, Step_2 };

        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockEvent();
        }

        [Test]
        public void TestControl() {
            var controllable = new Mock<IControllable>();
            controllable.Setup(mock => mock.Control()).Returns(new ReturnedValue(true));

            List<ControlStep<TestControlPipelineEnum>> pipelineEnum = new List<ControlStep<TestControlPipelineEnum>>();
            var ctrlStepOne = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_1);
            var ctrlStepTwo = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_2);
            pipelineEnum.Add(ctrlStepOne);
            pipelineEnum.Add(ctrlStepTwo);

            IControllable pipe = new ControlPipeline<TestControlPipelineEnum>(pipelineEnum);

            Assert.True(pipe.Control());
        }

        [Test]
        public void TestControlWithRect() {
            var controllable = new Mock<IControllable>();
            controllable.Setup(mock => mock.Control()).Returns(new ReturnedValue(true));

            List<ControlStep<TestControlPipelineEnum>> pipelineEnum = new List<ControlStep<TestControlPipelineEnum>>();
            var ctrlStepOne = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_1);
            var ctrlStepTwo = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_2);
            pipelineEnum.Add(ctrlStepOne);
            pipelineEnum.Add(ctrlStepTwo);

            IControllable pipe = new ControlPipeline<TestControlPipelineEnum>(pipelineEnum);

            IRect rect = DB.NewRect(0, 0, 600, 300);
            Assert.True(pipe.Control(rect));
        }

        [Test]
        public void TestNoControl() {
            var controllable = new Mock<IControllable>();
            controllable.Setup(mock => mock.Control()).Returns(ReturnedValue.noReturnValue);

            List<ControlStep<TestControlPipelineEnum>> pipelineEnum = new List<ControlStep<TestControlPipelineEnum>>();
            var ctrlStepOne = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_1);
            var ctrlStepTwo = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_2);
            pipelineEnum.Add(ctrlStepOne);
            pipelineEnum.Add(ctrlStepTwo);

            IControllable pipe = new ControlPipeline<TestControlPipelineEnum>(pipelineEnum);

            Assert.False(pipe.Control());
        }

        [Test]
        public void TestNoControlWithRect() {
            var controllable = new Mock<IControllable>();
            controllable.Setup(mock => mock.Control()).Returns(ReturnedValue.noReturnValue);

            List<ControlStep<TestControlPipelineEnum>> pipelineEnum = new List<ControlStep<TestControlPipelineEnum>>();
            var ctrlStepOne = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_1);
            var ctrlStepTwo = new ControlStep<TestControlPipelineEnum>(controllable.Object, TestControlPipelineEnum.Step_2);
            pipelineEnum.Add(ctrlStepOne);
            pipelineEnum.Add(ctrlStepTwo);

            IControllable pipe = new ControlPipeline<TestControlPipelineEnum>(pipelineEnum);

            Assert.False(pipe.Control(DB.NewRect(0, 0, 4, 4)));
        }
    }
}
