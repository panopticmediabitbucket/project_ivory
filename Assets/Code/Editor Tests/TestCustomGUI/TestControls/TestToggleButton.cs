﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Moq;
using NUnit.Framework;

namespace Assets.Code.Editor.CustomGUI.TestCustomGUI.TestControls
{
    public class TestToggleButton {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockGUI();
            StaticMocks.MockEvent();
            StaticMocks.MockFont();
            StaticMocks.MockHandles();

            _mockTextureA = new Mock<ITexture>();
            _mockTextureB = new Mock<ITexture>();
        }

        private static Mock<ITexture> _mockTextureB;
        private static Mock<ITexture> _mockTextureA;
        [Test]
        public void TestControlDraw() {
            var button = new ToggleButton(_mockTextureA.Object, _mockTextureB.Object, DB.NewVector2(20, 10));
            var mockGUI = StaticMocks.MockGUI();
            var mockEvent = StaticMocks.MockEvent();
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));

            Assert.True(button.Control());
            button.OnGUI();
            mockGUI.Verify(mock => mock.DrawTexture(It.Is<IRect>(x => x.x == 0 && x.y == 0 && x.width.Equals(20) && x.height.Equals(10)), _mockTextureB.Object));

            button.Deactivate();
            button.OnGUI();
            mockGUI.Verify(mock => mock.DrawTexture(It.Is<IRect>(x => x.x == 0 && x.y == 0 && x.width.Equals(20) && x.height.Equals(10)), _mockTextureA.Object));
        }
    }
}
