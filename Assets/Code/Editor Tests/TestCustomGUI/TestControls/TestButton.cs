﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Controls;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Moq;
using NUnit.Framework;

namespace Assets.Code.Editor.CustomGUI.TestCustomGUI.TestControls {
    public class TestButton     {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockGUI();
            StaticMocks.MockEvent();
            StaticMocks.MockFont();
            StaticMocks.MockHandles();

            _mockTexture = new Mock<ITexture>();
        }

        private static Mock<ITexture> _mockTexture;

        [Test]
        public void TestDraw() {
            var mockGUI = StaticMocks.MockGUI();
            var button = new Button(_mockTexture.Object, DB.NewVector2(20, 10));
            button.Draw();
            mockGUI.Verify(mock => mock.DrawTexture(It.Is<IRect>(x => x.x == 0 && x.y == 0 && x.height.Equals(10) && x.width.Equals(20)), _mockTexture.Object));
        }

        [Test]
        public void TestDrawWithRect() {
            var mockGUI = StaticMocks.MockGUI();
            var button = new Button(_mockTexture.Object, DB.NewVector2(20, 10));
            button.Draw(DB.NewRect(10, 20, 100, 20));
            mockGUI.Verify(mock => mock.DrawTexture(It.Is<IRect>(x => x.x == 10 && x.y == 20 && x.width.Equals(40) && x.height.Equals(20)), _mockTexture.Object));
        }

        [Test]
        public void TestControlClicked() {
            var mockEvent = StaticMocks.MockEvent();
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));

            var button = new Button(_mockTexture.Object, DB.NewVector2(20, 10));
            Assert.True(button.Control());
        }
    }
}
