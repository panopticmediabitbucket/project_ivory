﻿using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Objects;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.AdapterLayer.Impl.Static_Classes;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.CustomGUI.Controls;
using Moq;
using NUnit.Framework;
using System;
using Assets.Code.Editor.Utilities;

namespace Assets.Code.Editor.CustomGUI.TestCustomGUI.TestControls {
    public class TestKnobControl {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void Setup() {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockAssetDatabase(null);
            StaticMocks.MockEvent();
            StaticMocks.MockHandles();
            StaticMocks.MockGUIStyle();
            StaticMocks.MockGUI();
            StaticMocks.MockEditorGUI();
            StaticMocks.MockFont();
        }

        [Test]
        public void TestDraw() {
            var mockGUI = StaticMocks.MockGUI();
            var knob = new KnobControl(DB.NewVector2(1, 0));

            knob.Draw();
            mockGUI.Verify(mock => mock.Box(It.Is<IRect>(x => x.x == 0 && x.y == 0 && x.width.Equals(10) && x.height.Equals(10)), It.IsAny<ITexture>(), It.IsAny<IGUIStyle>()));
        }

        [Test]
        public void TestDrawWithRect() {
            var mockGUI = StaticMocks.MockGUI();
            var knob = new KnobControl(DB.NewVector2(1, 0));

            knob.Draw(DB.NewRect(20, 20, 300, 300));
            mockGUI.Verify(mock => mock.Box(It.Is<IRect>(x => x.x == 20 && x.y == 20 && x.width.Equals(300) && x.height.Equals(300)), It.IsAny<ITexture>(), It.IsAny<IGUIStyle>()));
        }

        [Test]
        public void TestControl() {
            var mockEvent = StaticMocks.MockEvent();
            var knob = new KnobControl(DB.NewVector2(20, 20));

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));
            Assert.True(knob.Control());
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseUp);
            Assert.False(knob.Control());
        }

        [Test]
        public void TestControlWithRect() {
            var mockEvent = StaticMocks.MockEvent();
            var knob = new KnobControl(DB.NewVector2(20, 20));
            IRect rect = DB.NewRect(0, 0, 20, 20);
            
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));
            Assert.True(knob.Control(rect));
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseUp);
            Assert.False(knob.Control(rect));
        }

        [Test]
        public void TestSnappingRoutine() {
            var mockEvent = StaticMocks.MockEvent();
            var knob = new KnobControl(DB.NewVector2(20, 20));
            //position one of mouse pointer, at 0 degrees.
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(0, 5));
            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            var degreeReturn = knob.Control();
            Assert.True(degreeReturn);
            Assert.AreEqual(180, degreeReturn.value);

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.keyDown);
            mockEvent.Setup(mock => mock.keyCode).Returns(KeyCode.LeftControl);
            InputUtilities.IsKeyHeld(KeyCode.LeftControl);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(6, -20));
            Assert.AreEqual(knob.Control().value, 270f);

            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 10));
            Assert.AreEqual(knob.Control().value, 90f);

            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(10, 10));
            Assert.AreEqual(knob.Control().value, 45f);

            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(4, 30));
            Assert.AreEqual(knob.Control().value, 90f);
        }

        [Test]
        public void TestCalculateDegree() {
            var knob = new KnobControl(DB.NewVector2(200, 200));
            var mockEvent = StaticMocks.MockEvent();
            double degree = 0.0;

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));
            Assert.True(knob.Control());

            Assert.AreEqual(It.IsAny<double>(), degree);
        }
        
        [Test]
        public void TestDegreeCalcRect() {
            var mockGUI = StaticMocks.MockGUI();
            var mockEvent = StaticMocks.MockEvent();
            var mockGUIStyle = StaticMocks.MockGUIStyle();
            var knob = new KnobControl(DB.NewVector2(200,200));

            knob.Draw(DB.NewRect(250, 100, 50, 20));
            mockGUIStyle.Setup(mock => mock.fontSize).Returns(12);
            mockGUIStyle.Setup(mock => mock.alignment).Returns(AdapterLayer.Objects.TextAnchor.MiddleCenter);

            mockEvent.Setup(mock => mock.rawType).Returns(EventType.MouseDown);
            mockEvent.Setup(mock => mock.mousePosition).Returns(DB.NewVector2(5, 5));

            mockGUI.Verify(mock => mock.Label(
                It.IsAny<IRect>(),
                It.IsAny<string>(),
                It.IsAny<IGUIStyle>()));
        }
    }
}