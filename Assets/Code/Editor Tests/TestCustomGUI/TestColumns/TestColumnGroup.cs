﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Adapter;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Reflection;
using static Assets.Code.Editor.Tests.Mocks.ObjectMocks;

namespace Assets.Code.Editor.CustomGUI.TestCustomGUI.TestColumns
{
    public class TestColumnGroup
    {
        //private static IDependencyBuilder DB => DependencyManager.Instance;

        private static Mock<IEditorWindow> _editorWindowMock;

        [SetUp]
        public static void Setup()
        {
            var dependencyBuilder = StaticMocks.DependencyBuilderMock();
            StaticMocks.MockGUI();
            StaticMocks.MockEditorUtility();
            StaticMocks.MockEditorStyles();
            StaticMocks.MockReflectionHandler();
            StaticMocks.MockSelection();
            StaticMocks.MockPrefabUtility();
            StaticMocks.MockEditorGUI();

            _editorWindowMock = new Mock<IEditorWindow>();

            _editorWindowMock.SetupGet(x => x.position).Returns(dependencyBuilder.Object.NewRect(0, 0, 0, 0));
        }

        [Test]
        public void TestOnGUI()
        {
            var window = AASWindow.GetInstance(_editorWindowMock.Object);
            window.OnEnable();

            var list = window.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            var columnGroup = (ColumnGroup)list.Find(x => x.Name.Equals("_columnGroup")).GetValue(window);

            var mockFunctionWrapper = new Mock<TEnumWrapper>();

            columnGroup.SetDrawFunction(mockFunctionWrapper.Object.TestColumnMethod, 0);
            columnGroup.SetDrawFunction(mockFunctionWrapper.Object.TestColumnMethod, 1);

            window.OnGUI();

            mockFunctionWrapper.Verify(mock => mock.TestColumnMethod(It.Is<Column>(x => x.Equals(columnGroup.GetColumn(0)))));
            mockFunctionWrapper.Verify(mock => mock.TestColumnMethod(It.Is<Column>(x => x.Equals(columnGroup.GetColumn(1)))));
        }
    }
}
