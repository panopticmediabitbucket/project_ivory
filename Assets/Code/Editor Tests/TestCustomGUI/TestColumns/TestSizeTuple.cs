﻿using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.Tests.Mocks;
using Moq;
using NUnit.Framework;

namespace Assets.Code.Editor.Tests.TestCustomGUI.TestColumns
{
    public class TestSizeTuple
    {
        [SetUp]
        public static void Setup()
        {
            StaticMocks.DependencyBuilderMock();

        }

        [Test]
        public void TestFloatTupleAddition() { 
            var lhs = 10f;
            var rhs = new SizeTuple(20f, true);
            var sum = lhs + rhs;
            Assert.AreEqual((10f + 20f), sum);
        }

        [Test]
        public void TestTupleFloatAddition()
        {
            var lhs = new SizeTuple(20f, true);
            var rhs = 10f;
            var widthSum = lhs.width + rhs;
            
            Assert.AreEqual(30f, widthSum);
            Assert.IsTrue(lhs.isFixed);
        }

        [Test]
        public void TestTupleTupleAddition()
        {
            var lhs = new SizeTuple(20f, true);
            var rhs = new SizeTuple(30f, true);
            var widthSum = lhs.width + rhs.width;
            var AreTuplesFixed = lhs.isFixed && rhs.isFixed;

            Assert.AreEqual(50f, widthSum);
            Assert.IsTrue(AreTuplesFixed);
        }

        [Test]
        public void TestDivision() {
            SizeTuple lhs = new SizeTuple(20f, true);
            float rhs = 10f;
            float quotient = lhs / rhs;
            Assert.AreEqual((20f/10f), quotient);
        }
    }
}
