﻿using Assets.Code.Editor.AdapterLayer;
using Assets.Code.Editor.AdapterLayer.Structs;
using Assets.Code.Editor.CustomGUI.Columns;
using Assets.Code.Editor.InterfaceAdapterLayer.Impl;
using Assets.Code.Editor.Tests.Mocks;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Adapter;
using Assets.Code.Editor.Windows.AnimatedAccelerationStates.Impl;
using Moq;
using NUnit.Framework;
using System.Linq;
using System.Reflection;
using static Assets.Code.Editor.Tests.Mocks.ObjectMocks;

namespace Assets.Code.Editor.CustomGUI.TestCustomGUI.TestColumns
{
    public class TestColumn
    {
        private static IDependencyBuilder DB => DependencyManager.Instance;

        [SetUp]
        public static void Setup()
        {
            StaticMocks.DependencyBuilderMock();
            StaticMocks.MockReflectionHandler();
            StaticMocks.MockGUI();
            StaticMocks.MockFont();
            StaticMocks.MockHandles();
        }

        //[Test]
        //private void TestSplitHorizontalPost() {

        //}

        [Test]
        public void TestBeginScrollView() {
            var mockGUI = StaticMocks.MockGUI();
            Column column = new Column(0);
            IRect mockPos = DB.NewRect(1440, 0, 480, 1080); //Right-hand size 
            IVector2 mockCurrentPos = DB.NewVector2(0, 0);
            IVector2 mockContentSize = DB.NewVector2(5, 5);

            column.Rewind();
            column.BeginScrollView(mockCurrentPos, mockContentSize, true, true);

            mockGUI.Verify(mock => mock.BeginScrollView(It.IsAny<IRect>(), It.IsAny<IVector2>(), It.IsAny<IRect>(), true, true));
        }

        [Test]
        public void TestEndScrollView()
        {
            var mockGUI = StaticMocks.MockGUI();
            Column column = new Column(0);

            column.EndScrollView();
            mockGUI.Verify(mock => mock.EndScrollView());
        }
    }
}   