﻿
using System;

namespace Assets.Code.GameCode.Meta.Documentation {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = false)]
    public class SeeAlso : Attribute {
        public Type[] otherTypes { get; private set; }

        public SeeAlso(params Type[] types) {
            otherTypes = types;
        }
    }
}
