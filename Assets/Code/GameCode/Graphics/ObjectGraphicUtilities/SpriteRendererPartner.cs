﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.Graphics;

public class SpriteRendererPartner : MonoBehaviour
{
    public MaterialType type;
    private Material _material;
    private Animator _animator;

    //NormalMappedSprite variables
    public Sprite normalSprite;
    private int _normalId;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        switch(type)
        {
            case MaterialType.NormalMappedSprite:
                _material = gameObject.GetComponent<Renderer>().material;
                _normalId = Shader.PropertyToID("_BumpMap");
                _material.SetTexture(_normalId, normalSprite.texture);
                break;
        }
    }
}
