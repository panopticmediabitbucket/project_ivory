﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Assets.Code.Graphics;

public class AnimationControllerPartner : MonoBehaviour
{

    public MaterialType type;
    private Material _material;
    private Animator _animator;
    private Renderer _renderer;

    //NormalMappedSprite variables
    public Sprite normalFrame;
    private int _normalId;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<Renderer>();
        _renderer.receiveShadows = true;
        switch(type)
        {
            case MaterialType.NormalMappedSprite:
                _material = gameObject.GetComponent<Renderer>().material;
                _normalId = Shader.PropertyToID("_BumpMap");
                break;
        }
    }

    int _count = 0;
    void LateUpdate()
    {
        //if(normalFrame != null)
        //_material.SetTexture(_normalId, normalFrame.texture);
        //UnityEngine.Debug.Log("Postupdate normal: " + _material.GetTexture(_normalId) + "\nPostupdate texture: " + gameObject.GetComponent<SpriteRenderer>().sprite.texture + "\n" + ++_count);
    }
}
