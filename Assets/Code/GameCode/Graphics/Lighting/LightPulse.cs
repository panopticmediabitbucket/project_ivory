﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPulse : MonoBehaviour
{
    private Light _light;

    private float _intensity;
    [Tooltip("The number of seconds it takes to cycle through a sine wave.")]
    public float arcWidth; // in seconds
    [Tooltip("The variation in intensity on the target light.")]
    [Range(0, 1)]
    public float arcHeight; // in intensity

    private float _sampleTime = 0;


    void Start()
    {
        _light = (Light)gameObject.GetComponent(typeof(Light));
        _intensity = _light.intensity;
    }
    
    void Update()
    {
        _sampleTime += Time.deltaTime;
        if(_sampleTime >= arcWidth)
        {
            _sampleTime -= arcWidth;
        }
        _light.intensity = _intensity + arcHeight * Mathf.Sin((2 * Mathf.PI / arcWidth) * _sampleTime);
    }
}
