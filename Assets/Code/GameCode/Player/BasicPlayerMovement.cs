﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Assets.Code.Physics;
using Assets.Code.Physics.Utilities;
using Assets.Code.System;

/*
 * Basic Architecture:
 * The Enumeration State is used to define which routines are accessed in a given frame. On each Update(), the _state variable is used to slot into the _stateRoutineVector and invoke the corresponding routines.
 * During those routines, relevant player inputs are examined and converted into Accelerator objects which are stored in the _xAccelerators and _yAccelerators arrays; additionally, the new _state value is set in those routines.
 * As a result, for each corresponding state routine, the most important input detection must come last, because whichever state is saved last is the one that will be adhered to in the following frame.
 * After the state routine is finished, Update() calls CalculateKinematicTransform() to determine the character's movement based on the accelerators. The accumulated velocity of terminated accelerators is added to the scalar velocity.
 * LateUpdate() is used for collision detection, and it has final say in determining the transform and state to be used in the following frame.
 * 
 * Termination conditions for accelerators are checked every frame (See AcceleratorData.cs for more information)
 */


public enum PlayerState //Player routine
{
    Idle, StartTrot, Trot, StartGallop, Gallop, Airborne, Landing, AirborneMoving, WallCling, WallClimb, WallSlide
}

public sealed class BasicPlayerMovement : PhysicsStateMachineWorldObject<PlayerState> {

    enum Face //Direction
    {
        Left, Right
    }

    delegate void StateMethod();
    private readonly StateMethod[] _stateRoutineVector = new StateMethod[Enum.GetValues(typeof(PlayerState)).Length]; 
    private Face _facing;

    PlayerState PlayerState { get { return state; } set { state = value; } }
    Face Facing { get { return _facing; } //used instead of _facing. The only place where left/right animation triggers are set
        set
        {
            if(value == Face.Left)
            {
                Animations.SetTrigger("Leftward");
                Animations.ResetTrigger("Rightward");
                _facing = Face.Left;
            }
            else
            {
                Animations.SetTrigger("Rightward");
                Animations.ResetTrigger("Leftward");
                _facing = Face.Right;
            }
        }
    }

    //Resets animation triggers in bulk. Prevents repetition.
    void ResetTriggers()
    {
        Animations.ResetTrigger("Trot");
        Animations.ResetTrigger("Idle");
        Animations.ResetTrigger("StandardJump");
        Animations.ResetTrigger("Gallop");
        Animations.ResetTrigger("WallCling");
        Animations.ResetTrigger("WallSlide");
    }

    //protected override void First()
    //{
    //    ResetTriggers();
    //    _stateRoutineVector[(int)state].Invoke(); //Run routines associated with state
    //    _updateTransform = StaticPhysicsCalculations.CalculateKinematicTransform(_xAccelerators, _yAccelerators, ref _xVelocity, ref _yVelocity);
    //    CollisionDetection();
    //    transform.Translate(_updateTransform, Space.World);
    //    base.First();
    //}

    //protected override void Second() {
    //    // No implementation
    //}

    //void LateUpdate() // Player movement is handled in LateUpdate so that world elements have finished moving and Collision Detection can be more precise.
    //{

    //}

    void IdleRoutine() // State.Idle
    {
        Animations.SetTrigger("Idle");
        UnityEngine.Debug.Log("Successful slot into Idle");
        GroundOrAir();
        GroundedStandardMovement();
        StandardJump();
    }

    void StartTrotRoutine() // State.StartTrot -- Sprint routine is not exposed until we enter the Trot routine
    {
        Animations.SetTrigger("Trot");
        GroundOrAir();
        GroundedStopTrot();
        StandardJump();
        UnityEngine.Debug.Log("Successful slot into StartTrot");
    }

    public bool automationTesting = false;
    void TrotRoutine() //State.Trot
    {
        if(automationTesting) UnityEngine.Debug.Log("trot cycle complete");
        automationTesting = false;
        Animations.SetTrigger("Trot");
        GroundOrAir();
        GroundedStopTrot();
        StartGallop();
        StandardJump();
        UnityEngine.Debug.Log("Successful slot into Trot");
    }

    void StartGallopRoutine() //State.StartGallop
    {
        Animations.SetTrigger("Gallop");
        GroundOrAir();
        GroundedStopTrot();
    }

    void GallopRoutine() //State.Gallop
    {
        Animations.SetTrigger("Gallop");
        GroundOrAir();
        GroundedStopTrot();
    }

    void WallClingRoutine()
    {
        ResetTriggers();
        Animations.SetTrigger("WallCling");
        WallClimb();
        StartWallSlide();
        WallRelease();
    }

    private int wcCount = 0;
    void WallClimbRoutine()
    {
        UnityEngine.Debug.Log("In WallClimbRoutine: " + ++wcCount);
        //ResetTriggers();
        //Animations.SetTrigger("WallCling");
        //WallClimbStop();
    }

    void WallSlideRoutine()
    {
        if(!_startingSlide) WallCling();
    }

    void StartGallop()
    {
        if(_cm.Gallop)
        {
            UnityEngine.Debug.Log(_xVelocity);
            Animations.SetTrigger("Gallop");
            state = PlayerState.StartGallop;
            _windup.Reset();
            _windup.Start();
            float accel = _gallopA;
            int LRscale = Facing == Face.Left ? -1 : 1;
            UnityEngine.Debug.Log((LRscale * _gallopA) + ": acceleration");
            if(_xVelocity * LRscale > TrotV)
            {
                accel = (GallopV - Mathf.Abs(_xVelocity)) / GallopWindupt;
            }
            _xAccelerators[PLAYER].Mutate(LRscale * accel, 0, 0, GallopWindupt, ConstAccelToPosition, ConstAccelToVelocity, () => (float)_windup.Elapsed.TotalSeconds > GallopWindupt || state == PlayerState.Airborne,
                delegate ()
                {
                    if((float)_windup.Elapsed.TotalSeconds <= GallopWindupt)
                    {
                        //_xAccelerators[PLAYER].TermUpperBound = _xAccelerators[PLAYER].LowerBound;
                    }
                    else
                        state = PlayerState.Gallop;
                });
        }
    }

    void AirborneRoutine() //State.Airborne
    {
        UnityEngine.Debug.Log("Successful slot into Airborne");
        if(!_yAccelerators[GRAVITY].Active) _yAccelerators[GRAVITY].Mutate(-g, 0, 0, 40000f, ConstAccelToPosition, ConstAccelToVelocity, () => false, null);
        AirborneMovementDetection();
        if(_xVelocity < 0) Facing = Face.Left;
        else if(_xVelocity > 0) Facing = Face.Right;
        WallCling();
    }

    void AirborneMovingRoutine() //State.AirborneMoving
    {
        UnityEngine.Debug.Log("Successful slot into Airborne moving routine");
    }

    void GroundOrAir()
    {
        if(feetCA.getTouch()) // on the ground?
        {
            //_yVelocity = 0;
            //_yAccelerators[GRAVITY].Reset();
            UnityEngine.Debug.Log("got touch");
        }
        else
        {
            Animations.ResetTrigger("Grounded");
            Animations.SetTrigger("AirPeaked");
            _elapsedJumpTime.Reset();
            _elapsedJumpTime.Start();
            _yVelocity = 0;
            state = PlayerState.Airborne;
            if(!_yAccelerators[GRAVITY].Active) _yAccelerators[GRAVITY].Mutate(-g, 0, 0, 40000f, ConstAccelToPosition, ConstAccelToVelocity, () => false, null);
        }
    }

    //Calculations for standard-speed movement, accounts for changes in midair
    Stopwatch _windup = new Stopwatch();
    void GroundedStandardMovement()
    {
        if(_cm.RightInput)
        {
            Facing = Face.Right;
            Trot(); 
        }
        else if(_cm.LeftInput)
        {
            Facing = Face.Left;
            Trot(); 
        }
    }

    void Trot()
    {
        Animations.SetTrigger("Trot");
        state = PlayerState.StartTrot;
        _windup.Reset(); _windup.Start();

        int LRscale = Facing == Face.Left ? -1 : 1;
        float adjustedA;
        if(Mathf.Abs(_xVelocity) > TrotWindUpV)
        {
            adjustedA = (LRscale * TrotV - _xVelocity) / TrotWindUpt;
        }
        else
        {
            adjustedA = LRscale * _trotA;
            _xVelocity = LRscale * TrotWindUpV;
        }
        _xAccelerators[PLAYER].Mutate(adjustedA, 0, 0,  TrotWindUpt, ConstAccelToPosition, ConstAccelToVelocity, () => (float)_windup.Elapsed.TotalSeconds > TrotWindUpt || state == PlayerState.Airborne,
            delegate ()
            {
                if(_windup.Elapsed.TotalSeconds <= TrotWindUpt)
                    _xAccelerators[PLAYER].TermUpperBound = _xAccelerators[PLAYER].LowerBound;
                else if(state == PlayerState.StartTrot)
                    state = PlayerState.Trot;
            });
    }

    void GroundedStopTrot()
    {
        if((Facing == Face.Left && !_cm.LeftInput) || (Facing == Face.Right && !_cm.RightInput))
        {
            Animations.SetTrigger("Idle");
            state = PlayerState.Idle;
            _xAccelerators[PLAYER].Reset();
            _xVelocity = 0;
        }
    }

    public float AirborneAccel;
    void AirborneMovementDetection()
    {
        if(_cm.RightInput) AirborneMove(Face.Right);
        else if(_cm.LeftInput) AirborneMove(Face.Left);
    }

    void AirborneMove(Face dir)
    {
        int LRscale = dir == Face.Left ? -1 : 1;
        CasterArray_old lrCA = dir == Face.Left ? leftCA : rightCA;
        Func<bool> inp = (dir == Face.Left) ? (Func<bool>)(() => _cm.LeftInput) : () => _cm.RightInput;

        if(!(LRscale * _xVelocity > TrotV) && !lrCA.getTouch())
        {
            float windupTime = (LRscale * TrotV - _xVelocity) / (LRscale * AirborneAccel);
            _windup.Reset(); _windup.Start();
            state = PlayerState.AirborneMoving;
            _xAccelerators[PLAYER].Mutate(LRscale * AirborneAccel, 0, 0, windupTime, ConstAccelToPosition, ConstAccelToVelocity, () => !inp() || _windup.Elapsed.TotalSeconds >= windupTime,
                delegate ()
                {
                    if(_windup.Elapsed.TotalSeconds < windupTime)
                        _xAccelerators[PLAYER].TermUpperBound = _xAccelerators[PLAYER].LowerBound;
                    state = PlayerState.Airborne;
                }
            );
        }
    }

    void WallCling()
    {
        if(_cm.Cling & !_cm.DownInput)
        {
            if(WallTouch(Facing))
            {
                ResetTriggers();
                Animations.SetTrigger("WallCling");
                PlayerState = PlayerState.WallCling;
                _yVelocity = 0;
                _yAccelerators[GRAVITY].Reset();
                _yAccelerators[PLAYER].Reset();
            }
        }
    }

    void WallRelease()
    {
        if(!_cm.Cling)
        {
            PlayerState = PlayerState.Airborne;
            ResetTriggers();
            Animations.SetTrigger("AirPeaked");
            UnityEngine.Debug.Log("In wall release");
        }
    }

    private static readonly float _climbHertz = Mathf.PI * 2 / .5f; //number of cycles per second, may change if framecount changes
    private Func<float, float, float, float> _climbPosFunc
    {
        get
        {
            //return (a, lowerbound, upperbound) => a * ((upperbound - lowerbound) - Mathf.Cos(upperbound * _climbHertz) / _climbHertz + Mathf.Cos(lowerbound * _climbHertz) / _climbHertz);
            return (a, lowerbound, upperbound) => a * ((upperbound - lowerbound) - Mathf.Sin(upperbound * _climbHertz) / _climbHertz + Mathf.Sin(lowerbound * _climbHertz) / _climbHertz); //starts and ends at 0 v
            //return (a, lowerbound, upperbound) => a * ((upperbound - lowerbound) - Mathf.Pow(Mathf.Cos(upperbound * _climbHertz) / _climbHertz + Mathf.Cos(lowerbound * _climbHertz) / _climbHertz, ClimbSteepness));
        }
    }
    private Func<float, float, float, float> _zeroFunc = (a, lowerbound, upperbound) => 0;

    void WallClimb()
    {
        if(_cm.UpInput)
        {
            Animations.SetTrigger("WallClimb");
            PlayerState = PlayerState.WallClimb;
            _yVelocity = ClimbConstVel;
            _yAccelerators[PLAYER].Mutate(ClimbSinAccel, -Time.deltaTime, 0, .5f, _climbPosFunc, _zeroFunc, () => _yAccelerators[PLAYER].UpperBound >= _yAccelerators[PLAYER].TermUpperBound,
                delegate
            {
                if(!_cm.UpInput)
                {
                    PlayerState = PlayerState.WallCling;
                    _yVelocity = 0;
                    ResetTriggers();
                    Animations.SetTrigger("WallCling");
                }
                else
                {
                    PlayerState = PlayerState.WallClimb;
                    _yAccelerators[PLAYER].Terminated = false;
                    _yAccelerators[PLAYER].TermUpperBound += .5f;
                }
            });
        }
    }

    Boolean _startingSlide;
    float _slideStartTime = .33333f;
    void StartWallSlide()
    {
        if(_cm.DownInput)
        {
            ResetTriggers();
            Animations.SetTrigger("WallSlide");
            PlayerState = PlayerState.WallSlide;
            _startingSlide = true;
            //uses the effect of gravity
            float termUpperBound = _slideStartTime + Time.deltaTime;
            _yAccelerators[GRAVITY].Mutate(-g, 0, 0, termUpperBound, ConstAccelToPosition, ConstAccelToVelocity, () => _yAccelerators[GRAVITY].LowerBound >= termUpperBound, 
                delegate
                {
                    _startingSlide = false;
                });
        }
    }

    void WallClimbStop()
    {
        if(!_cm.UpInput)
        {
            PlayerState = PlayerState.WallCling;
            _yVelocity = 0;
            _yAccelerators[PLAYER].Reset();
        }
    }

    bool WallTouch(Face dir)
    {
        switch(dir)
        {
            case Face.Left:
                return leftCA.getTouch();
            case Face.Right:
                return rightCA.getTouch();
            default:
                return false;
        }
    }

    void StandardJump()
    {
        if(_cm.JumpDown)
        {
            _elapsedJumpTime.Reset();
            _elapsedJumpTime.Start();
            _yVelocity += StJumpVNaught;
            _yAccelerators[PLAYER].Mutate(CounterActGravity, 0, 0,  MaxJumpS, ConstAccelToPosition, ConstAccelToVelocity, () => !_cm.Jump || _yAccelerators[PLAYER].UpperBound >= MaxJumpS,
                delegate ()
                {
                    if((float)_yAccelerators[PLAYER].LowerBound < MinJumpS)
                        _yAccelerators[PLAYER].Mutate(CounterActGravity, 0, (float)_yAccelerators[PLAYER].LowerBound, MinJumpS, ConstAccelToPosition, 
                            (a, lowerbound, upperbound) => ConstAccelToVelocity(a, 0, (float)_yAccelerators[PLAYER].LowerBound) + ConstAccelToVelocity(a, lowerbound, upperbound), 
                            () => _yAccelerators[PLAYER].UpperBound >= MinJumpS,
                            delegate ()
                            {
                                Animations.ResetTrigger("StandardJump");
                                Animations.SetTrigger("AirPeaked");
                            });
                    else if(_yAccelerators[PLAYER].UpperBound < MaxJumpS)
                    {
                        _yAccelerators[PLAYER].TermUpperBound = (float)_yAccelerators[PLAYER].LowerBound;
                        Animations.ResetTrigger("StandardJump");
                        Animations.SetTrigger("AirPeaked");
                    }
                    else
                    {
                        Animations.ResetTrigger("StandardJump");
                        Animations.SetTrigger("AirPeaked");
                    }
                });
            _yAccelerators[GRAVITY].Mutate(-g, 0, 0, 40000f,  ConstAccelToPosition, ConstAccelToVelocity, () => false, null);
            state = PlayerState.Airborne;
            ResetTriggers();
            Animations.SetTrigger("StandardJump");
        }
    }

    RaycastHit2D _blHit;
    RaycastHit2D _brHit;

    int rightCount = 0;
    void CollisionDetection()
    {
        if(_updateTransform.y > 0)
        {
            RaycastHit2D ceilingCollision = topCA.GetHit(_updateTransform.magnitude, _updateTransform.normalized, Vector2.down);
            if(ceilingCollision)
            {
                UnityEngine.Debug.Log("hit");
                _yVelocity = 0;
                _updateTransform.y = _updateTransform.normalized.y * (ceilingCollision.distance);

            }
        }

        else if(_updateTransform.y < 0)
        {
            RaycastHit2D groundCollision = feetCA.GetHit(_updateTransform.magnitude, _updateTransform.normalized, Vector2.up);
            if(groundCollision)
            {
                UnityEngine.Debug.Log("hit");
                _stateRoutineVector[(int)PlayerState.Landing].Invoke();
                _updateTransform.y = _updateTransform.normalized.y * (groundCollision.distance - StaticPhysicsCalculations.microCorrection);
            }
        }

        if(_updateTransform.x > 0)
        {
            RaycastHit2D wallCollision = rightCA.GetHit(_updateTransform.magnitude, _updateTransform.normalized, Vector2.left);
            if(wallCollision)
            {
                UnityEngine.Debug.Log("right hit #" + ++rightCount);
                _updateTransform.x = _updateTransform.normalized.x * (wallCollision.distance - StaticPhysicsCalculations.microCorrection);
                _xVelocity = 0;
            }
        }

        else if(_updateTransform.x < 0)
        {
            RaycastHit2D wallCollision = leftCA.GetHit(_updateTransform.magnitude, _updateTransform.normalized, Vector2.right);
            if(wallCollision)
            {
                UnityEngine.Debug.Log("wall hit");
                _updateTransform.x = _updateTransform.normalized.x * (wallCollision.distance - StaticPhysicsCalculations.microCorrection);
                _xVelocity = 0;
            }
        }

        Vector2 oldTransform = _updateTransform;
    }

    void LandingRoutine()
    {
        Animations.ResetTrigger("AirPeaked");
        Animations.SetTrigger("Grounded");
        GroundedStopTrot();
        _yAccelerators[GRAVITY].Reset();
        _yVelocity = 0;
        if(_cm.Gallop)
        {
            StartGallop();
        }
        else
        {
            GroundedStandardMovement(); //Checks input to determine if we're going into an idle state or a moving state
        }
    }

    /*
     * General setup, variables, and Awake() function follow
     */
    //readonly variables

    readonly Func<float, float, float, float> ConstAccelToPosition = (a, lowerBound, upperBound) => .5f * a * (Mathf.Pow(upperBound, 2) - Mathf.Pow(lowerBound, 2));
    readonly Func<float, float, float, float> ConstAccelToVelocity = (a, lowerBound, upperBound) => a * (upperBound - lowerBound);

    private ControlManager _cm;

    readonly int WORLD = 0;
    readonly int PLAYER = 1;
    readonly int GRAVITY = 2;

    Accelerator[] _xAccelerators;
    Accelerator[] _yAccelerators;

    [Header("Caster Arrays: ")]
    public CasterArray_old feetCA;
    public CasterArray_old rightCA;
    public CasterArray_old leftCA;
    public CasterArray_old topCA;

    //High-level Editor variables 
    [Header("Animation Controller: ")]
    public Animator Animations;
    public float AirborneAcceleration = 16f;
    [Header("Trot variables")]
    public float TrotV = 40.8f; // units per s
    public float TrotWindUpV = 25f;
    public float TrotWindUpt = .2f;
    [Header("Gallop variables")]
    public float GallopWindupt = .4f;
    public float GallopV = 65f;
    [Header("Climb variables")]
    public float ClimbSinAccel = 18f;
    public float ClimbConstVel = 3f; //the power to which the sin function is raised.
    [Header("Standard Jump variables")]
    public float StJumpVNaught = 48f;
    [Tooltip("Max and min determine how long the CounterActGravity acceleration is active in response to holding the jump button")]
    public float MaxJumpS = .3f; //Amount of time that Standard jump has linear velocity
    [Tooltip("Max and min determine how long the CounterActGravity acceleration is active in response to holding the jump button")]
    public float MinJumpS = .10f;
    [Tooltip("Max and min determine how long the CounterActGravity acceleration is active in response to holding the jump button")]
    public float CounterActGravity = 115f;
    public float g = 60f; // units per s^2 / 2
    Stopwatch _elapsedJumpTime;
    public LayerMask GroundMask;

    float _gallopA;
    float _trotA;
    float _stJumpA;

    //Movement variables updated every frame
    float _yVelocity;
    float _xVelocity;

    //Collision-detection
    Vector2 _updateTransform;
    BoxCollider2D _boxCollider;
    ContactFilter2D _ground;
    [Header("Particle-firing objects")]
    public GameObject BottomLeft;
    public GameObject BottomRight;

    protected void Start()
    {
        _cm = ControlManager.GetInstance();

        _facing = Face.Right;
        Animations.SetTrigger("Rightward");
        Animations.ResetTrigger("Leftward");
        state = PlayerState.Idle;
        //_stJumpA = (StJumpVFinal - StJumpVNaught) / StJumpWindupt;

        _xAccelerators = new Accelerator[3];
        _yAccelerators = new Accelerator[3];

        for(int i = 0; i < _xAccelerators.Length; i++)
        {
            _xAccelerators[i] = new Accelerator();
            _yAccelerators[i] = new Accelerator();
        }

        _stateRoutineVector[(int)PlayerState.Idle] = IdleRoutine;
        _stateRoutineVector[(int)PlayerState.StartTrot] = StartTrotRoutine;
        _stateRoutineVector[(int)PlayerState.Trot] = TrotRoutine;
        _stateRoutineVector[(int)PlayerState.StartGallop] = StartGallopRoutine;
        _stateRoutineVector[(int)PlayerState.Gallop] = GallopRoutine;
        _stateRoutineVector[(int)PlayerState.Airborne] = AirborneRoutine;
        _stateRoutineVector[(int)PlayerState.Landing] = LandingRoutine;
        _stateRoutineVector[(int)PlayerState.AirborneMoving] = AirborneMovingRoutine;
        _stateRoutineVector[(int)PlayerState.WallCling] = WallClingRoutine;
        _stateRoutineVector[(int)PlayerState.WallClimb] = WallClimbRoutine;
        _stateRoutineVector[(int)PlayerState.WallSlide] = WallSlideRoutine;

        _trotA = (TrotV - TrotWindUpV) / TrotWindUpt;
        _gallopA = (GallopV - TrotV) / GallopWindupt;
        _boxCollider = (BoxCollider2D)gameObject.GetComponent("BoxCollider2D");
        _elapsedJumpTime = new Stopwatch();
        _updateTransform = new Vector2(0, 0);
        _ground = new ContactFilter2D();
        _ground.layerMask = GroundMask;
        _ground.useLayerMask = true;
        _yVelocity = 0;
        _xVelocity = 0;
        GroundOrAir();
    }

    protected override void HitGround() {
        throw new NotImplementedException();
    }

    protected override void OnNoGround() {
        throw new NotImplementedException();
    }

    protected override void HitWall() {
        
    }

    protected override void OffWall()
    {
        throw new NotImplementedException();
    }

    protected override void HitBackWall() {
        throw new NotImplementedException();
    }
}
