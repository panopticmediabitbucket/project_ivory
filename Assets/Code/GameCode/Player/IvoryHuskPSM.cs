﻿using System;
using System.Collections.Generic;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.GameCode.System.Schemata;
using Assets.Code.System.TimeUtil;
using Assets.Code.Physics.Accelerators;
using Assets.Code.System;
using UnityEngine;

public sealed class IvoryHuskPSM : PhysicsStateMachineWorldObject<IvoryHuskPSM.IvoryHuskStates> {
    public enum IvoryHuskStates {
        Idle, Trot, SlowDown, Airborne, Land, StandardJump, WallSlide,
        SlowAirborne, MoveAirborne
    }

    private ControlManager _cm = ControlManager.GetInstance();
    ConstantAccel<IvoryHuskStates> _airborneMovement;

    private ConditionsGate _jumpConditions;
    private static readonly float JUMPCOOLDOWNTIME = .15f;


    protected override void Start() {
        base.Start();

        //_wallFrictionAccel = new CustomAccel<External>(WallFrictionDeltas, WallFrictionDirection, External.DynamicFriction);

        TimedBoolean jumpCooldown = new TimedBoolean("JumpCooldown_" + gameObject.name, JUMPCOOLDOWNTIME, true);
        ConditionAtom jumpCooldownFinished = new ConditionAtom(() => jumpCooldown, true, () => jumpCooldown.Restart());
        ConditionAtom jumpButtonDown = new ConditionAtom(() => _cm.JumpDown, true);
        _jumpConditions = new ConditionsGate(ConditionType.And, jumpButtonDown, jumpCooldownFinished);
    }

    protected override void HitGround() {
        state = IvoryHuskStates.Land;
        _jumpConditions.Reset();
        InterruptMotion(IvoryHuskStates.SlowAirborne);
        InterruptMotion(IvoryHuskStates.MoveAirborne);
    }

    protected override void OnNoGround() {
        if (state != IvoryHuskStates.StandardJump) {
            state = IvoryHuskStates.Airborne;
        }
    }

    protected override void HitWall() {
        switch (state) {
            case IvoryHuskStates.Airborne:
            case IvoryHuskStates.StandardJump:
            case IvoryHuskStates.MoveAirborne:
            case IvoryHuskStates.SlowAirborne:
                state = IvoryHuskStates.WallSlide;
                break;
            default:
                break;
        }
    }

    protected override void OffWall() {
        state = IvoryHuskStates.Airborne;
    }

    protected override void HitBackWall() {
    }

    private void Idle() {
        if (DirectionInput()) {
            state = IvoryHuskStates.Trot;
        } else if (OppDirectionInput()) {
            faceRight = !faceRight;
            state = IvoryHuskStates.Trot;
        }
    }

    private void SlowDown() {
        if (IsMotionComplete(state)) {
            state = IvoryHuskStates.Idle;
            Idle();
        }
    }

    private bool DirectionInput() => faceRight ? _cm.RightInput : _cm.LeftInput;
    private bool OppDirectionInput() => faceRight ? _cm.LeftInput : _cm.RightInput;
    private void Trot() {
        if (!(DirectionInput())) {
            state = IvoryHuskStates.SlowDown;
        } else if (_jumpConditions.Met()) {
            state = IvoryHuskStates.StandardJump;
            LeavingSurface();
        }
    }

    private void Airborne() {
        if (DirectionInput()) {
            state = IvoryHuskStates.MoveAirborne;
        } else if (OppDirectionInput()) {
            state = IvoryHuskStates.SlowAirborne;
        }
    }

    private void MoveAirborne() {
        if (!DirectionInput()) {
            state = IvoryHuskStates.Airborne;
        }
    }

    private void SlowAirborne() {
        if (!OppDirectionInput()) {
            state = IvoryHuskStates.Airborne;
        }
    }

    private void Land() {
        if (DirectionInput()) {
            state = IvoryHuskStates.Trot;
        } else if (IsMotionComplete(state)) {
            state = IvoryHuskStates.Idle;
        }
    }

    private void StandardJump() {
        if (IsMotionComplete(IvoryHuskStates.StandardJump)) {
            state = IvoryHuskStates.Airborne;
        }
        //Airborne();
    }

    private void WallSlide() { }

    // Custom Accels

    private CustomAccel<External> _wallFrictionAccel;

    private IEnumerable<VDelta> WallFrictionDeltas(float frictionCoefficient) {
        yield return new VDelta(0, 0);
    }

    private Vector2 WallFrictionDirection() {
        return Vector2.zero;
    }

}
