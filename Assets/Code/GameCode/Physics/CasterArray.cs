﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CasterArray_old : MonoBehaviour{
    
    public float width; //total distance from origin
    public float rotation; //about the z-axis
    public int count; //number of casters
    private BoxCollider2D _boxCollider;

    float _resolution; //units of distance per ray

    private Vector3[] _castingPositions;

    void Awake()
    {
        _resolution = (float) width / count + width / (count * count);
        _boxCollider = (BoxCollider2D)gameObject.AddComponent(typeof(BoxCollider2D));
        _boxCollider.size = new Vector2(.2f, width);
        _boxCollider.offset = new Vector2(0, width/2);
        _boxCollider.isTrigger = true;
    }

    public Vector2 NormalizedLocalUp { get { return transform.rotation * Vector2.up; } }

    /*
     * Used to detect if there is a collision in the given direction (movementDirection) within the given distance (magnitude)
     * normalDir creates a mask such that only plane collisions of the given normal Direction register
     * Zero vector for normalDir makes GetHit maskless.
     */
    public RaycastHit2D GetHit(float magnitude, Vector3 movementDirection, Vector2 normalDir)
    {
        Func<Vector2, bool> func;
        if(normalDir != Vector2.zero)
        {
            func = new Func<Vector2, bool>((v2) => v2 == normalDir);
        }
        else
        {
            func = new Func<Vector2, bool>((v2) => true);
        }
        
        RaycastHit2D hit;
        RaycastHit2D retVal = new RaycastHit2D();
        retVal.distance = 2 * magnitude; //outside of the possible distance initially

        for(int i = 0; i < count; i++)
        {
            hit = Physics2D.Raycast((Vector2)transform.position + (i * _resolution * NormalizedLocalUp), movementDirection, magnitude, 512);
            if(hit)
            {
                if(hit.distance < retVal.distance && func(hit.normal))
                {
                    retVal = hit; //return value is set to new hit if the distance is less than it was before
                }
            }
        }
        return retVal;
    }

    public bool getTouch()
    {
        return Physics2D.IsTouchingLayers(_boxCollider, 512);
    }

    private Vector3[] _castingLocations = new Vector3[1];
    public Vector3[] GetCastingLocations()
    {
        if(_castingLocations.Length != count)
        {
            _castingLocations = new Vector3[count];
        }

        float tempRes = width / (count - 1);
        for(int i = 0; i < count; i++)
        {
            _castingLocations[i] = (Vector2)transform.position + i * tempRes * NormalizedLocalUp;
        }

        return _castingLocations;
    }
}
