﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Code.Physics.Collision;
using UnityEngine;

namespace Assets.Code.Physics.Interfaces
{
    public interface IPhysicsStateMachine
    {
        GameObject gameObject { get; }
        Vector2 momentum { get; }
        Vector2 voluntaryMomentum { get; }
        Vector2 involuntaryMomentum { get; }
    }
}
