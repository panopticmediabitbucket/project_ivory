﻿using Assets.Code.Physics.Collision;

namespace Assets.Code.GameCode.Physics.Interfaces {
    public interface ICollidable {
        CollisionBox GetCollisionSystem();
    }
}
