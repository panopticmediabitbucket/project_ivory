﻿using Assets.Code.Physics.Accelerators;
using System;
using Assets.Code.System.TimeUtil;
using UnityEngine;

namespace Assets.Code.Physics.Interfaces {
    public interface IStateBasedMotionMachine<TEnum>: IPhysicsStateMachine where TEnum: Enum {
        ITimeZone timeZone { get; set; }
        void FlagInternalForInterrupt(TEnum state);
        void InterruptExternal(External external);
        Vector2 CalculateFrameMotionAndUpdateMomentum();
        void DeliverInternalAccel(Accel<TEnum> accel);
        void DeliverExternalAccel(Accel<External> accel);
        void ResetMomentum();
        bool IsActive(TEnum state);
        void SetInternalMomentum(float x, float y);

    }
}
