﻿using System;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.GameCode.Physics.Collision;
using Assets.Code.GameCode.Physics.Collision.Util;
using Assets.Code.GameCode.Physics.Surfaces;
using UnityEngine;

namespace Assets.Code.Physics.Collision {

    [Serializable]
    public class CasterArray {
        [SerializeField]
        private GameObject _gameObject;
        [SerializeField]
        public Vector2 offset;
        [SerializeField]
        [Range(1, 30)]
        public float length = 1; //total distance from origin
        [SerializeField]
        [Range(0, 360)]
        public float rotationalOffset = 0; //about the z-axis
        [SerializeField]
        [Range(2, 50)]
        public int count = 2; //number of casters
        [SerializeField]
        public Color color = Color.black;

        private Quaternion _rotation;
        public Vector2 position { get { return (Vector2)_gameObject.transform.position; } }
        public Vector2 casterStartPosition { get { return position - normalizedLocalUp * length / 2; } }
        public Vector2 normalizedLocalUp { get { return _gameObject.transform.rotation * Vector2.up; } }

        private BoxCollider2D _internalCollider;
        float _resolution; //units of distance per ray
        private Vector3[] _castingLocations;

        // basic constructor for default editor color values
        public CasterArray() { }

        public void Initialize(GameObject gameObject) {
            _rotation = Quaternion.Euler(0, 0, rotationalOffset);
            _resolution = (float)length / count + length / (count * count);
            _castingLocations = new Vector3[count];

            _gameObject = GameObject.Instantiate(GameManager.GetInstance().EmptyPrefab, gameObject.transform.position + (Vector3)offset, _rotation);
            _gameObject.transform.parent = gameObject.transform;

            _internalCollider = (BoxCollider2D)_gameObject.AddComponent(typeof(BoxCollider2D));
            _internalCollider.size = new Vector2(.25f, length);
            _internalCollider.isTrigger = true;
        }

        /*
         * Used to detect if there is a collision in the given normal range. The Raycast is returned, and the position of the caster that has the nearest collision is outed
         * as 'casterSource.' Additionally, a vector2 indicating the averageNormals of a distant plane is included as well. The distant plane is defined as the plane some
         * extra distance away in the same direction as the current movement. If collisions occur in that direction, the object is notified using the weightedAverageNormals.
         * As the name suggests, the single vector is weighted according to the relative distance of each of the collisions, so in the case that the surface is bent,
         * the client object has an opportunity to make the necessary adjustments to account for this. 
         */
        public RaycastHit2D GetHit(float magnitude, Vector3 movementDirection, VectorRange range, out Vector2 weightedAverageNormals, out Vector2 casterSource) {
            casterSource = Vector2.zero;
            float div = Time.timeScale > 1 ? 1 / Time.timeScale : Time.timeScale;
            float distantPlane = magnitude * (4 / div);
            RaycastHit2D retVal = new RaycastHit2D();
            retVal.distance = magnitude + GlobalPhysicsVariables.SURFACECORRECTION; //outside of the possible distance initially
            var castingLocations = GetCastingLocations();
            weightedAverageNormals = Vector2.zero;
            float hitWeight = 0f;

            for (int i = 0; i < count; i++) {
                RaycastHit2D insideCollider = Physics2D.Raycast(castingLocations[i], Vector2.zero, 0, 512);
                RaycastHit2D hit = Physics2D.Raycast(castingLocations[i], movementDirection, distantPlane, 512);

                if (insideCollider) {
                    UnityEngine.Debug.Log("inside collider");
                }

                if (!insideCollider && hit && range.Contains(hit.normal)) {
                    if (hit.distance <= retVal.distance) {
                        retVal = hit; //return value is set to new hit if the distance is less than it was before
                        casterSource = castingLocations[i];
                    }

                    if (hit.distance <= distantPlane) {
                        hitWeight += (1 - hit.fraction);
                        weightedAverageNormals += hit.normal * (1 - hit.fraction);
                    }
                }
            }

            if (hitWeight != 0) {
                weightedAverageNormals /= hitWeight;
                weightedAverageNormals = weightedAverageNormals.normalized;
            }

            return retVal.distance <= magnitude ? retVal : new RaycastHit2D();
        }

        public bool GetTouch() {
            return Physics2D.IsTouchingLayers(_internalCollider, 512);
        }


        private Collider2D[] _colliderContacts = new Collider2D[5];
        public ISurface GetSurface() {
            int count = _internalCollider.GetContacts(_colliderContacts);
            if (count > 0) {
                return (ISurface)CollisionUtil.GetCollideableDataFromCollider(_colliderContacts[0]);
            } else {
                return null;

            }
        }

        public Vector3[] GetCastingLocations() {
            Vector2 startPos = casterStartPosition;
            Vector2 up = normalizedLocalUp;
            for (int i = 0; i < count; i++) {
                _castingLocations[i] = startPos + i * up * _resolution;
            }

            return _castingLocations;
        }

        private Vector3 GetFinalLocation(Vector2 startPosition) {
            return startPosition + normalizedLocalUp * length;
        }

        public CollisionData AcquireAverageNormal(Vector2 positionOffset, Vector2 direction, VectorRange range) {
            CollisionData data = new CollisionData();
            Vector2 startPos = casterStartPosition + positionOffset;
            Vector2 endPos = GetFinalLocation(startPos);

            RaycastHit2D hit_a = Physics2D.Raycast(startPos, direction, 1, 512);
            RaycastHit2D hit_b = Physics2D.Raycast(endPos, direction, 1, 512);
            if (range.Contains(hit_a.normal) && range.Contains(hit_b.normal) && hit_a.collider != hit_b.collider) {
                data.normal = ((hit_b.normal + hit_a.normal) / 2).normalized;
                data.pivot = casterStartPosition;
                data.collisionType = WorldCollisionFlags.Ground;
                return data;
            }

            if (range.Contains(hit_a.normal)) {
                data.normal = hit_a.normal;
                data.pivot = casterStartPosition;
                data.collisionType = WorldCollisionFlags.Ground;
            } else {
                data.normal = hit_b.normal;
                data.pivot = GetFinalLocation(casterStartPosition);
                data.collisionType = WorldCollisionFlags.Ground;
            }
            return data;
        }
    }
}
