﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Code.GameCode.Physics.Collision;
using Assets.Code.GameCode.Physics.Surfaces;
using JetBrains.Annotations;
using UnityEngine;

namespace Assets.Code.Physics.Collision {
    public abstract class AbstractCollisionPolygon {
        public readonly string type;

        protected AbstractCollisionPolygon(string type) {
            this.type = type;
        }

        public abstract CollisionData GetAllCollisions(Vector2 initialChangeInPosition, out Vector2 outChangeInPosition);

        public abstract RaycastHit2D RotationalCollision(Vector2 pivot, ref Quaternion rotation);
    }

}