﻿using Assets.Code.GameCode.Physics.Surfaces;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Collision.Util {
    public static class CollisionUtil {
        public static ICollideable GetCollideableDataFromCollider(Collider2D collider) {
            if (collider != null) {
                var attachedGo = collider.gameObject;
                var colliderComponent = attachedGo.GetComponentInParent<ICollideable>();
                return colliderComponent != null ? colliderComponent : DefaultSurface.defaultSurface;
            } else {
                return null;
            }
        }
    }
}
