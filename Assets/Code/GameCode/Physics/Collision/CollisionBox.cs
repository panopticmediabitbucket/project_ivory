﻿using System;
using System.ComponentModel;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.GameCode.Physics.Collision;
using Assets.Code.GameCode.Physics.Collision.Util;
using Assets.Code.GameCode.Physics.Surfaces;
using UnityEngine;

namespace Assets.Code.Physics.Collision {
    [Serializable]
    public class CollisionBox : AbstractCollisionPolygon {
        GameObject _gameObject;
        private Quaternion objRotation => _gameObject.transform.rotation;
        private BoxCollider2D _collider;

        private CasterArray _back;
        private CasterArray _front;
        private CasterArray _up;
        private CasterArray _down;

        [Range(.1f, 100)]
        [SerializeField]
        public float width = 1f;
        public float w_squared { get; private set; }
        [Range(.1f, 100)]
        [SerializeField]
        public float height = 1f;
        [Range(2, 50)]
        [SerializeField]
        public int castersPerArray;
        [SerializeField]
        public Vector2 offset;

        public CollisionBox() : base("Box") { }

        public void Initialize(GameObject go) {
            _gameObject = go;
            BuildCasterBox(go);
        }

        private void BuildCasterBox(GameObject go) {
            w_squared = Mathf.Pow(width, 2);

            Vector2 left = offset + new Vector2(0, height / 2);
            Vector2 right = left + new Vector2(width, 0);
            Vector2 bottom = offset + new Vector2(width / 2, 0);
            Vector2 top = bottom + new Vector2(0, height);

            _up = new CasterArray() { count = castersPerArray, length = width, offset = top, rotationalOffset = 90 };
            _up.Initialize(go);
            _front = new CasterArray() { count = castersPerArray, length = height, offset = right, rotationalOffset = 0 };
            _front.Initialize(go);
            _down = new CasterArray() { count = castersPerArray, length = width, offset = bottom, rotationalOffset = -90 };
            _down.Initialize(go);
            _back = new CasterArray() { count = castersPerArray, length = height, offset = left, rotationalOffset = 180 };
            _back.Initialize(go);
        }

        private CollisionData GetContactAndModifyChangeInPosition(CasterArray array, WorldCollisionFlags mode, ref Vector2 changeInPosition) {
            float distance = changeInPosition.magnitude;
            Vector2 norm = changeInPosition / distance;

            VectorRange checkRange = GlobalPhysicsVariables.GetRangeFromModeAndDirection(mode, _front.position.x > _back.position.x);

            var hit = array.GetHit(distance, norm, checkRange, out Vector2 normalAv, out Vector2 casterPoint);
            ICollideable collideable = CollisionUtil.GetCollideableDataFromCollider(hit.collider);
            CollisionData collisionData = new CollisionData() { normal = normalAv, pivot = casterPoint, collidedObjectData = collideable };
            if (hit) {
                float cosTh = Vector2.Dot(-hit.normal, norm);
                float penetrationDistance = distance - hit.distance;
                float correction = GlobalPhysicsVariables.SURFACECORRECTION + cosTh * penetrationDistance;
                changeInPosition += hit.normal * correction;

                collisionData.collisionType = mode;
                collisionData.collisionDataType = CollisionDataType.Contact;
                return collisionData;

            } else if (normalAv != Vector2.zero) {
                collisionData.collisionDataType = CollisionDataType.Incoming;
            } else {
                collisionData.collisionDataType = CollisionDataType.None;
            }

            return collisionData;
        }

        public CollisionData GetGroundCollision(Vector2 startingChange, out Vector2 changeInPosition) {
            changeInPosition = startingChange;
            CollisionData collisionData = GetContactAndModifyChangeInPosition(_down, WorldCollisionFlags.Ground, ref changeInPosition);
            return collisionData;
        }

        public CollisionData GetWallCollisions(Vector2 startingChange, out Vector2 changeInPosition) {
            changeInPosition = startingChange;
            CollisionData collisionData = GetContactAndModifyChangeInPosition(_front, WorldCollisionFlags.Wall, ref changeInPosition);
            collisionData |= GetContactAndModifyChangeInPosition(_back, WorldCollisionFlags.BackWall, ref changeInPosition);
            return collisionData;
        }

        public Vector2 bottomFrontCorner => _front.position + (Vector2)(objRotation * Vector2.up) * -(height / 2);

        public Vector2 centerPoint => bottomPivot + (Vector2)(objRotation * Vector2.up) * height / 2;
        public Vector2 topPivot => _up.position;
        public Vector2 bottomPivot => _down.position;
        public Vector2 frontPivot => _front.position;
        public Vector2 backPivot => _back.position;

        public override CollisionData GetAllCollisions(Vector2 initialChangeInPosition, out Vector2 outChangeInPosition) {
            outChangeInPosition = initialChangeInPosition;
            CollisionData collisionData = new CollisionData();

            if (Vector2.Dot(Vector2.down, initialChangeInPosition) > .00001f) {
                collisionData |= GetContactAndModifyChangeInPosition(_down, WorldCollisionFlags.Ground, ref outChangeInPosition);

            } else if (Vector2.Dot(Vector2.up, initialChangeInPosition) > 0) {
                collisionData |= GetContactAndModifyChangeInPosition(_up, WorldCollisionFlags.Ceiling, ref outChangeInPosition);
            }

            if (Vector2.Dot(Vector2.right, initialChangeInPosition) != 0) {
                collisionData |= GetContactAndModifyChangeInPosition(_front, WorldCollisionFlags.Wall, ref outChangeInPosition);
                collisionData |= GetContactAndModifyChangeInPosition(_back, WorldCollisionFlags.Wall, ref outChangeInPosition);
            }

            return collisionData;
        }

        private Vector2[] GetCorners() {
            Vector2[] corners = new Vector2[] { _up.casterStartPosition, _down.casterStartPosition, _front.casterStartPosition, _back.casterStartPosition };
            return corners;
        }

#pragma warning disable CS0618 // Type or member is obsolete
        public override RaycastHit2D RotationalCollision(Vector2 pivot, ref Quaternion rotation) {
            var corners = GetCorners();
            float theta = Quaternion.ToEulerAngles(rotation).z;
            RaycastHit2D retHit = new RaycastHit2D();
            float radTheta = theta * Mathf.Deg2Rad;

            for (int i = 0; i < 4; i++) {
                Vector2 corner = corners[i];
                Vector2 vectorToCorner = corner - pivot;
                Vector2 rotatedVectorToCorner = rotation * (vectorToCorner);

                float magnitude = rotatedVectorToCorner.magnitude;
                Vector2 normalizedVecToC = rotatedVectorToCorner / magnitude;
                RaycastHit2D hit = Physics2D.Raycast(pivot, normalizedVecToC, magnitude, 512);

                if (hit) {
                    UnityEngine.Debug.Log("rotational collision registered");
                    float distance = hit.distance;
                    float collisionYComp = distance * Mathf.Sin(radTheta);
                    float reducedTheta = Mathf.Asin(collisionYComp / magnitude) * Mathf.Rad2Deg;
                    rotation = Quaternion.Euler(0, 0, reducedTheta);
                    retHit = hit;
                }
            }
            return retHit;
        }
#pragma warning restore CS0618 // Type or member is obsolete

        public WorldCollisionFlags GetTouch() {
            WorldCollisionFlags collision = WorldCollisionFlags.None;

            collision |= _up.GetTouch() ? WorldCollisionFlags.Ceiling : WorldCollisionFlags.None;
            collision |= _down.GetTouch() ? WorldCollisionFlags.Ground : WorldCollisionFlags.None;
            collision |= _front.GetTouch() ? WorldCollisionFlags.Wall : WorldCollisionFlags.None;
            collision |= _back.GetTouch() ? WorldCollisionFlags.Wall : WorldCollisionFlags.None;

            return collision;
        }

        public WorldCollisionFlags GroundContact(out ISurface surface) {
            surface = _down.GetSurface();
            return _down.GetTouch() ? WorldCollisionFlags.Ground : WorldCollisionFlags.None;
        }

        public WorldCollisionFlags WallContact(out ISurface surface) {
            if (_front.GetTouch()) {
                surface = _front.GetSurface();
                return WorldCollisionFlags.Wall;
            } else if (_back.GetTouch()) {
                surface = _back.GetSurface();
                return WorldCollisionFlags.BackWall;
            } else {
                surface = null;
                return WorldCollisionFlags.None;
            }
        }

        public CollisionData AcquireAverageNormal(Vector2 offset, WorldCollisionFlags flags) {
            switch (flags) {
                case WorldCollisionFlags.Ground:
                    return _down.AcquireAverageNormal(offset, Vector2.down, GlobalPhysicsVariables.surfaceRange);
                case WorldCollisionFlags.Ceiling:
                    return _up.AcquireAverageNormal(offset, Vector2.up, GlobalPhysicsVariables.ceilingRange);
                default:
                    break;
            }
            return new CollisionData();
        }

        public void GetCorrespondingCornerPositions(WorldCollisionFlags mode, out Vector2 frontCorner, out Vector2 backCorner) {
            switch (mode) {
                case WorldCollisionFlags.Ground:
                    Vector2 bottomOffset = (Vector2)(objRotation * Vector2.up) * (-height / 2);
                    frontCorner = _front.position + bottomOffset;
                    backCorner = _back.position + bottomOffset;
                    break;
                case WorldCollisionFlags.Ceiling:
                    Vector2 topOffset = (Vector2)(objRotation * Vector2.up) * (height / 2);
                    frontCorner = _front.position + topOffset;
                    backCorner = _back.position + topOffset;
                    break;
                case WorldCollisionFlags.Wall:
                    Vector2 heightOffset = (Vector2)(objRotation * Vector2.up) * (height / 2);
                    frontCorner = _front.position + heightOffset;
                    backCorner = _front.position - heightOffset;
                    break;
                case WorldCollisionFlags.BackWall:
                    heightOffset = (Vector2)(objRotation * Vector2.up) * (height / 2);
                    frontCorner = _back.position + heightOffset;
                    backCorner = _back.position + heightOffset;
                    break;
                default:
                    throw new InvalidEnumArgumentException("Too many flags set in 'mode'", (int)mode, typeof(WorldCollisionFlags));
            }
        }

        public RaycastHit2D GetConcaveCollision(Vector2 changeInPosition, VectorRange checkRange, Vector2 frontCorner, Vector2 backCorner) {
            float magnitude = changeInPosition.magnitude;
            var hit = Physics2D.Raycast(frontCorner, changeInPosition / magnitude, magnitude, 512);
            return checkRange.Contains(hit.normal) ? hit : new RaycastHit2D();
        }

        private static readonly float CONVEX_FORGIVENESS = .7f;
        public RaycastHit2D GetConvexCollision(Vector2 changeInPosition, VectorRange checkRange, Vector2 frontCorner) {
            Vector2 fc_prime = frontCorner + changeInPosition;
            RaycastHit2D convexCollision = Physics2D.Raycast(fc_prime, Vector2.down, CONVEX_FORGIVENESS, 512);
            return checkRange.Contains(convexCollision.normal) ? convexCollision : new RaycastHit2D();
        }
    }
}
