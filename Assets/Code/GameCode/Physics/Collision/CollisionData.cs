﻿using System;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Collision {
    public enum CollisionDataType {
        None, Incoming, Contact
    }
    public struct CollisionData : IComparable<CollisionData> {
        public Vector2 normal { get; set; }
        public Vector2 pivot { get; set; }
        public WorldCollisionFlags collisionType { get; set; }
        public CollisionDataType collisionDataType { get; set; }
        public ICollideable collidedObjectData { get; set; }
        public CollisionData[] secondaryCollisionData { get; private set; }
        //public CollisionData? secondaryCollisionData { get; set; }

        public static implicit operator bool(CollisionData collisionData) {
            return collisionData.normal != Vector2.zero;
        }

        public static CollisionData operator |(CollisionData a, CollisionData b) {
            switch (a.collisionDataType) {
                case CollisionDataType.None:
                    if(b.collisionDataType == CollisionDataType.Contact || b.collisionDataType == CollisionDataType.Incoming) {
                        return b;
                    }
                    return a.PopulateMissingData(b);
                case CollisionDataType.Contact:
                    if (b.collisionDataType == CollisionDataType.Contact) {
                        return a.CompareTo(b) >= 0 ? a.WithSecondaryCollision(b) : b.WithSecondaryCollision(a);
                    }
                    return a;
                case CollisionDataType.Incoming:
                    if (b.collisionDataType == CollisionDataType.Contact) {
                        return b;
                    } else if (b.collisionDataType == CollisionDataType.Incoming) {
                        return a.CompareTo(b) >= 0 ? a.WithSecondaryCollision(b) : b.WithSecondaryCollision(a);
                    }
                    return a;
                default:
                    return a;
            }
        }

        public CollisionData WithSecondaryCollision(CollisionData secondary) {
            secondaryCollisionData = new CollisionData[] { secondary };
            return this;
        }

        public CollisionData PopulateMissingData(CollisionData other) {
            collisionType |= other.collisionType;
            collidedObjectData = collidedObjectData ?? other.collidedObjectData;
            if(normal == Vector2.zero) {
                normal = other.normal;
                pivot = other.pivot;
            }
            return this;
        }

        public static implicit operator WorldCollisionFlags(CollisionData data) {
            return data.collisionType;
        }

        /*
        public static CollisionData Order([NotNull]params CollisionData[] collisionsData) {
            List<CollisionData> list = collisionsData.ToList();
            list.Sort();
            var enumerator = list.GetEnumerator();
            var nextEnumerator = list.GetEnumerator();
            nextEnumerator.MoveNext();
            while (nextEnumerator.MoveNext()) {
                enumerator.MoveNext();
                enumerator.Current.secondaryCollisionData = 

            }
            return list.First();
        }
        */

        public int CompareTo(CollisionData other) {
            return -normal.magnitude.CompareTo(other.normal.magnitude);
        }
    }
}
