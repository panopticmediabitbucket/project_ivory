﻿using System;

namespace Assets.Code.GameCode.Physics.Collision {
    /*
     * A flag-based enumeration designed for examining different world-position posibilities and collisions
     */
    [Flags]
    public enum WorldCollisionFlags {
        None = 0,
        Ground = 1,
        Wall = 2,
        BackWall = 4,
        Ceiling = 8,
        LeavingGround = 16,
        FollowGround = 32,
        LeavingWall = 64,
        FollowWall = 128,
        Landing = 256,
        WallCollision = 512,
        AirHang = 1028,
        CG = Ceiling | Ground,
        EitherWall = BackWall | Wall,
        GroundWall = Ground | Wall
    }

    /*
     * Static utilities for WorldCollisionFlags
     */
    public static class WCFUtil {
        public static bool FlagsMatch(WorldCollisionFlags a, WorldCollisionFlags b) {
            return (a ^ b) == 0;
        }

        public static bool PartialMatch(WorldCollisionFlags value, WorldCollisionFlags pattern, out WorldCollisionFlags missing, out WorldCollisionFlags nonMatching) {
            WorldCollisionFlags xor = (value ^ pattern);
            missing = xor & pattern;
            nonMatching = xor & value;
            return (int)missing < (int)pattern;
        }
    }
}
