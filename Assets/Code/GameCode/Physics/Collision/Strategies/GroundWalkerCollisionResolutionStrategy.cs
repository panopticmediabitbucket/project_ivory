﻿using Assets.Code.GameCode.Physics.Surfaces;
using Assets.Code.Physics.Collision;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Collision.Strategies {
    public class GroundWalkerCollisionResolutionStrategy : ICollisionResolutionStrategy {
        public AbstractCollisionPolygon collisionDetector => _collisionBox;

        private CollisionBox _collisionBox;

        public GroundWalkerCollisionResolutionStrategy(CollisionBox collisionBox) {
            _collisionBox = collisionBox;
        }

        public CollisionData ResolveCollisions(WorldCollisionFlags worldCollisionState, ref Vector2 changeInPosition) {
            CollisionData collisionData = new CollisionData();
            Debug.Log("World State: " + worldCollisionState);

            if ((worldCollisionState & WorldCollisionFlags.Ground) != 0) {
                collisionData |= GroundedRoutine(ref changeInPosition);
            } else if (worldCollisionState == WorldCollisionFlags.None) {
                collisionData |= AirborneRoutine(ref changeInPosition);
            } else if ((worldCollisionState & WorldCollisionFlags.EitherWall) != 0) {
                collisionData |= WallRoutine(ref changeInPosition);
            }

            return collisionData;
        }

        private CollisionData GroundedRoutine(ref Vector2 changeInPosition) {
            CollisionData collisionData = new CollisionData();
            var touchFlags = _collisionBox.GroundContact(out ISurface groundSurface);
            collisionData.collidedObjectData = groundSurface;
            if ((touchFlags & WorldCollisionFlags.Ground) == 0) {
                collisionData.collisionType = WorldCollisionFlags.LeavingGround;
            } else if (changeInPosition != Vector2.zero) {
                collisionData.collisionType |= WorldCollisionFlags.FollowGround;
            }
            collisionData |= _collisionBox.GetWallCollisions(changeInPosition, out changeInPosition);
            return collisionData;
        }

        private CollisionData AirborneRoutine(ref Vector2 changeInPosition) {
            CollisionData collisionData = _collisionBox.GetGroundCollision(changeInPosition, out changeInPosition);
            collisionData |= _collisionBox.GetWallCollisions(changeInPosition, out changeInPosition);

            if (collisionData.collisionDataType == CollisionDataType.Contact) {
                Debug.Log("Collision registered");
                if ((collisionData & WorldCollisionFlags.Ground) != 0) {
                    collisionData.collisionType = WorldCollisionFlags.Landing;
                } else {
                    collisionData.collisionType = WorldCollisionFlags.WallCollision;
                }
            } else {
                collisionData.collisionType = WorldCollisionFlags.AirHang;
            }
            return collisionData;
        }

        private CollisionData WallRoutine(ref Vector2 changeInPosition) {
            CollisionData collisionData = new CollisionData();
            WorldCollisionFlags wallTouch = _collisionBox.WallContact(out ISurface wallSurface);
            collisionData.collidedObjectData = wallSurface;

            if ((wallTouch & WorldCollisionFlags.EitherWall) == 0) {
                collisionData.collisionType |= WorldCollisionFlags.LeavingWall;
            } else {
                collisionData.collisionType |= WorldCollisionFlags.FollowWall;
            }

            collisionData |= _collisionBox.GetGroundCollision(changeInPosition, out changeInPosition);
            if ((collisionData & WorldCollisionFlags.Ground) != 0) {
                collisionData.collisionType = WorldCollisionFlags.Landing;
            }
            return collisionData;
        }
    }
}
