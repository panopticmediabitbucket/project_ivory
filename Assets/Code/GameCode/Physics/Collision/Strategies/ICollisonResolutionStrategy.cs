﻿using Assets.Code.Physics.Collision;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Collision.Strategies {
    public interface ICollisionResolutionStrategy {
        AbstractCollisionPolygon collisionDetector { get; }
        CollisionData ResolveCollisions(WorldCollisionFlags worldCollisionState, ref Vector2 changeInPosition);
    }
}
