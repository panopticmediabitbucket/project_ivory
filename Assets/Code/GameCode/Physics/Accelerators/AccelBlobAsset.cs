﻿using Assets.Code.GameCode.Meta.Documentation;
using Assets.Code.Physics.Accelerators;
using Unity.Entities;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Accelerators {
    /**
     * An asset used to store static AccelData related to any single instnace of an Accel
     */
    [SeeAlso(typeof(BlobAssetReference<AccelBlobAsset>))]
    public struct AccelBlobAsset : IComponentData {
        public BlobArray<AccelZone> accelZones;
        public float attenuationRatio;
        public float vNaught;
        public ScaleType scaleType;
        public DirectionType directionType;
    }

    [SeeAlso(typeof(AccelBlobAsset), typeof(Accel))]
    public struct AccelComponentData : IComponentData {
        public BlobAssetReference<AccelBlobAsset> accelAssetReference;
        public Vector2 normalizedDirection;
    }
}
