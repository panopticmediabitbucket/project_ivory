﻿using Assets.Code.System.Utilities;
using System;
using System.Collections.Generic;
using Assets.Code.GameCode.System.Schemata;
using Assets.Code.System.TimeUtil;
using UnityEngine;
using Assets.Code.Physics.Utilities;
using Unity.Entities;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.GameCode.System.TimeUtil;
using Assets.Code.System.ECS;

namespace Assets.Code.Physics.Accelerators {
    public abstract class Accel<TEnum> : Accel where TEnum : Enum {
        public Accel(BlobAssetReference<AccelBlobAsset> accelAssetReference) : base(accelAssetReference) { }
        public new TEnum state { get => (TEnum)base.state; }
    }

    public abstract class Accel: IEnlistEntity {

        private readonly static EntityArchetype ACCEL_ARCHETYPE;
        static Accel() {
            EntityManager em = World.DefaultGameObjectInjectionWorld.EntityManager;
            ACCEL_ARCHETYPE = em.CreateArchetype(typeof(AccelComponentData), typeof(TimerComponentData));
        }

        /*
         * Immutable instance data
         */
        protected Enum _state;
        private readonly DirectionType _directionType;
        protected FrictionResponseType _responseType;
        private readonly float _attenuationRatio;
        protected BlobAssetReference<AccelBlobAsset> _accelAssetReference;

        /*
         * Mutable instance data
         */
        private Vector2 _initialVelocity = Vector2.zero;
        private Vector2 _normalizedDirection;
        private Vector2 _rotatedDirection;
        protected Accel _normalAccel;
        public bool flaggedForInterrupt { get; protected set; }

        /*
         * Cautiously mutable instance data
         */
        protected List<AccelZone> _accelZones = new List<AccelZone>();
        private SharedValue<Vector2> _refVector;
        private ICondition _condition;
        public virtual ITimer timer { get; protected set; }

        /*
         * Accessors
         */
        public FrictionResponseType frictionResponseType => _responseType;
        public Enum state { get => _state; }
        public float deltaTime => timer.deltaTime;
        public float vNaught => _accelAssetReference.Value.vNaught;
        public ScaleType scaleType => _accelAssetReference.Value.scaleType;

        public virtual Vector2 normalizedDirection {
            get => _refVector != null ? _refVector.value : _rotatedDirection != Vector2.zero ? _rotatedDirection : _normalizedDirection;
            protected set => _normalizedDirection = value;
        }

        internal Accel(BlobAssetReference<AccelBlobAsset> accelAssetReference) {
            _accelAssetReference = accelAssetReference;
            _condition = new ConditionAtom(() => true, true);
        }

        /**
         * An override that can be used if it is necessary to control the stopwatch externally. Use with extreme caution.
         */
        public Accel DeliverRuntimeStopWatch(ITimer stopwatch) {
            timer = stopwatch;
            return this;
        }

        public T DeliverRuntimeInterruptCondition<T>(ICondition condition) where T : Accel {
            _condition = condition;
            return (T)this;
        }

        /**
         * An extensible method for initializing an Accel object. 
         */
        public virtual void Initialize(Vector2 initialVelocity, Quaternion initialRotation, Accel normalAccel) {
            flaggedForInterrupt = false;

            _normalAccel = normalAccel;
            _initialVelocity = initialVelocity;

            switch (_directionType) {
                case DirectionType.Absolute:
                    _rotatedDirection = _normalizedDirection;
                    break;
                case DirectionType.Relative:
                    _rotatedDirection = initialRotation * _normalizedDirection;
                    break;
                case DirectionType.AttenuatedRelative:
                    _rotatedDirection = initialRotation * _normalizedDirection;
                    _rotatedDirection = StaticPhysicsCalculations.InterpolateNormalized(_normalizedDirection, _rotatedDirection, _attenuationRatio);
                    break;
                case DirectionType.Reference:
                    break;
            }

            if (timer == null) {
                DeliverRuntimeStopWatch(new GameStopwatch(state.ToString()));
                timer.Start();
                EnlistEntity(World.DefaultGameObjectInjectionWorld.EntityManager);
            } else {
                if (!timer.active) {
                    timer.Restart();
                }
            }
        }

        public virtual void Rotate(Quaternion rotation) {
            if (scaleType != ScaleType.Regular) {
                _rotatedDirection = rotation * _rotatedDirection;
                _initialVelocity = rotation * _initialVelocity;
            }
        }

        public Vector2 SetStartTimeFromMomentumAndGetVoluntaryMomentum(Vector2 momentum, float frictionCoefficient) {
            Vector2 normalizedDirection = this.normalizedDirection;
            float startV = Vector2.Dot(momentum, normalizedDirection);
            float time = 0;
            Vector2 inheritedVoluntaryMomentum = Vector2.zero;

            if (scaleType == ScaleType.Terminal && startV > 0) {
                float velocityAccumulation = _accelAssetReference.Value.vNaught;
                foreach (var zone in _accelZones) {
                    float diffV = (zone.upperBound - zone.lowerBound) * zone.rate;
                    float endV = velocityAccumulation + diffV;

                    if (diffV > 0) { // going uphill
                        if (endV > startV && startV >= velocityAccumulation) {
                            float v = startV - velocityAccumulation;
                            time += v / zone.rate;
                            velocityAccumulation = startV;
                            break;
                        }
                    } else { // going downhill
                        if (endV < startV && startV <= velocityAccumulation) {
                            float v = startV - velocityAccumulation;
                            time += v / zone.rate;
                            velocityAccumulation = startV;
                            break;
                        }
                    }
                    velocityAccumulation = endV;
                    time += zone.upperBound - zone.lowerBound;
                }
                inheritedVoluntaryMomentum = velocityAccumulation * normalizedDirection;
            }

            var timeStretcher = GetFrictionBasedTimeStretcher(frictionCoefficient);
            timer.SetStartTime(time / timeStretcher(1));
            return inheritedVoluntaryMomentum;
        }

        protected Func<float, float> GetFrictionBasedTimeStretcher(float frictionCoefficient) {
            if (scaleType != ScaleType.Stopper && scaleType != ScaleType.FrictionBooster && _normalAccel != null && _responseType == FrictionResponseType.Normal) {
                Vector2 normalDirection = _normalAccel.normalizedDirection;
                float cosTh = Mathf.Abs(Vector2.Dot(normalDirection, Vector2.up));
                frictionCoefficient = frictionCoefficient >= 1 ? 1 : frictionCoefficient;
                float coef = frictionCoefficient * cosTh;
                return (t) => coef * t;
            } else {
                return (t) => (t);
            }
        }

        public void FlagForInterrupt() {
            flaggedForInterrupt = true;
        }

        public bool UpdateAndCheckInterruptConditions() {
            return _condition.Met();
        }

        // Returns the VDeltas that the motion machine uses to calculate changes in position
        // If this IEnumerable is empty, then the accel is finished and will be removed from the active accels
        public abstract IEnumerable<VDelta> GetVDeltas(float frictionCoefficient);
        public virtual void Reset() {
            _normalAccel = null;
            _condition.Reset();
            if (!(timer is PreviewTimer)) {
                timer.Reset();
            }
        }

        public static implicit operator bool(Accel a) {
            return a != null;
        }

        // IMPLEMENTATION OF 'IEnlistEntity' INTERFACE
        public Entity EnlistEntity(EntityManager em) {
            Entity entity = em.CreateEntity(ACCEL_ARCHETYPE);
            em.SetComponentData(entity, new AccelComponentData() {
                accelAssetReference = _accelAssetReference,
                normalizedDirection = _normalizedDirection
            });
            timer.AugmentEntity(em, entity);
            return entity;
        }

        public void DelistEntity(EntityManager em, Entity entity) {
            em.DestroyEntity(entity);
        }
    }
}

