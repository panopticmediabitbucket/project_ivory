﻿using System;
using Unity.Entities;

namespace Assets.Code.Physics.Accelerators {
    public enum External {
        Gravity, Normal, Balance, StaticFriction, DynamicFriction, NonSentientObject, Ground, Stop
    }

    public struct AccelZone: IComponentData {
        public int position;
        public float rate;
        public bool final;
        public float lowerBound, upperBound;

        public AccelZone(int position, float rate, bool final, float lowerBound, float upperBound) {
            this.position = position;
            this.rate = rate;
            this.final = final;
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
        }

        public AccelZone Dup() {
            return new AccelZone(this.position, this.rate, this.final, this.lowerBound, this.upperBound);
        }
    }

    public struct VDelta {
        public float delta; // change in velocity
        public float period; // time period

        public VDelta(float delta, float period) {
            this.delta = delta;
            this.period = period;
        }
    }

    public enum AccelType {
        Animated, Constant, Reference, AnimatedReference, Custom
    }

    public enum ScaleType {
        Regular, // 1 * yVal
        FrictionBooster,
        Stopper,
        Terminal // Terminal velocities indicate that the accel could be part-way toward approaching a certain Velocity 
    }

    public enum FrictionResponseType {
        None,
        Normal, // time-scaled according to the magnitude of the normal force
        Absolute, // time-scaled based on the friction coefficients irrespective of normal force
    }

    public enum DirectionType {
        Relative, // The direction of motion is relative to the direction the gameobject is facing
        AttenuatedRelative, // The direction of motion is relative to the gameobject, but also partially in an absolute direction
        Absolute,
        Reference
    }
}
