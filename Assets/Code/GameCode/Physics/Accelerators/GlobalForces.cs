﻿using System.Collections.Generic;
using Assets.Code.GameCode.Physics.Collision;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Collision;
using Assets.Code.System.TimeUtil;
using Assets.Code.System.Utilities;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Accelerators {
    public class GlobalPhysicsVariables {
        public static readonly float SURFACECORRECTION = .08f;
        public static readonly float DEFAULTFRICTIONCOEFFICIENT = 2;
        public readonly ConstantAccel<External> gravity;
        public static readonly Vector2 gravityDirection = Vector2.down;

        public GlobalPhysicsVariables(float g) {

            Key[] keys = new Key[2];
            keys[0] = new Key(0, 0);
            keys[1] = new Key(1000, g);

            gravity = new AccelBuilder<External>().SetState(External.Gravity).SetAccelType(AccelType.Constant).SetDirection(gravityDirection).BuildAccelZones(keys, ScaleType.Regular)
                .Build() as ConstantAccel<External>;

            gravity.DeliverRuntimeStopWatch(new GameStopwatch("Gravity"));
            gravity.timer.Start();
        }

        public static VectorRange GetRangeFromModeAndDirection(WorldCollisionFlags mode, bool faceRight) {
            return mode == WorldCollisionFlags.Ground ? GlobalPhysicsVariables.surfaceRange :
                mode == WorldCollisionFlags.Ceiling ? GlobalPhysicsVariables.ceilingRange :
                (mode == WorldCollisionFlags.Wall && faceRight) || (mode == WorldCollisionFlags.BackWall && !faceRight) ? GlobalPhysicsVariables.rightRange :
                GlobalPhysicsVariables.leftRange;
        }

        public static readonly VectorRange surfaceRange = new VectorRange(Quaternion.AngleAxis(-30, Vector3.forward) * Vector3.up, Quaternion.AngleAxis(30, Vector3.forward) * Vector3.up , "surface");
        public static readonly VectorRange ceilingRange = new VectorRange(Quaternion.AngleAxis(150, Vector3.forward) * Vector3.up, Quaternion.AngleAxis(210, Vector3.forward) * Vector3.up, "ceiling");
        public static readonly VectorRange rightRange = new VectorRange(Quaternion.AngleAxis(150, Vector3.forward) * Vector3.right, Quaternion.AngleAxis(210, Vector3.forward) * Vector3.right, "right");
        public static readonly VectorRange leftRange = new VectorRange(Quaternion.AngleAxis(-30, Vector3.forward) * Vector3.right, Quaternion.AngleAxis(30, Vector3.forward) * Vector3.right, "left" );
    }
}
