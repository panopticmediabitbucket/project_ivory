﻿using System;
using System.Collections.Generic;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Interfaces;
using Unity.Entities;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Accelerators {
    public class StopperAccel<TEnum> : AnimatedAccel<TEnum> where TEnum : Enum {
        private IPhysicsStateMachine _motionMachine;
        protected float resistanceCoefficient { get; private set; }

        public StopperAccel(TEnum state, List<AccelZone> accelZones, string trigger, IPhysicsStateMachine motionMachine, BlobAssetReference<AccelBlobAsset> accelAssetReference)
            : base(state, Vector2.zero, accelZones, trigger, accelAssetReference) {
            _motionMachine = motionMachine;
        }

        public override Vector2 normalizedDirection { get => (-_motionMachine.momentum).normalized; }

        public override void Initialize(Vector2 initialVelocity, Quaternion initialRotation, Accel normalAccel) {
            resistanceCoefficient = _accelAssetReference.Value.vNaught;
            base.Initialize(initialVelocity, initialRotation, normalAccel);
        }

        public override IEnumerable<VDelta> GetVDeltas(float frictionCoefficient) {
            float delta = (resistanceCoefficient * deltaTime * _motionMachine.momentum).magnitude;
            yield return new VDelta(delta, deltaTime);
            UpdateResistanceCoefficient(frictionCoefficient);
        }

        private void UpdateResistanceCoefficient(float frictionCoefficient) {
            var changeInRes = base.GetVDeltas(frictionCoefficient);
            foreach (var delta in changeInRes) {
                resistanceCoefficient += delta.delta;
            }
        }
    }
}
