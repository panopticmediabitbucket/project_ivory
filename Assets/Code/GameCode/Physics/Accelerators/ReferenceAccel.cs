﻿using System;
using System.Collections.Generic;
using Assets.Code.System.TimeUtil;
using UnityEngine;

namespace Assets.Code.Physics.Accelerators {
    /**
    public class ReferenceAccel<TEnum> : Accel<TEnum> where TEnum : Enum {
        public override ITimer timer { get => _referenceAccel.timer; }
        public override Vector2 normalizedDirection { get => _referenceAccel.normalizedDirection; }

        private readonly Accel _referenceAccel;
        public ReferenceAccel(Accel reference) : base(reference.vNaught, ScaleType.Regular, DirectionType.Reference, 1) {
            _referenceAccel = reference;
        }

        public override IEnumerable<VDelta> GetVDeltas(float frictionCoefficient) {
            return _referenceAccel.GetVDeltas(frictionCoefficient);
        }
    }
    */
}
