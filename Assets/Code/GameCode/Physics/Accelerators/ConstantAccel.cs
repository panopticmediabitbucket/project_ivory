﻿using Assets.Code.GameCode.Physics.Accelerators;
using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Assets.Code.Physics.Accelerators {
    [Serializable]
    public class ConstantAccel<TEnum> : Accel<TEnum> where TEnum : Enum {
        public ConstantAccel(TEnum state, Vector2 direction, List<AccelZone> accelZones, BlobAssetReference<AccelBlobAsset> accelAssetReference) : base(accelAssetReference) {
            normalizedDirection = direction.normalized;
            _accelZones = accelZones;
            _state = state;
        }

        public override IEnumerable<VDelta> GetVDeltas(float frictionCoefficient) {
            float delta = timer.deltaTime;
            yield return new VDelta(delta * _accelZones[0].rate, delta);
        }
    }
}
