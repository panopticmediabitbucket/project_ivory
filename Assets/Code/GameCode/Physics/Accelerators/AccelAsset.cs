﻿using System;
using Assets.Code.GameCode.System.Schemata;
using UnityEngine;

namespace Assets.Code.Physics.Accelerators {
    [Serializable]
    public class AccelAsset {
        public Key[] keys;
        public string name; // corresponds to the enumerated state. 
        public float xDirection;
        public float yDirection;

        public string pairedAnimation;
        public string animationTrigger;
        public string scaleType;
        public string frictionResponse;
        public string directionType;
        public float directionAttenuation; // Range 0 - 100. 0 is Absolute; 100 is Relative
        public float interruptTime;

        public ConditionSchema interruptConditions;

        public Vector2 Direction() {
            return new Vector2(xDirection, yDirection).normalized;
        }
    }

    [Serializable]
    public class Key {
        public int position; // order in the keys
        public float time; // time code for key
        public float v; // velocity value of the key

        public float percentage => v / 20;

        public Key() {
        }

        public Key(float time, float v) {
            this.time = time;
            this.v = v;
        }
    }
}
