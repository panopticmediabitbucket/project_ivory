﻿using Assets.Code.GameCode.Physics.Accelerators;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;
using UnityEngine;

namespace Assets.Code.Physics.Accelerators {
    [Serializable]
    public class AnimatedAccel<TEnum> : Accel<TEnum>, IAnimationTrigger where TEnum : Enum {
        public readonly DirectionType dirType;
        private float _interruptableTime;
        public float interruptableTime => _interruptableTime;
        private bool _setToInterrupt;
        public string animationTrigger { get; protected set; }

        public AnimatedAccel(TEnum state, Vector2 direction, List<AccelZone> accelZones, string animationTrigger, BlobAssetReference<AccelBlobAsset> accelAssetReference,
            FrictionResponseType responseType = FrictionResponseType.None, float interruptableTime = 0)
            : base(accelAssetReference) {
            _state = state;
            _responseType = responseType;

            normalizedDirection = direction.normalized;
            _accelZones.AddRange(accelZones);
            this.animationTrigger = animationTrigger;
            _interruptableTime = 0;
        }

        public override IEnumerable<VDelta> GetVDeltas(float frictionCoefficient) {
            Func<float, float> timeStretcher = GetFrictionBasedTimeStretcher(frictionCoefficient);
            float startTime = timeStretcher(timer.elapsedSeconds - timer.deltaTime);
            startTime = startTime < 0 ? 0 : startTime;
            float endTime = timeStretcher(timer.elapsedSeconds);
            float delta = timeStretcher(timer.deltaTime);

            foreach (var zone in _accelZones) {
                if (zone.lowerBound <= startTime && startTime < zone.upperBound) {
                    if (endTime <= zone.upperBound) {
                        yield return new VDelta(delta * zone.rate, delta);
                        break;
                    } else {
                        float period = zone.upperBound - startTime;
                        yield return new VDelta(period * zone.rate, period);
                        delta -= period;
                        startTime += period;
                    }
                }
            }
        }

        public sealed override void Reset() {
            _setToInterrupt = false;
            base.Reset();
        }
    }

    public interface IAnimationTrigger {
        string animationTrigger { get; }
    }
}

