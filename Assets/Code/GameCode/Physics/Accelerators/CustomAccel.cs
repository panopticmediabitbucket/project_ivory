﻿using System;
using System.Collections.Generic;
using Assets.Code.Physics.Accelerators;
using Unity.Entities;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Accelerators {
    public interface IGetVDeltasStrategy {
        IEnumerable<VDelta> GetVDeltas(float frictionCoefficient);
    }

    public interface IGetDirectionStrategy {
        Vector2 GetNormalizedDirection();
    }

    public interface IDirectionVDeltasCompositeStrategy : IGetVDeltasStrategy, IGetDirectionStrategy { }

    public class CustomAccel<TEnum> : Accel<TEnum> where TEnum : Enum {
        private readonly IGetVDeltasStrategy _vDeltasStrategy;
        private readonly IGetDirectionStrategy _directionStrategy;

        public override Vector2 normalizedDirection { get => _directionStrategy.GetNormalizedDirection(); }

        public CustomAccel(IGetVDeltasStrategy vDeltasStrategy, IGetDirectionStrategy directionStrategy, TEnum state, BlobAssetReference<AccelBlobAsset> accelAssetReference) 
            : base(accelAssetReference) {
            _accelZones.Add(new AccelZone(0, 0, false, 0, 1000f));
            _vDeltasStrategy = vDeltasStrategy;
            _directionStrategy = directionStrategy;
            _state = state;
        }

        public override IEnumerable<VDelta> GetVDeltas(float frictionCoefficient) {
            return _vDeltasStrategy.GetVDeltas(frictionCoefficient);
        }
    }
}
