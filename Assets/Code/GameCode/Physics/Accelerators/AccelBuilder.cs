﻿using System;
using System.Collections.Generic;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.Physics.Interfaces;
using JetBrains.Annotations;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Assets.Code.Physics.Accelerators {
    /*
     * Builder that can be used to construct Accels of different types. Can also be used in conjunction with an
     * AccelAsset to construct animated accels
     */
    public class AccelBuilder<TEnum> where TEnum : Enum {
        private readonly BlobBuilder _blobBuilder;
        private AccelBlobAsset _accelBlobAsset;
        public AccelBuilder() {
            _blobBuilder = new BlobBuilder(Allocator.Temp);
            ref AccelBlobAsset accelBlobAsset = ref _blobBuilder.ConstructRoot<AccelBlobAsset>();
        }

        public AccelBuilder(AccelAsset model, IPhysicsStateMachine motionMachine) {
            _blobBuilder = new BlobBuilder(Allocator.Temp);
            BuildAccelZones(model.keys, (ScaleType)Enum.Parse(typeof(ScaleType), model.scaleType))
                .SetAnimationTrigger(model.animationTrigger).SetDirection(model.Direction()).SetState(model.name)
                .SetFrictionResponseType((FrictionResponseType)Enum.Parse(typeof(FrictionResponseType), model.frictionResponse))
                .SetMotionMachine(motionMachine).SetAttenuationRatio(model.directionAttenuation).SetDirectionType(model.directionType)
                .SetInterruptableTime(model.interruptTime).SetAccelType(AccelType.Animated);
        }

        private IPhysicsStateMachine _motionMachine;
        public AccelBuilder<TEnum> SetMotionMachine(IPhysicsStateMachine motionMachine) {
            _motionMachine = motionMachine;
            return this;
        }

        List<AccelZone> _azList;
        float _vNaught;
        ScaleType _scaleType = ScaleType.Regular;
        public AccelBuilder<TEnum> BuildAccelZones(Key[] keys, ScaleType type) {
            _vNaught = keys[0].v;

            _scaleType = type;
            _azList = new List<AccelZone>();
            Key prevKey = keys[0];
            for (int i = 0; i < keys.Length - 1; i++) {
                Key currentKey = keys[i + 1];
                float timeDifference = (currentKey.time - prevKey.time) / 1000;
                float rate;
                switch (_scaleType) {
                    default:
                        rate = (currentKey.v - prevKey.v) / timeDifference;
                        break;
                    case ScaleType.Stopper:
                        rate = (currentKey.percentage - prevKey.percentage) / timeDifference;
                        break;
                }

                bool final = i == keys.Length - 1;
                _azList.Add(new AccelZone(i, rate, final, prevKey.time / 1000, currentKey.time / 1000));
                prevKey = currentKey;
            }
            return this;
        }

        string _animTrigger;
        public AccelBuilder<TEnum> SetAnimationTrigger(string trigger) {
            _animTrigger = trigger;
            return this;
        }

        TEnum _state;
        public AccelBuilder<TEnum> SetState(string stateName) {
            _state = (TEnum)Enum.Parse(typeof(TEnum), stateName);
            return this;
        }

        public AccelBuilder<TEnum> SetState(TEnum state) {
            _state = state;
            return this;
        }

        private DirectionType _directionType;
        public AccelBuilder<TEnum> SetDirectionType(string directionType) {
            _directionType = (DirectionType)Enum.Parse(typeof(DirectionType), directionType);
            return this;
        }

        Vector2 _direction;
        public AccelBuilder<TEnum> SetDirection(Vector2 direction) {
            _direction = direction;
            return this;
        }

        FrictionResponseType _responseType = FrictionResponseType.None;

        public AccelBuilder<TEnum> SetFrictionResponseType(FrictionResponseType type) {
            _responseType = type;
            return this;
        }

        private float _attenuationRatio;

        public AccelBuilder<TEnum> SetAttenuationRatio(float ratio) {
            _attenuationRatio = ratio;
            return this;
        }

        private float _interruptableTime;
        public AccelBuilder<TEnum> SetInterruptableTime(float time) {
            _interruptableTime = time;
            return this;
        }

        private AccelType _accelType = AccelType.Constant;
        public AccelBuilder<TEnum> SetAccelType(AccelType type) {
            _accelType = type;
            return this;
        }

        private IGetVDeltasStrategy _vDeltasStrategy;
        public AccelBuilder<TEnum> SetVDeltasStrategy([NotNull]IGetVDeltasStrategy vDeltasStrategy) {
            _vDeltasStrategy = vDeltasStrategy;
            return this;
        }

        private IGetDirectionStrategy _directionStrategy;
        public AccelBuilder<TEnum> SetDirectionStrategy([NotNull]IGetDirectionStrategy directionStrategy) {
            _directionStrategy = directionStrategy;
            return this;
        }

        public AccelBuilder<TEnum> SetCompositeStrategy([NotNull]IDirectionVDeltasCompositeStrategy compositeStrategy) {
            _vDeltasStrategy = compositeStrategy;
            _directionStrategy = compositeStrategy;
            return this;
        }

        /**
         * Build call finalizes the AccelBlobAsset and creates the Singleton instance for the specific Accel.
         * The Accel will contain the BlobAssetReference, which can be used to refer to important accel data later on. 
         */
        public Accel<TEnum> Build() {

            ref AccelBlobAsset accelBlob = ref _blobBuilder.ConstructRoot<AccelBlobAsset>();
            CopyDataToBlob(ref accelBlob);
            BlobAssetReference<AccelBlobAsset> assetReference = _blobBuilder.CreateBlobAssetReference<AccelBlobAsset>(Allocator.Persistent);

            // Disposing of the blobbuilder
            _blobBuilder.Dispose();

            switch (_accelType) {
                case AccelType.Animated:
                case AccelType.AnimatedReference:
                    if (_scaleType != ScaleType.Stopper) {
                        return new AnimatedAccel<TEnum>(_state, _direction, _azList, _animTrigger, assetReference, _responseType, _interruptableTime);
                    } else {
                        return new StopperAccel<TEnum>(_state, _azList, _animTrigger, _motionMachine, assetReference);
                    }
                case AccelType.Custom:
                    return new CustomAccel<TEnum>(_vDeltasStrategy, _directionStrategy, _state, assetReference);
                case AccelType.Constant:
                    return new ConstantAccel<TEnum>(_state, _direction, _azList, assetReference);
            }
            throw new IncompleteBuildException("No matching routine for accel type: " + _accelType);
        }

        private void CopyDataToBlob(ref AccelBlobAsset accelBlob) {
            if (_azList != null && _azList.Count > 0) {
                BlobBuilderArray<AccelZone> accelZones = _blobBuilder.Allocate(ref accelBlob.accelZones, _azList.Count);
                int i = 0;
                foreach (var zone in _azList) {
                    accelZones[i++] = zone;
                }
            }
            accelBlob.vNaught = _vNaught;
            accelBlob.attenuationRatio = _attenuationRatio;
        }
    }

    public class IncompleteBuildException : Exception {
        public IncompleteBuildException(string message) : base(message) {
        }

        public IncompleteBuildException(string message, Exception innerException) : base(message, innerException) {
        }

        public IncompleteBuildException() {
        }
    }
}
