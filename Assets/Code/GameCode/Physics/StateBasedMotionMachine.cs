﻿using Assets.Code.Physics.Accelerators;
using System;
using System.Linq;
using System.Collections.Generic;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.GameCode.Physics.Surfaces;
using UnityEngine;
using Assets.Code.Physics.Interfaces;
using Assets.Code.Physics.Utilities;
using Assets.Code.System.TimeUtil;
using Assets.Code.GameCode.Meta.Documentation;

namespace Assets.Code.Physics {
    /*
     * StateBasedMotionMachine is responsible for calculating position change based on object momentum
     * and the active forces on a given object. 
     * 
     * It does absolutely nothing with that information, collision detection is handled elsewhere
     */
     [SeeAlso(typeof(PhysicsStateMachineWorldObject<>), typeof(IPhysicsStateMachine))]
    public class StateBasedMotionMachine<TEnum> : IStateBasedMotionMachine<TEnum> where TEnum : Enum {
        public Animator animator { get; }
        public GameObject gameObject { get; }
        public Vector2 momentum { get; private set; }
        public Vector2 voluntaryMomentum { get; private set; }
        public Vector2 involuntaryMomentum { get; private set; }

        // Accel lists
        private List<Accel<TEnum>> _activeInternalAccels = new List<Accel<TEnum>>();
        private List<Accel<External>> _activeExternalAccels = new List<Accel<External>>();
        private List<Accel> _activeFrictionBoosters = new List<Accel>();

        // Friction private variables 
        private float _frictionCoefficient = GlobalPhysicsVariables.DEFAULTFRICTIONCOEFFICIENT;
        private float _frictionBoostPercentage = 0;
        protected float frictionCoefficient => _frictionCoefficient * (1 + _frictionBoostPercentage);

        protected Quaternion objRotation { get => gameObject.transform.rotation; set => gameObject.transform.rotation = value; }

        public ITimeZone timeZone { get; set; }

        public StateBasedMotionMachine(GameObject gameObject) {
            this.gameObject = gameObject;
            animator = gameObject.GetComponent<Animator>();
            momentum = Vector2.zero;
            timeZone = ClockManager.globalTimeZone;

            Accel<External> dynamicFriction = new AccelBuilder<External>().SetAccelType(AccelType.Custom)
                .SetState(External.DynamicFriction).SetCompositeStrategy(new DynamicFrictionCompositeStrategy(this)).Build(); 

            DeliverExternalAccel(dynamicFriction);
        }

        /**
         * Use with caution. Internal momentum generally should be calculated by the motion machine.
         */
        public void SetInternalMomentum(float x, float y) {
            voluntaryMomentum = new Vector2(x, y);
        }

        public void RotateInternals(Quaternion rotation) {
            voluntaryMomentum = rotation * voluntaryMomentum;
            foreach (var accel in _activeInternalAccels) {
                accel.Rotate(rotation);
            }
        }

        /* Interrupt function for active accels and friction boosters */
        public void FlagInternalForInterrupt(TEnum state) {
            Accel<TEnum> animatedAccel = _activeInternalAccels.Find(x => x.state.Equals(state));
            if (animatedAccel) {
                animatedAccel.FlagForInterrupt();
            } else {
                Accel<TEnum> frictionBooster = (Accel<TEnum>)_activeFrictionBoosters.Find(x => x.GetType().BaseType == typeof(Accel<TEnum>) && (x.state.ToString()).Equals(state.ToString()));
                if (frictionBooster) {
                    frictionBooster.FlagForInterrupt();
                }
            }
        }

        private void InterruptInList<T>(List<T> accelList, T accel) where T : Accel {
            if (accel != null) {
                accel.Reset();
                accelList.Remove(accel);
            }
        }

        private void InterruptInternal(Accel<TEnum> accel) => InterruptInList(_activeInternalAccels, accel);
        private void InterruptFriction(Accel frictionBooster) => InterruptInList(_activeFrictionBoosters, frictionBooster);
        public void InterruptExternal(Accel<External> ex) => InterruptInList(_activeExternalAccels, ex);

        public void InterruptExternal(External external) {
            InterruptExternal(_activeExternalAccels.Find(x => x.state.Equals(external)));
        }

        private Vector2 RegisterNormalForce(Vector2 surfaceNormal) {
            float cosTh = Vector2.Dot(Vector2.up, surfaceNormal);
            cosTh = cosTh < 0 ? -cosTh : cosTh;
            Vector2 normalForce = GameManager.GetInstance().GRAVITY * cosTh * surfaceNormal;

            Key[] keys = new Key[2];
            keys[0] = new Key(0, 0);
            keys[1] = new Key(1000, normalForce.magnitude);

            AccelBuilder<External> accelBuilder = new AccelBuilder<External>().SetAccelType(AccelType.Constant).SetState(External.Normal)
                .SetDirection(normalForce.normalized).BuildAccelZones(keys, ScaleType.Regular);

            DeliverExternalAccel(accelBuilder.Build() as ConstantAccel<External>);
            return normalForce;
        }

        public void RegisterGroundForces(Vector2 surfaceNormal, ISurface surface) {
            surface = surface ?? DefaultSurface.defaultSurface;

            Vector2 projectedInvoluntaryMomentum = StaticPhysicsCalculations.Project(involuntaryMomentum, StaticPhysicsCalculations.LocalRight(gameObject.transform));
            involuntaryMomentum = projectedInvoluntaryMomentum;

            if(_frictionCoefficient != surface.coefficientOfFriction || !IsActive(External.Normal)) {
                if (IsActive(External.Normal)) {
                    RemoveSurfaceForces();
                }

                _frictionCoefficient = surface.coefficientOfFriction;
                Vector2 normalForce = RegisterNormalForce(surfaceNormal);
                Vector2 balanceVec = (GameManager.GetInstance().GRAVITY * Vector2.up) - normalForce;
                Key[] keys = new Key[2];
                keys[0] = new Key(0, 0);
                keys[1] = new Key(balanceVec.magnitude, 1000);
                AccelBuilder<External> builder = new AccelBuilder<External>().SetState(External.Balance).SetAccelType(AccelType.Constant).SetDirection(balanceVec.normalized).BuildAccelZones(keys, ScaleType.Regular);

                DeliverExternalAccel(builder.Build() as ConstantAccel<External>);
            }
        }

        public void RegisterWallForces(Vector2 wallNormal, ISurface surface) {
            Vector2 normalForce = RegisterNormalForce(wallNormal);

            Vector2 projectedExternalMomentum = StaticPhysicsCalculations.Project(involuntaryMomentum, StaticPhysicsCalculations.LocalUp(gameObject.transform));
            involuntaryMomentum = projectedExternalMomentum;
        }

        public void RemoveSurfaceForces() {
            InterruptExternal(_activeExternalAccels.Find(x => x.state.Equals(External.Normal)));
            InterruptExternal(_activeExternalAccels.Find(x => x.state.Equals(External.Balance)));
        }

        public void RemoveWallForces() {

        }

        public bool IsActive(TEnum state) {
            bool activeInternal = _activeInternalAccels.Find(x => x.state.Equals(state));
            bool activeFriction = _activeFrictionBoosters.Find(x => x.GetType().BaseType == typeof(Accel<TEnum>) && ((TEnum)x.state).Equals(state));
            return activeInternal || activeFriction;
        }

        public bool IsActive(External external) {
            bool activeExternal = _activeExternalAccels.Find(x => x.state.Equals(external));
            return activeExternal;
        }

        // clamp squishes any transient forces caused by floating point arithmetic
        private Vector2 Clamp(Vector2 clamp) {
            if (clamp.x <= .00001f && clamp.x >= -.00001f) {
                clamp.x = 0;
            }

            if (clamp.y <= .00001f && clamp.y >= -.00001f) {
                clamp.y = 0;
            }
            return clamp;
        }

        public void ConsolidateMomentum() {
            involuntaryMomentum += voluntaryMomentum;
            voluntaryMomentum = Vector2.zero;
        }

        /**
         * The primary call made each frame. Calculates change and position and deals with whichever removals need to be processed. 
         * Momentum is also updated. Collision is handled in the calling layer. 
         */
        public Vector2 CalculateFrameMotionAndUpdateMomentum() {
            momentum = voluntaryMomentum + involuntaryMomentum;
            Vector2 changeInPosition = momentum * timeZone.deltaTime;

            voluntaryMomentum += ProcessActiveAccels(_activeInternalAccels, ref changeInPosition, InterruptInternal);
            involuntaryMomentum += ProcessActiveAccels(_activeExternalAccels, ref changeInPosition, InterruptExternal);

            _frictionBoostPercentage = CalculateFrictionBoostChange();

            changeInPosition = Clamp(changeInPosition);
            voluntaryMomentum = Clamp(voluntaryMomentum);
            involuntaryMomentum = Clamp(involuntaryMomentum);

            return changeInPosition;
            //UnityEngine.Debug.Log("External Momentum:" + externalMomentum.x.ToString() + ", " + externalMomentum.y.ToString());
        }

        /**
         * Processes all active accels in a given list, updating the positional change for the current frame and returning 
         * the change in momentum from all the accels in that list. 
         */
        private Vector2 ProcessActiveAccels<T>(List<T> accelList, ref Vector2 changeInPosition, Action<T> removalAction) where T : Accel {
            Vector2 changeInMomentum = Vector2.zero;
            List<T> removals = null;

            foreach (var accel in accelList) {
                bool interrupt = accel.UpdateAndCheckInterruptConditions() && accel.flaggedForInterrupt;
                var vDeltas = accel.GetVDeltas(frictionCoefficient);

                if (interrupt || vDeltas.Count() == 0) {
                    removals = removals ?? new List<T>();
                    removals.Add(accel);
                    if (interrupt)
                        continue;
                }

                var exitMomentumChangeInMagnitude = GetChangeFromVDeltas(accel, vDeltas, out var translationMagnitudeInDirection);
                changeInMomentum += accel.normalizedDirection * exitMomentumChangeInMagnitude;
                changeInPosition += accel.normalizedDirection * translationMagnitudeInDirection;
            }

            if (removals != null) {
                removals.ForEach(accel => { removalAction(accel); });
            }
            return changeInMomentum;
        }

        /**
         * Similar to the previous method, but it's simplified for friction boosters
         */
        private float CalculateFrictionBoostChange() {
            float frictionalChange = 0;
            List<Accel> removals = null;
            foreach (var accel in _activeFrictionBoosters) {
                bool interrupt = accel.UpdateAndCheckInterruptConditions() && accel.flaggedForInterrupt;
                var vDeltas = accel.GetVDeltas(frictionCoefficient);

                if (interrupt || vDeltas.Count() == 0) {
                    removals = removals ?? new List<Accel>();
                    removals.Add(accel);
                    if (interrupt)
                        continue;
                }

                frictionalChange += GetChangeFromVDeltas(accel, vDeltas, out float positionalChange);
            }

            if (removals != null) {
                removals.ForEach(x => InterruptFriction(x));
            }
            return frictionalChange;
        }

        private static float GetChangeFromVDeltas(Accel accel, IEnumerable<VDelta> vDeltas, out float positionChangeMagnitude) {
            float changeInMagnitude = 0;
            positionChangeMagnitude = 0;
            float remainingTime = accel.deltaTime;
            foreach (var vDelta in vDeltas) {
                remainingTime -= vDelta.period;

                // calculating the triangle representing positional increase/decrease in this zone
                positionChangeMagnitude += vDelta.period * vDelta.delta / 2;

                // calculating the rectangle of additional displacement caused by the change in 'velocity'
                if (remainingTime > 0) {
                    positionChangeMagnitude += remainingTime * vDelta.delta;
                }

                changeInMagnitude += vDelta.delta;
            }

            return changeInMagnitude;
        }

        /**
         * Handles all necessary internal mutations for the introduction of a new state. 
         */
        public void DeliverInternalAccel(Accel<TEnum> accel) {
            if (accel != null && !IsActive(accel.state)) {
                switch (accel) {
                    case IAnimationTrigger trigger when animator != null:
                        if (!string.IsNullOrEmpty(((IAnimationTrigger)accel).animationTrigger) && !((IAnimationTrigger)accel).animationTrigger.Equals("None")) {
                            animator.SetTrigger(((AnimatedAccel<TEnum>)accel).animationTrigger);
                        }
                        break;
                }

                accel.Initialize(voluntaryMomentum, objRotation, _activeExternalAccels.Find(x => x.state.Equals(External.Normal)));


                switch (accel.scaleType) {
                    case ScaleType.Regular:
                        voluntaryMomentum += accel.vNaught * accel.normalizedDirection;
                        _activeInternalAccels.Add(accel);
                        break;
                    case ScaleType.Stopper:
                        _activeInternalAccels.Add(accel);
                        break;
                    case ScaleType.Terminal:
                        Vector2 newVoluntaryMomentum = accel.SetStartTimeFromMomentumAndGetVoluntaryMomentum(momentum, frictionCoefficient);
                        voluntaryMomentum += newVoluntaryMomentum;
                        involuntaryMomentum -= newVoluntaryMomentum;
                        _activeInternalAccels.Add(accel);
                        break;
                    case ScaleType.FrictionBooster:
                        _frictionBoostPercentage = 0;
                        ConsolidateMomentum();
                        _activeFrictionBoosters.Add(accel);
                        break;
                }
            }
        }

        public void DeliverExternalAccel(Accel<External> accel) {
            var found = _activeExternalAccels.Find(x => x.state.Equals(accel.state));
            if (found != null) {
                _activeExternalAccels.Remove(found);
            }
            accel.Initialize(involuntaryMomentum, gameObject.transform.rotation, null);
            _activeExternalAccels.Add(accel);
        }

        public void ResetMomentum() {
            voluntaryMomentum = Vector2.zero;
            involuntaryMomentum = Vector2.zero;
        }

        // custom accel functions

        private class DynamicFrictionCompositeStrategy : IDirectionVDeltasCompositeStrategy {
            StateBasedMotionMachine<TEnum> motionMachine;
            public DynamicFrictionCompositeStrategy(StateBasedMotionMachine<TEnum> motionMachine) {
                this.motionMachine = motionMachine;
            }

            public IEnumerable<VDelta> GetVDeltas(float frictionCoefficient) {
                var normalAccel = motionMachine._activeExternalAccels.Find(x => x.state.Equals(External.Normal));
                if (normalAccel && motionMachine.involuntaryMomentum != Vector2.zero) {
                    var dynamicDirection = GetNormalizedDirection();
                    var normalVDelta = normalAccel.GetVDeltas(motionMachine.frictionCoefficient).First();

                    // coefficient of friction is hardcoded here
                    float frictionDelta = normalVDelta.delta * motionMachine.frictionCoefficient;
                    float momentumMagnitude = motionMachine.involuntaryMomentum.magnitude;
                    Vector2 frictionForce = frictionDelta * dynamicDirection;

                    if (Vector2.Dot(dynamicDirection, motionMachine.involuntaryMomentum) > 0) {
                        frictionForce = -frictionForce;
                        frictionDelta = -frictionDelta;
                        momentumMagnitude = -momentumMagnitude;
                    }

                    if (Vector2.Dot(motionMachine.involuntaryMomentum + frictionForce, motionMachine.involuntaryMomentum) < 0) {
                        VDelta retVal = new VDelta(momentumMagnitude, normalVDelta.period);
                        yield return retVal;
                    } else {
                        VDelta retVal = new VDelta(frictionDelta, normalVDelta.period);
                        yield return new VDelta(frictionDelta, normalVDelta.period);
                    }
                } else {
                    yield return new VDelta(0, 0);
                }
            }

            public Vector2 GetNormalizedDirection() {
                var normalAccel = motionMachine._activeExternalAccels.Find(x => x.state.Equals(External.Normal));
                if (normalAccel) {
                    Vector2 normalForceDirection = normalAccel.normalizedDirection;
                    Vector3 zAxis = normalForceDirection.x >= 0 ? Vector3.back : Vector3.forward;
                    return Vector3.Cross(zAxis, normalForceDirection).normalized;
                }
                return Vector2.zero;
            }
        }

        // wall magnetism custom accel
        private CustomAccel<External> _wallMagnetism;

        private IEnumerable<VDelta> WallMagnetismGetVDeltas() {
            Vector2 wallDirection = WallMagnetismGetDirection();
            if (wallDirection.y > 0) {

            } else {

            }
            yield return new VDelta(0, 0);
        }

        private Vector2 WallMagnetismGetDirection() {
            return StaticPhysicsCalculations.LocalRight(gameObject.transform);
        }
    }
}

