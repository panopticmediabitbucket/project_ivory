﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets.Code.GameCode.Physics;
using Assets.Code.GameCode.Physics.Collision;
using Assets.Code.GameCode.Physics.Interfaces;
using Assets.Code.GameCode.Physics.Rotators;
using Assets.Code.GameCode.Physics.Surfaces;
using Assets.Code.GameCode.System.CodeGeneration;
using Assets.Code.GameCode.System.Schemata;
using UnityEngine;
using Assets.Code.Physics.Collision;
using Assets.Code.Physics;
using Assets.Code.Physics.Interfaces;
using Assets.Code.Physics.Accelerators;
using Assets.Code.Physics.Utilities;
using Assets.Code.System;
using Assets.Code.GameCode.Physics.Collision.Strategies;
using Assets.Code.GameCode.Physics.Surfaces.Strategies;
using Assets.Code.GameCode.Meta.Documentation;
using Assets.Code.GameCode.Physics.Rotors;

public enum ObjectType {
    World, Player, NPC, Enemy
}

[SeeAlso(typeof(ICollisionResolutionStrategy), typeof(ISurfaceResponseStrategy), typeof(IStateBasedMotionMachine<>))]
public abstract class PhysicsStateMachineWorldObject<TEnum> : DualBehaviour, IPhysicsStateMachine, ICollidable where TEnum : Enum {
    protected delegate void StateRoutine();

    [Header("Physics State Machine Settings:")]
    public ObjectType objectType = ObjectType.NPC;

    [Header("Collision System")]
    [SerializeField]
    public CollisionBox collisionSystem = new CollisionBox();
    public CollisionBox GetCollisionSystem() => collisionSystem;

    [HideInInspector]
    public AccelAsset[] accelAssets;
    protected Dictionary<TEnum, Accel<TEnum>> _internalAccels = new Dictionary<TEnum, Accel<TEnum>>();

    // Core state variables
    private Dictionary<TEnum, StateRoutine> _stateRoutines;
    private TEnum _prevState;
    protected TEnum state;
    public TEnum startingState;

    private WorldCollisionFlags _collisionState = WorldCollisionFlags.Ground;

    [NonSerialized]
    private bool _faceRight;

    private static readonly Quaternion ABOUT_Y = new Quaternion(0, 1, 0, 0);
    protected bool faceRight {
        get => _faceRight; set {
            if (_faceRight != value) {
                Vector3 v = (Vector3)collisionSystem.centerPoint - gameObject.transform.position;
                gameObject.transform.rotation *= ABOUT_Y;
                Vector3 w_ = (Vector3)collisionSystem.centerPoint - gameObject.transform.position;
                gameObject.transform.position += (v - w_);
            }
            _faceRight = value;
        }
    }

    /*
     * Key strategies
     */
    private StateBasedMotionMachine<TEnum> _stateBasedMotionMachine;
    private BasicRotationMachine _rotationMachine;
    private ICollisionResolutionStrategy _collisionStrategy;
    private ISurfaceResponseStrategy _surfaceResponseStrategy;

    protected Animator animator { get; private set; }
    public Vector2 voluntaryMomentum => _stateBasedMotionMachine.voluntaryMomentum;
    public Vector2 involuntaryMomentum => _stateBasedMotionMachine.involuntaryMomentum;
    public Vector2 momentum => _stateBasedMotionMachine.momentum;
    public Vector2 changeInPosition { get; private set; }

    /*
     * Internal, generic setup routines
     */
    private Dictionary<TEnum, StateRoutine> BindStateRoutinesToEnumeration() {
        List<MethodInfo> methods = GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToList();
        var enumValues = (TEnum[])Enum.GetValues(typeof(TEnum));
        var stateRoutines = new Dictionary<TEnum, StateRoutine>();

        for (int i = 0; i < enumValues.Length; i++) {
            var method = methods.Find(x => x.Name.Equals(enumValues[i].ToString()));

            if (method == null) {
                UnityEngine.Debug.LogError("No state routine is available to bind to the enumerated state '" + enumValues[i] + ",' please create a method named '" + enumValues[i] + "' as a routine to service the state.");
            } else {
                stateRoutines[enumValues[i]] = () => method.Invoke(this, null);
            }
        }
        return stateRoutines;
    }

    private Dictionary<string, object> GetObjectDictionary() {
        Dictionary<string, object> objDictionary = new Dictionary<string, object>();
        _internalAccels.ToList().ForEach(x => objDictionary.Add(x.Key.ToString(), x.Value));
        objDictionary.Add("Control Manager", ControlManager.GetInstance());
        return objDictionary;
    }

    // Registers functions with the update phases
    protected sealed override void Awake() {
        base.Awake();
    }

    // This can be extended by the child class
    protected virtual void Start() {
        _stateRoutines = BindStateRoutinesToEnumeration();

        _stateBasedMotionMachine = new StateBasedMotionMachine<TEnum>(gameObject);
        collisionSystem.Initialize(gameObject);
        _surfaceResponseStrategy = new GroundWalkerSurfaceResponseStrategy(gameObject, collisionSystem);
        _collisionStrategy = new GroundWalkerCollisionResolutionStrategy(collisionSystem);
        _rotationMachine = new BasicRotationMachine(gameObject, collisionSystem);
        animator = gameObject.GetComponent<Animator>();

        foreach (var asset in accelAssets.ToList()) {
            Accel<TEnum> accel = new AccelBuilder<TEnum>(asset, this).Build();
            _internalAccels.Add(accel.state, accel);
        }

        var objDictionary = GetObjectDictionary();
        ConditionFactory cf = new ConditionFactory(objDictionary);
        IEnumerable<ICondition> conditions = cf.BuildAll(accelAssets.Select(x => x.interruptConditions).ToArray());
        var iterator = conditions.GetEnumerator();

        foreach (var accel in _internalAccels) {
            iterator.MoveNext();
            accel.Value.DeliverRuntimeInterruptCondition<Accel>(iterator.Current);
        }

        _faceRight = true;

        _stateBasedMotionMachine.DeliverExternalAccel(GameManager.GetInstance().globalForces.gravity);
        _stateBasedMotionMachine.DeliverInternalAccel(_internalAccels[startingState]);
        state = startingState;
        _prevState = startingState;
    }

    /*
     * The abstract interface follows. These are protected, meaning they are intended to be used internally only.
     * The base implementation calls these methods alongside the corresponding events, and inheriting classes
     * are expected to implement a routine to handle the corresponding events.
     */
    protected abstract void HitGround();
    protected abstract void OnNoGround();
    protected abstract void HitWall();
    protected abstract void OffWall();
    protected abstract void HitBackWall();

    /*
     * The interface inherited and used by the child follows
     */
    protected bool IsMotionComplete(TEnum state) {
        return !_stateBasedMotionMachine.IsActive(state);
    }

    // This method can be used by the inheriting class in situations where a given state is never 'active,' 
    // But we want to use the state's motion data to move through space
    protected void RegisterStateMotion(TEnum state) {
        _stateBasedMotionMachine.DeliverInternalAccel(_internalAccels[state]);
    }

    protected void RegisterExternalForce(Accel<External> accel) {
        _stateBasedMotionMachine.DeliverExternalAccel(accel);
    }

    protected void InterruptMotion(TEnum state) {
        _stateBasedMotionMachine.FlagInternalForInterrupt(state);
    }

    protected void LeavingSurface() {
        _collisionState = WorldCollisionFlags.None;
        _stateBasedMotionMachine.RemoveSurfaceForces();
    }

    /*
     * The core, abstract routines follow
     * State changes processed in the inheriting class must be examined EVERY time control is passed to the inheriting class
     */
    private void ExamineState() {
        if (!_prevState.Equals(state)) {
            _stateBasedMotionMachine.FlagInternalForInterrupt(_prevState);
            _stateBasedMotionMachine.DeliverInternalAccel(_internalAccels[state]);
            _prevState = state;
        }
    }

    // This is where the state routines of the inheriting class are invoked. The motion machine is also updated.
    protected sealed override void First() {
        _stateRoutines[state]();
        ExamineState();
        changeInPosition = _stateBasedMotionMachine.CalculateFrameMotionAndUpdateMomentum();
    }

    // The 'second' routine is primarily responsible for adjudicating collisions. Agents need to be aware of different
    // types of collisions depending on if they are grounded, airborne, or positioned against a wall, etc. 
    protected sealed override void Second() {
        Vector2 changeInPosition = this.changeInPosition;

        CollisionData collisionData = _collisionStrategy.ResolveCollisions(_collisionState, ref changeInPosition);
        IEnumerable<Rotor> adjustments = _surfaceResponseStrategy.GetRotorsForSurfaceAdjustment(collisionData, changeInPosition, _faceRight);

        if (adjustments != null) {
            foreach(var rotor in adjustments) {
                if (!_rotationMachine.IsActive(rotor.name)) {
                    _rotationMachine.DeliverRotor(rotor);
                }
                if(rotor is InstantRotor) {
                    _stateBasedMotionMachine.RotateInternals(rotor.GetRotation());
                }
            }
        }

        // Processing collision data can yield state changes, so we need to examine the state again after
        ProcessCollisionData(collisionData, ref changeInPosition);
        ExamineState();

        _rotationMachine.ProcessRotations();
        gameObject.transform.Translate(changeInPosition, Space.World);
    }

    private void ProcessCollisionData(CollisionData collisionData, ref Vector2 changeInPosition) {
        Debug.Log(collisionData.collisionType);
        switch (collisionData.collisionType) {
            case WorldCollisionFlags.FollowGround:
                _stateBasedMotionMachine.RegisterGroundForces(StaticPhysicsCalculations.LocalUp(transform), (ISurface)collisionData.collidedObjectData);
                break;
            case WorldCollisionFlags.FollowWall:
                _stateBasedMotionMachine.RegisterWallForces(StaticPhysicsCalculations.LocalRight(transform), (ISurface)collisionData.collidedObjectData);
                break;

            case WorldCollisionFlags.Landing:
                Debug.Log("Landing registered");
                _collisionState ^= WorldCollisionFlags.Ground;
                _stateBasedMotionMachine.ConsolidateMomentum();
                _stateBasedMotionMachine.RegisterGroundForces(StaticPhysicsCalculations.LocalUp(transform), (ISurface)collisionData.collidedObjectData);
                HitGround();
                break;

            case WorldCollisionFlags.LeavingGround:
                _collisionState ^= WorldCollisionFlags.Ground;
                OnNoGround();
                _stateBasedMotionMachine.RemoveSurfaceForces();
                break;

            case WorldCollisionFlags.WallCollision:
                _collisionState ^= WorldCollisionFlags.Wall;
                HitWall();
                WallCollision();
                _stateBasedMotionMachine.RegisterWallForces(StaticPhysicsCalculations.LocalLeft(transform), (ISurface)collisionData.collidedObjectData);
                break;

            case WorldCollisionFlags.LeavingWall:
                _collisionState ^= WorldCollisionFlags.Wall;
                OffWall();
                break;

            case WorldCollisionFlags.BackWall:
                HitBackWall();
                WallCollision();
                break;
        }
    }

    private void WallCollision() {
        Vector2 internalMomentumXObjSpace = StaticPhysicsCalculations.Project(voluntaryMomentum, transform.rotation * Vector2.right);
        Vector2 internalMomentumZeroedX = voluntaryMomentum - internalMomentumXObjSpace;
        _stateBasedMotionMachine.SetInternalMomentum(internalMomentumZeroedX.x, internalMomentumZeroedX.y);
        HitWall();
        ExamineState();
    }
}
