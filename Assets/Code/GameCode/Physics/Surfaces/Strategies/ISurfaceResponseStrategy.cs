﻿using Assets.Code.GameCode.Physics.Collision;
using Assets.Code.GameCode.Physics.Rotators;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Surfaces.Strategies {
    public interface ISurfaceResponseStrategy {
        /*
         * Returns an IEnumerable of rotations that were made to accomdate following a surface
         */
        IEnumerable<Rotor> GetRotorsForSurfaceAdjustment(CollisionData collisionData, Vector2 changeInPosition, bool faceRight);
    }
}
