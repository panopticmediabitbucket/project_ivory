﻿using System;
using System.Collections.Generic;
using Assets.Code.GameCode.Physics.Accelerators;
using Assets.Code.GameCode.Physics.Collision;
using Assets.Code.GameCode.Physics.Rotators;
using Assets.Code.GameCode.Physics.Rotors;
using Assets.Code.Physics.Collision;
using Assets.Code.Physics.Utilities;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Surfaces.Strategies {
    public class GroundWalkerSurfaceResponseStrategy : ISurfaceResponseStrategy {
        public GameObject gameObject { get; }
        private readonly CollisionBox _collisionBox;

        public Transform transform => gameObject.transform;

        public GroundWalkerSurfaceResponseStrategy(GameObject gameObject, CollisionBox collisionBox) {
            this.gameObject = gameObject;
            _collisionBox = collisionBox;
        }

        public IEnumerable<Rotor> GetRotorsForSurfaceAdjustment(CollisionData collisionData, Vector2 changeInPosition, bool faceRight) {
            switch (collisionData.collisionDataType) {
                case CollisionDataType.None:
                    switch (collisionData.collisionType) {
                        case WorldCollisionFlags.FollowGround:
                            return GetRotorsToFollowSurface(changeInPosition, WorldCollisionFlags.Ground, faceRight);
                        case WorldCollisionFlags.FollowWall:
                            return GetRotorsToFollowSurface(changeInPosition, WorldCollisionFlags.Wall, faceRight);
                        case WorldCollisionFlags.AirHang:
                            return AirCorrection();
                        default: break;
                    }
                    break;

                case CollisionDataType.Incoming:
                    return GetRotorsForIncomingSurface(collisionData);

                case CollisionDataType.Contact:
                    return GetRotorToAdhereToSurface(collisionData);

                default:
                    break;
            }
            return null;
        }

        private IEnumerable<Rotor> GetRotorToAdhereToSurface(CollisionData collisionData) {
            Vector2 objectOrientation = Vector2.zero;
            switch (collisionData.collisionType) {
                case WorldCollisionFlags.Landing:
                    objectOrientation = StaticPhysicsCalculations.LocalUp(transform);
                    break;
                case WorldCollisionFlags.WallCollision:
                    objectOrientation = StaticPhysicsCalculations.LocalLeft(transform);
                    break;
                case WorldCollisionFlags.BackWall:
                    objectOrientation = StaticPhysicsCalculations.LocalRight(transform);
                    break;
                case WorldCollisionFlags.Ceiling:
                    objectOrientation = StaticPhysicsCalculations.LocalDown(transform);
                    break;
                default: break;
            }

            Quaternion rotation = StaticPhysicsCalculations.GetQuaternionBetweenNormalizedVectors(objectOrientation, collisionData.normal);
            yield return new InstantRotor(rotation, collisionData.pivot);

            /*
            Vector2 internalMomentumProjectedAgainstOrientation = StaticPhysicsCalculations.Project(voluntaryMomentum, objectOrientation);
            Vector2 fixedInternalMomentum = voluntaryMomentum - internalMomentumProjectedAgainstOrientation;
            _stateBasedMotionMachine.SetInternalMomentum(fixedInternalMomentum.x, fixedInternalMomentum.y);
            */
        }

        private IEnumerable<Rotor> GetRotorsForIncomingSurface(CollisionData incomingNormal) {
            Vector2 weightedAverageNormal = incomingNormal.normal;
            float lengthOfNormal = weightedAverageNormal.magnitude;
            Vector2 normalizedNormal = weightedAverageNormal / lengthOfNormal;

            Vector2 localOrientation = Vector2.zero;
            Vector2 pivot = transform.position;
            switch (incomingNormal.collisionType) {
                case WorldCollisionFlags.Ground:
                    localOrientation = StaticPhysicsCalculations.LocalUp(transform);
                    pivot = _collisionBox.bottomPivot;
                    break;
                case WorldCollisionFlags.Wall:
                    localOrientation = StaticPhysicsCalculations.LocalLeft(transform);
                    pivot = _collisionBox.frontPivot;
                    break;
                case WorldCollisionFlags.BackWall:
                    localOrientation = StaticPhysicsCalculations.LocalRight(transform);
                    pivot = _collisionBox.backPivot;
                    break;
                case WorldCollisionFlags.Ceiling:
                    localOrientation = StaticPhysicsCalculations.LocalDown(transform);
                    pivot = _collisionBox.topPivot;
                    break;
                default: break;
            }

            pivot = incomingNormal.pivot != Vector2.zero ? incomingNormal.pivot : pivot;

            float theta = Vector2.SignedAngle(localOrientation, normalizedNormal);
            yield return new AnimatedRotor(theta, pivot, "incoming");
        }

        private IEnumerable<Rotor> GetRotorsToFollowSurface(Vector2 changeInPosition, WorldCollisionFlags mode, bool faceRight) {
            _collisionBox.GetCorrespondingCornerPositions(mode, out Vector2 frontCorner, out Vector2 backCorner);

            bool movingAlongForwardTrajectory = Vector2.Dot(StaticPhysicsCalculations.LocalRight(gameObject.transform), changeInPosition) >= 0;
            bool flipTheta = !faceRight ^ !movingAlongForwardTrajectory;
            if (!movingAlongForwardTrajectory) { // switch orientation if we're going backward
                var temp = frontCorner;
                frontCorner = backCorner;
                backCorner = temp;
            }

            VectorRange range = GlobalPhysicsVariables.GetRangeFromModeAndDirection(mode, faceRight);
            RaycastHit2D concaveCollision = _collisionBox.GetConcaveCollision(changeInPosition, range, frontCorner, backCorner);

            if (concaveCollision) {
                // ConcaveCollision
                Vector2 bc_prime = backCorner + changeInPosition;
                Vector2 collisionVector = changeInPosition * concaveCollision.fraction;
                Vector2 p_1 = frontCorner + collisionVector;

                float x_1 = (p_1 - bc_prime).magnitude;
                Vector2 normal = concaveCollision.normal;
                Vector2 ds = changeInPosition.normalized;
                Vector3 z = Vector3.Cross(normal, ds).normalized;
                Vector2 slope = (Vector2)Vector3.Cross(z, normal).normalized;
                float phi = Mathf.Deg2Rad * Vector2.SignedAngle(ds, slope);
                float tan_phi = Mathf.Tan(phi);
                float one_pl_tan_phi_squared = 1 + Mathf.Pow(tan_phi, 2);
                StaticPhysicsCalculations.QuadraticFormula(one_pl_tan_phi_squared, x_1 * 2, Mathf.Pow(x_1, 2) - _collisionBox.w_squared,
                    out float x_2_a, out float x_2_b);

                float theta = Mathf.Rad2Deg * Mathf.Acos((x_1 + x_2_a) / _collisionBox.width) + .5f; // minor correction here
                theta = Single.IsNaN(theta) ? 0 : theta;
                theta = flipTheta ? -theta : theta;

                Quaternion rotation = Quaternion.Euler(0, 0, theta);
                yield return new InstantRotor(rotation, backCorner);
            } else {
                // ConvexCollision
                RaycastHit2D convexCollision = _collisionBox.GetConvexCollision(changeInPosition, range, frontCorner);
                RaycastHit2D backCollision;
                if (convexCollision && GlobalPhysicsVariables.surfaceRange.Contains(convexCollision.normal) && convexCollision.distance >= GlobalPhysicsVariables.SURFACECORRECTION) {
                    convexCollision.distance -= GlobalPhysicsVariables.SURFACECORRECTION;

                    float theta = Mathf.Asin(-convexCollision.distance / _collisionBox.width) * Mathf.Rad2Deg;
                    theta = Single.IsNaN(theta) ? 0 : theta;
                    theta = flipTheta ? -theta : theta;

                    Quaternion rotation = Quaternion.Euler(0, 0, theta);
                    yield return new InstantRotor(rotation, backCorner);

                    Vector3 newBack = StaticPhysicsCalculations.RotateAround(frontCorner, rotation, backCorner);
                    backCollision = Physics2D.Raycast(newBack, changeInPosition.normalized, changeInPosition.magnitude, 512);

                    if (backCollision && GlobalPhysicsVariables.surfaceRange.Contains(backCollision.normal)) {
                        yield return new InstantRotor(rotation, frontCorner);
                    }
                }
            }
        }

        private IEnumerable<Rotor> AirCorrection() {
            Vector2 localUp = StaticPhysicsCalculations.LocalUp(transform);
            float theta = Vector2.SignedAngle(localUp, Vector2.up);
            yield return new AnimatedRotor(theta, _collisionBox.topPivot, "airbalance", (t) => 5 * (t));
        }
    }
}
