﻿using Assets.Code.GameCode.Physics.Collision;

namespace Assets.Code.GameCode.Physics.Surfaces {
    public interface ISurface: ICollideable {
        SurfaceType surfaceType { get; }
        float coefficientOfFriction { get; }
    }
}
