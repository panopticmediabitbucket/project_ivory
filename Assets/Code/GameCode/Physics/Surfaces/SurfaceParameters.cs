﻿using Assets.Code.GameCode.Physics.Accelerators;

namespace Assets.Code.GameCode.Physics.Surfaces {
    public class SurfaceParameters : MiddleBehaviour, ISurface {
        public SurfaceType surfaceType => SurfaceType.Static;
        public float frictionCoefficient = GlobalPhysicsVariables.DEFAULTFRICTIONCOEFFICIENT;
        public float coefficientOfFriction => frictionCoefficient;
    }

    public class DefaultSurface : ISurface {
        public static readonly DefaultSurface defaultSurface = new DefaultSurface();
        private DefaultSurface() { }
        public SurfaceType surfaceType => SurfaceType.Static;
        public float coefficientOfFriction => GlobalPhysicsVariables.DEFAULTFRICTIONCOEFFICIENT; 
    }
}
