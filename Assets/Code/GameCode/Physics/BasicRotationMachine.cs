﻿using System;
using UnityEngine;
using Assets.Code.GameCode.Physics.Rotators;
using Assets.Code.Physics.Utilities;
using Assets.Code.System.TimeUtil;
using Assets.Code.Physics.Collision;
using Assets.Code.GameCode.Physics.Rotors;

namespace Assets.Code.GameCode.Physics {
    public class BasicRotationMachine {
        public GameObject gameObject { get; }
        public Transform transform => gameObject.transform;

        AnimatedRotor _animatedRotation;

        public AbstractCollisionPolygon collisionSystem { get; }

        public BasicRotationMachine(GameObject gameObject, AbstractCollisionPolygon collisionSystem) {
            this.collisionSystem = collisionSystem;
            this.gameObject = gameObject;
        }

        public void ProcessRotations() {
            if (_animatedRotation != null) {
                Quaternion rotation = _animatedRotation.GetRotation();
                var hit = collisionSystem.RotationalCollision(_animatedRotation.pivot, ref rotation);
                StaticPhysicsCalculations.RotateAround(gameObject, rotation, _animatedRotation.pivot);
                if (_animatedRotation.IsFinished()) {
                    InterruptRotation();
                }
            }
        }

        public bool IsActive(string name) {
            return _animatedRotation != null && _animatedRotation.name.Equals(name);
        }

        public void DeliverRotor(Rotor rotor) {
            switch (rotor) {
                case AnimatedRotor animatedRotor:
                    if (_animatedRotation != null) {
                        _animatedRotation.Reset();
                    }
                    _animatedRotation = animatedRotor;
                    if (_animatedRotation.timer == null) {
                        _animatedRotation.DeliverRuntimeStopwatch(new GameStopwatch(rotor.name + Guid.NewGuid().ToString()));
                    }
                    _animatedRotation.timer.Start();
                    break;
                case InstantRotor instantRotor:
                    StaticPhysicsCalculations.RotateAround(gameObject, instantRotor.GetRotation(), instantRotor.pivot);
                    InterruptRotation();
                    break;
            }
        }

        public void InterruptRotation() {
            if (_animatedRotation != null) {
                _animatedRotation.Reset();
                _animatedRotation = null;
            }
        }
    }
}
