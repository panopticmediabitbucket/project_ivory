﻿using System;
using Assets.Code.Physics.Collision;
using Assets.Code.System.TimeUtil;
using UnityEngine;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.GameCode.Physics.Rotators {
    public class AnimatedRotor : Rotor {
        public static readonly Func<float, float> defaultLerp = t => 20 * t;

        private Func<float, float> _lerpFunc;
        private float _cosTh_2;
        private float _theta;

        ITimer _timer;
        public ITimer timer => _timer;

        public void DeliverRuntimeStopwatch(ITimer timer) {
            _timer = timer;
        }

        public bool IsFinished() {
            return _lerpFunc(timer.elapsedSeconds) >= 1;
        }

        public AnimatedRotor(float theta, Vector2 pivot, string name, Func<float, float> lerpFunc = null)
            : base(pivot, name) {
            _lerpFunc = lerpFunc == null ? defaultLerp : lerpFunc;
            _theta = theta;
        }

        public override Quaternion GetRotation() {
            float upperBound = _lerpFunc(timer.elapsedSeconds);
            upperBound = upperBound > 1 ? 1 : upperBound;
            float lowerBound = _lerpFunc(timer.elapsedSeconds - timer.deltaTime);
            lowerBound = lowerBound > 1 ? 1 : lowerBound;
            float sampleTime = upperBound - lowerBound;

            return Quaternion.Euler(0, 0, _theta * sampleTime);
        }

        public override Vector3 GetRotationalInertiaChange() {
            throw new NotImplementedException();
        }

        public override void Reset() {
            timer.Reset();
        }
    }
}
