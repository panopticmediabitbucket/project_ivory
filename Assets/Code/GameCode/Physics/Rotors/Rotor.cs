﻿using Assets.Code.System.TimeUtil;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Rotators {
    public abstract class Rotor {
        private readonly string _name;
        public string name => _name;
        public Vector3 pivot { get; }

        public Rotor(Vector3 pivot, string name) {
            this.pivot = pivot;
            _name = name;
        }

        public abstract Vector3 GetRotationalInertiaChange();
        public abstract Quaternion GetRotation();
        public abstract void Reset();
    }
}
