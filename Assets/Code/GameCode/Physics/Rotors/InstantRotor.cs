﻿using Assets.Code.GameCode.Physics.Rotators;
using UnityEngine;

namespace Assets.Code.GameCode.Physics.Rotors {
    public class InstantRotor : Rotor {
        public Quaternion rotation { get; }

        public InstantRotor(Quaternion rotation, Vector3 pivot) : base(pivot, "instant") {
            this.rotation = rotation;
        }

        public override Quaternion GetRotation() {
            return rotation;
        }

        public override Vector3 GetRotationalInertiaChange() {
            return Vector3.zero;
        }

        public override void Reset() {
        }
    }
}
