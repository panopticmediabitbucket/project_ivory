﻿using Assets.Code.System.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Code.Physics.Skeletons
{
    public class PhysicalNode
    {
        private Vector2 _offset;

        // In pseudo-kg. The relative distribution of mass this Node accounts for
        public float massDistribution { get; private set; }
        public SharedValue<Transform> sharedTransform { get; private set; }

        public PhysicalNode(SharedValue<Transform> gameObjectTransform, Vector2 offset)
        {
            _offset = offset;
            sharedTransform = gameObjectTransform;
        }

        public Vector2 GetLocation()
        {
            Transform transform = sharedTransform.value;
            return transform.position + transform.rotation * _offset;
        }
    }
}
