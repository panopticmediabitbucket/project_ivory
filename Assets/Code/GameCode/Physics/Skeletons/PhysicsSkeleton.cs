﻿using System;
using UnityEngine;
using UnityEditor;

namespace Assets.Code.Physics.Skeletons
{
    [Serializable]
    public abstract class PhysicsSkeleton
    {
        [SerializeField]
        private PhysicalNode _centerOfMass;
        public PhysicalNode centerOfMass { get => _centerOfMass; private set => _centerOfMass = value; }
        [SerializeField]
        private PhysicalNode[] _nodes;
        public PhysicalNode[] nodes { get => _nodes; private set => _nodes = value; }
        [SerializeField]
        private bool _affectedByRotationalTorque; 
        // Skeletons with this field set to true can tumble and roll around. 
        public bool affectedByRotationalTorque { get => _affectedByRotationalTorque; private set => _affectedByRotationalTorque = value; }
    }
}
