﻿
using Unity.Entities;

namespace Assets.Code.System.ECS {
    public interface IEnlistEntity {
        Entity EnlistEntity(EntityManager em);
        void DelistEntity(EntityManager em, Entity entity);
    }
}
