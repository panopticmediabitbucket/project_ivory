﻿using Unity.Entities;

namespace Assets.Code.GameCode.System.ECS {
    public interface IAugmentEntity {
        void AugmentEntity(EntityManager em, Entity entity);
    }
}
