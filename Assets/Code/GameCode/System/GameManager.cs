﻿using Assets.Code.GameCode.Physics.Accelerators;
using UnityEngine;
using Assets.Code.System;
using Unity.Entities;

public class GameManager : MonoBehaviour {
    // Initialization with nothing function 
    public static void Nothing() { }
    public static UpdatePhaseCallback Time = Nothing;
    public static UpdatePhaseCallback Middle = Nothing;
    public static UpdatePhaseCallback CollisionA = Nothing;
    public static UpdatePhaseCallback CollisionB = Nothing;
    public static UpdatePhaseCallback Camera = Nothing;

    public static UpdatePhaseCallback Awaken;
    public static UpdatePhaseCallback Startup;

    private static GameManager _instance;
    private static EntityManager _entityManager;

    public float GRAVITY;
    public float DEBUGTIMESCALE = 1;
    public GameObject EmptyPrefab;
    public TextAsset DynamicMethodCacheLink;

    public GlobalPhysicsVariables globalForces { get; private set; }

    public static GameManager GetInstance() {
        return _instance;
    }

    private void Awake() {
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        if (_instance == null) {
            _instance = this;
            DontDestroyOnLoad(this);
        } else {
            Destroy(this);
            return;
        }
        globalForces = new GlobalPhysicsVariables(GRAVITY);
#if UNITY_EDITOR
#else
        DynamicMethodCache.Initialize(DynamicMethodCacheLink);
#endif
    }

    // The Update call to rule them all
    void Update() {
        UnityEngine.Time.timeScale = DEBUGTIMESCALE; 
        Time.Invoke();
        Middle.Invoke();
        CollisionA.Invoke();
        CollisionB.Invoke();
    }

    // The Late Update call to rule them all
    void LateUpdate() {
        Camera.Invoke();
    }

    public static void EnqueueAwakenFunction(UpdatePhaseCallback awaken) {

    }

    public static void RegisterFunctionWithUpdatePhase(UpdatePhase updatePhase, UpdatePhaseCallback action) {
        switch (updatePhase) {
            case UpdatePhase.Time:
                Time += action;
                break;
            case UpdatePhase.Middle:
                Middle += action;
                break;
            case UpdatePhase.CollisionA:
                CollisionA += action;
                break;
            case UpdatePhase.CollisionB:
                CollisionB += action;
                break;
            case UpdatePhase.Camera:
                Camera += action;
                break;
        }
    }

    public static void DeleteFunctionWithUpdatePhase(UpdatePhase updatePhase, UpdatePhaseCallback action) {
        switch (updatePhase) {
            case UpdatePhase.Time:
                Time -= action;
                break;
            case UpdatePhase.Middle:
                Middle -= action;
                break;
            case UpdatePhase.CollisionA:
                CollisionA -= action;
                break;
            case UpdatePhase.CollisionB:
                CollisionB -= action;
                break;
        }
    }
}
