﻿
using System;

namespace Assets.Code.System.TimeUtil {
    public interface ITimeZone {
        Guid id { get; }
        float deltaTime { get; }
        float timeScale { get; }
        void Update();
    }
}
