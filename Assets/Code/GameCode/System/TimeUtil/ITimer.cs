﻿using Assets.Code.GameCode.System.ECS;
using Assets.Code.GameCode.System.TimeUtil;
using Assets.Code.System.ECS;
using Unity.Entities;

namespace Assets.Code.System.TimeUtil {
    public interface ITimer : IAugmentEntity, IEnlistEntity {
        bool active { get; }
        float elapsedSeconds { get; }
        float deltaTime { get; }

        void SetStartTime(float time);
        void Update();

        void Reset();
        void Restart();
        void Pause();

        void Start();
    }
}

