﻿using Assets.Code.GameCode.Physics.Accelerators;
using Unity;
using UnityEngine;
using Unity.Entities;

namespace Assets.Code.GameCode.System.TimeUtil {
    public class TimerComponentSystem : SystemBase {
        protected override void OnUpdate() {
            float deltaTime = Time.DeltaTime;
            Entities.WithNone<TimeZoneComponentData>()
                .ForEach((ref TimerComponentData tcData) => {
                    tcData.deltaTime = deltaTime;
                    tcData.elapsedSeconds += deltaTime;
                }).ScheduleParallel();
        }
    }

    public class TimerBoxComponentSystem : SystemBase {
        protected override void OnUpdate() {
            float deltaTime = Time.DeltaTime;
            Entities.ForEach((ref TimerComponentData tcData,
                             ref TimeZoneComponentData timeZone) => {
                                 tcData.elapsedSeconds = deltaTime + timeZone.zone;
                             }).ScheduleParallel();
        }
    }

    public class AccelTestComponentSystem : SystemBase {
        protected override void OnUpdate() {
            Entities.ForEach((ref AccelComponentData accelComponent, ref TimerComponentData tcData) => {
                if(accelComponent.accelAssetReference.Value.accelZones.Length > 0) {
                    UnityEngine.Debug.Log(accelComponent.accelAssetReference.Value.accelZones[0].rate);
                }
            }).ScheduleParallel();
        }
    }
}
