﻿using Assets.Code.System.Utilities;
using Unity.Entities;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.System.TimeUtil {
    public class TimedBoolean : ITimer {
        private ITimer underlyingTimer;
        public float targetTime { get; private set; }

        public bool active => underlyingTimer.active; 
        public float deltaTime => underlyingTimer.deltaTime; 
        public float elapsedSeconds => underlyingTimer.elapsedSeconds;

        public Entity entity { get; set; }

        public TimedBoolean(string name, float targetTime, bool startFinished = true) {
            this.targetTime = targetTime;
            underlyingTimer = new GameStopwatch(name);
            if (startFinished) {
                SetStartTime(targetTime);
            }
        }

        public void SetStartTime(float time) {
            underlyingTimer.SetStartTime(time);
        }

        public void Update() {
            underlyingTimer.Update();
            if (this) {
                Pause();
            }
        }

        public void Reset() {
            underlyingTimer.Reset();
        }

        public void Restart() {
            underlyingTimer.Restart();
        }

        public void Pause() {
            underlyingTimer.Pause();
        }

        public void Start() {
            underlyingTimer.Start();
        }

        public void AugmentEntity(EntityManager em, Entity entity) {
            throw new NotImplementedException();
        }

        public Entity EnlistEntity(EntityManager em) {
            throw new NotImplementedException();
        }

        public void DelistEntity(EntityManager em, Entity entity) {
            throw new NotImplementedException();
        }

        public static implicit operator bool(TimedBoolean timer) {
            return timer != null && timer.elapsedSeconds >= timer.targetTime;
        }
    }
}
