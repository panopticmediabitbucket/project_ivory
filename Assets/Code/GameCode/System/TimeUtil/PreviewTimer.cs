﻿using System;
using System.Diagnostics;
using Unity.Entities;
using UnityEngine;
namespace Assets.Code.System.TimeUtil {
    public class PreviewTimer : ITimer {
        public float elapsedSeconds { get; private set; }
        //public float deltaTime { get; private set; }
        public float maxTime { get; set; }
        public bool active { get; private set; }

        public float deltaTime { get; private set; }
        public Entity entity { get; set; }

        private Stopwatch _stopwatch;
        public string name;

        public PreviewTimer() {
            active = false;
            _stopwatch = new Stopwatch();
            name = "preview timer" + new global::System.Random().Next(10000);
        }

        public PreviewTimer(float startDelta) {
            deltaTime = startDelta;
            elapsedSeconds = startDelta;
            active = true;
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
            name = "preview timer" + new global::System.Random().Next(10000);
        }

        public void SetStartTime(float time) {
            elapsedSeconds += time;
        }

        public void Update() {
            deltaTime = (float)_stopwatch.Elapsed.TotalSeconds;
            elapsedSeconds += deltaTime;
            _stopwatch.Restart();
        }

        public void Update(float timeScale) {
            deltaTime = (float)_stopwatch.Elapsed.TotalSeconds * timeScale;
            elapsedSeconds += deltaTime;
            _stopwatch.Restart();
        }

        public void Reset() {
            Pause();
            _stopwatch.Reset();
            elapsedSeconds = 0;
        }

        public void Start() {
            _stopwatch.Start();
            active = true;
        }

        public void Pause() {
            _stopwatch.Stop();
            active = false;
        }

        public override int GetHashCode() {
            return name.GetHashCode();
        }

        public void Restart() {
            Reset();
            Start();
        }

        public void AugmentEntity(EntityManager em, Entity entity) {
            throw new NotImplementedException();
        }

        public Entity EnlistEntity(EntityManager em) {
            throw new NotImplementedException();
        }

        public void DelistEntity(EntityManager em, Entity entity) {
            throw new NotImplementedException();
        }
    }
}



