﻿using System;
using UnityEngine;

namespace Assets.Code.System.TimeUtil {
    public class TimeZone : ITimeZone{
        private readonly Guid _id;
        public Guid id => _id;

        private float _timeScale = 1;
        public float timeScale => _timeScale;

        public float deltaTime { get; private set; }

        public void Update() {
            deltaTime = UnityEngine.Time.deltaTime * _timeScale;
        }

        public TimeZone(float timeScale = 1) {
            _timeScale = timeScale;
            _id = Guid.NewGuid();
        }

        public sealed override int GetHashCode() {
            return _id.GetHashCode();
        }

        public sealed override bool Equals(object obj) {
            return _id.Equals(((TimeZone)obj)._id);
        }
    }
}
