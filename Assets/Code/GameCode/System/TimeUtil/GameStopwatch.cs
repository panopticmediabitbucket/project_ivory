﻿using Assets.Code.GameCode.System.TimeUtil;
using Unity.Entities;
using UnityEngine;
using NotImplementedException = System.NotImplementedException;

namespace Assets.Code.System.TimeUtil {
    public class GameStopwatch : ITimer {
        public string name { get; private set; }
        public bool active { get; private set; }

        public float elapsedSeconds => _timerComponentData.elapsedSeconds;

        public float deltaTime => _timerComponentData.deltaTime;

        private Entity? _entity;

        private TimerComponentData _timerComponentData;

        public GameStopwatch(string name) {
            this.name = name;
            active = false;
        }

        public void SetStartTime(float time) {
            _timerComponentData.elapsedSeconds = time;
        }

        public virtual void Update() {
            _timerComponentData.deltaTime = Time.deltaTime;
            _timerComponentData.elapsedSeconds += Time.deltaTime;
        }

        public void Reset() {
            _timerComponentData.deltaTime = 0;
            _timerComponentData.elapsedSeconds = 0;
            Pause();
            active = false;
        }

        public void Start() {
            ClockManager.RegisterITimer(this);
            active = true;
        }

        // Use to split the difference between the last frame and this frame
        // good for player-controlled GameObjects
        public void ResponsiveStart() {
            float deltaTime = Time.deltaTime / 2;
            _timerComponentData.deltaTime = deltaTime;
            _timerComponentData.elapsedSeconds = deltaTime;
            Start();
        }

        public void ResponsiveStart(float startTime) {
            _timerComponentData.deltaTime = startTime;
            _timerComponentData.elapsedSeconds = startTime;
            Start();
        }

        public void Pause() {
            ClockManager.DequeueITimer(this);
        }

        public override int GetHashCode() {
            return name.GetHashCode();
        }

        public void Restart() {
            Reset();
            Start();
        }

        public void AugmentEntity(EntityManager em, Entity entity) {
            if(_entity == null) {
                _entity = EnlistEntity(em);
            } else {
                _timerComponentData = em.GetComponentData<TimerComponentData>(_entity.Value);
            }

            em.SetComponentData(entity, _timerComponentData);
        }

        public Entity EnlistEntity(EntityManager em) {
            Entity entity = em.CreateEntity(typeof(TimerComponentData));
            em.SetComponentData(entity, _timerComponentData);
            return entity;
        }

        public void DelistEntity(EntityManager em, Entity entity) {
        }
    }
}
