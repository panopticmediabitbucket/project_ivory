﻿using Assets.Code.Physics.Accelerators;
using Unity.Entities;

namespace Assets.Code.GameCode.System.TimeUtil {
    public struct TimerComponentData : IComponentData {
        public float elapsedSeconds;
        public float deltaTime;
        public bool active;
    }
}
