﻿using System;
using Unity.Entities;

namespace Assets.Code.GameCode.System.TimeUtil {
    public struct TimeZoneComponentData : IComponentData, IComparable<TimeZoneComponentData> {
        public float zone;

        public int CompareTo(TimeZoneComponentData other) {
            return zone >= other.zone ? 1 : -1;
        }
    }
}
