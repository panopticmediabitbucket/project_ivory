﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Assets.Code.GameCode.System.Utilities {
    /**
     * A universal, indexable hash utility. 
     */
    public struct HashWrapper : IEquatable<HashWrapper> {
        private static readonly SHA256 SHA = SHA256.Create();
        public static HashWrapper Get(object obj) {
            return new HashWrapper(SHA.ComputeHash(Encoding.ASCII.GetBytes(obj.ToString())));
        }

        private readonly byte[] _bytes;
        public byte[] bytes => _bytes;
        public HashWrapper(byte[] bytes) {
            _bytes = bytes;
        }

        public override int GetHashCode() {
            int accumulator = 0;
            foreach (var @byte in bytes) {
                accumulator += @byte;
            }
            return accumulator;
        }

        public override bool Equals(object obj) {
            return obj is HashWrapper other && Equals(other);
        }

        public bool Equals(HashWrapper other) {
            return _bytes.SequenceEqual(other.bytes);
        }

        public static bool operator ==(HashWrapper left, HashWrapper right) {
            return left.Equals(right);
        }

        public static bool operator !=(HashWrapper left, HashWrapper right) {
            return !(left.Equals(right));
        }
    }
}
