﻿using System;

namespace Assets.Code.System.Utilities {
    public class SharedValue<TValue> {
        // The owner of the shared value instantiates it using this static method
        // and stores the key however they see fit.
        public static Tuple<SharedValue<T>, Guid> CreateSharedValue<T>(T value) {
            Guid key = Guid.NewGuid();
            return new Tuple<SharedValue<T>, Guid>(new SharedValue<T>(value, key), key);
        }

        public TValue value { get; private set; }

        private Guid _key;

        private Callback _onValueChange;

        private SharedValue() { }
        private SharedValue(TValue value, Guid key) {
            this.value = value;
            this._key = key;
            _onValueChange += DoNothing;
        }

        // A key is needed to set the value
        public void SetValue(TValue newValue, Guid key) {
            if (key.Equals(this._key)) {
                value = newValue;
                _onValueChange.Invoke(newValue);
            }
        }

        public void AddCallback(Callback callback) {
            _onValueChange += callback;
        }

        public delegate void Callback(TValue newValue);
        private void DoNothing(TValue newValue) { }
    }
}
