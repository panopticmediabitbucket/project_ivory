﻿using Assets.Code.System;
using Assets.Code.System.TimeUtil;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Assets.Code.GameCode.System.TimeUtil;

public class ClockManager : MonoBehaviour {
    private static HashSet<ITimer> Timers = new HashSet<ITimer>();
    private static HashSet<ITimeZone> TimeZones = new HashSet<ITimeZone>();
    private static ITimeZone _globalTimeZone = new TimeZone(1);
    public static ITimeZone globalTimeZone => _globalTimeZone;

    private static EntityManager _entityManager;
    private static EntityArchetype _timerArchetype;
    private static EntityArchetype _timeArchetype;
    static int count = 0;

    public static void RegisterITimer(ITimer ITimer) {
        if (!Timers.Contains(ITimer)) {
            //Entity entity = _entityManager.CreateEntity(count++ % 2 == 0 ? _timerArchetype : _timeArchetype);
            //Debug.Log("Entity created");
            //ITimer.entity = entity;
            Timers.Add(ITimer);
        }
    }

    public static void RegisterTimeZone(ITimeZone timeZone) {
        TimeZones.Add(timeZone);
    }

    public static void DequeueITimer(ITimer ITimer) {
        if (Timers.Contains(ITimer)) 
            Timers.Remove(ITimer);
    }

    private void Awake() {
        DontDestroyOnLoad(this);
        GameManager.RegisterFunctionWithUpdatePhase(UpdatePhase.Time, Time);
        TimeZones.Add(_globalTimeZone);
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        _timerArchetype = _entityManager.CreateArchetype(typeof(TimerComponentData), typeof(TimeZoneComponentData));
        _timeArchetype = _entityManager.CreateArchetype(typeof(TimerComponentData));
    }

    void Time() {
        foreach (ITimeZone timeZone in TimeZones) {
            timeZone.Update();
        }

        foreach (ITimer ITimer in Timers) {
            ITimer.Update();
        }
    }

    public static ITimeZone GetTimeZone(Collider2D collider) {
        throw new System.NotImplementedException(); 
    }

    private void OnDestroy() {
        GameManager.DeleteFunctionWithUpdatePhase(UpdatePhase.Time, Time);
    }
}
