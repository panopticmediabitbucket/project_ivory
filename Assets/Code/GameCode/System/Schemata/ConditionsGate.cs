﻿using System;
using UnityEngine;

namespace Assets.Code.GameCode.System.Schemata {
    public class ConditionsGate: ICondition {

        ICondition[] _conditions;
        Func<bool> _booleanFunction;
        public ConditionsGate(ConditionType conditionType, params ICondition[] conditions) {
            _conditions = conditions;
            switch (conditionType) {
                case ConditionType.And:
                    _booleanFunction = And;
                    break;
                case ConditionType.Or:
                    _booleanFunction = Or;
                    break;
            }
        }

        public bool Met() => _booleanFunction();

        private bool And() {
            bool accumulator = true;
            foreach (var cond in _conditions) {
                accumulator &= cond.Met();
                if (!accumulator)
                    break;
            }
            return accumulator;
        }

        private bool Or() {
            bool accumulator = false;
            foreach (var cond in _conditions) {
                accumulator |= cond.Met();
                if (accumulator)
                    break;
            }
            return accumulator;
        }

        public void Reset() {
            foreach (var cond in _conditions) {
                cond.Reset();
            }
        }
    }

    public class ConditionAtom: ICondition {
        public Func<bool> resetFunction;
        public Func<bool> conditionFunction;
        private bool onceMetAlwaysMetUntilReset;
        private Action _resetAction;

        public ConditionAtom(Func<bool> conditionFunction, bool onceMetAlwaysMetUntilReset, Action resetAction = null) {
            _resetAction = resetAction == null ? () => { } : resetAction;
            this.onceMetAlwaysMetUntilReset = onceMetAlwaysMetUntilReset;
            this.conditionFunction = conditionFunction;
            resetFunction = conditionFunction;
            ResetCondFunction();
        }

        public bool Met() {
            return conditionFunction();
        }

        private bool OnceMetAlwaysMet() {
            if (resetFunction()) {
                conditionFunction = () => true;
                return true;
            }
            return false;
        }

        public void Reset() {
            _resetAction();
            ResetCondFunction();
        }

        private void ResetCondFunction() {
            if (onceMetAlwaysMetUntilReset) {
                this.conditionFunction = OnceMetAlwaysMet;
            }
        }
    }

    public interface ICondition {
        bool Met();
        void Reset();
    }
}
