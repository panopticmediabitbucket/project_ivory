﻿using System;

namespace Assets.Code.GameCode.System.Schemata.Validator {
    public static class ConditionSchemaValidator {
        public static Validation ValidateConditionSchema(ConditionSchema schema) {
            Validation validity = Validation.Valid;
            switch (schema.conditionType) {
                case ConditionType.Atom:
                    validity &= ValidateAtom(schema);
                    break;
                default:
                    validity &= ValidateGate(schema);
                    break;
            }

            return validity;
        }

        private static Validation ValidateGate(ConditionSchema schema) {
            Validation validity = schema.children.Length > 0;
            for(int i = 0; i < schema.children.Length && validity; i++) {
                validity &= ValidateConditionSchema(schema.children[i]);
            }
            return validity;
        }

        /**
         * Validates an Atom-type Condition. The properties need to be validated, as do the return types.
         */
        private static Validation ValidateAtom(ConditionSchema schema) {
            Validation validity = PropertySchemaValidator.ValidatePropertySchema(schema.targetSchema);
            if(schema.comparisonType == ComparisonType.True || schema.comparisonType == ComparisonType.False) {
                validity &= schema.targetSchema.destinationType == typeof(Boolean);
            } else {
                validity &= schema.argumentSchemata.Length >= 0;
            }

            for(int i = 0; i < schema.argumentSchemata.Length && validity; i++) {
                validity &= PropertySchemaValidator.ValidatePropertySchema(schema.argumentSchemata[i]);
            }
            return validity;
        }
    }
}
