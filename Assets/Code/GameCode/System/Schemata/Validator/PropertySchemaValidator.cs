﻿using System;
using System.Reflection;

namespace Assets.Code.GameCode.System.Schemata.Validator {
    /**
     * A simple utility for validating property schemata
     * @author Luke Randazzo
     */
    public static class PropertySchemaValidator {
        public static Validation ValidatePropertySchema(PropertySchema schema) {
            Validation validity = Validation.Valid;
            switch (schema.primitiveType) {
                case PrimitiveType.NonPrimitive:
                    validity = schema.destinationType != null && schema.propertyType != null;
                    Type previousType = schema.propertyType;

                    for(int i = 0; i < schema.propertySequence.Length && validity; i++) {
                        PropertyNode node = schema.propertySequence[i];
                        PropertyInfo property = previousType.GetProperty(node.propertyName);
                        validity &= property.PropertyType == node.propertyType && node.propertyType != null;
                        previousType = node.propertyType;
                    }
                    break;
                case PrimitiveType.String:
                    validity &= schema.primString != null;
                    break;
                default:
                    break;
            }
            return validity;
        }
    }
}
