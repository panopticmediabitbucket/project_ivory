﻿namespace Assets.Code.GameCode.System.Schemata.Validator {
    public class Validation {
        public static readonly Validation Valid = new Validation("Valid");
        public static readonly Validation Invalid = new Validation("Invalid");

        private string value;

        private Validation(string value) {
            this.value = value;
        }

        public static implicit operator Validation(bool value) {
            return value ? Valid : Invalid;
        }

        public static implicit operator bool(Validation value) {
            return value == Valid;
        }
    }
}
