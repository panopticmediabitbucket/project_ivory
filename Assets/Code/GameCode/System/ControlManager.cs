﻿using UnityEngine;

namespace Assets.Code.System {


    public class ControlManager {
        private static ControlManager _controlManager;

        static ControlManager() {
            GetInstance();
        }

        public static ControlManager GetInstance() {
            if (_controlManager == null) {
                _controlManager = new ControlManager();
            }
            return _controlManager;
        }

        private KeyCode _jumpJoyStick;
        private KeyCode _gallopJoyStick;
        private KeyCode _clingJoyStick;

        private KeyCode _jumpKeyboard;
        private KeyCode _gallopKeyboard;
        private KeyCode _clingKeyboard;
        private KeyCode _leftKeyboard;
        private KeyCode _rightKeyboard;
        private KeyCode _upKeyboard;
        private KeyCode _downKeyboard;

        //external accessors for control properties
        public bool Jump { get { return Input.GetKey(_jumpKeyboard) || Input.GetKey(_jumpJoyStick); } }
        public bool JumpDown { get { return Input.GetKeyDown(_jumpKeyboard) || Input.GetKeyDown(_jumpJoyStick); } }
        public bool JumpUp { get { return Input.GetKeyUp(_jumpKeyboard) || Input.GetKeyUp(_jumpJoyStick); } }
        public bool Gallop { get { return Input.GetKey(_gallopKeyboard) || Input.GetKey(_gallopJoyStick); } }
        public bool GallopDown { get { return Input.GetKeyDown(_gallopKeyboard) || Input.GetKeyDown(_gallopJoyStick); } }
        public bool Cling { get { return Input.GetKey(_clingKeyboard) || Input.GetKey(_clingJoyStick); } }
        public bool ClingDown { get { return Input.GetKeyDown(_clingKeyboard) || Input.GetKeyDown(_clingJoyStick); } }
        public bool LeftInput { get { return (Input.GetKey(_leftKeyboard) && !Input.GetKey(_rightKeyboard)); } }
        public bool RightInput { get { return (Input.GetKey(_rightKeyboard) && !Input.GetKey(_leftKeyboard)); } }
        public bool UpInput { get { return (Input.GetKey(_upKeyboard) && !Input.GetKey(_downKeyboard)); } }
        public bool DownInput { get { return (Input.GetKey(_downKeyboard) && !Input.GetKey(_upKeyboard)); } }

        ControlManager() {
            Defaults();
        }

        private void Defaults() {
            _jumpJoyStick = KeyCode.JoystickButton0;
            _gallopJoyStick = KeyCode.JoystickButton2;
            _clingJoyStick = KeyCode.JoystickButton4;

            _jumpKeyboard = KeyCode.Space;
            _gallopKeyboard = KeyCode.LeftShift;
            _clingKeyboard = KeyCode.LeftControl;
            _leftKeyboard = KeyCode.A;
            _rightKeyboard = KeyCode.D;
            _upKeyboard = KeyCode.W;
            _downKeyboard = KeyCode.S;
        }
    }
}
