﻿namespace Assets.Code.System
{
    public enum UpdatePhase {
        Time, Middle, CollisionA, CollisionB, Camera
    }

    public delegate void UpdatePhaseCallback();
}
