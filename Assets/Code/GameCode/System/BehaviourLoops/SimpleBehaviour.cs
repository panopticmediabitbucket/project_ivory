﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Code.System;

public abstract class MiddleBehaviour : MonoBehaviour
{
    protected UpdatePhase updatePhase { get; private set; }
    // can be overriden, but maintain base implementation
    protected virtual void Awake()
    {
        updatePhase = UpdatePhase.Middle;
        GameManager.RegisterFunctionWithUpdatePhase(updatePhase, Middle);
    }

    public void Update() { }

    // override for update behaviour
    protected virtual void Middle() { }

    // can be overriden, but maintain base implementation
    protected virtual void OnDestroy()
    {
        GameManager.DeleteFunctionWithUpdatePhase(updatePhase, Middle);
    }
}
