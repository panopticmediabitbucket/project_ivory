﻿using UnityEngine;
using Assets.Code.System;

public abstract class DualBehaviour : MonoBehaviour {
    // these are the defaults, they can be adjusted in inheriting class
    private bool _phasesSet = false;
    private bool _fpSet = false;
    private UpdatePhase _firstPhase;
    protected UpdatePhase firstPhase {
        get {
            return _firstPhase;
        }
        set {
            if (_phasesSet) {
                UnityEngine.Debug.LogError("Update phase can not be set after the Awake() phase. " +
                    "Make sure you're setting the update phase in the implementing class's Awake() function before" +
                    " passing control to the base implementation's Awake() function. **FAILING CLASS: " + GetType());
            } else if (value.Equals(UpdatePhase.Time) || value.Equals(UpdatePhase.CollisionB)) {
                UnityEngine.Debug.LogError("First update phase must be set to either 'Middle' or 'Movement.' " +
                    "**FAILING CLASS: " + GetType());
            } else {
                _firstPhase = value;
                _fpSet = true;
            }
        }
    }
    private bool _spSet = false;
    private UpdatePhase _secondPhase;
    protected UpdatePhase secondPhase {
        get {
            return _secondPhase;
        }
        set {
            if (_phasesSet) {
                UnityEngine.Debug.LogError("Update phase can not be set after the Awake() phase. " +
                    "Make sure you're setting the update phase in the implementing class's Awake() function before" +
                    " passing control to the base implementation's Awake() function. **FAILING CLASS: " + GetType());
            } else if (value.Equals(UpdatePhase.Time) || value.Equals(UpdatePhase.Middle)) {
                UnityEngine.Debug.LogError("Second update phase must be set to either 'Movement' or 'Collision.' " +
                    "**FAILING CLASS: " + GetType());
            } else {
                _secondPhase = value;
                _spSet = true;
            }
        }
    }

    // can be overriden, but maintain base implementation
    protected virtual void Awake() {
        if (!_fpSet)
            firstPhase = UpdatePhase.Middle;
        if (!_spSet)
            secondPhase = UpdatePhase.CollisionB;
        GameManager.RegisterFunctionWithUpdatePhase(firstPhase, First);
        GameManager.RegisterFunctionWithUpdatePhase(secondPhase, Second);
    }

    private void Update() { }
    private void LateUpdate() { }

    // override for update behaviour
    protected abstract void First();
    protected abstract void Second();

    // can be overriden, but maintain base implementation
    protected virtual void OnDestroy() {
        GameManager.DeleteFunctionWithUpdatePhase(firstPhase, First);
        GameManager.DeleteFunctionWithUpdatePhase(secondPhase, Second);
    }

}
